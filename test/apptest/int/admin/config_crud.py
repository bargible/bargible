"""
test read, update functionality for application config
"""

import os
import sys
import unittest
import datetime

from .base import BaseAdminTestCase
from .base import formatDatetime

from bargible.model import Config

class ConfigReadTests(BaseAdminTestCase):

    def testReadDefaults(self):
        """ the update form is also used to read current values """
        configPage = self.getPage('config')
        # Config.get() follows self.getPage() due to db session scoping
        configModel = Config.get()
        configForm = configPage.crudForm
        self.assertIsNotNone(configForm)
        for name, val in configForm.fieldNamesAndValues.items():
            self.assertEqual(getattr(configModel, name), val)


class ConfigUpdateTests(BaseAdminTestCase):

    def _submitUpdate(self, fieldNameAndValUpdates):
        fieldNamesAndValues = (self.getPage('config')
                                   .crudForm.fieldNamesAndValues)
        fieldNamesAndValues.update(fieldNameAndValUpdates)
        return self.submitForm('config', fieldNamesAndValues)

    def _testValidatePositiveOneField(self, fieldName):
        for val in [0, -1]:
            dbValBeforeUpdate = getattr(Config.get(), fieldName)
            page, code = self._submitUpdate({fieldName: val})
            dbValAfterUpdate = getattr(Config.get(), fieldName)
            self.assertEqual(code, 200)
            self.assertEqual(dbValBeforeUpdate, dbValAfterUpdate)
            self.assertIn(fieldName, page.crudForm.fieldErrs)

    def testValidatePositive(self):
        # currently, all config fields are expected
        # to be positive numbers
        positiveFieldNames = self.getPage('config').crudForm.fieldNames
        for fieldName in positiveFieldNames:
            self._testValidatePositiveOneField(fieldName)

    def testUpdateMaxUsers(self):
        fieldName = 'max_users_per_auction'
        newVal = 5
        page, code = self._submitUpdate({fieldName: newVal})
        dbValAfterUpdate = getattr(Config.get(), fieldName)
        self.assertEqual(dbValAfterUpdate, newVal)
        self.assertEqual(
            page.crudForm.fieldNamesAndValues.get(fieldName, None),
            newVal)
        self.assertIn('updated config', page.messages)


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(
            ConfigReadTests
        ))
        suite.addTests(loader.loadTestsFromTestCase(
            ConfigUpdateTests
        ))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
