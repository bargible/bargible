""" base functionality for admin ui tests """
import os.path
import unittest
import configparser
import re
from warnings import warn

from bs4 import BeautifulSoup as Soup

from app import initFlaskApp
from app import model

import gentestmodels

CONF = configparser.ConfigParser()
CONF.read(os.path.join(os.path.dirname(__file__), 'defaults.conf'))

TEST_DATA_IDXS = {
    k: int(CONF['testdata'][k]) for k in CONF['testdata'].keys()
}

def formatDatetime(datetimeInst):
    return datetimeInst.isoformat()[:-3] + 'Z'

def resetDb():
    model.shutdownSession()
    model.dropAllTables()
    model.initDb()


class CrudForm(object):

    VALUE_PARSERS = {
        'number': int,
    }

    def __init__(self, formTag):
        self._soup = formTag

    @property
    def fieldNamesAndValues(self):
        nonSubmitInputTags = [
            t for t in self._soup.find_all('input')
            if t.attrs['type'] != 'submit' ]
        inputTagNamesAndValues = [
            (t.attrs['name'], (
                self.VALUE_PARSERS.get(t.attrs['type'], id)(
                    t.attrs['value']))
            ) for t in nonSubmitInputTags ]
        return dict(inputTagNamesAndValues)

    @property
    def fieldNames(self):
        nonSubmitInputTags = [
            t for t in self._soup.find_all('input')
            if t.attrs['type'] != 'submit' ]
        inputTagNames = [ t.attrs['name'] for t in nonSubmitInputTags ]
        selectTagNames = [
            t.attrs['name'] for t in self._soup.find_all('select') ]
        return inputTagNames + selectTagNames

    @property
    def fieldErrs(self):
        errs = {}
        for fieldTag in self._soup.find_all(class_='crud-field'):
            errTags = fieldTag.find(class_='errors').find_all('li')
            if len(errTags) > 0:
                errs[
                    fieldTag.find('label').attrs['for']
                    ] = [errTag.text for errTag in errTags]
        return errs


class AdminPage(object):

    def __init__(self, pageSoup):
        self._soup = pageSoup

    @property
    def messages(self):
        wrapperTag = self._soup.find(class_='flashes')
        if not wrapperTag:
            return []
        return [
            li.text.strip() for li
            in (wrapperTag
                    .find('ul')
                    .find_all('li')) ]

    @property
    def crudForm(self):
        formTag = (self._soup
                        .find(class_='crud-form')
                        .find('form'))
        if formTag:
            return CrudForm(formTag)
        return None


class BaseAdminTestCase(unittest.TestCase):

    def setUp(self):
        resetDb()
        gentestmodels.createConfig()
        gentestmodels.createItems(TEST_DATA_IDXS['items'])
        gentestmodels.createCreditPackages(
            TEST_DATA_IDXS['credit_packages'])
        self._app = initFlaskApp()
        self._app.config.from_object('config.TestingConfig')
        self._c = self._app.test_client()

    def tearDown(self):
        pass


    def getPage(self, urlPath):
        # check that the form exists
        urlPath = '/admin/' + urlPath
        resp = self._c.get(urlPath)
        if resp.status_code != 200:
            raise ValueError((
                "didn't recv ok HTTP status for GET "
                "when looking for form page"),
                (urlPath, resp))
        return AdminPage(Soup(resp.data, 'html.parser'))

    def submitForm(self, urlPath, formData):
        page = self.getPage(urlPath)
        urlPath = '/admin/' + urlPath
        self.assertIsNotNone(page.crudForm)
        self.assertEqual(set(page.crudForm.fieldNames),
                             set(formData.keys()))
        # submit the form
        resp = self._c.post(
            urlPath,
            data=formData,
            )
        page = AdminPage(Soup(resp.data, 'html.parser'))
        return page, resp.status_code
