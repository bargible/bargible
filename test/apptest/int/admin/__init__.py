""" admin ui tests """
import unittest

from . import auction_crud as auctionCrudTests
from . import config_crud as configCrudTests
from . import physical_item_crud as physicalItemCrudTests

def load_tests(loader, standard_tests, pattern):
    if pattern == 'admin':
        pattern = None
    suite = unittest.TestSuite()
    for testModule in [
            auctionCrudTests,
            configCrudTests,
            physicalItemCrudTests,
    ]:
        tests = (unittest.defaultTestLoader
                 .loadTestsFromModule(testModule, pattern=pattern))
        suite.addTests(tests)
    return suite

