"""
test create, read, update, delete functionality for auctions
"""
import os
import sys
import unittest
import datetime

from .base import BaseAdminTestCase
from .base import formatDatetime

from bargible.model import Auction

class AuctionCreateTests(BaseAdminTestCase):

    def testCreate(self):
        startTime = datetime.datetime.utcnow()
        endTime = startTime + datetime.timedelta(minutes=5)
        page, code = self.submitForm('auctions/create', {
            'reward': 1,
            'start_datetime': formatDatetime(startTime),
            'end_datetime': formatDatetime(endTime),
            'real_price': '12.34',
            'join_cost': '4321',
        })
        self.assertEqual(code, 200)
        self.assertIn('created auction', page.messages)
        auctions = Auction.all()
        self.assertEqual(len(auctions), 1)
        auction = auctions[0]
        self.assertEqual(auction.real_price, 1234)
        self.assertEqual(auction.join_cost, 4321)
        self.assertEqual(auction.intermediate_timer,
                             auction.end_datetime)
        self.assertEqual(auction.min_price, auction.real_price)

    def testLowRealPrice(self):
        """ auction real price less than giftcard real price """
        self.assertEqual(len(Auction.all()), 0)
        startTime = datetime.datetime.utcnow()
        endTime = startTime + datetime.timedelta(minutes=5)
        page, code = self.submitForm('auctions/create', {
            'reward': 1,
            'start_datetime': formatDatetime(startTime),
            'end_datetime': formatDatetime(endTime),
            'real_price': '0.01',
            'join_cost': '1000',
        })
        self.assertEqual(code, 200)
        self.assertNotIn('created auction', page.messages)
        self.assertEqual(len(Auction.all()), 0)

    def testNegativePrices(self):
        """ auction real price less than giftcard real price """
        self.assertEqual(len(Auction.all()), 0)
        startTime = datetime.datetime.utcnow()
        endTime = startTime + datetime.timedelta(minutes=5)
        page, code = self.submitForm('auctions/create', {
            'reward': 1,
            'start_datetime': formatDatetime(startTime),
            'end_datetime': formatDatetime(endTime),
            'real_price': '-10',
            'join_cost': '1000',
        })
        self.assertEqual(code, 200)
        self.assertNotIn('created auction', page.messages)
        self.assertIsNotNone(page.crudForm)
        self.assertIn('real_price', page.crudForm.fieldErrs)
        page, code = self.submitForm('auctions/create', {
            'reward': 1,
            'start_datetime': formatDatetime(startTime),
            'end_datetime': formatDatetime(endTime),
            'real_price': '10',
            'join_cost': '-1000',
        })
        self.assertEqual(code, 200)
        self.assertNotIn('created auction', page.messages)
        self.assertEqual(len(Auction.all()), 0)

    def testMissingDate(self):
        page, code = self.submitForm('auctions/create', {
            'reward': 1,
            'start_datetime': (
                datetime.datetime.utcnow().isoformat()[:-3] + 'Z'),
            'end_datetime': '',
            'real_price': '100',
            'join_cost': '1000',
        })
        self.assertEqual(code, 200)
        self.assertNotIn('created auction', page.messages)
        self.assertIsNotNone(page.crudForm)
        self.assertIn('end_datetime', page.crudForm.fieldErrs)


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(
            AuctionCreateTests
        ))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
