"""
test create, read, update, delete functionality for physical items
"""
import os
import sys
import unittest
import json
import datetime
import io

from .base import BaseAdminTestCase
from .base import formatDatetime

from bargible.model import PhysicalItem

PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..', '..', '..', '..')
)

DIR_IMAGES = os.path.join(PROJ_ROOT, 'test_data',
                              'images', 'physical-item')

def _fixtureToFormDataMapper(dataPhysItem):
    filenamePicture = dataPhysItem.pop('picture')
    with open(os.path.join(DIR_IMAGES, filenamePicture), 'rb') as f:
        # the werkzeug test api implicitly converts this tuple to a
        # werkzeug FileStorage instance
        dataPhysItem['picture'] = (io.BytesIO(f.read()), filenamePicture)
    dataPhysItem['price'] = ('%.2f' % (dataPhysItem['price'] / 100))
    return dataPhysItem


with open(os.path.join(
    PROJ_ROOT, 'test_data', 'fixtures', 'physical_items_00.json'),
              'r') as f:
    DATA_PHYS_ITEM = _fixtureToFormDataMapper(json.load(f)[0])

class PhysicalItemCreateTests(BaseAdminTestCase):

    def testCreate(self):
        self.assertEqual(len(PhysicalItem.all()), 0)
        page, code = self.submitForm('physicalItems/create',
                                         DATA_PHYS_ITEM)
        self.assertEqual(code, 200)
        physicalItems = PhysicalItem.all()
        self.assertEqual(len(physicalItems), 1)
        physicalItem = physicalItems[0]
        self.assertEqual(physicalItem.brand,
                             DATA_PHYS_ITEM['brand'])
        self.assertEqual(physicalItem.name,
                             DATA_PHYS_ITEM['name'])
        self.assertEqual(physicalItem.short_description,
                             DATA_PHYS_ITEM['short_description'])
        self.assertEqual(physicalItem.description,
                             DATA_PHYS_ITEM['description'])
        self.assertEqual(physicalItem.sku,
                             DATA_PHYS_ITEM['sku'])
        self.assertEqual(physicalItem.price,
                             int(100* float(DATA_PHYS_ITEM['price'])))


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(
            PhysicalItemCreateTests
        ))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
