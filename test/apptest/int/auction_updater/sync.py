"""
tests functionality of bargible.periodic_auc_updater used by
bin/auction_updater.py --sync (loosely during cleanup)
"""
import os
import sys
import unittest
import datetime
from time import sleep

from bargible.periodic_auc_updater import updateAuctions

from .base import PeriodicAucUpdaterTestsBase

class SyncPeriodicAucUpdaterTests(PeriodicAucUpdaterTestsBase):

    def setUp(self):
        super().setUp()
        self._total_sleep = 0

    def _sleep(self, secsSleep):
        self._total_sleep += secsSleep
        sleep(secsSleep)

    def _sleepTil(self, secsTotalSleep):
        secsSleep = secsTotalSleep - self._total_sleep
        self._total_sleep = secsTotalSleep
        sleep(secsSleep)

    def _createAndJoinAuction(self, durSecs):
        ## create an auction
        auction = self._createAuction(
            datetime.timedelta(seconds=durSecs))
        self.assertFalse(auction.started)
        self.assertFalse(auction.ended)
        ## start the auction
        updateAuctions()
        self.assertTrue(auction.started)
        self.assertFalse(auction.ended)
        # there is no 'start' event
        self.assertIsNone(self.eventRecvr.nextEvent())
        auctionManager = self._getAuctionManager(auction)
        self._sleepTil(durSecs * .2)
        ## join the auction
        auctionManager.joinAuction(self._user)
        # the join event is not emitted by the periodic updater
        self.assertIsNone(self.eventRecvr.nextEvent())
        return auction

    def testAucEnd(self):
        durSecs = 2
        auction = self._createAndJoinAuction(durSecs)
        self._sleepTil(durSecs * 1.1)
        ## end the auction
        updateAuctions()
        self.assertTrue(auction.started)
        self.assertTrue(auction.ended)
        endEventData = self.eventRecvr.nextEvent()
        self.assertEqual(endEventData['auctionId'], auction.id)
        self.assertEqual(endEventData['updateType'], 'end')

    def testBidsThaw(self):
        aucDurSecs = 6
        freezeDurSecs = 2
        freezeLatencySecs = 0.1
        auction = self._createAndJoinAuction(aucDurSecs)
        auction.freezeBids(
            self._user,
            datetime.timedelta(seconds=freezeDurSecs))
        freezeStarted = datetime.datetime.utcnow()
        updateAuctions()
        self.assertIsNone(self.eventRecvr.nextEvent())
        secSinceFreezeStarted = (
            datetime.datetime.utcnow() - freezeStarted
            ).total_seconds()
        if (secSinceFreezeStarted > 0 and
                secSinceFreezeStarted < (
                    freezeDurSecs + freezeLatencySecs)):
            self._sleep((freezeDurSecs + freezeLatencySecs) -
                            secSinceFreezeStarted)
        updateAuctions()
        thawEventData = self.eventRecvr.nextEvent()
        self.assertEqual(thawEventData['auctionId'], 1)
        self.assertEqual(thawEventData['updateType'], 'thaw')


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(
            SyncPeriodicAucUpdaterTests))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
