"""
tests functionality of bargible.periodic_auc_updater used by
bin/auction_updater.py _without_ --sync flag
"""
import os
import sys
import unittest
import datetime
import asyncio as aio
from time import sleep
from concurrent.futures import CancelledError

from bargible.periodic_auc_updater import updateAuctionsAsyncLoop

from .base import PeriodicAucUpdaterTestsBase

UPDATE_LATENCY = .75

class AsyncPeriodicAucUpdaterTests(PeriodicAucUpdaterTestsBase):

    def setUp(self):
        super().setUp()
        self._loop = aio.new_event_loop()
        # enforce using self._loop rather than global event loop
        aio.set_event_loop(None)
        self._exited_go = False
        self._go_coro = None
        self._total_sleep = 0

    def tearDown(self):
        self._runAsyncPortion()
        super().tearDown()

    def _sleep(self, secsSleep, sync=False):
        self._total_sleep += secsSleep
        if sync:
            sleep(secsSleep)
        return aio.sleep(secsSleep)

    def _sleepTil(self, secsTotalSleep, sync=False):
        secsSleep = secsTotalSleep - self._total_sleep
        self._total_sleep = secsTotalSleep
        if sync:
            sleep(secsSleep)
        return aio.sleep(secsSleep)

    def _runAsyncPortion(self):
        async def go():
            await self._go_coro()
            ## stop the updater task
            self._exited_go = True
            for task in aio.Task.all_tasks(loop=self._loop):
                task.cancel()
        goUpdaterFuture = aio.gather(
            go(),
            updateAuctionsAsyncLoop(),
            loop=self._loop,
            )
        try:
            self._loop.run_until_complete(goUpdaterFuture)
        except CancelledError:
            self.assertTrue(self._exited_go)

    async def _createAndJoinAuction(self, durSecs, startDelaySecs):
        ## create an auction
        auction = self._createAuction(
            datetime.timedelta(seconds=durSecs))
        self.assertFalse(auction.started)
        self.assertFalse(auction.ended)
        await self._sleepTil(startDelaySecs * 1.1)
        self.assertTrue(auction.started)
        self.assertFalse(auction.ended)
        # there is no 'start' event
        self.assertIsNone(self.eventRecvr.nextEvent())
        auctionManager = self._getAuctionManager(auction)
        await self._sleepTil(startDelaySecs + durSecs * .2)
        ## join the auction
        auctionManager.joinAuction(self._user)
        # the join event is not emitted by the periodic updater
        self.assertIsNone(self.eventRecvr.nextEvent())
        return auction

    def testAucEnd(self):
        async def go():
            """ to be run in parallel with update logic """
            startDelaySecs = .5
            durSecs = 4
            auction = await self._createAndJoinAuction(
                durSecs, startDelaySecs)
            await self._sleepTil(startDelaySecs +
                                     durSecs +
                                     UPDATE_LATENCY)
            ## end the auction
            self.assertTrue(auction.started)
            self.assertTrue(auction.ended)
            endEventData = self.eventRecvr.nextEvent()
            self.assertEqual(endEventData['auctionId'], auction.id)
            self.assertEqual(endEventData['updateType'], 'end')
        self._go_coro = go

    def testBidsThaw(self):
        async def go():
            startDelaySecs = .5
            aucDurSecs = 30
            freezeDurSecs = 2
            freezeLatencySecs = 0.1
            auction = await self._createAndJoinAuction(
                aucDurSecs, startDelaySecs)
            auction.freezeBids(
                self._user,
                datetime.timedelta(seconds=freezeDurSecs))
            freezeStarted = datetime.datetime.utcnow()
            self.assertIsNone(self.eventRecvr.nextEvent())
            secSinceFreezeStarted = (
                datetime.datetime.utcnow() - freezeStarted
                ).total_seconds()
            if (secSinceFreezeStarted > 0 and
                    secSinceFreezeStarted < (
                        freezeDurSecs + freezeLatencySecs)):
                await self._sleep((freezeDurSecs + freezeLatencySecs) -
                                      secSinceFreezeStarted)
            await self._sleep(UPDATE_LATENCY)
            thawEventData = self.eventRecvr.nextEvent()

            # x =
            # from pdb import set_trace; set_trace()

            self.assertIsNotNone(thawEventData)
            self.assertEqual(thawEventData['auctionId'], 1)
            self.assertEqual(thawEventData['updateType'], 'thaw')
        self._go_coro = go

def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(
            AsyncPeriodicAucUpdaterTests))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
