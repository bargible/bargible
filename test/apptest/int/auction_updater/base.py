"""
base functionality for bargible.periodic_auc_updater
integration tests
"""
import unittest
import datetime
import pickle
from time import sleep

import redis

from bargible import model
from bargible.model import saveModels
from bargible.model import Auction
from bargible.model import User
from bargible.model import Item
from bargible.model import Config
from bargible.auc_core import AuctionManager

import gentestmodels

REDIS_LATENCY = 1 # sec
USER_SID = '1234'

def resetDb():
    model.shutdownSession()
    model.dropAllTables()
    model.initDb()


class AppEventSocketIORecvr(object):
    """
    abstract over the auction updater -> web application
    events interface
    by pulling event information directly out of redis,
    according to Flask-SocketIO specific details as well as
    configuration details of the application.
    """

    CHANNEL = 'auc-socketio'

    def __init__(self, url='redis://localhost:6379/11'):
        self._redis = redis.Redis.from_url(url)
        self._pubsub = self._redis.pubsub(
            ignore_subscribe_messages=True,
            )
        self._pubsub.subscribe(self.CHANNEL)
        # even with the `ignore_subscribe_messages` flag,
        # it's necessary to clear an initial `None` message after
        # the subscribe() call to have the state flushed
        self._pubsub.get_message()

    def nextEvent(self):
        """
        events consist of some data and, optionally, a User.
        """
        sleep(REDIS_LATENCY)
        msg = self._pubsub.get_message()
        if msg is None:
            return None
        assert msg['type'] == 'message'
        data = pickle.loads(msg['data'])
        assert data['event'] == 'auction update'
        assert data['namespace'] == '/'
        assert data['method'] == 'emit'
        assert data['skip_sid'] is None # ?
        assert data['callback'] is None # ?
        # futher tightening -- not to just to Flask-SocketIO
        # or application details, but to test-specific details.
        # namely, all test cases currently assume one user.
        assert data['room'] == USER_SID
        return data['data']

    def disconnect(self):
        self._pubsub.unsubscribe()


class PeriodicAucUpdaterTestsBase(unittest.TestCase):

    def setUp(self):
        resetDb()
        gentestmodels.createItems(0)
        gentestmodels.createUsers(0)
        gentestmodels.createConfig()
        self._item = Item.byId(1)
        user = User.byId(1)
        user.sid = USER_SID
        saveModels(user)
        self._user = user
        self._config = Config.get()
        self.eventRecvr = AppEventSocketIORecvr()

    def tearDown(self):
        self.eventRecvr.disconnect()

    def _createAuction(self, duration,
                           startDelaySecs=0):
        """
        duration (timedelta)
        """
        start = (datetime.datetime.utcnow() +
                     datetime.timedelta(seconds=startDelaySecs))
        end = start + duration
        auction = Auction(
            reward=self._item,
            start_datetime=start,
            end_datetime=end,
            real_price=2000,
            join_cost=1000,
            )
        saveModels(auction)
        return auction

    def _getAuctionManager(self, auction):
        dbConfigDict = {
            str(col.name).upper(): int(getattr(self._config, col.name))
            for col in self._config.__table__.columns
        }
        return AuctionManager(auction, dbConfigDict)
