"""
tests bin/auction_updater.py (loosely during cleanup)
"""
import unittest

from . import sync as syncTests
from . import async as asyncTests

def load_tests(loader, standard_tests, pattern):
    suite = unittest.TestSuite()
    for testModule in [
            syncTests,
            asyncTests,
    ]:
        tests = (unittest.defaultTestLoader
                     .loadTestsFromModule(testModule, pattern=pattern))
        suite.addTests(tests)
    return suite

