"""
test the watch feature
"""
import os
import sys
import json
import unittest
from time import sleep

import gentestmodels

from bargible.periodic_auc_updater.updater import update as updateAllAuctions

from .base import BaseHttpTestCase
from .base import flaskUserLogin

with open(os.path.join(os.path.dirname(__file__),
                           'watch_00.json'), 'r') as f:
    DATA_AUCTIONS_FIXTURE = json.load(f)


SECS_TIL_AUCTION_START = 60
SECS_AUCTION_DURATION = 360

class WatchTests(BaseHttpTestCase):

    def _createUsersAndSetIds(self):
        user = gentestmodels.createUsers(0)[0]
        self.userId = str(user.id)

    def _getUpcomingData(self):
        with flaskUserLogin(self.c, self.userId):
            respJsonSingle, respStatus = self.fetchJson(
                ('/api/auction/auctions/{aid}'
                 .format(aid=self.auctionId)))
            respJsonMult, respStatus = self.fetchJson(
                '/api/auction/auctions?status=upcoming')
        self.assertEqual(len(respJsonMult), 1)
        self.assertEqual(respJsonSingle, respJsonMult[0])
        for key in ['active', 'joined', 'ended']:
            self.assertIsNone(respJsonSingle[key])
        return respJsonSingle['upcoming']

    @unittest.mock.patch('bargible.client_auc_events.auctionUpdateEvent')
    def setUp(self, mockUpdateEvent):
        super().setUp()
        gentestmodels.createItems(0)
        gentestmodels.createPhysicalItems(0)
        for auction in DATA_AUCTIONS_FIXTURE:
            auction['start']['inSecs'] = SECS_TIL_AUCTION_START
            auction['end']['inSecs'] = (SECS_TIL_AUCTION_START +
                                        SECS_AUCTION_DURATION)
        auction = gentestmodels.createAuctionsFromFixture(
            DATA_AUCTIONS_FIXTURE)[0]
        self.auctionId = auction.id
        with self.app.app_context():
            self._createUsersAndSetIds()
        updateAllAuctions()
        sleep(1)
        updateAllAuctions()

    def testHttpApi(self):
        upcomingData = self._getUpcomingData()
        self.assertIn('is_watching', upcomingData)
        self.assertFalse(upcomingData['is_watching'])
        with flaskUserLogin(self.c, self.userId):
            respJson, respStatus = self.updateJson(
                ('/api/user/auctions/{aid}?status=upcoming'
                 .format(aid=self.auctionId)),
                jsonData={
                    'watch': True
                    })
        self.assertEqual(respStatus, 200)
        upcomingData = self._getUpcomingData()
        self.assertEqual(respJson['upcoming'], upcomingData)
        self.assertIn('is_watching', upcomingData)
        self.assertTrue(upcomingData['is_watching'])


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(
            WatchTests
        ))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
