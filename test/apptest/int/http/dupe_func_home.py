"""
duplicate original home-page functional test at http layer
"""
import sys
import unittest
from time import sleep

import gentestmodels

from bargible.periodic_auc_updater.updater import update as updateAllAuctions
from bargible.model import saveModels # used in setup only

from .base import BaseHttpTestCase
from .base import flaskUserLogin

# should exceed delay in starting auctions by
# database latency + variability in python's sleep() function
SECS_TIL_AUCTION_START_UPDATE = 3

# removed keys:
# intermediateTimer
# timeRemaining
# bidPrice
DATA_AUCTION_JSON = {
    'id': 1,
    # removed keys: image, minPrice
    'reward': {
        'type': 'item',
        'data': {
            'itemId': 1,
            'name': 'Barnes & Noble eGift Card',
            'description': 'Lower Prices on Millions of Books, Movies and TV Show DVDs and Blu-ray, Music, Toys, and Games. Shop online for eBooks, NOOK, and textbooks.',
            'disclaimer': 'Barnes & Noble is not a sponsor or co-sponsor of this promotion. The logos and other identifying marks attached are trademarks of and owned by each represented company and/or its affiliates.  Please visitwww.bn.com for terms and conditions of use. Barnes & Noble is not liable for any alleged or actual claims related to this offer.',
        }
    },
}

class MockUpdateEventCall(object):
    """ initialize with a mock call object
    to match auctionUpdateEvent implementation parsing """

    def __init__(self, mockCall):
        mockCallName, mockCallArgs, mockCallKwargs = mockCall
        self.room = mockCallArgs[0]
        updateEventArgs = mockCallArgs[1]
        self.auctionId = updateEventArgs['auctionId']
        self.updateType = updateEventArgs['updateType']
        self.updateInfo = updateEventArgs.get('updateInfo')

    def __repr__(self):
        return (
            "<MockUpdateEventCall " +
            "room: " + str(self.room) + " -- "
            "auction: " + str(self.auctionId) + " -- "
            "type: " + str(self.updateType) + " -- " +
            "data: " + str(self.updateInfo) + ">"
        )

class DupeFuncHomeTests(BaseHttpTestCase):
    """
    closely (albeit not exactly) duplicate original functional
    (e2e) test coverage, including expectations related to js client
    at time of writing

    _test* methods are used to decompose functional test, although these
    attempt to isolated test cases, this is not guaranteed.
    isolated test cases are provided by test* methods.

    no asserts occur outside of test* and _test* methods.
    """

    def _setupUsersAndVars(self):
        users = gentestmodels.createUsers(0)[:2]
        # these login session ids are used analogously to
        # interface keys in the original functional tests
        self.userOneId, self.userTwoId = [
            str(user.id) # Flask-User/Flask-Login impl detail
            for user in users]
        # ... such that usernames, etc. may be looked up by id ...
        self.usernames = dict([
            (str(user.id), user.username)
            for user in users])
        # ... and login session id doubles (stubbed) events session id
        users[0].sid = self.userOneId
        users[1].sid = self.userTwoId
        saveModels(*users)

    def setUp(self):
        super().setUp()
        gentestmodels.createItems(0)
        gentestmodels.createFuncTestAuctions()
        sleep(SECS_TIL_AUCTION_START_UPDATE)
        updateAllAuctions()
        self.auctionId = 1;
        with self.app.app_context():
            self._setupUsersAndVars()
        self.userOneBids = None
        self.userTwoBids = None
        self.numJoinedUsers = 0

    #### utility functions ##

    def getUserCredit(self, userId):
        with flaskUserLogin(self.c, userId):
            respJson, respCode = self.fetchJson('/api/user/get')
        # typecast b/c client-side source doesn't parse
        return int(respJson['credit'])

    def getUserBids(self, userId, auctionId=None):
        if auctionId is None:
            auctionId = self.auctionId
        with flaskUserLogin(self.c, userId):
            respJson, respStatus = self.fetchJson(
                '/api/user/auctions/{id}?status=joined'
                .format(id=auctionId))
        joinedJson = respJson['joined']
        return joinedJson['bids']

    def getUserIsBidsFrozen(self, userId, auctionId=None):
        if auctionId is None:
            auctionId = self.auctionId
        with flaskUserLogin(self.c, userId):
            respJson, respStatus = self.fetchJson(
                '/api/user/auctions/{id}?status=joined'
                .format(id=auctionId))
        joinedJson = respJson['joined']
        return joinedJson['isBidsFrozen']

    def getBidPriceActive(self, userId, auctionId=None):
        if auctionId is None:
            auctionId = self.auctionId
        with flaskUserLogin(self.c, userId):
            respJson, respStatus = self.fetchJson(
                '/api/auction/auctions/{id}'.format(id=auctionId))
        activeJson = respJson['active']
        return activeJson['bidPrice']

    def getBidPriceJoined(self, userId, auctionId=None):
        if auctionId is None:
            auctionId = self.auctionId
        with flaskUserLogin(self.c, userId):
            respJson, respStatus = self.fetchJson(
                '/api/user/auctions/{id}?status=joined'
                .format(id=auctionId))
        joinedJson = respJson['joined']
        return joinedJson['bidPrice']

    def getNumUsersActive(self, userId, auctionId=None):
        """ get the number of users from the active auction view """
        if auctionId is None:
            auctionId = self.auctionId
        with flaskUserLogin(self.c, userId):
            respJson, respStatus = self.fetchJson(
                '/api/auction/auctions/{id}'.format(id=auctionId))
        activeJson = respJson['active']
        return activeJson['numUsers']

    def getNumUsersJoined(self, userId, auctionId=None):
        """ get the number of users from the active auction view """
        if auctionId is None:
            auctionId = self.auctionId
        with flaskUserLogin(self.c, userId):
            respJson, respStatus = self.fetchJson(
                '/api/user/auctions/{id}?status=joined'
                .format(id=auctionId))
        joinedJson = respJson['joined']
        return joinedJson['numUsers']

    def _testFetchUser(self, userId):
        with flaskUserLogin(self.c, userId):
            respJson, respCode = self.fetchJson('/api/user/get')
        self.assertEqual(respCode, 200)
        self.assertEqual((set(['first_name', 'credit'])
                          - set(respJson.keys())), set())

    def _testFetchJoined(self, userId, auctionId=None):
        if auctionId is None:
            auctionId = self.auctionId
        with flaskUserLogin(self.c, userId):
            respJson, respStatus = self.fetchJson(
                '/api/user/auctions/{id}?status=joined'
                .format(id=self.auctionId))
        joinedJson = respJson['joined']
        self.assertEqual(respStatus, 200)
        varData = {
            'isBidsFrozen': joinedJson.pop('isBidsFrozen'),
            'bids': joinedJson.pop('bids'),
            'position': joinedJson.pop('position'),
            'leaderboard': joinedJson.pop('leaderboard'),
            'secsFreezeRemaining': joinedJson.pop('secsFreezeRemaining'),
            'intermediateTimer': joinedJson.pop('intermediateTimer'),
            'numUsers': joinedJson.pop('numUsers'),
            'timeRemaining': joinedJson.pop('timeRemaining'),
            'bidPrice': joinedJson.pop('bidPrice'),
            'rewardImage': joinedJson['reward']['data'].pop('image'),
            # below entries are from the AuctionManager serialization
            'bidsTilActivation': joinedJson.pop('bidsTilActivation'),
            'numActivations': joinedJson.pop('numActivations'),
        }
        self.assertEqual(joinedJson, DATA_AUCTION_JSON)
        self.assertIs(type(varData['isBidsFrozen']), bool)
        self.assertIs(type(varData['bids']), int)
        self.assertIs(type(varData['intermediateTimer']), int)
        self.assertIs(type(varData['numUsers']), int)
        self.assertIs(type(varData['timeRemaining']), int)
        self.assertIs(type(varData['bidPrice']), int)
        self.assertIs(type(varData['rewardImage']), str)
        if varData['position'] is not None: # no position before bid
            self.assertIs(type(varData['position']), int)
        if varData['secsFreezeRemaining'] is not None:
            self.assertIs(type(varData['secsFreezeRemaining']), int)
            self.assertTrue(varData['secsFreezeRemaining'] > 0)
        self.assertIs(type(varData['leaderboard']), list)
        for leaderboardEntry in varData['leaderboard']:
            self.assertEqual(set(leaderboardEntry.keys()),
                             set([
                                 'position',
                                 'username',
                                 'avatar',
                                 'bids', # bids remaining
                                 ]))
            if leaderboardEntry['position'] is not None:
                self.assertIs(type(leaderboardEntry['position']), int)
            if leaderboardEntry['avatar'] is not None:
                self.assertIs(type(leaderboardEntry['position']), str)
            self.assertIs(type(leaderboardEntry['username']), str)
            self.assertIs(type(leaderboardEntry['bids']), int)
        for bidToActivateDict in [varData['bidsTilActivation'],
                                  varData['numActivations']]:
            self.assertIs(type(bidToActivateDict), dict)
            self.assertEqual(set(bidToActivateDict.keys()),
                             set(['freeze', 'steal', 'destroy']))
            for val in bidToActivateDict.values():
                self.assertEqual(type(val), int)

    @unittest.mock.patch('bargible.client_auc_events.auctionUpdateEvent')
    def _testJoinAuctionParam(self, userId, mockUpdateEvent,
                              auctionId=None):
        """ parameterized join auction test
        every time a user joins an auction, either this method
        is to be used or `numJoinedUsers` need be incremented """
        if auctionId is None:
            auctionId = self.auctionId
        with flaskUserLogin(self.c, userId):
            respJson, respStatus = self.fetchJson(
                'api/user/auctions?status=joined')
        self.assertEqual(respStatus, 200)
        self.assertIs(type(respJson), list)
        self.assertEqual(len(respJson), 0)
        mockUpdateEvent.reset_mock()
        with flaskUserLogin(self.c, userId):
            respJson, respStatus = self.createJson(
            ('/api/user/auctions/{aid}/actions/join'
             .format(aid=auctionId)))
        self.assertEqual(respStatus, 200)
        self.assertEqual(respJson, {})
        self.assertEqual(len(mockUpdateEvent.mock_calls), 1)
        mockUpdateEventCall = MockUpdateEventCall(
            mockUpdateEvent.mock_calls[0])
        self.assertEqual(mockUpdateEventCall.room, userId)
        self.assertEqual(mockUpdateEventCall.auctionId, auctionId)
        self.assertEqual(mockUpdateEventCall.updateType, 'join')
        # called via auction_update_handler.js
        self._testFetchJoined(userId, auctionId=auctionId)
        self._testFetchUser(userId)
        self.numJoinedUsers += 1

    @unittest.mock.patch('bargible.client_auc_events.auctionUpdateEvent')
    def _testBid(self, userId, mockUpdateEvent, auctionId=None,
                 isFirst=False, numMultBids=None):
        """ mimic js behavior on bid button click """
        if auctionId is None:
            auctionId = self.auctionId
        # assertion a/b test impl
        assert numMultBids is None or numMultBids in range(1, 9)
        if self.getUserIsBidsFrozen(userId, auctionId):
            return
        mockUpdateEvent.reset_mock()
        with flaskUserLogin(self.c, userId):
            respJson, respStatus = self.updateJson(
                ('/api/user/auctions/{aid}/actions/bid'
                 .format(aid=auctionId)))
        allCalls = [ MockUpdateEventCall(mockCall) for mockCall
                     in mockUpdateEvent.mock_calls ]
        bidCalls = [ call for call in allCalls
                     if (call.updateType == 'bid' or
                         call.updateType == 'bid-self')]
        firstBidCalls = [ call for call in allCalls
                          if call.updateType == 'first-bid' ]
        bidMultCalls = [ call for call in allCalls
                         if call.updateType == 'bid-mult' ]
        for bidCall in bidCalls:
            self.assertEqual(bidCall.auctionId, auctionId)
        if isFirst:
            self.assertEqual(len(firstBidCalls), 1)
            self.assertEqual(firstBidCalls[0].room, userId)
        else:
            self.assertEqual(len(firstBidCalls), 0)
        userBidCalls = [ call for call in bidCalls
                         if call.room == userId]
        self.assertEqual(len(userBidCalls), 1)
        # called via auction_update_handler.js
        self._testFetchJoined(userId, auctionId=auctionId)
        self._testFetchUser(userId)
        for bidMultCall in bidMultCalls:
            self.assertEqual(set(bidMultCall.updateInfo.keys()),
                             set([
                                 'username',
                                 'numConsecutiveBids',
                                 'complete',
                             ]))
            self.assertEqual(bidMultCall.updateInfo['username'],
                             self.usernames[userId])
            if numMultBids is not None:
                self.assertEqual(bidMultCall.updateInfo['username'],
                                 self.usernames[userId])
                self.assertEqual(
                    bidMultCall.updateInfo['numConsecutiveBids'],
                    numMultBids)
                if numMultBids == 8:
                    self.assertTrue(bidMultCall.updateInfo['complete'])
        if numMultBids is not None:
            if numMultBids in range(1, 4):
                self.assertEqual(len(bidMultCalls), 0)
            else:
                self.assertEqual(
                    len(set([call.room for call in bidMultCalls])),
                    self.numJoinedUsers)

    @unittest.mock.patch('bargible.client_auc_events.auctionUpdateEvent')
    def _testSteal(self, userId, bidsStolen, mockUpdateEvent,
                   auctionId=None,
                   otherUsersBidsStolen={}):
        """ mimic js behavior on steal button click
        * `bidsStolen` (int): the number of bids stolen by stealing user
        * `otherUsersBidsStolen` (dict str -> int):
            key-value pairs of user ids --to-> number of bids stolen
        """
        if auctionId is None:
            auctionId = self.auctionId
        stolenFromUsernames = [ self.usernames[uid] for uid
                                in otherUsersBidsStolen.keys() ]
        mockUpdateEvent.reset_mock()
        with flaskUserLogin(self.c, userId):
            respJson, respStatus = self.updateJson(
                ('/api/user/auctions/{aid}/actions/steal'
                 .format(aid=auctionId)))
        self.assertEqual(respStatus, 200)
        allCalls = [ MockUpdateEventCall(mockCall) for mockCall
                     in mockUpdateEvent.mock_calls ]
        userCalls = [call for call in allCalls
                     if (call.room == userId
                         and call.updateType != 'game-element')]
        self.assertEqual(len(userCalls), 1)
        userCall = userCalls[0]
        self.assertEqual(userCall.updateType, 'steal-self')
        gameElCalls = [call for call in allCalls
                     if call.updateType == 'game-element']
        self.assertEqual(len(gameElCalls), 0)
        self.assertEqual(set(userCall.updateInfo.keys()),
                         set(['numBids', 'usernames']))
        self.assertEqual(userCall.updateInfo['numBids'], bidsStolen)
        self.assertEqual(set(userCall.updateInfo['usernames']),
                         set(stolenFromUsernames))
        # called via auction_update_handler.js
        self._testFetchJoined(userId, auctionId=auctionId)
        self._testFetchUser(userId)
        otherUsersCalls = [ call for call in allCalls
                            if (call.room != userId
                                and call.updateType != 'game-element') ]
        for otherUserCall in otherUsersCalls:
            self.assertEqual(otherUserCall.updateType, 'steal')
            self.assertEqual(set(otherUserCall.updateInfo.keys()),
                             set(['numBids', 'username']))
            self.assertEqual(self.usernames[userId],
                             otherUserCall.updateInfo['username'])
        self.assertEqual(set([call.room for call in otherUsersCalls]),
                         set(otherUsersBidsStolen.keys()))
        for otherUserId, numBids in otherUsersBidsStolen.items():
            otherUserCalls = [ call for call in otherUsersCalls
                               if (call.room == otherUserId
                                   and call.updateType != 'game-element') ]
            self.assertEqual(len(otherUserCalls), 1)
            otherUserCall = otherUserCalls[0]
            self.assertEqual(otherUserCall.updateInfo['numBids'], numBids)

    @unittest.mock.patch('bargible.client_auc_events.auctionUpdateEvent')
    def _testDestroy(self, userId, mockUpdateEvent,
                     auctionId=None,
                     otherUsersBidsDestroyed={}):
        """ mimic js behavior on destroy button click.
        analogous to `_testSteal` """
        if auctionId is None:
            auctionId = self.auctionId
        fromUsernames = [ self.usernames[uid] for uid
                          in otherUsersBidsDestroyed.keys() ]
        mockUpdateEvent.reset_mock()
        with flaskUserLogin(self.c, userId):
            respJson, respStatus = self.updateJson(
                ('/api/user/auctions/{aid}/actions/destroy'
                 .format(aid=auctionId)))
        allCalls = [ MockUpdateEventCall(mockCall) for mockCall
                     in mockUpdateEvent.mock_calls ]
        userCalls = [call for call in allCalls
                     if (call.room == userId
                         and call.updateType != 'game-element')]
        self.assertEqual(len(userCalls), 1)
        userCall = userCalls[0]
        self.assertEqual(userCall.updateType, 'destroy-self')
        gameElCalls = [call for call in allCalls
                       if call.updateType == 'game-element']
        self.assertEqual(len(gameElCalls), 0)
        self.assertEqual(set(userCall.updateInfo.keys()),
                         set(['usernames']))
        self.assertEqual(set(userCall.updateInfo['usernames']),
                         set(fromUsernames))
        # called via auction_update_handler.js
        self._testFetchJoined(userId, auctionId=auctionId)
        self._testFetchUser(userId)
        otherUsersCalls = [ call for call in allCalls
                            if (call.room != userId
                                and call.updateType != 'game-element') ]
        for otherUserCall in otherUsersCalls:
            self.assertEqual(otherUserCall.updateType, 'destroy')
            self.assertEqual(set(otherUserCall.updateInfo.keys()),
                             set(['numBids', 'username']))
            self.assertEqual(self.usernames[userId],
                             otherUserCall.updateInfo['username'])
        self.assertEqual(set([call.room for call in otherUsersCalls]),
                         set(otherUsersBidsDestroyed.keys()))
        for otherUserId, numBids in otherUsersBidsDestroyed.items():
            otherUserCalls = [ call for call in otherUsersCalls
                               if call.room == otherUserId ]
            self.assertEqual(len(otherUserCalls), 1)
            otherUserCall = otherUserCalls[0]
            self.assertEqual(otherUserCall.updateInfo['numBids'], numBids)

    #### decomposed functional test ##

    def _testJoin(self):
        self._testJoinAuctionParam(self.userOneId)
        self.assertEqual(self.getBidPriceJoined(self.userOneId), 0)

    @unittest.skip("included in testGameTwoP")
    def testJoin(self):
        self._testJoin()

    def _testBidOneP(self):
        # expected values
        userOneBids = self.getUserBids(self.userOneId)
        userOneBids += 10 # first bid bonus
        bidPrice = 1
        self._testBid(self.userOneId, isFirst=True)
        self.assertEqual(self.getUserBids(self.userOneId), userOneBids)
        self.assertEqual(
            self.getBidPriceJoined(self.userOneId),
            bidPrice)
        for _ in range(2):
            userOneBids -= 1
            bidPrice += 1
            self._testBid(self.userOneId)
            self.assertEqual(
                self.getUserBids(self.userOneId), userOneBids)
            self.assertEqual(
                self.getBidPriceJoined(self.userOneId),
                bidPrice)

    @unittest.skip("included in testGameTwoP")
    def testBidOneP(self):
        self._testJoin()
        self._testBidOneP()

    def _testGameOneP(self):
        # bid enough times to activate one steal and one destroy
        for _ in range(30):
            self._testBid(self.userOneId)
        # expected values
        userOneBids = self.getUserBids(self.userOneId)
        credit = self.getUserCredit(self.userOneId)
        ## steal
        self._testSteal(self.userOneId, 0)
        self.assertEqual(self.getUserBids(self.userOneId), userOneBids)
        self.assertEqual(self.getUserCredit(self.userOneId), credit)
        ## destroy
        self._testDestroy(self.userOneId)
        self.assertEqual(self.getUserBids(self.userOneId), userOneBids)
        self.assertEqual(self.getUserCredit(self.userOneId), credit)

    @unittest.skip("included in testGameTwoP")
    def testGameOneP(self):
        self._testJoin()
        self._testBidOneP()
        self._testGameOneP()

    def _testBidTwoP(self):
        bidPriceUnjoined = self.getBidPriceActive(self.userTwoId)
        self._testJoinAuctionParam(self.userTwoId)
        # expected values
        bidPrice = self.getBidPriceJoined(self.userOneId)
        userTwoBids = self.getUserBids(self.userTwoId)
        self.assertEqual(bidPrice, bidPriceUnjoined)
        self.assertEqual(
            self.getBidPriceJoined(self.userTwoId),
            bidPrice)
        for _ in range(3):
            userTwoBids -= 1
            bidPrice += 1
            self._testBid(self.userTwoId)
            self.assertEqual(
                self.getUserBids(self.userTwoId), userTwoBids)
            self.assertEqual(
                self.getBidPriceJoined(self.userTwoId),
                bidPrice)

    @unittest.skip("included in testGameTwoP")
    def testBidTwoP(self):
        self._testJoin()
        self._testBidOneP()
        self._testGameOneP()
        self._testBidTwoP()

    def _testGameTwoP(self):
        # bid enough times to activate one steal and one destroy
        for _ in range(30):
            self._testBid(self.userTwoId)
        userOneBids = self.getUserBids(self.userOneId)
        userTwoBids = self.getUserBids(self.userTwoId)
        bidPrice = self.getBidPriceJoined(self.userTwoId)
        ## steal
        userOneBids -= 2
        userTwoBids += 2
        self._testSteal(self.userTwoId, 2, otherUsersBidsStolen={
            self.userOneId: 2,
        })
        self.assertEqual(self.getUserBids(self.userOneId), userOneBids)
        self.assertEqual(self.getUserBids(self.userTwoId), userTwoBids)
        ## destroy
        userOneBids -= 4
        self._testDestroy(self.userTwoId, otherUsersBidsDestroyed={
            self.userOneId: 4,
        })
        self.assertEqual(self.getUserBids(self.userOneId), userOneBids)
        self.assertEqual(self.getUserBids(self.userTwoId), userTwoBids)

    def testGameTwoP(self):
        self._testJoin()
        self._testBidOneP()
        self._testGameOneP()
        self._testBidTwoP()
        self._testGameTwoP()

    #### isolated test functionality ##
    # tests in this section make the tradeoff of duplicating the
    # original functional tests less exactly if favor of
    # increased test isolation

    def testBidMult(self):
        self._testJoinAuctionParam(self.userOneId)
        self._testJoinAuctionParam(self.userTwoId)
        self._testBid(self.userTwoId, isFirst=True)
        for numMultBids in range(1, 8):
            self._testBid(self.userOneId, numMultBids=numMultBids)
        userOneBids = self.getUserBids(self.userOneId)
        userOneBids -= 1
        userOneBids += 16 # bid mult refund
        self._testBid(self.userOneId, numMultBids=8)
        self.assertEqual(self.getUserBids(self.userOneId), userOneBids)

    @unittest.mock.patch('bargible.client_auc_events.auctionUpdateEvent')
    def testFreezeBids(self, mockUpdateEvent):
        userOneId = self.userOneId
        userTwoId = self.userTwoId
        auctionId = self.auctionId
        self._testJoinAuctionParam(userOneId, auctionId=auctionId)
        self._testJoinAuctionParam(userTwoId, auctionId=auctionId)
        for _ in range(10): # ensure freeze activated
            with flaskUserLogin(self.c, userOneId):
                respJson, respStatus = self.updateJson(
                    ('/api/user/auctions/{aid}/actions/bid'
                     .format(aid=auctionId)))
        self.assertFalse(self.getUserIsBidsFrozen(userOneId, auctionId))
        self.assertFalse(self.getUserIsBidsFrozen(userTwoId, auctionId))
        mockUpdateEvent.reset_mock()
        with flaskUserLogin(self.c, userOneId):
            respJson, respStatus = self.updateJson(
                ('/api/user/auctions/{aid}/actions/freeze'
                 .format(aid=auctionId)))
        self.assertEqual(respStatus, 200)
        allCalls = [ MockUpdateEventCall(mockCall) for mockCall
                     in mockUpdateEvent.mock_calls ]
        bidsFreezeCalls = [ call for call in allCalls
                            if call.updateType == 'bids-freeze' ]
        frozeBidsCalls = [ call for call in allCalls
                           if call.updateType == 'froze-bids' ]
        self.assertEqual(len(bidsFreezeCalls), 1)
        bidsFreezeCall = bidsFreezeCalls[0]
        self.assertEqual(bidsFreezeCall.room, userTwoId)
        self.assertEqual(bidsFreezeCall.auctionId, auctionId)
        self.assertEqual(set(bidsFreezeCall.updateInfo.keys()),
                         set(['username']))
        self.assertEqual(bidsFreezeCall.updateInfo['username'],
                         self.usernames[userOneId])
        self.assertEqual(len(frozeBidsCalls), 1)
        frozeBidsCall = frozeBidsCalls[0]
        self.assertEqual(bidsFreezeCall.room, userTwoId)
        gameElCalls = [call for call in allCalls
                       if call.updateType == 'game-element']
        self.assertEqual(len(gameElCalls), 0)
        self.assertEqual(frozeBidsCall.auctionId, auctionId)
        self.assertEqual(set(frozeBidsCall.updateInfo.keys()),
                         set(['usernames']))
        self.assertIs(type(frozeBidsCall.updateInfo['usernames']), list)
        self.assertEqual(set(frozeBidsCall.updateInfo['usernames']),
                         set([self.usernames[userTwoId],]))
        self.assertFalse(self.getUserIsBidsFrozen(userOneId, auctionId))
        self.assertTrue(self.getUserIsBidsFrozen(userTwoId, auctionId))

    #### new tests ##
    # tests in this section were not part of the original
    # functional tests

    def testNumUsers(self):
        self._testJoinAuctionParam(self.userOneId)
        self.assertEqual(self.getNumUsersJoined(self.userOneId), 1)
        self.assertEqual(self.getNumUsersActive(self.userTwoId), 1)
        self._testJoinAuctionParam(self.userTwoId)
        self.assertEqual(self.getNumUsersJoined(self.userOneId), 2)
        self.assertEqual(self.getNumUsersJoined(self.userTwoId), 2)



def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(DupeFuncHomeTests))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
