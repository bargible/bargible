""" test http api as consumed by home page """
import os
import sys
import unittest
import unittest.mock
import json
import math
import re
from datetime import datetime
from datetime import timedelta
from time import sleep

from bs4 import BeautifulSoup as BS

from flask import current_app

from sqlalchemy_imageattach.context import store_context

from bargible.model import imageStore
from bargible.model import saveModels
from bargible.model import User
from bargible.model import Reward
from bargible.model import Auction
from bargible.model import Item
from bargible.model import Config

from bargible.periodic_auc_updater.updater import update as updateAllAuctions
from bargible.auc_core.auction_manager import AuctionManager

from .base import BaseHttpTestCase

PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..', '..', '..', '..')
)

PATH_DIR_ITEM_IMGS = os.path.join(
    PROJ_ROOT, 'test_data', 'images', 'item')

with open(os.path.join(
        PROJ_ROOT, 'test_data', 'fixtures', 'items_00.json')) as f:
    DATA_ITEMS = json.load(f)

USERS_FIELDS = [
    {
        'last_name': 'Belcher',
        'first_name': 'Linda',
        'email': 'l@b.org',
        'password': 'Temp321!',
        'username': 'linda',
    },
    {
        'last_name': 'Belcher',
        'first_name': 'Tina',
        'email': 'b@b.org',
        'password': 'Temp321!',
        'username': 'tina',
    },
    {
        'last_name': 'Belcher',
        'first_name': 'Gene',
        'email': 'b@b.org',
        'password': 'Temp321!',
        'username': 'gene',
    },
    {
        'last_name': 'Belcher',
        'first_name': 'Louise',
        'email': 'b@b.org',
        'password': 'Temp321!',
        'username': 'louise',
    },
]

NAV_LINKS = {
    'home': '/home',
    'profile': '/profile',
    'faq': '/faq',
    'logout': '/logout',
}

NOW = datetime.utcnow()

SEC_TIL_AUCTION_END = 2

# start datetimes are set per test run to ensure updater functionality
DATA_TEST_AUCTIONS = [
    {
        'endTimedelta': timedelta(minutes=5),
        'minPrice': 1000,
    },
    {
        'endTimedelta': timedelta(minutes=10),
        'minPrice': 2000,
    },
    {
        'endTimedelta': timedelta(minutes=10),
        'minPrice': 2000,
    },
]

NON_EXISTENT_AUCTION_ID = 123



def _initItemFromFixture(fixtureData):
    item = Item(**{ key: fixtureData[key]
                    for key in fixtureData.keys()
                    if key != 'picture' })
    with store_context(imageStore):
        with open(os.path.join(PATH_DIR_ITEM_IMGS,
                               fixtureData['picture']
        ), mode='rb') as f:
            item.picture.from_file(f)
    return item

def _aucDataToParams(testData, isUpcoming=False):
    """ convert test data to model parameters """
    now = datetime.utcnow()
    startDatetime = now
    if isUpcoming:
        startDatetime += timedelta(hours=2)
    endDatetime = startDatetime + testData['endTimedelta']
    return {
        'start_datetime': startDatetime,
        'end_datetime': endDatetime,
        'min_price': testData['minPrice'],
        'join_cost': 1000,
    }

class HomeTests(BaseHttpTestCase):

    FREEZE_DURATION = timedelta(seconds=.4)

    def _createRewards(self):
        items = [ _initItemFromFixture(dataItem)
              for dataItem in DATA_ITEMS ]
        saveModels(*items)
        return items

    def _createAuctions(self):
        rewards = self._createRewards()
        auctionsParamsNoReward = [_aucDataToParams(testData)
                                    for testData
                                    in DATA_TEST_AUCTIONS[:-1] ]
        auctionsParamsNoReward += [
            _aucDataToParams(testData, isUpcoming=True)
            for testData in DATA_TEST_AUCTIONS[-1:] ]
        auctionsParams = [dict(paramsNoReward, reward=reward)
                          for (paramsNoReward, reward)
                          in zip(auctionsParamsNoReward, rewards)]
        auctions = [Auction(**auctionParams)
                    for auctionParams in auctionsParams]
        saveModels(*auctions)
        sleep(1) # Allow time for auctions to start
        self.assertNotIn(
            None, [auction.id for auction in auctions],
            msg="set primary keys for auction insert")
        return [auction.id for auction in auctions]

    def _createUser(self, userFields):
        """ functionality from flask-user register endpoint """
        userFields['credit'] = Config.get().init_user_credits
        user = User(**userFields)
        saveModels(user)
        return user

    def _isBidsFrozen(self, username):
        """
        abstract out non-http details of how bids are frozen
        and get the frozen status for a particular user
        """
        maybeFrozenUser = User.findByUsername(username)
        maybeFrozenUserJsonObj = (Auction.byId(self.auctionId)
                                  .getUserJsonObj(maybeFrozenUser))
        return maybeFrozenUserJsonObj['isBidsFrozen']

    def _simulateFreezeBids(self, username):
        """
        abstract out non-http details of how bids are frozen
        and freeze bids for a particular user
        """
        user = User.findByUsername(username)
        (Auction.byId(self.auctionId)
         .freezeBids(user, self.FREEZE_DURATION))

    def _getNumsBids(self):
        auction = Auction.byId(self.auctionId)
        return dict([ (uam.user.username, uam.bids)
                      for uam in auction.getUserRelations() ])

    def _createUsersInAuction(self):
        users = [self._createUser(fields) for fields in USERS_FIELDS]
        auctionManager = AuctionManager(Auction.byId(self.auctionId))
        updateAllAuctions()
        for user in users:
            auctionManager.joinAuction(user)
        auction = Auction.byId(self.auctionId)
        self.assertEqual(len(User.query.all()), 1 + len(USERS_FIELDS))
        self.assertEqual(len(auction.getUserRelations()),
                         len(USERS_FIELDS))
        for user in User.query.all():
            user.sid = user.username
            saveModels(user)

    def setUp(self):
        super().setUp()
        # register hits the Flask app's /register endpoint
        # and needs to be placed before other calls
        self.register()
        self.auctionId = self._createAuctions()[0]
        # unlike other models, User requires app context,
        # possibly due to the Flask-User extension
        with self.app.app_context():
            self._createUsersInAuction()

    def tearDown(self):
        super().tearDown()
        self.auctionId = None

    @unittest.mock.patch('bargible.client_auc_events.auctionUpdateEvent')
    def testBid(self, mockUpdateEvent):
        joinResp = self.c.post(
            ('/api/user/auctions/{aid}/actions/join'
             .format(aid=self.auctionId)))
        self.assertEqual(joinResp.status_code, 200)
        bidResp = self.c.put(
            ('/api/user/auctions/{aid}/actions/bid'
             .format(aid=self.auctionId)),
            data=json.dumps({}),
            content_type='application/json')
        self.assertEqual(bidResp.status_code, 200)

    @unittest.mock.patch('bargible.client_auc_events.auctionUpdateEvent')
    def testBidMult(self, mockUpdateEvent):
        joinResp = self.c.post(
            ('/api/user/auctions/{aid}/actions/join'
             .format(aid=self.auctionId)))
        self.assertEqual(joinResp.status_code, 200)
        for _ in range(1, 4):
            bidResp = self.c.put(
                ('/api/user/auctions/{aid}/actions/bid'
                 .format(aid=self.auctionId)),
                data=json.dumps({}),
                content_type='application/json')
            self.assertEqual(bidResp.status_code, 200)
        mockUpdateEvent.reset_mock()
        for numBids in range(4, 8):
            bidResp = self.c.put(
                ('/api/user/auctions/{aid}/actions/bid'
                 .format(aid=self.auctionId)),
                data=json.dumps({}),
                content_type='application/json')
            self.assertEqual(bidResp.status_code, 200)
            nonBidMockCalls = [ (name, args, kwargs) for
                                (name, args, kwargs) in
                                mockUpdateEvent.mock_calls if
                                ('updateType' not in args[1].keys() or
                                 (args[1]['updateType'] != 'bid' and
                                  args[1]['updateType'] != 'bid-self')) ]
            self.assertEqual(len(nonBidMockCalls),
                             len(USERS_FIELDS) + 1)
            self.assertEqual(set([ args[0] for (name, args, kwargs) in
                                   nonBidMockCalls ]),
                             set([ user.sid for user in
                                   User.query.all() ]))
            for msgDict in [ args[1] for (name, args, kwargs) in
                             nonBidMockCalls ]:
                self.assertEqual(msgDict, {
                    'auctionId': self.auctionId,
                    'updateType': 'bid-mult',
                    'updateInfo': {
                        'complete': False,
                        'numConsecutiveBids': numBids,
                        'username': 'bburgers',
                    },
                })
            mockUpdateEvent.reset_mock()
        initNumsBids = self._getNumsBids()
        bidResp = self.c.put(
            ('/api/user/auctions/{aid}/actions/bid'
             .format(aid=self.auctionId)),
            data=json.dumps({}),
            content_type='application/json')
        self.assertEqual(bidResp.status_code, 200)
        nonBidMockCalls = [ (name, args, kwargs) for
                            (name, args, kwargs) in
                            mockUpdateEvent.mock_calls if
                            ('updateType' not in args[1].keys() or
                                 (args[1]['updateType'] != 'bid' and
                                  args[1]['updateType'] != 'bid-self')) ]
        self.assertEqual(len(nonBidMockCalls),
                         len(USERS_FIELDS) + 1)
        for msgDict in [ args[1] for (name, args, kwargs) in
                             nonBidMockCalls ]:
                self.assertEqual(msgDict, {
                    'auctionId': self.auctionId,
                    'updateType': 'bid-mult',
                    'updateInfo': {
                        'username': 'bburgers',
                        'numConsecutiveBids': 8,
                        'complete': True,
                    },
                })
        # keyed by username, this dictionary's values are
        # the named argument dictionary to the mock auctionUpdateEvent
        # function that have a 'bids' argument
        dictHasBidDicts = dict([ (args[0], args[1]) for
                                 (_, args, _) in
                                 mockUpdateEvent.mock_calls if
                                 'bids' in args[1].keys() ])
        self.assertEqual(len(dictHasBidDicts), 0)
        mockUpdateEvent.reset_mock()
        bidResp = self.c.put(
            ('/api/user/auctions/{aid}/actions/bid'
             .format(aid=self.auctionId)),
            data=json.dumps({}),
            content_type='application/json')
        self.assertEqual(bidResp.status_code, 200)
        nonBidMockCalls = [ (name, args, kwargs) for
                            (name, args, kwargs) in
                            mockUpdateEvent.mock_calls if
                            ('updateType' not in args[1].keys() or
                                 (args[1]['updateType'] != 'bid' and
                                  args[1]['updateType'] != 'bid-self')) ]
        for msgDict in [ args[1] for (name, args, kwargs) in
                             nonBidMockCalls ]:
                self.assertEqual(msgDict, {
                    'auctionId': self.auctionId,
                    'message': "bburgers got the bidding bonus!",
                })

    @unittest.mock.patch('bargible.client_auc_events.auctionUpdateEvent')
    def testFreeze(self, mockUpdateEvent):
        joinResp = self.c.post(
            ('/api/user/auctions/{aid}/actions/join'
             .format(aid=self.auctionId)))
        self.assertEqual(joinResp.status_code, 200)
        for _ in range(10): # ensure freeze activated
            bidResp = self.c.put(
                ('/api/user/auctions/{aid}/actions/bid'
                 .format(aid=self.auctionId)),
                data=json.dumps({}),
                content_type='application/json')
            self.assertEqual(bidResp.status_code, 200)
        mockUpdateEvent.reset_mock()
        bidResp = self.c.put(
                ('/api/user/auctions/{aid}/actions/freeze'
                 .format(aid=self.auctionId)))
        self.assertEqual(bidResp.status_code, 200)
        nonBidMockCalls = [ (name, args, kwargs) for
                            (name, args, kwargs) in
                            mockUpdateEvent.mock_calls if
                            ('updateType' not in args[1].keys() or
                             (args[1]['updateType'] != 'bid' and
                              args[1]['updateType'] != 'bid-self') and
                             args[1]['updateType'] != 'game-element') ]
        self.assertEqual(len(nonBidMockCalls),
                         # +1 for freeze notification
                         math.ceil(len(USERS_FIELDS) *.25) + 1)
        bidsFreezeMockCallsArgs = [
            args for (name, args, kwargs) in
            mockUpdateEvent.mock_calls if
            args[1]['updateType'] == 'bids-freeze' ]
        frozenUsername = bidsFreezeMockCallsArgs[0][0]
        updateAllAuctions()
        self.assertTrue(self._isBidsFrozen(frozenUsername))

    @unittest.mock.patch('bargible.client_auc_events.auctionUpdateEvent')
    def testFrozenBids(self, mockUpdateEvent):
        joinResp = self.c.post(
            ('/api/user/auctions/{aid}/actions/join'
             .format(aid=self.auctionId)))
        self.assertEqual(joinResp.status_code, 200)
        self._simulateFreezeBids(self.httpUsername)
        resp = self.c.get('/api/user/auctions?status=joined')
        self.assertEqual(resp.status_code, 200)
        respJson = json.loads(resp.data.decode('utf-8'))
        self.assertIs(type(respJson), list)
        self.assertEqual(len(respJson), 1)
        joinedJson = [ aucJson['joined'] for aucJson in respJson ]
        self.assertIn('isBidsFrozen', joinedJson[0].keys())
        self.assertTrue(joinedJson[0]['isBidsFrozen'])

    def testReadAuctions(self):
        """ tests several collections endpoints """
        updateAllAuctions()
        activeResp = self.c.get(
            '/api/auction/auctions?status=active')
        self.assertEqual(activeResp.status_code, 200)
        activeRespJson = json.loads(activeResp.data.decode('utf-8'))
        self.assertIs(type(activeRespJson), list)
        self.assertEqual(len(activeRespJson), 2)
        upcomingResp = self.c.get(
            '/api/auction/auctions?status=upcoming')
        self.assertEqual(upcomingResp.status_code, 200)
        upcomingRespJson = json.loads(upcomingResp.data.decode('utf-8'))
        self.assertIs(type(upcomingRespJson), list)
        self.assertEqual(len(upcomingRespJson), 1)

    def testGetAuction(self):
        """ test /api/auction/auctions/:id endpoint """
        updateAllAuctions()
        noAucResp = self.c.get('/api/auction/auctions/' +
                               str(NON_EXISTENT_AUCTION_ID))
        self.assertEqual(noAucResp.status_code, 404)
        aucResp = self.c.get('/api/auction/auctions/' +
                             str(self.auctionId))
        self.assertEqual(aucResp.status_code, 200)
        respJson = json.loads(aucResp.data.decode('utf-8'))
        activeJson = respJson['active']
        self.assertIs(type(activeJson), dict)
        self.assertEqual(0, activeJson['bidPrice'])

    @unittest.mock.patch('bargible.client_auc_events.auctionUpdateEvent')
    def testJoinedAuction(self, mockUpdateEvent):
        """ test fetching joined auctions """
        updateAllAuctions()
        noAucResp = self.c.get(
            '/api/user/auctions/{id}?status=joined'
            .format(id=NON_EXISTENT_AUCTION_ID))
        self.assertEqual(noAucResp.status_code, 404)
        unjoinedAucResp = self.c.get(
            '/api/user/auctions/{id}?status=joined'
            .format(id=NON_EXISTENT_AUCTION_ID))
        self.assertEqual(unjoinedAucResp.status_code, 404)
        joinResp = self.c.post(
            ('/api/user/auctions/{aid}/actions/join'
             .format(aid=self.auctionId)))
        self.assertEqual(joinResp.status_code, 200)
        joinedAucResp = self.c.get(
            '/api/user/auctions/{id}?status=joined'
            .format(id=self.auctionId))
        self.assertEqual(joinedAucResp.status_code, 200)

    def testGetPages(self):
        resp = self.c.get('/home')
        self.assertEqual(resp.status_code, 200)
        soup = BS(resp.data.decode('utf-8'), "html.parser")
        self.assertEqual(
            NAV_LINKS,
            json.loads(soup.find(id='navbar')['data-nav-links']))
        resp = self.c.get('/', follow_redirects=True)
        self.assertEqual(resp.status_code, 200)
        soup = BS(resp.data.decode('utf-8'), "html.parser")
        self.assertEqual(
            NAV_LINKS,
            json.loads(soup.find(id='navbar')['data-nav-links']))
        resp = self.c.get('/index', follow_redirects=True)
        self.assertEqual(resp.status_code, 200)
        soup = BS(resp.data.decode('utf-8'), "html.parser")
        self.assertEqual(
            NAV_LINKS,
            json.loads(soup.find(id='navbar')['data-nav-links']))
        resp = self.c.get('/faq')
        self.assertEqual(resp.status_code, 200)
        soup = BS(resp.data.decode('utf-8'), "html.parser")
        self.assertEqual(
            NAV_LINKS,
            json.loads(soup.find(id='navbar')['data-nav-links']))
        resp = self.c.get('/profile')
        self.assertEqual(resp.status_code, 200)
        soup = BS(resp.data.decode('utf-8'), "html.parser")
        self.assertEqual(
            NAV_LINKS,
            json.loads(soup.find(id='navbar')['data-nav-links']))
        self.assertEqual(self.c.get('/logout').status_code, 302)


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(HomeTests))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
