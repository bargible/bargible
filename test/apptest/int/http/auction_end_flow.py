"""
test the auction ending server-side functionality to support
modals an auction winner might use to pay for the auction's reward
"""
import os
import sys
import json
import unittest
from copy import deepcopy
from collections import namedtuple
from time import sleep

import gentestmodels

from bargible.model import Auction # use only in getAuctionData
from bargible.periodic_auc_updater.updater import update as updateAllAuctions

from .base import BaseHttpTestCase
from .base import flaskUserLogin


with open(os.path.join(os.path.dirname(__file__),
                       'auction_end_flow_00.json'), 'r') as f:
    DATA_AUCTIONS_FIXTURE = json.load(f)

DATA_TEST_SHIPPING_ADDRESS = {
    'name': 'stub name',
    'addr_1': 'stub addr_1',
    'addr_2': 'stub addr_2',
    'city': 'stub city',
    'usa_state': 'AL',
    'zip_code': '12345',
}

SECS_TIL_AUCTION_START = 0
SECS_AUCTION_DURATION = 5

class AuctionEndFlowTests(BaseHttpTestCase):

    def _joinAuctions(self):
        respJson, respStatus = self.createJson(
        ('/api/user/auctions/{aid}/actions/join'
         .format(aid=self.auctionIdGiftcard)))
        self.assertEqual(respStatus, 200)
        respJson, respStatus = self.createJson(
        ('/api/user/auctions/{aid}/actions/join'
         .format(aid=self.auctionIdPhysicalItem)))
        self.assertEqual(respStatus, 200)

    def getAuctionData(self):
        # this breaks some conceptual boundaries:
        # the http int tests are really are intended to test
        # external to http layer.  this information is/will be also
        # available via the admin UI.
        return dict(zip([
            'giftcard',
            'physical_item',
        ], [{
            'hasShippingAddress': (
                auction.shipping_address is not None),
            'hasRedemption': (
                auction.redemption is not None),
        } for auction in [
            Auction.byId(auctionId)
            for auctionId in [
                    self.auctionIdGiftcard,
                    self.auctionIdPhysicalItem,
            ]]
        ]))

    def _createUsersAndSetIds(self):
        user, userTwo = gentestmodels.createUsers(0)[:2]
        self.userId = str(user.id)
        self.userTwoId = str(userTwo.id)

    @unittest.mock.patch('bargible.client_auc_events.auctionUpdateEvent')
    def setUp(self, mockUpdateEvent):
        super().setUp()
        gentestmodels.createItems(0)
        gentestmodels.createPhysicalItems(0)
        for auction in DATA_AUCTIONS_FIXTURE:
            auction['start']['inSecs'] = SECS_TIL_AUCTION_START
            auction['end']['inSecs'] = (SECS_TIL_AUCTION_START +
                                        SECS_AUCTION_DURATION)
        giftcardAuction, physicalItemAuction = (
            gentestmodels .createAuctionsFromFixture(
                DATA_AUCTIONS_FIXTURE))
        self.auctionIdGiftcard = giftcardAuction.id
        self.auctionIdPhysicalItem = physicalItemAuction.id
        with self.app.app_context():
            self._createUsersAndSetIds()
        updateAllAuctions()
        with flaskUserLogin(self.c, self.userId):
            self._joinAuctions()
        with flaskUserLogin(self.c, self.userTwoId):
            self._joinAuctions()
        updateAllAuctions()
        sleep(1)
        with flaskUserLogin(self.c, self.userId):
            respJson, respStatus = self.updateJson(
                ('/api/user/auctions/{aid}/actions/bid'
                 .format(aid=self.auctionIdGiftcard)))
        with flaskUserLogin(self.c, self.userId):
            respJson, respStatus = self.updateJson(
                ('/api/user/auctions/{aid}/actions/bid'
                 .format(aid=self.auctionIdPhysicalItem)))
        sleep(SECS_AUCTION_DURATION)
        updateAllAuctions()

    @unittest.mock.patch(('bargible.third_party_api.braintree.util.'
                              'makeSale'))
    def testPayGiftcard(self, mockMakeSale):
        mockMakeSale.return_value = namedtuple(
            'MockTransactionResult', [
                'isSuccess',
            ])(True)
        with flaskUserLogin(self.c, self.userTwoId):
            respJson, respStatus = self.createJson(
                '/api/user/auctionPayments',
                jsonData={
                    'auctionId': self.auctionIdGiftcard,
                    'nonce': 'asdf'})
        self.assertEqual(respStatus, 400,
                         msg="only win user may pay for auction item")
        self.assertFalse(self.getAuctionData(
        )['giftcard']['hasRedemption'],
                         msg=("found reward record before "
                              "auction payment"))
        with flaskUserLogin(self.c, self.userId):
            respJson, respStatus = self.createJson(
                '/api/user/auctionPayments',
                jsonData={'auctionId': self.auctionIdGiftcard,
                          'nonce': 'asdf'})
        self.assertEqual(respStatus, 200,
                         msg="win user payment failed: " +
                         repr(respJson))
        self.assertTrue(self.getAuctionData(
        )['giftcard']['hasRedemption'],
                        msg=("didn't find reward record after "
                             "auction payment"))
        with flaskUserLogin(self.c, self.userId):
            respJson, respStatus = self.createJson(
                '/api/user/auctionPayments',
                jsonData={'auctionId': self.auctionIdGiftcard,
                          'nonce': 'asdf'})
        self.assertEqual(respStatus, 400,
                         msg="auction item may be paid for only once")

    @unittest.mock.patch(('bargible.third_party_api.braintree.util'
                              '.makeSale'))
    def testFlowPhysicalItem(self, mockMakeSale):
        mockMakeSale.return_value = namedtuple(
            'MockTransactionResult', [
                'isSuccess',
            ])(True)
        self.assertFalse(self.getAuctionData(
        )['physical_item']['hasShippingAddress'], msg=(
            "found shipping address record before create POST"))
        with flaskUserLogin(self.c, self.userId):
            respJson, respStatus = self.createJson(
                '/api/user/auctionAddress',
                jsonData={
                    'auctionId': self.auctionIdPhysicalItem,
                    'shippingAddress': DATA_TEST_SHIPPING_ADDRESS,
                })
        self.assertEqual(respStatus, 200,
                         msg="win user shipping address failed: " +
                         repr(respJson))
        self.assertTrue(self.getAuctionData(
        )['physical_item']['hasShippingAddress'], msg=(
            "didn't find shipping address record after create POST"))
        self.assertFalse(self.getAuctionData(
        )['physical_item']['hasRedemption'], msg=(
            "found reward record before payment"))
        with flaskUserLogin(self.c, self.userId):
            respJson, respStatus = self.createJson(
                '/api/user/auctionPayments',
                jsonData={'auctionId': self.auctionIdPhysicalItem,
                          'nonce': 'asdf'})
        self.assertTrue(self.getAuctionData(
        )['physical_item']['hasRedemption'], msg=(
            "didn't find reward record after payment"))
        self.assertEqual(respStatus, 200,
                         msg="win user payment failed: " +
                         repr(respJson))

    def testDupeShippingAddress(self):
        self.assertFalse(self.getAuctionData(
        )['physical_item']['hasShippingAddress'], msg=(
            "found shipping address record before create POST"))
        with flaskUserLogin(self.c, self.userId):
            respJson, respStatus = self.createJson(
                '/api/user/auctionAddress',
                jsonData={
                    'auctionId': self.auctionIdPhysicalItem,
                    'shippingAddress': DATA_TEST_SHIPPING_ADDRESS,
                })
        self.assertEqual(respStatus, 200,
                         msg=("win user shipping address failed: " +
                              repr(respJson)))
        self.assertTrue(self.getAuctionData(
        )['physical_item']['hasShippingAddress'], msg=(
            "didn't find shipping address record after create POST"))
        with flaskUserLogin(self.c, self.userId):
            respJson, respStatus = self.createJson(
                '/api/user/auctionAddress',
                jsonData={
                    'auctionId': self.auctionIdPhysicalItem,
                    'shippingAddress': DATA_TEST_SHIPPING_ADDRESS,
                })
        self.assertEqual(respStatus, 400,
                         msg=("didn't get a client error on second " +
                              "shipping address creation :" +
                              repr(respJson)))
        self.assertIn('code', respJson)
        self.assertEqual(self.ERR_CODE_DB, respJson['code'])

    def testGiftcardShippingAddress(self):
        """shipping addresses for physical item auctions only"""
        with flaskUserLogin(self.c, self.userId):
            respJson, respStatus = self.createJson(
                '/api/user/auctionAddress',
                jsonData={
                    'auctionId': self.auctionIdGiftcard,
                    'shippingAddress': DATA_TEST_SHIPPING_ADDRESS,
                })
        self.assertEqual(respStatus, 400,
                         msg="no client error code on shipping  " +
                         "for giftcard auction " +
                         repr(respJson))
        self.assertIn('code', respJson)
        self.assertEqual(self.ERR_CODE_DB, respJson['code'])

    def testMissingState(self):
        """no (USA) state data in shipping address"""
        dataAddr = deepcopy(DATA_TEST_SHIPPING_ADDRESS)
        dataAddr.pop('usa_state')
        with flaskUserLogin(self.c, self.userId):
            respJson, respStatus = self.createJson(
                '/api/user/auctionAddress',
                jsonData={
                    'auctionId': self.auctionIdGiftcard,
                    'shippingAddress': dataAddr
                })
        self.assertEqual(respStatus, 200,
                         msg="no client error code on shipping  " +
                         "for giftcard auction " +
                         repr(respJson))
        self.assertIn('code', respJson)
        self.assertEqual(self.ERR_CODE_SCHEMA, respJson['code'])
        self.assertIn('data', respJson)
        self.assertIn('usa_state', respJson['data'])

    def testInvalidState(self):
        """invalid (USA) state data in shipping address"""
        dataAddr = deepcopy(DATA_TEST_SHIPPING_ADDRESS)
        dataAddr['usa_state'] = 'ZZ'
        with flaskUserLogin(self.c, self.userId):
            respJson, respStatus = self.createJson(
                '/api/user/auctionAddress',
                jsonData={
                    'auctionId': self.auctionIdGiftcard,
                    'shippingAddress': dataAddr
                })
        self.assertEqual(respStatus, 200,
                         msg="no client error code on shipping  " +
                         "for giftcard auction " +
                         repr(respJson))
        self.assertIn('code', respJson)
        self.assertEqual(self.ERR_CODE_SCHEMA, respJson['code'])
        self.assertIn('data', respJson)
        self.assertIn('usa_state', respJson['data'])


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(
            AuctionEndFlowTests
        ))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
