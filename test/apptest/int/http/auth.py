""" test http api as consumed by user profile page """
import sys
import unittest
import json
from copy import deepcopy

from .base import BaseHttpTestCase

FORM_FIELDS_REGISTER = {
    'username': 'bob',
    'email': 'b@b.org',
    'firstName': 'bob',
    'lastName': 'belcher',
    'password': 'Temp321!',
    'passwordRepeat': 'Temp321!',
}

class AuthTests(BaseHttpTestCase):

    def _isLoggedIn(self):
        indexResp = self.c.get('/')
        self.assertIn(indexResp.status_code, [200, 302])
        return indexResp.status_code == 302

    def testFormRegister(self):
        self.register()
        self.assertTrue(self._isLoggedIn())

    def testApiRegisterBadPass(self):
        """ test register json api w/ invalid password """
        formFields = deepcopy(FORM_FIELDS_REGISTER)
        formFields['password'] = 'temp321'
        formFields['passwordRepeat'] = formFields['password']
        resp = self.c.post(
            '/api/register',
            data=json.dumps({
                'formFields': formFields,
            }),
            content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        respJson = json.loads(resp.data.decode('utf-8'))
        respJson = json.loads(resp.data.decode('utf-8'))
        self.assertIs(type(respJson), dict)
        self.assertIn('data', respJson.keys())
        self.assertIn('password', respJson['data'].keys())
        self.assertIs(type(respJson['data']['password']), list)
        self.assertIs(type(respJson['data']['password'][0]), str)

    def testApiRegisterPassMismatch(self):
        """ test register json api with password retype not matching """
        formFields = deepcopy(FORM_FIELDS_REGISTER)
        formFields['passwordRepeat'] += 'z'
        resp = self.c.post(
            '/api/register',
            data=json.dumps({
                'formFields': formFields,
            }),
            content_type='application/json')
        self.assertEqual(resp.status_code, 400)
        respJson = json.loads(resp.data.decode('utf-8'))
        respJson = json.loads(resp.data.decode('utf-8'))
        self.assertIs(type(respJson), dict)
        self.assertIn('data', respJson.keys())
        self.assertIn('passwordRepeat', respJson['data'].keys())
        self.assertIs(
            type(respJson['data']['passwordRepeat']),
            list)
        self.assertIs(
            type(respJson['data']['passwordRepeat'][0]),
            str)

    def testApiRegister(self):
        indexResp = self.c.get('/')
        self.assertEqual(indexResp.status_code, 200)
        resp = self.c.post(
            '/api/register',
            data=json.dumps(FORM_FIELDS_REGISTER),
            content_type='application/json')
        self.assertEqual(resp.status_code, 200)
        respJson = json.loads(resp.data.decode('utf-8'))
        self.assertIs(type(respJson), dict)
        self.assertIn('next', respJson.keys())
        self.assertEqual(respJson['next'], '/home')
        self.assertTrue(self._isLoggedIn())

    def testLogout(self):
        self.testApiRegister()
        self.assertTrue(self._isLoggedIn())
        logoutResp = self.c.get('/logout')
        self.assertEqual(logoutResp.status_code, 302)
        self.assertFalse(self._isLoggedIn())

    def testApiLoginUsername(self):
        self.testLogout()
        loginResp = self.c.post(
            '/api/login',
            data=json.dumps({
                'usernameOrEmail': FORM_FIELDS_REGISTER['username'],
                'password': FORM_FIELDS_REGISTER['password'],
            }),
            content_type='application/json')
        self.assertEqual(loginResp.status_code, 200)
        self.assertTrue(self._isLoggedIn())

    def testApiLoginEmail(self):
        self.testLogout()
        loginResp = self.c.post(
            '/api/login',
            data=json.dumps({
                'usernameOrEmail': FORM_FIELDS_REGISTER['email'],
                'password': FORM_FIELDS_REGISTER['password'],
            }),
            content_type='application/json')
        self.assertEqual(loginResp.status_code, 200)
        self.assertTrue(self._isLoggedIn())

    def testApiLoginEmailBadPass(self):
        self.testLogout()
        loginResp = self.c.post(
            '/api/login',
            data=json.dumps({
                'usernameOrEmail': FORM_FIELDS_REGISTER['email'],
                'password': 'asdf',
            }),
            content_type='application/json')
        self.assertEqual(loginResp.status_code, 400)
        self.assertFalse(self._isLoggedIn())


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(AuthTests))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
