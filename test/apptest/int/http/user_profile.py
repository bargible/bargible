""" test http api as consumed by user profile page """
import os
import sys
import unittest
import json

from sqlalchemy_imageattach.context import store_context

from bargible.model import imageStore

# see comment on setUp method regarding model layer imports
from bargible.model import saveModels
from bargible.model import User

from .base import BaseHttpTestCase

PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..', '..', '..', '..')
)
SAMPLE_AVATAR_IMG = os.path.join(
    PROJ_ROOT, 'test_data', 'images', 'avatar_00.jpg')

class UserProfileTests(BaseHttpTestCase):

    def setUp(self):
        """
        sets up user data directly through model layer.
        while this nominally breaks the conceptual boundary
        regarding the portions of the code are under test,
        it also allows these user profile tests to run
        independently of `app.core`.

        at the time of writing, practical considerations suggest
        the latter consideration is of more practical value than
        purely conceptual separation,
        although these considerations may change as `app.core` develops
        """
        super().setUp()
        self.register()

    def testUploadAvatar(self):
        user = User.byId(1)
        # this is the form currently used to check for existance of
        # a user avatar in the application the code.
        # it makes use of the SQLAlchemy Dynamic collection API,
        # `sqlalchemy.orm.dynamic`
        self.assertEqual(user.avatar.count(), 0)
        # this is an additional check that there is no avatar
        with self.assertRaises(OSError):
            user.avatar.locate()
        with open(SAMPLE_AVATAR_IMG, 'rb') as avatarFile:
            avatarData = avatarFile.read()
        respJson, respStatus = self.updateFile(
            '/api/user/avatar',
            avatarData,
            'image/*')
        self.assertEqual(respStatus, 200)
        user = User.byId(1)
        self.assertEqual(user.avatar.count(), 1)
        with store_context(imageStore):
            self.assertIs(type(user.avatar.locate()), str)


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(UserProfileTests))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
