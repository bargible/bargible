""" http api tests """
import unittest

from . import user_profile as userProfileTests
from . import home as homeTests
from . import dupe_func_home as dupeFuncHomeTests
from . import auction_end as auctionEndTests
from . import auction_end_flow as auctionEndFlowTests
from . import auth as authTests
from . import watch as watchTests

def load_tests(loader, standard_tests, pattern):
    suite = unittest.TestSuite()
    for testModule in [
            userProfileTests,
            homeTests,
            dupeFuncHomeTests,
            auctionEndTests,
            auctionEndFlowTests,
            authTests,
            watchTests,
    ]:
        tests = (unittest.defaultTestLoader
                 .loadTestsFromModule(testModule, pattern=pattern))
        suite.addTests(tests)
    return suite

