"""
test auction endings
up to the point where a user would see the auction end modal
but not including server-side functionality to support
modals an auction winner might use to pay for the auction's reward
"""
import os
import sys
import unittest
from time import sleep
from datetime import datetime

import gentestmodels

from bargible.periodic_auc_updater.updater import update as updateAllAuctions
from bargible.model import saveModels # used in setup only

from .base import BaseHttpTestCase
from .base import flaskUserLogin

# use to parse strings formatted according to ISO 8601 standard
# to python datetime objects using datetime.strptime
ISO_8601_FORMAT = '%Y-%m-%dT%H:%M:%S.%f'

# match value in test fixture and be enough to account for
# db + runtime latency
SECS_TIL_AUCTION_END = 5

PATH_TEST_FIXTURE = os.path.join(os.path.dirname(__file__),
                                 'auction_end_00.json')

# matches fixture value
DATA_AUCTION_ENDED_JSON = {
    # removed keys: image, start, end, realPrice
    'id': 1,
    'reward': {
        'type': 'item',
        'data': {
            'itemId': 1,
            'name': 'Barnes & Noble eGift Card',
            'description': 'Lower Prices on Millions of Books, Movies and TV Show DVDs and Blu-ray, Music, Toys, and Games. Shop online for eBooks, NOOK, and textbooks.',
            'disclaimer': 'Barnes & Noble is not a sponsor or co-sponsor of this promotion. The logos and other identifying marks attached are trademarks of and owned by each represented company and/or its affiliates.  Please visitwww.bn.com for terms and conditions of use. Barnes & Noble is not liable for any alleged or actual claims related to this offer.',
        }
    },
    'lastBidUsername': None,
    'position': None,
    'minPrice': 1000,
}

class AuctionEndTests(BaseHttpTestCase):

    def _createUsersAndSetIds(self):
        userList = gentestmodels.createUsers(0)
        self.userOneId = str(userList[0].id)
        self.userTwoId = str(userList[1].id)

    @unittest.mock.patch('bargible.client_auc_events.auctionUpdateEvent')
    def setUp(self, mockUpdateEvent):
        super().setUp()
        gentestmodels.createItems(0)
        gentestmodels.createAuctionsFromFixture(PATH_TEST_FIXTURE)
        self.auctionId = 1
        with self.app.app_context():
            self._createUsersAndSetIds()
        updateAllAuctions()
        with flaskUserLogin(self.c, self.userOneId):
            respJson, respStatus = self.createJson(
            ('/api/user/auctions/{aid}/actions/join'
             .format(aid=self.auctionId)))
        self.assertEqual(respStatus, 200)
        updateAllAuctions()

    def testFetchNotEnded(self):
        with flaskUserLogin(self.c, self.userOneId):
            respJson, respStatus = self.fetchJson(
                '/api/user/auctions/{id}?status=ended'
                .format(id=self.auctionId))
        self.assertEqual(respStatus, 400)
        self.assertEqual(set(respJson.keys()), set(['msg']))

    def testFetchEndedAuction(self):
        sleep(SECS_TIL_AUCTION_END)
        updateRes = updateAllAuctions()
        self.assertEqual(len(updateRes), 1)
        endUpdate = updateRes[0]
        self.assertEqual(endUpdate['updateType'], 'end')
        self.assertEqual(endUpdate['auctionId'], self.auctionId)
        endUpdateUsers = endUpdate['updateUsers']
        self.assertEqual(len(endUpdateUsers), 1)
        endUpdateUser = endUpdateUsers[0]
        self.assertEqual(str(endUpdateUser.id), self.userOneId)
        with flaskUserLogin(self.c, self.userOneId):
            respJson, respStatus = self.fetchJson(
                '/api/user/auctions/{id}?status=ended'
                .format(id=self.auctionId))
        self.assertEqual(respStatus, 200)
        endedJson = respJson['ended']
        varDataEnded = {
            'start': datetime.strptime(endedJson.pop('start'),
                                       ISO_8601_FORMAT),
            'end': datetime.strptime(endedJson.pop('end'),
                                     ISO_8601_FORMAT),
            'realPrice': endedJson.pop('realPrice'),
            'rewardImage': endedJson['reward']['data'].pop('image'),
            'bidPrice': endedJson.pop('bidPrice'),
        }
        self.assertIs(type(varDataEnded['realPrice']), int)
        self.assertIs(type(varDataEnded['bidPrice']), int)
        self.assertTrue(varDataEnded['start'] < varDataEnded['end'])
        self.assertIs(type(varDataEnded['rewardImage']), str)
        self.assertEqual(endedJson, DATA_AUCTION_ENDED_JSON)

    def testFetchEndedNonePosition(self):
        """
        Test user's position is None if they've not bid.
        """
        with flaskUserLogin(self.c, self.userTwoId):
            respJson, respStatus = self.createJson(
            ('/api/user/auctions/{aid}/actions/join'
             .format(aid=self.auctionId)))
        with flaskUserLogin(self.c, self.userOneId):
            respJson, respStatus = self.updateJson(
                ('/api/user/auctions/{aid}/actions/bid'
                 .format(aid=self.auctionId)))
        sleep(SECS_TIL_AUCTION_END)
        updateAllAuctions()
        with flaskUserLogin(self.c, self.userOneId):
            respJson, respStatus = self.fetchJson(
                '/api/user/auctions/{id}?status=ended'
                .format(id=self.auctionId))
        self.assertEqual(respStatus, 200)
        endedAuctionJson = respJson['ended']
        self.assertEqual(endedAuctionJson.get('position', -1), 1)
        with flaskUserLogin(self.c, self.userTwoId):
            respJson, respStatus = self.fetchJson(
                '/api/user/auctions/{id}?status=ended'
                .format(id=self.auctionId))
        self.assertEqual(respStatus, 200)
        endedAuctionJson = respJson['ended']
        self.assertEqual(endedAuctionJson.get('position', -1), None)

    def testFetchEndedAuctions(self):
        sleep(SECS_TIL_AUCTION_END)
        updateRes = updateAllAuctions()
        self.assertEqual(len(updateRes), 1)
        endUpdate = updateRes[0]
        self.assertEqual(endUpdate['updateType'], 'end')
        self.assertEqual(endUpdate['auctionId'], self.auctionId)
        endUpdateUsers = endUpdate['updateUsers']
        self.assertEqual(len(endUpdateUsers), 1)
        endUpdateUser = endUpdateUsers[0]
        self.assertEqual(str(endUpdateUser.id), self.userOneId)
        with flaskUserLogin(self.c, self.userOneId):
            respJson, respStatus = self.fetchJson(
                '/api/user/auctions?status=ended')
        self.assertEqual(respStatus, 200)
        self.assertIs(type(respJson), list)
        self.assertEqual(len(respJson), 1)
        endedJson = respJson[0]['ended']
        varDataEnded = {
            'start': datetime.strptime(endedJson.pop('start'),
                                       ISO_8601_FORMAT),
            'end': datetime.strptime(endedJson.pop('end'),
                                     ISO_8601_FORMAT),
            'realPrice': endedJson.pop('realPrice'),
            'rewardImage': endedJson['reward']['data'].pop('image'),
            'bidPrice': endedJson.pop('bidPrice'),
        }
        self.assertIs(type(varDataEnded['realPrice']), int)
        self.assertIs(type(varDataEnded['bidPrice']), int)
        self.assertTrue(varDataEnded['start'] < varDataEnded['end'])
        self.assertIs(type(varDataEnded['rewardImage']), str)
        self.assertEqual(endedJson, DATA_AUCTION_ENDED_JSON)


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(AuctionEndTests))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
