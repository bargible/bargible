""" base functionality for http api integration tests """
import json
import unittest
from contextlib import contextmanager

from sqlalchemy import Table

from bargible.model import User

from app import initFlaskApp
from app import model
import gentestmodels

def _getRespJson(resp):
    """ get json data from Flask test client response """
    try:
        return json.loads(resp.data.decode('utf-8'))
    except json.decoder.JSONDecodeError:
        raise ValueError("expected json data from endpoint", resp.data)


@contextmanager
def flaskUserLogin(flaskClient, userId):
    """
    `with` statement context
    to provide Flask test client with Flask-User login

    rather than using two Flask clients, this context manager
    allows succinct setup & teardown on a single client such that
    it may be used as if there were two Flask clients, each with
    a separate login session.

    note that the current implementation does not support nesting:
    creating a user login context within another login context
    will likely fail, possibly w/o clear error-reporting
    """
    with flaskClient.session_transaction() as sess:
        # these keys are specific to Flask-Login, a dep for Flask-User,
        # and the meaning of 'user_id' is by Flask-User
        sess['user_id'] = userId
        sess['_fresh'] = True
    yield flaskClient
    with flaskClient.session_transaction() as sess:
        sess.pop('user_id')
        sess.pop('_fresh')


def resetDb():
    model.shutdownSession()
    model.dropAllTables()
    model.initDb()


class BaseHttpTestCase(unittest.TestCase):
    """ base class for http api tests """

    ERR_CODE_DB = 97
    ERR_CODE_SCHEMA = 98
    ERR_CODE_SALE = 99

    def setUp(self):
        resetDb()
        self.app = initFlaskApp()
        self.app.config.from_object('config.TestingConfig')
        self.c = self.app.test_client()
        self._httpUsername = 'bburgers'
        gentestmodels.createConfig()

    def tearDown(self):
        pass

    def register(self):
        resp = self.c.post(
            '/api/register',
            data=json.dumps({
                'username': self.httpUsername,
                'email': 'b@b.org',
                'firstName': 'bob',
                'lastName': 'belcher',
                'password': 'Temp321!',
                'passwordRepeat': 'Temp321!',
            }),
            content_type='application/json',
            follow_redirects=True)
        self.assertEqual(resp.status_code, 200)

    @property
    def httpUsername(self):
        return self._httpUsername

    ### *Json methods to abstract over the same interface
    #     as the javascript client http util api

    def fetchJson(self, strUri, c=None):
        if c is None:
            c = self.c
        resp = c.get(strUri)
        respJson = _getRespJson(resp)
        respStatusCode = resp.status_code
        return respJson, respStatusCode

    def createJson(self, strUri, jsonData={}, c=None):
        if c is None:
            c = self.c
        resp = c.post(strUri,
                      data=json.dumps(jsonData),
                      content_type='application/json')
        respJson = _getRespJson(resp)
        respStatusCode = resp.status_code
        return respJson, respStatusCode

    def updateJson(self, strUri, jsonData={}, c=None):
        if c is None:
            c = self.c
        resp = c.put(strUri,
                     data=json.dumps(jsonData),
                     content_type='application/json')
        respJson = _getRespJson(resp)
        respStatusCode = resp.status_code
        return respJson, respStatusCode

    def updateFile(self, strUri, binaryData, mimeType, c=None):
        if c is None:
            c = self.c
        resp = c.put(strUri,
                     data=binaryData,
                     content_type=mimeType)
        respJson = _getRespJson(resp)
        respStatusCode = resp.status_code
        return respJson, respStatusCode
