import unittest

from . import item as itemTests
from . import shipping_address as shippingAddressTests
from . import redemption as redemptionTests
from . import user as userTests
from . import credit_package as creditPackageTests
from . import auction as auctionTests

def load_tests(loader, standard_tests, pattern):
    suite = unittest.TestSuite()
    for testModule in [
            itemTests,
            shippingAddressTests,
            redemptionTests,
            userTests,
            creditPackageTests,
            auctionTests,
    ]:
        try:
            tests = (unittest.defaultTestLoader
                     .loadTestsFromModule(testModule, pattern=pattern))
        except AttributeError as err:
            from pdb import set_trace as st; st()
            pass
        suite.addTests(tests)
    return suite
