""" base functionality for model layer tests """
import unittest

from sqlalchemy import Table

from app import initFlaskApp
from bargible import model

from bargible.model import Config
from bargible.model import User
from bargible.model import saveModels

INIT_CREDIT = 1000

def resetDb():
    model.shutdownSession()
    model.dropAllTables()
    model.initDb()


class BaseModelTestCase(unittest.TestCase):
    """ base class for model layer tests """

    def setUp(self):
        resetDb()
        app = initFlaskApp()
        app.config.from_object('config.TestingConfig')
        # todo: the following -- based on app factory pattern
        # ensure the model layer can determine image urls.
        # application factories and the host_url_getter parameter
        # are probably a cleaner way to do this than using http
        # functionality within model-layer tests
        self.c = app.test_client()
        self.c.get('/')

    def tearDown(self):
        pass

    def createUser(self, userFields):
        """ functionality from flask-user register endpoint """
        userFields['credit'] = INIT_CREDIT
        user = User(**userFields)
        saveModels(user)
        return user
