import os
import json
import sys
import unittest

import gentestmodels

from bargible.model import saveModels
from bargible.model import ShippingAddress

from .base import BaseModelTestCase

DATA_SHIPPING_ADDRESS = {
    'name': 'stub name',
    'addr_1': 'stub addr_1',
    'addr_2': 'stub addr_2',
    'city': 'stub city',
    'usa_state': 'AL',
    'zip_code': '12345'
}


class ShippingAddressTests(BaseModelTestCase):

    def _createItemAuction(self):
        gentestmodels.createItems(0)
        return gentestmodels.createAuctions(0)[0]

    def _createPItemAuction(self):
        gentestmodels.createPhysicalItems(0)
        return gentestmodels.createAuctions(3)[0]

    def testCreate(self):
        auction = self._createPItemAuction()
        shippingAddress = ShippingAddress(
            auction=auction,
            **DATA_SHIPPING_ADDRESS)
        saveModels(shippingAddress)

    def testCreateFailItem(self):
        auction = self._createItemAuction()
        with self.assertRaises(ValueError, msg=(
                "shipping address may not be supplied "
                "for giftcard auctions")):
            shippingAddress = ShippingAddress(
                auction=auction,
                **DATA_SHIPPING_ADDRESS)
            saveModels(shippingAddress)


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(ShippingAddressTests))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
