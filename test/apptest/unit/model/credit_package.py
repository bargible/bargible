import sys
import unittest

from sqlalchemy.orm.exc import NoResultFound

from bargible.model import saveModels
from bargible.model import CreditPackage

from .base import BaseModelTestCase

import gentestmodels

NON_EXISTENT_ID = 123

class CreditPackageTests(BaseModelTestCase):

    def testCreate(self):
        gentestmodels.createCreditPackages(0)

    def testRead(self):
        self.assertIsNone(CreditPackage.byId(NON_EXISTENT_ID))
        self.testCreate()
        creditPackages = CreditPackage.all()
        self.assertNotEqual(len(creditPackages), 0)
        return creditPackages

    def testToJsonObj(self):
        creditPackages = self.testRead()
        for creditPackage in creditPackages:
            creditPackageJson = creditPackage.toJsonObj()
            self.assertIs(type(creditPackageJson), dict)
            self.assertIn('numCredits', creditPackageJson.keys())
            self.assertIn('price', creditPackageJson.keys())
            self.assertIs(type(creditPackageJson['numCredits']), int)
            self.assertIs(type(creditPackageJson['price']), int)
            self.assertTrue(
                creditPackageJson['numCredits'] > 0 and
                creditPackageJson['price'] > 0,
                msg="positive values for number of credits and price")

def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(CreditPackageTests))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
