import os
import json
import sys
import unittest
import datetime

from sqlalchemy_imageattach.context import store_context

from bargible.model import saveModels
from bargible.model import Item
from bargible.model import imageStore

from .base import BaseModelTestCase

ITEM_IMG_PATH = os.path.join(
    os.path.dirname(__file__),
    'gap.png',
)

ITEM_FIELDS = {
    'name': "Barnes & Noble eGift Card",
    'minimum_price': 5,
    'description': "Millions of Books, Movies and TV",
    'disclaimer': "Barnes & Noble is not a sponsor or co-sponsor",
    'sku': "BRNS1-E-V-STD",
}

INVALID_SKUS = [
    'a-E-V-STD',
    'BRNS1-A-B-CED',
    'BRNS1-P-V-STD',
    'BRNS1-1-E-V-STD',
]

class ItemTests(BaseModelTestCase):

    def testCreate(self):
        item = Item(**ITEM_FIELDS)
        with store_context(imageStore):
            with open(ITEM_IMG_PATH, mode='rb') as f:
                item.picture.from_file(f)
        saveModels(item)
        return item

    def testById(self):
        item = self.testCreate()
        foundItem = Item.byId(item.id)
        self.assertIs(
            type(foundItem), Item, msg="found item instance by id")
        self.assertEqual(
            foundItem.id, item.id,
            msg="found correct id on item object")
        self.assertEqual(
            foundItem.sku, item.sku,
            msg="found correct sku on item object")

    def testBySku(self):
        item = self.testCreate()
        foundItem = Item.bySku(item.sku)
        self.assertIs(
            type(foundItem), Item, msg="found item instance by sku")
        self.assertEqual(
            foundItem.id, item.id,
            msg="found correct id on item object")
        self.assertEqual(
            foundItem.sku, item.sku,
            msg="found correct sku on item object")

    def testValidateSku(self):
        for invalidSku in INVALID_SKUS:
            with self.assertRaises(ValueError, msg="on create"):
                item = Item(**{**ITEM_FIELDS, **{'sku': invalidSku}})
        item = self.testCreate()
        for invalidSku in INVALID_SKUS:
            with self.assertRaises(ValueError, msg="on update"):
                item.sku = invalidSku

    def testToJsonObj(self):
        item = self.testCreate()
        jsonObj = item.toJsonObj()
        self.assertEqual(jsonObj['type'], 'item')
        self.assertEqual(jsonObj['data']['name'], ITEM_FIELDS['name'])


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(ItemTests))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
