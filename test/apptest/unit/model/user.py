import os
import json
import sys
import unittest
import datetime

from bargible.model import saveModels
from bargible.model import User

from .base import BaseModelTestCase

USER_FIELDS = {
    'last_name': 'Belcher',
    'first_name': 'Bob',
    'email': 'b@b.org',
    'password': 'Temp321!',
    'username': 'bob'
}

class UserTests(BaseModelTestCase):

    def testCreate(self):
        user = self.createUser(USER_FIELDS)
        self.assertIsNotNone(
            user,
            msg="createUser returns not None")
        self.assertIs(
            type(user), User,
            msg="createUser returns User type")
        return user

    def testFindByUsername(self):
        self.testCreate()
        user = User.findByUsername(USER_FIELDS['username'])
        self.assertIsNotNone(user, msg="find by username")
        self.assertEqual(user.username, USER_FIELDS['username'],
                         msg="correct username")

    def testById(self):
        user = self.testCreate()
        foundUser = User.byId(user.id)
        self.assertIs(
            type(foundUser), User, msg="found user instance by id")
        self.assertEqual(
            foundUser.id, user.id,
            msg="found correct id on auction object")

    def testToJsonObj(self):
        user = self.testCreate()
        userJson = user.toJsonObj()
        self.assertEqual(userJson.pop('id'), user.id)
        varData = { key: userJson.pop(key) for key in [
            'credit',
            'avatar',
        ]}
        for key in userJson.keys():
            self.assertEqual(userJson[key], USER_FIELDS[key])

    def testGetId(self):
        """
        check behavior of Flask-User's use of Flask-Login's `getId`
        https://flask-login.readthedocs.io/en/latest/#your-user-class
        this is the function used to store a user id in a Flask session
        """
        user = self.testCreate()
        self.assertEqual(str(user.id), user.get_id())


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(UserTests))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
