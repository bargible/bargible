import os
import json
import sys
import urllib
import unittest
from copy import deepcopy
from datetime import datetime
from datetime import timedelta
from time import sleep

from sqlalchemy.orm.exc import NoResultFound

from sqlalchemy_imageattach.context import store_context

from bargible.model import imageStore
from bargible.model import saveModels
from bargible.model import Item
from bargible.model import PhysicalItem
from bargible.model import Reward
from bargible.model import Auction

from .base import BaseModelTestCase

PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..', '..', '..', '..')
)

PATH_DIR_ITEM_IMGS = os.path.join(
    PROJ_ROOT, 'test_data', 'images', 'item')

DATA_TEST_ITEMS = [
    {
        'name': 'Barnes & Noble eGift Card',
        'minimum_price': 10,
        'description': 'This is an amazing test Barnes & Noble card',
        'disclaimer': 'disclaimer',
        'image_filename': 'barnes-noble.png',
        'sku': 'BRNS1-E-V-STD',
    },
    {
        'name': 'Test Best Buy Gift Card',
        'minimum_price': 10,
        'description': 'This is an amazing test Best Buy card',
        'disclaimer': 'disclaimer',
        'image_filename': 'best-buy.png',
        'sku': 'BSTB1-E-V-STD',
    },
    {
        'name': 'Test Dominos Gift Card',
        'minimum_price': 10,
        'description': 'This is an amazing test Dominos card',
        'disclaimer': 'disclaimer',
        'image_filename': 'dominos.jpg',
        'sku': 'DOMINOS1-E-V-STD',
    },
    {
        'name': 'Test Gamestop Gift Card',
        'minimum_price': 10,
        'description': 'This is an amazing test Gamestop card',
        'disclaimer': 'disclaimer',
        'image_filename': 'gamestop.png',
        'sku': 'GAMESTOP-1-E-V-STD',
    },
]

USERS_FIELDS = [
    {
        'last_name': 'Belcher',
        'first_name': 'Linda',
        'email': 'l@b.org',
        'password': 'Temp321!',
        'username': 'linda',
    },
    {
        'last_name': 'Belcher',
        'first_name': 'Tina',
        'email': 'b@b.org',
        'password': 'Temp321!',
        'username': 'tina',
    },
    {
        'last_name': 'Belcher',
        'first_name': 'Gene',
        'email': 'b@b.org',
        'password': 'Temp321!',
        'username': 'gene',
    },
    {
        'last_name': 'Belcher',
        'first_name': 'Louise',
        'email': 'b@b.org',
        'password': 'Temp321!',
        'username': 'louise',
    },
]

NOW = datetime.utcnow()

DATA_TEST_AUCTIONS = [
    {
        'startDatetime': NOW,
        'endDatetime': NOW + timedelta(minutes=5),
        'minPrice': 1000,
        'joinCost': 1000
    },
    {
        'startDatetime': NOW,
        'endDatetime': NOW + timedelta(minutes=10),
        'minPrice': 2000,
        'joinCost': 1000
    },
]

INITIAL_BIDS = 300

def _aucDataToParams(testData):
    """ convert test data to model parameters """
    startDatetime = testData['startDatetime']
    endDatetime = testData['endDatetime']
    assert endDatetime > startDatetime
    joinCost = testData['joinCost']
    return {
        'start_datetime': startDatetime,
        'end_datetime': endDatetime,
        'min_price': testData['minPrice'],
        'join_cost': joinCost
    }


class AuctionTestsBase(BaseModelTestCase):

    def testMakeRewards(self):
        """
        derived classes implement.
        returns a list of reward primary keys
        """
        raise NotImplementedError("implement in derived classes")

    def testCreate(self):
        rewards = self.testMakeRewards()
        auctionsParamsNoReward = [_aucDataToParams(testData)
                                  for testData in DATA_TEST_AUCTIONS]
        auctionsParams = [dict(paramsNoReward, reward=reward)
                          for (paramsNoReward, reward)
                          in zip(auctionsParamsNoReward, rewards)]
        auctions = [Auction(**auctionParams)
                    for auctionParams in auctionsParams]
        saveModels(*auctions)
        sleep(1)
        self.assertNotIn(
            None, [auction.id for auction in auctions],
            msg="set primary keys for auction insert")
        return auctions

    def testValidateEndAfterStartOnCreate(self):
        rewards = self.testMakeRewards()
        auctionsParamsNoReward = [_aucDataToParams(testData)
                                  for testData in DATA_TEST_AUCTIONS]
        auctionsParams = [dict(paramsNoReward, reward=reward)
                          for (paramsNoReward, reward)
                          in zip(auctionsParamsNoReward, rewards)]
        invalidAuctionParams = auctionsParams[0]
        invalidAuctionParams['end_datetime'] = (
            invalidAuctionParams['start_datetime'] -
            timedelta(seconds=5))
        with self.assertRaises(
                ValueError,
                msg="validation on create"):
            Auction(**invalidAuctionParams)

    def testValidateEndAfterStartOnUpdate(self):
        rewards = self.testMakeRewards()
        auctionsParamsNoReward = [_aucDataToParams(testData)
                                  for testData in DATA_TEST_AUCTIONS]
        auctionsParams = [dict(paramsNoReward, reward=reward)
                          for (paramsNoReward, reward)
                          in zip(auctionsParamsNoReward, rewards)]
        auctions = [Auction(**auctionParams)
                    for auctionParams in auctionsParams]
        saveModels(*auctions)
        auction = auctions[0]
        with self.assertRaises(
                ValueError,
                msg="validation on update"):
            auction.start_datetime = (
                auction.end_datetime + timedelta(seconds=5))
        with self.assertRaises(
                ValueError,
                msg="validation on update"):
            auction.end_datetime = (
                auction.start_datetime - timedelta(seconds=5))

    def testById(self):
        auctions = self.testCreate()
        auctionToFind = auctions[0]
        auctionA = Auction.byId(auctionToFind.id)
        self.assertIs(type(auctionA), Auction,
                      msg="found auction instance by auction id")
        self.assertEqual(
            auctionA.id, auctionToFind.id,
            msg="found correct id on auction object")

    def testAddUsers(self):
        auctions = self.testCreate()
        users = [self.createUser(fields) for fields in USERS_FIELDS]
        auctionA = auctions[0]
        auctionB = auctions[1]
        usersA = users[:2]
        usersB = users[2:]
        for user in usersA:
            auctionA.addUser(user, INITIAL_BIDS)
        for user in usersB:
            auctionB.addUser(user, INITIAL_BIDS)

        saveModels(auctionA, auctionB)
        self.assertEqual(len(auctionA.users), len(usersA),
                         msg="correct number of users")
        self.assertEqual(len(auctionB.users), len(usersB),
                         msg="correct number of users")

    def testGetUserRelations(self):
        auction = self.testCreate()[0]
        users = [self.createUser(fields) for fields in USERS_FIELDS]
        assert len(users) > 2 # test data, not fail cond
        for (user, idx) in zip(users[:-1], range(len(users) - 1)):
            auction.addUser(user, INITIAL_BIDS - idx)

        saveModels(auction)
        userRelations = auction.getUserRelations(numUsers=len(users) - 2,
                                         omitUser=users[0])
        self.assertEqual(
            [userRelation.user.id for userRelation in userRelations],
            [user.id for user in users[1:-1]],
            msg="expected user relations")

    def testGetUserRelation(self):
        auction = self.testCreate()[0]
        users = [self.createUser(fields) for fields in USERS_FIELDS]
        addUser = users[0]
        noAddUser = users[1]
        auction.addUser(addUser, INITIAL_BIDS)
        saveModels(auction)
        addUserRelation = auction.getUserRelation(addUser)
        self.assertEqual(
            addUser.id, addUserRelation.user.id,
            msg="gets added user relation")
        noAddUserRelation = auction.getUserRelation(noAddUser, allowNone=True)
        self.assertIsNone(noAddUserRelation,
                          msg="got NoneType for unadded user relation")
        with self.assertRaises(
                NoResultFound,
                msg="got expected exception for unadded user relation"):
            auction.getUserRelation(noAddUser)

    def testFindByUser(self):
        auction = self.testCreate()[0]
        addUser = self.createUser(USERS_FIELDS[0])
        noAddUser = self.createUser(USERS_FIELDS[1])
        auction.addUser(addUser, INITIAL_BIDS)
        saveModels(auction)
        foundAuctionsA = addUser.auctions
        self.assertEqual(
            len(foundAuctionsA), 1,
            msg="found auctions by user")
        foundAuctionsB = noAddUser.auctions
        self.assertEqual(
            len(foundAuctionsB), 0,
            msg="found no auctions by unadded user")

    def testDeductBid(self):
        auction = self.testCreate()[0]
        user = self.createUser(USERS_FIELDS[0])
        auction.addUser(user, INITIAL_BIDS)
        saveModels(auction)
        self.assertIsNone(
            auction.getUserRelation(user).last_bid_datetime)
        auction.deductBid(user)
        saveModels(auction)
        self.assertIsNotNone(
            auction.getUserRelation(user).last_bid_datetime)
        auction.deductBid(user)
        saveModels(auction)
        self.assertEqual(
            len(auction.getUserRelation(user).user_bids), 2)

    def testNumConsecutiveBids(self):
        auction = self.testCreate()[0]
        userA, userB = [ self.createUser(fields) for fields in
                         USERS_FIELDS[:2] ]
        auction.addUser(userA, INITIAL_BIDS)
        auction.addUser(userB, INITIAL_BIDS)
        saveModels(auction)
        self.assertEqual(auction.numConsecutiveBids, 0)
        auction.deductBid(userA)
        saveModels(auction)
        self.assertEqual(auction.numConsecutiveBids, 1)
        auction.deductBid(userB)
        saveModels(auction)
        self.assertEqual(auction.numConsecutiveBids, 1)
        auction.deductBid(userB)
        auction.deductBid(userB)
        saveModels(auction)
        self.assertEqual(auction.numConsecutiveBids, 3)
        auction.deductBid(userA)
        saveModels(auction)
        self.assertEqual(auction.numConsecutiveBids, 1)

    def testFreezeBids(self):
        FREEZE_DURATION = timedelta(seconds=1)
        auction = self.testCreate()[0]
        userA, userB = [ self.createUser(fields) for fields in
                         USERS_FIELDS[:2] ]
        auction.addUser(userA, INITIAL_BIDS)
        auction.addUser(userB, INITIAL_BIDS)
        saveModels(auction)
        for user in [userA, userB]:
            userJsonObj = auction.getUserJsonObj(user)
            self.assertIn('isBidsFrozen', userJsonObj.keys())
            self.assertFalse(userJsonObj['isBidsFrozen'])
        auction.freezeBids(userA, FREEZE_DURATION)
        self.assertTrue(auction.getUserJsonObj(userA)['isBidsFrozen'])
        self.assertFalse(auction.getUserJsonObj(userB)['isBidsFrozen'])
        sleep((.2 * FREEZE_DURATION).total_seconds())
        (auction.getUserRelation(userA)
         .updateIsBidsFrozen(datetime.utcnow()))
        self.assertTrue(auction.getUserJsonObj(userA)['isBidsFrozen'])
        self.assertFalse(auction.getUserJsonObj(userB)['isBidsFrozen'])
        sleep((.8 * FREEZE_DURATION).total_seconds())
        (auction.getUserRelation(userA)
         .updateIsBidsFrozen(datetime.utcnow()))
        self.assertFalse(auction.getUserJsonObj(userA)['isBidsFrozen'])
        self.assertFalse(auction.getUserJsonObj(userB)['isBidsFrozen'])

    def testRewardLookup(self):
        auction = self.testCreate()[0]
        self.assertIn(type(auction.reward), [Item, PhysicalItem])


class AuctionItemsTests(AuctionTestsBase):

    def testMakeRewards(self):
        items = [ Item(**{ key: itemData[key] for key in itemData.keys()
                           if key != 'image_filename' }) for itemData
                  in DATA_TEST_ITEMS ]
        for item, imageFilename in zip(items, [
                itemData['image_filename'] for itemData
                in DATA_TEST_ITEMS ]):
            with open(os.path.join(PATH_DIR_ITEM_IMGS, imageFilename),
                      mode='rb') as f:
                item.picture.from_file(f)
        saveModels(*items)
        return items

    def testInvalidMinPrice(self):
        item = self.testMakeRewards()[0]
        auction = Auction()
        auction.min_price = item.minimum_price - 1
        with self.assertRaises(
                ValueError,
                msg="validation on set item with exceeding min price"):
            auction.reward = item
        auction = Auction()
        auction.reward = item
        with self.assertRaises(
                ValueError,
                msg="validation on set min price with exceeding item's"):
            auction.min_price = item.minimum_price - 1

    # crufty, incomplete, and likely to change
    # as db -> screen data pipeline evolves
    def testSerialization(self):
        """ test data serialzation """
        dataItem = DATA_TEST_ITEMS[0]
        auctions = self.testCreate()
        auction = auctions[0]
        auction.started = True
        saveModels(auction)
        dictAuction = auction.getActiveJsonObj()
        self.assertIn('reward', dictAuction.keys(),
                      msg="has reward attr")
        dictReward = dictAuction['reward']
        self.assertEqual(dictReward['type'], 'item',
                         msg="correct reward type")
        self.assertEqual(
            dictReward['data']['name'], dataItem['name'],
            msg="correct item name")


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(AuctionItemsTests))
    else:
        tests = loader.loadTestsFromName(
            pattern, module=sys.modules[__name__])
        # check for "synthetic tests" resulting unfound name
        # (see loadTestsFromName docs)
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
