""" base functionality for .core module unit tests """
import unittest

from sqlalchemy import Table

from app import initFlaskApp
from app import model

from bargible.model import User
from bargible.model import saveModels

import gentestmodels

INIT_CREDIT = 1000

def resetDb():
    model.shutdownSession()
    model.dropAllTables()
    model.initDb()


class BaseCoreTestCase(unittest.TestCase):
    """ base class for .core module tests """

    def setUp(self):
        resetDb()
        gentestmodels.createConfig()
        app = initFlaskApp()
        app.config.from_object('config.TestingConfig')
        # todo: the following -- based on app factory pattern
        # ensure the model layer can determine image urls.
        # application factories and the host_url_getter parameter
        # are probably a cleaner way to do this than using http
        # functionality within .core module tests
        self.c = app.test_client()
        self.c.get('/')

    def tearDown(self):
        pass

    def createUser(self, userFields):
        """ functionality from flask-user register endpoint """
        userFields['credit'] = INIT_CREDIT
        user = User(**userFields)
        saveModels(user)
        return user
