""" auction gameplay and "business-logic" tests """
import unittest

from . import auction_manager as auctionManagerTests

def load_tests(loader, standard_tests, pattern):
    suite = unittest.TestSuite()
    for testModule in [
            auctionManagerTests,
    ]:
        tests = (unittest.defaultTestLoader
                 .loadTestsFromModule(testModule, pattern=pattern))
        suite.addTests(tests)
    return suite

