""" test AuctionManager class """
import os
import json
import sys
import unittest
from datetime import datetime
from datetime import timedelta
from time import sleep

from sqlalchemy_imageattach.context import store_context

from bargible.model import imageStore
from bargible.model import saveModels
from bargible.model import Auction
from bargible.model import Item

from bargible.periodic_auc_updater.updater import update as updateAllAuctions
from bargible.auc_core.auction_manager import AuctionManager
from bargible.auc_core.exc import AucCoreError

from .base import BaseCoreTestCase

NOW = datetime.utcnow()

PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..', '..', '..', '..')
)

PATH_DIR_ITEM_IMGS = os.path.join(
    PROJ_ROOT, 'test_data', 'images', 'item')

with open(os.path.join(
        PROJ_ROOT, 'test_data', 'fixtures', 'items_00.json')) as f:
    DATA_ITEMS = json.load(f)

USERS_FIELDS = [
    {
        'last_name': 'Belcher',
        'first_name': 'Linda',
        'email': 'l@b.org',
        'password': 'Temp321!',
        'username': 'linda',
    },
    {
        'last_name': 'Belcher',
        'first_name': 'Tina',
        'email': 'b@b.org',
        'password': 'Temp321!',
        'username': 'tina',
    },
    {
        'last_name': 'Belcher',
        'first_name': 'Gene',
        'email': 'b@b.org',
        'password': 'Temp321!',
        'username': 'gene',
    },
    {
        'last_name': 'Belcher',
        'first_name': 'Louise',
        'email': 'b@b.org',
        'password': 'Temp321!',
        'username': 'louise',
    },
]

INITIAL_BIDS = 300
START_DELAY = 1

DATA_AUCTIONS = [
    {
        'startTimedelta': timedelta(seconds=START_DELAY),
        'minPrice': 1000,
        'joinCost': 1000,
    },
]

def _aucDataToParams(endTimedelta, testData):
    """ convert test data to model parameters """
    now = datetime.utcnow()
    startDatetime = now + testData['startTimedelta']
    endDatetime = now + endTimedelta
    assert endDatetime > startDatetime
    # data model doesn't support starting and ending on different days
    assert startDatetime.date() == endDatetime.date()
    joinCost = testData['joinCost']
    return {
        'start_datetime': startDatetime,
        'end_datetime': endDatetime,
        'min_price': testData['minPrice'],
        'join_cost': joinCost
    }

class AuctionManagerTests(BaseCoreTestCase):

    def _initItemFromFixture(self, fixtureData):
        item = Item(**{ key: fixtureData[key]
                        for key in fixtureData.keys()
                        if key != 'picture' })
        with store_context(imageStore):
            with open(os.path.join(PATH_DIR_ITEM_IMGS,
                                   fixtureData['picture']
            ), mode='rb') as f:
                item.picture.from_file(f)
        return item

    def _createRewards(self):
        items = [ self._initItemFromFixture(dataItem)
              for dataItem in DATA_ITEMS ]
        saveModels(*items)
        return items

    def _createAuctions(self, endTimedelta=timedelta(seconds=10)):
        """
        auction data is initialized here rather than in a fixture-like
        function to ensure that start and end datetimes are appropriate
        to the test run
        """
        rewards = self._createRewards()
        auctionsParamsNoReward = [
            _aucDataToParams(endTimedelta, testData)
            for testData in DATA_AUCTIONS]
        auctionsParams = [
            dict(paramsNoReward, reward=reward)
            for (paramsNoReward, reward)
            in zip(auctionsParamsNoReward,
                   rewards[:len(auctionsParamsNoReward)])]
        auctions = [Auction(**auctionParams)
                    for auctionParams in auctionsParams]
        saveModels(*auctions)
        sleep(START_DELAY)
        self.assertNotIn(
            None, [auction.id for auction in auctions],
            msg="set primary keys for auction insert")
        return [auction.id for auction in auctions]

    def testJoin(self):
        users = [self.createUser(fields) for fields in USERS_FIELDS]
        joinUser = users[0]
        auctionId = self._createAuctions()[0]
        creditBeforeJoin = joinUser.credit
        # updateAllAuctions() # this can erroniously (?) start the auction
                           # if the auction start is within a minute
                           # of the current time
        with self.assertRaises(
                AucCoreError,
                msg="got expected exception on join before start"):
            AuctionManager(Auction.byId(auctionId)).joinAuction(joinUser)
        self.assertEqual(joinUser.credit, creditBeforeJoin)

    def testJoinMaxUsers(self):
        users = [self.createUser(fields) for fields in USERS_FIELDS]
        auction = Auction.byId(self._createAuctions()[0])
        auctionManager = AuctionManager(auction)
        updateAllAuctions()
        maxUsers = 2
        auctionManager.config['MAX_USERS_PER_AUCTION'] = maxUsers
        for user in users[:maxUsers]:
            auctionManager.joinAuction(user)
        with self.assertRaises(
                AucCoreError,
                msg="expected exception on gt max users join"):
            auctionManager.joinAuction(users[maxUsers])

    def testLastBidder(self):
        """ test that the last_bidder computed property of the
        Auction model is correctly updated by
        AuctionManager.bidOnAuction """
        users = [self.createUser(fields) for fields in USERS_FIELDS]
        auction = Auction.byId(self._createAuctions()[0])
        auctionManager = AuctionManager(auction)
        updateAllAuctions()
        for user in users:
            auctionManager.joinAuction(user)
        bidUserA, bidUserB, bidUserC = users[:3]
        self.assertIsNone(auction.last_bidder)
        auctionManager.bidOnAuction(bidUserA)
        self.assertEqual(auction.last_bidder.id, bidUserA.id)
        auctionManager.bidOnAuction(bidUserB)
        self.assertEqual(auction.last_bidder.id, bidUserB.id)
        auctionManager.bidOnAuction(bidUserA)
        self.assertEqual(auction.last_bidder.id, bidUserA.id)
        auctionManager.bidOnAuction(bidUserB)
        self.assertEqual(auction.last_bidder.id, bidUserB.id)
        auctionManager.bidOnAuction(bidUserC)
        self.assertEqual(auction.last_bidder.id, bidUserC.id)

    def testBidMult(self):
        """ test bid multiple times feature """
        users = [self.createUser(fields) for fields in USERS_FIELDS]
        auctionId = self._createAuctions()[0]
        auctionManager = AuctionManager(Auction.byId(auctionId))
        updateAllAuctions()
        for user in users:
            auctionManager.joinAuction(user)
        bidUser = user
        for _ in range(1, 4):
            self.assertIsNone(auctionManager.bidOnAuction(bidUser)[1])
        for bidIdx in range(4, 8):
            self.assertEquals(auctionManager.bidOnAuction(bidUser)[1],
                              {'complete': False, 'consecutiveBids': bidIdx})
        resBidMult = auctionManager.bidOnAuction(bidUser)[1]
        self.assertIs(type(resBidMult), dict)
        self.assertEqual(resBidMult,
                         {'complete': True, 'consecutiveBids': 8})

    def testBidNoBids(self):
        """ test error behavior as bids go to zero """
        users = [self.createUser(fields) for fields in USERS_FIELDS]
        auctionId = self._createAuctions()[0]
        auctionManager = AuctionManager(Auction.byId(auctionId))
        updateAllAuctions()
        for user in users:
            auctionManager.joinAuction(user)
        bidUser = user
        auction = Auction.byId(auctionId)
        auctionManager.bidOnAuction(bidUser)
        bidUam = auction.getUserRelation(bidUser)
        bidUam.bids = 2
        saveModels(bidUam)
        auctionManager.bidOnAuction(bidUser)
        auctionManager.bidOnAuction(bidUser)
        with self.assertRaises(AucCoreError):
            auctionManager.bidOnAuction(bidUser)

    def testFreezeBids(self):
        FREEZE_DUR_SEC = 6 # for faster tests.. longer than db latency
        users = [self.createUser(fields) for fields in USERS_FIELDS[:5]]
        auctionId = self._createAuctions(
            endTimedelta=timedelta(seconds=20)
        )[0]
        auctionManager = AuctionManager(Auction.byId(auctionId))
        auctionManager.config['FREEZE_DUR'] = FREEZE_DUR_SEC
        updateAllAuctions()
        for user in users:
            auctionManager.joinAuction(user)
        userA, userB, userC, userD = users[:4]
        for _ in range(10): # ensure userC has activated freeze
            auctionManager.bidOnAuction(userC)
        for _ in range(5): # enough to ensure userA has most bids
            auctionManager.bidOnAuction(userB)
            auctionManager.bidOnAuction(userD)
        auctionManager.freezeBids(userC)
        auctionManager.bidOnAuction(userC)
        self.assertTrue(Auction.byId(auctionId)
                        .getUserJsonObj(userA)['isBidsFrozen'])
        self.assertFalse(Auction.byId(auctionId)
                        .getUserJsonObj(userB)['isBidsFrozen'])
        self.assertFalse(Auction.byId(auctionId)
                        .getUserJsonObj(userC)['isBidsFrozen'])
        self.assertFalse(Auction.byId(auctionId)
                        .getUserJsonObj(userD)['isBidsFrozen'])
        # db calls in preceeding assertions ensure some time has
        # elapsed before update
        updateAllAuctions()
        self.assertTrue(Auction.byId(auctionId)
                        .getUserJsonObj(userA)['isBidsFrozen'])
        sleep(FREEZE_DUR_SEC)
        updateAllAuctions()
        self.assertFalse(Auction.byId(auctionId)
                         .getUserJsonObj(userA)['isBidsFrozen'])

    def testStealLowBids(self):
        """ test steal bids behavior when the user to steal from
        has fewer bids than will result in bids stolen """
        users = [self.createUser(fields) for fields in USERS_FIELDS[:5]]
        auctionId = self._createAuctions()[0]
        auctionManager = AuctionManager(auction = Auction.byId(auctionId))
        auctionManager.config['STEAL_FRACTION'] = .1
        auctionManager.config['MAX_STEAL_AMOUNT'] = 10
        auctionManager.config['INIT_NUM_BIDS'] = 9
        updateAllAuctions() # this can erroniously (?) _not_ start
                         # the auction if the auction start is after
                         # of the current time
        for user in users:
            auctionManager.joinAuction(user)
        stealUser = users[0]
        otherUsers = users[1:]
        for _ in range(20): # ensure stealUser has activated steal
            auctionManager.bidOnAuction(stealUser)
        dictsStolenBids = auctionManager.stealBids(stealUser)
        # there shouls be no bids stolen as the floor(9 * .1) == 0
        self.assertEqual(len(dictsStolenBids), 0)

    def testPosition(self):
        """
        this is as much of a of a test of UserAuctionMap.position
        model layer functionality as it is of .core module functionaly,
        which updates position information according to which users
        have bid
        """
        users = [self.createUser(fields) for fields in USERS_FIELDS]
        auction = Auction.byId(self._createAuctions()[0])
        auctionManager = AuctionManager(auction)
        updateAllAuctions()
        for user in users:
            auctionManager.joinAuction(user)
        bidUserA, bidUserB, bidUserC = users[:3]
        self.assertIsNone(auction.getUserRelation(bidUserA).position)
        auctionManager.bidOnAuction(bidUserA)
        self.assertEqual(auction.getUserRelation(bidUserA).position, 1)
        self.assertIsNone(auction.getUserRelation(bidUserB).position)
        auctionManager.bidOnAuction(bidUserB)
        self.assertEqual(auction.getUserRelation(bidUserB).position, 1)
        self.assertEqual(auction.getUserRelation(bidUserA).position, 2)
        self.assertIsNone(auction.getUserRelation(bidUserC).position)
        auctionManager.bidOnAuction(bidUserA)
        self.assertEqual(auction.getUserRelation(bidUserA).position, 1)
        self.assertEqual(auction.getUserRelation(bidUserB).position, 2)
        self.assertIsNone(auction.getUserRelation(bidUserC).position)
        auctionManager.bidOnAuction(bidUserC)
        self.assertEqual(auction.getUserRelation(bidUserC).position, 1)
        self.assertEqual(auction.getUserRelation(bidUserA).position, 2)
        self.assertEqual(auction.getUserRelation(bidUserB).position, 3)

    def testBidToActivate(self):
        """ test bid to activate game elements feature """
        user = self.createUser(USERS_FIELDS[0])
        auctionId = self._createAuctions()[0]
        auction = Auction.byId(auctionId)
        auctionManager = AuctionManager(auction)
        updateAllAuctions() # ensure auction is active
        auctionManager.joinAuction(user)
        with self.assertRaises(AucCoreError):
            auctionManager.freezeBids(user)
        with self.assertRaises(AucCoreError):
            auctionManager.stealBids(user)
        with self.assertRaises(AucCoreError):
            auctionManager.destroyBids(user)
        for _ in range(10): # 10 bids adds one freeze activation
            auctionManager.bidOnAuction(user)
        with self.assertRaises(AucCoreError):
            auctionManager.stealBids(user)
        with self.assertRaises(AucCoreError):
            auctionManager.destroyBids(user)
        # use freeze activation
        auctionManager.freezeBids(user)
        with self.assertRaises(AucCoreError):
            # check that freeze activation has been used
            auctionManager.freezeBids(user)
        for _ in range(20): # 30 bids: 2 freeze, 1 steal, 1 destroy
            auctionManager.bidOnAuction(user)
        auctionManager.stealBids(user)
        auctionManager.destroyBids(user)
        with self.assertRaises(AucCoreError):
            auctionManager.stealBids(user)
        with self.assertRaises(AucCoreError):
            auctionManager.destroyBids(user)
        auctionManager.freezeBids(user)
        auctionManager.freezeBids(user)
        with self.assertRaises(AucCoreError):
            auctionManager.freezeBids(user)

    def testGetUserJsonObjNoBids(self):
        """ getUserJsonObj before bidding -- part values, part format.
        the lack of seperation in this test's concerns corresponds,
        in part, to uncertainty about interfaces in the application
        """
        user = self.createUser(USERS_FIELDS[0])
        auctionId = self._createAuctions()[0]
        auction = Auction.byId(auctionId)
        auctionManager = AuctionManager(auction)
        updateAllAuctions() # ensure auction is active
        with self.assertRaises(ValueError):
            auctionManager.getUserJsonObj(user)
        auctionManager.joinAuction(user)
        userJsonObj = auctionManager.getUserJsonObj(user)
        self.assertIs(type(userJsonObj), dict)
        self.assertEqual(set(userJsonObj.keys()), set([
            'numActivations',
            'bidsTilActivation',
        ]))
        self.assertIs(type(userJsonObj['numActivations']), dict)
        self.assertEqual(set(userJsonObj['numActivations'].keys()),
                         set(['freeze', 'steal', 'destroy']))
        self.assertEqual(set([
            userJsonObj['numActivations'][actType]
            for actType in ['freeze', 'steal', 'destroy'] ]),
                         set([0]))
        self.assertIs(type(userJsonObj['bidsTilActivation']), dict)
        self.assertEqual(set(userJsonObj['bidsTilActivation'].keys()),
                         set(['freeze', 'steal', 'destroy']))
        self.assertEqual(userJsonObj['bidsTilActivation']['freeze'],
                         auctionManager.config['UNTIL_FREEZE'])
        self.assertEqual(userJsonObj['bidsTilActivation']['steal'],
                         auctionManager.config['UNTIL_STEAL'])
        self.assertEqual(userJsonObj['bidsTilActivation']['destroy'],
                         auctionManager.config['UNTIL_DESTROY'])

    def testGetUserJsonObjWithBids(self):
        """ getUserJsonObj after bidding -- part values, part format.
        the lack of seperation in this test's concerns corresponds,
        in part, to uncertainty about interfaces in the application
        """
        user = self.createUser(USERS_FIELDS[0])
        auctionId = self._createAuctions()[0]
        auction = Auction.byId(auctionId)
        auctionManager = AuctionManager(auction)
        updateAllAuctions() # ensure auction is active
        auctionManager.joinAuction(user)
        numAddtionalBidTimes = (min(
            auctionManager.config['UNTIL_FREEZE'],
            auctionManager.config['UNTIL_STEAL'],
            auctionManager.config['UNTIL_DESTROY']) - 1)
        for _ in range(max(
                auctionManager.config['UNTIL_FREEZE'],
                auctionManager.config['UNTIL_STEAL'],
                auctionManager.config['UNTIL_DESTROY']) +
                       numAddtionalBidTimes):
            auctionManager.bidOnAuction(user)
        userJsonObj = auctionManager.getUserJsonObj(user)
        self.assertNotIn(0, set([
            userJsonObj['numActivations'][actType]
            for actType in ['freeze', 'steal', 'destroy'] ]),
                         set([0]))
        self.assertTrue(userJsonObj['bidsTilActivation']['freeze'] <
                        auctionManager.config['UNTIL_FREEZE'])
        self.assertTrue(userJsonObj['bidsTilActivation']['steal'] <
                        auctionManager.config['UNTIL_STEAL'])
        self.assertTrue(userJsonObj['bidsTilActivation']['destroy'] <
                        auctionManager.config['UNTIL_DESTROY'])


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(AuctionManagerTests))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
