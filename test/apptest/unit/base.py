""" base functionality for auction test """
import time
import unittest

from sqlalchemy import Table

from app import initFlaskApp
from app import model

import gentestmodels

def resetDb():
    model.dropAllTables()
    model.initDb()


class BaseTestCase(unittest.TestCase):
    """ base class for auction test """

    def setUp(self):
        # todo: rm the app init _or_ push derived classes' __init__
        # into this method
        app = initFlaskApp()
        app.config.from_object('config.TestingConfig')
        resetDb()
        gentestmodels.createConfig()
        gentestmodels.createItems(0)
        gentestmodels.createAuctionTestAuctions()
        time.sleep(3)

    def tearDown(self):
        pass
