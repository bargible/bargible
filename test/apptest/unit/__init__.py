import unittest

from . import model as modelTests
from . import core as coreTests
from . import third_party_http_apis as apiTests

# map sting testTypes list to a test module
TYPE2MODULE = {
    'model': modelTests,
    'core': coreTests,
    'api': apiTests,
}

def suite(testTypes=None, testName=None):
    ## decide which tests to run
    testsModules = ([
        testModule for testModule in TYPE2MODULE.values()
    ] if testTypes is None else [
        TYPE2MODULE[testType] for testType in testTypes
    ])
    ## construct test suite
    suite = unittest.TestSuite()
    for testModule in testsModules:
        suite.addTests(
            unittest.defaultTestLoader
            .loadTestsFromModule(testModule, pattern=testName)
        )
    return suite
