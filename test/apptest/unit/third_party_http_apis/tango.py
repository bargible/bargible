"""
Tango gift card

https://www.tangocard.com/gift-card-api/
https://integration-www.tangocard.com/raas_api_console/v2/
https://www.tangocard.com/docs/raas-api/#introduction
https://integration-www.tangocard.com/raas_api_console/v1.1/
"""
import os
import sys
import json
import unittest
import configparser
from time import sleep

from bargible.third_party_api import tango

from .base import BaseApiTestCase

CONF = configparser.ConfigParser()
CONF.read(os.path.join(os.path.dirname(__file__), 'defaults.conf'))

with open(os.path.join(os.path.dirname(__file__),
            'fixtures', 'tango', 'redeem_00.json'), 'r') as f:
    DATA_REDEEM = json.loads(f.read())

class TangoModuleTests(BaseApiTestCase):
    """
    tests around the application module of our that interacts
    with the Tango API
    """

    def setUp(self):
        super().setUp()
        tango.setEnvironment(CONF['tango_creds']['env'])

    def testRedeem(self):
        res = tango.redeem(DATA_REDEEM)
        self.assertTrue(res.ok, msg='\n'.join([
            "tango.redeem failed.  result as string:",
            str(res),
            ]))

    def testGetRewards(self):
        res = tango.getRewards()
        self.assertTrue(res.ok, msg='\n'.join([
            "tango.getRewards failed.  result:",
            str(res),
            ]))


def load_tests(loader, tests, pattern):
    suite = unittest.TestSuite()
    if pattern is None:
        suite.addTests(loader.loadTestsFromTestCase(
            TangoModuleTests
        ))
    else:
        tests = loader.loadTestsFromName(pattern,
                                         module=sys.modules[__name__])
        failedTests = [t for t in tests._tests
                       if type(t) == unittest.loader._FailedTest]
        if len(failedTests) == 0:
            suite.addTests(tests)
    return suite
