"""
base functionality for third party HTTP API tests
"""
import os
import json
import unittest
from time import sleep

from sqlalchemy import Table

import gentestmodels

from app import initFlaskApp
from app import model

def resetDb():
    model.shutdownSession()
    model.dropAllTables()
    model.initDb()


class BaseApiTestCase(unittest.TestCase):
    """ base class for model layer tests """

    def setUp(self):
        resetDb()
        self._app = initFlaskApp()
        self._app.config.from_object('config.TestingConfig')
        # todo: the following -- based on app factory pattern
        # ensure the model layer can determine image urls.
        # application factories and the host_url_getter parameter
        # are probably a cleaner way to do this than using http
        # functionality within model-layer tests
        self._c = self._app.test_client()
        self._c.get('/')

    def tearDown(self):
        pass

