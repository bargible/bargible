""" 3rd-party http api tests """
import unittest

from . import tango as tangoTests

def load_tests(loader, standard_tests, pattern):
    suite = unittest.TestSuite()
    for testModule in [
            tangoTests,
    ]:
        tests = (unittest.defaultTestLoader
                 .loadTestsFromModule(testModule, pattern=pattern))
        suite.addTests(tests)
    return suite

