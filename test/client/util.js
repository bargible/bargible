/* global require, module, global */

var fs = require('fs')

const _ = require('lodash')
const jsdom = require('jsdom')
const jquery = require('jquery')
const ReactTools = require('react-tools')
var reactTemplates = require('react-templates')

const reactToolsTransformOpts =
        require('../../client/const').reactToolsTransformOpts

var requireJsxInstalled = false

/** allow require()ing .jsx files */
var installRequireJsx = function () {
  if (requireJsxInstalled) {
    return
  }

  /* eslint node/no-deprecated-api: "off" */
  // TODO: require.extensions' was deprecated since v0.12. Use compiling them ahead of time instead
  require.extensions['.jsx'] = function (module, filename) {
    var source = fs.readFileSync(filename, {encoding: 'utf8'})
    var code = ReactTools.transformWithDetails(source, _.merge(
            reactToolsTransformOpts,
            {sourceFilename: filename}))
                .code
    module._compile(code, filename)
  }

  requireJsxInstalled = true
}

var requireRtInstalled = false

/** allow require()ing .rt react template files */
var installRequireRt = function () {
  if (requireRtInstalled) {
    return
  }

  require.extensions['.rt'] = function (module, filename) {
    var source = fs.readFileSync(filename, {encoding: 'utf8'})
    var code = reactTemplates.convertTemplateToReact(
            source, {
              modules: 'commonjs',
                // react minor version mismatch intended
              targetVersion: '0.14.0'
            }
        )
    module._compile(code, filename)
  }

  requireRtInstalled = true
}

var domImplInstalled = false

/**
 * globally install a DOM implementation for React test utils.
 * additionally set up a global jquery instance as '$',
 * since instantiating jquery after making window and document
 * globals fails.
 */
var installDomImpl = function () {
  if (domImplInstalled) {
    return
  }

  var doc = jsdom.jsdom([
    '<!doctype html><html>',
    '<div id="navbar"></div>',
    '<div id="app"></div>',
    '<div id="modal-wrapper" hidden="true"><div><div class="modal-background"></div>',
    '<div id="modal-body"><div id="btn-modal-close">x</div>',
    '<div id="modal-content"><span>loading...</span></div></div></div></div>',
    '<body></body></html>'
  ].join(''))
  var win = doc.defaultView
  var $ = jquery(win)

  global.document = doc
  global.window = win
  global.$ = $
    // additional browser-like globals for react
  global.navigator = win.navigator

  domImplInstalled = true
}

var exports = {}

exports.installRequireJsx = installRequireJsx
exports.installRequireRt = installRequireRt
exports.installDomImpl = installDomImpl

module.exports = exports
