const React = require('react')

const shallow = require('enzyme/shallow')

const MainHome = require('../../../../client/app/comp/main_home')
const AuctionContainerStore = require('../../../../client/app/stores/auction_container_store')
const AllPagesActions = require('../../../../client/app/actions/all_pages_actions')
const HomeActions = require('../../../../client/app/actions/home_actions')

const DATA_USER = {
  credit: 500,
  first_name: 'bob',
  id: 1,
  last_name: "belcher",
  username: "bburgers",
  avatar: null,
  email: "b@b.org"
}

const CONFIG = {
  starLevels: {
    silver: 100,
    gold: 500,
    silverLevelUp: 5,
    goldLevelUp: 10
  }
}

AllPagesActions.updateUserData(DATA_USER)
HomeActions.setConfig(CONFIG)

var auctionsInitialized = function (t) {
  let element = React.createElement(MainHome, AuctionContainerStore.getState())
  let home = shallow(element)
  let state = home.state()
  let auctions = state.visibleAuctions
  t.ok(auctions.gold.length === 0, 'Gold Auctions should be empty')
  t.ok(auctions.silver.length === 0, 'Silver Auctions should be empty')
  t.ok(auctions.bronze.length === 0, 'Bronze Auctions should be empty')
  t.ok(state.levelUps.auctionCount === 0, "Number of auctions a user has bid in is in state")
  t.ok(!state.levelUps.silver, "User cannot participate in silver auctions")
  t.ok(!state.levelUps.gold, "User cannot participate in gold auctions")
  t.equals(typeof state.levelUps.silver, 'boolean', "Silver levelup is a boolean")
  t.equals(typeof state.levelUps.gold, 'boolean', "Gold levelup is a boolean")
  t.equals(state.levelUps.neededForSilver, 5, "5 Auctions needed for silver")
  t.equals(state.levelUps.neededForGold, 10, "10 Auctions needed for gold")
  t.equals(typeof state.levelUps.neededForSilver, 'number', "Number of additional auctions needed for silver is a number")
  t.equals(typeof state.levelUps.neededForGold, 'number', "Number of additional auctions needed for gold is a number")
  t.end()
}

var checkAuctions = function (t) {
  HomeActions.updateAllAuctions({
    updatedAuctions: require('../data/auctions.js').map(function (auction) {
      return {
        data: auction,
        status: auction.status
      }
    }),
    inits: [
      'endedAuctions',
      'joinedAuctions',
      'activeAuctions',
      'upcomingAuctions'
    ]
  })
  let element = React.createElement(MainHome, AuctionContainerStore.getState())
  let home = shallow(element)
  let state = home.state()
  let auctions = state.visibleAuctions

  t.ok(auctions, "Contains visibleAuctions")
  t.ok(auctions.live, "Contains live auctions")
  t.ok(auctions.gold, "Contains gold auctions")
  t.ok(auctions.silver, "Contains silver auctions")
  t.ok(auctions.bronze, "Contains live auctions")

  t.equals(auctions.live.length, 6, "6 Live Auctions")
  t.equals(auctions.gold.length, 2, "2 Gold Auctions")
  t.equals(auctions.silver.length, 2, "2 Silver Auctions")
  t.equals(auctions.bronze.length, 2, "2 Bronze Auctions")

  function isUpcoming (auction) {
    t.equals(auction.status, "upcoming", "Auction is Upcoming")
  }

  auctions.gold.forEach(isUpcoming)
  auctions.silver.forEach(isUpcoming)
  auctions.bronze.forEach(isUpcoming)

  t.end()
}

function checkStatusFilterActive (t) {
  let element = React.createElement(MainHome, AuctionContainerStore.getState())
  let home = shallow(element)
  let state = home.state()
  let auctions = state.visibleAuctions

  home.setState({statusFilter: "active"})
  home.instance().componentWillReceiveProps(AuctionContainerStore.getState())
  state = home.state()
  auctions = state.visibleAuctions
  t.equals(auctions.live.length, 6, "6 Live Auctions")
  t.equals(auctions.gold.length, 2, "2 Gold Auctions")
  t.equals(auctions.silver.length, 2, "2 Silver Auctions")
  t.equals(auctions.bronze.length, 2, "2 Bronze Auctions")

  function isLive (auction) {
    t.equals(auction.status, 'active', "Auction is Active")
  }

  auctions.gold.forEach(isLive)
  auctions.silver.forEach(isLive)
  auctions.bronze.forEach(isLive)

  t.end()
}

function checkStarFilterGold (t) {
  let element = React.createElement(MainHome, AuctionContainerStore.getState())
  let home = shallow(element)
  let state = home.state()
  let auctions = state.visibleAuctions

  home.setState({statusFilter: null, starFilter: 'gold'})
  home.instance().componentWillReceiveProps(AuctionContainerStore.getState())
  state = home.state()
  auctions = state.visibleAuctions
  t.equals(auctions.live.length, 2, "2 Live Auctions")
  t.equals(auctions.gold.length, 2, "2 Gold Auctions")
  t.equals(auctions.silver.length, 2, "2 Silver Auctions")
  t.equals(auctions.bronze.length, 2, "2 Bronze Auctions")
  t.end()
}

function checkLevelUps (t) {
  let element = React.createElement(MainHome, Object.assign(
    {},
    AuctionContainerStore.getState(),
    {endedAuctions: [0, 1, 2, 3, 4]}
  ))
  let home = shallow(element)
  let state = home.state()
  home.setProps({
    endedAuctions: [0, 1, 2, 3, 4]
  })
  home.update()
  state = home.state()
  t.ok(state.levelUps.silver, "Can participate in silver auctions")
  t.ok(state.levelUps.neededForSilver === 0, "no more auctions needed for silver")
  t.ok(state.levelUps.neededForGold === 5, "5 more auctions needed for gold")

  t.end()
}

module.exports = function (t) {
  t.test("Auctions Initialized Properly", auctionsInitialized)
  t.test("Auctions Populated from example data", checkAuctions)
  t.test("statusFilter('active') shows correct auctions", checkStatusFilterActive)
  t.test("starFilter('gold') shows correct auctions", checkStarFilterGold)
  t.test("LevelUps work properly", checkLevelUps)
}

/* TODO: switch to self contained test
   const tape = require('tape')
   tape.test('main home component', function (t) {
   let home = React.createElement(MainHome)
   console.log({home})
   t.ok(home, 'main home renders')
   t.end()
   })
*/
