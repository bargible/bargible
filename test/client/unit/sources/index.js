/* global require, module */

var testsMain = function (t) {
  t.test("auctions source", require('./auctions_source'))
  t.test("credit packages source", require('./credit_packages_source'))
}

var exports = testsMain
module.exports = exports
