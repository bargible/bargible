/* global require, module */

var testsMain = function (t) {
  t.test("fetch all", require('./fetch_all'))
}

var exports = testsMain
module.exports = exports
