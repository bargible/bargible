/* global require, module */

const _ = require('lodash')
const proxyquire = require('proxyquire')

const stubs = require('../../../int/stubs')

// clear node cache of all application modules before proxyquring
// so that proxying a different transitive dep from preceeding tests
// can be achieved without a global override as described in
// the int/home/index.js tests
_.forEach(_.keys(require.cache), function (moduleName) {
  if (_.includes(moduleName, 'client/app')) {
    delete require.cache[moduleName]
  }
})

proxyquire(
    '../../../../../client/app/util',
  {
    './http': stubs.http
  }
)

const setup = function (t) {
  t.end()
}

const teardown = function (t) {
  stubs.reset()
  t.end()
}

var exports = {}

exports.setup = setup
exports.teardown = teardown

module.exports = exports
