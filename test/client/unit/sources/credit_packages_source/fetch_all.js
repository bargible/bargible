/* global require, module */

const stubs = require('../../../int/stubs')
const setupTeardown = require('./setup_teardown')

const fetchAll = require(
    '../../../../../client/app/sources/credit_packages_source')
          .fetchAll

const DATA_VALID_CREDIT_PACKAGE = Object.freeze({
  id: 3,
  numCredits: 1000,
  price: 500
})

// additional key: description
const DATA_INVALID_CREDIT_PACKAGE = Object.freeze({
  id: 3,
  numCredits: 1000,
  description: "some credit package description",
  price: 500
})

const fetchValidTestMain = function (t) {
  stubs.endpoints.fetch['api/creditPackages']
        .set({creditPackages: [DATA_VALID_CREDIT_PACKAGE]})
  Promise.resolve().then(function () {
    return fetchAll()
  }).then(function (res) {
    t.ok(res, "got result from credit packages fetch")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const fetchValidTest = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("test", fetchValidTestMain)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const fetchInvalidTestMain = function (t) {
  stubs.endpoints.fetch['api/creditPackages']
        .set({creditPackages: [DATA_INVALID_CREDIT_PACKAGE]})
  Promise.resolve().then(function () {
    return fetchAll()
  }).then(function (res) {
    t.notOk(res, "got result from credit packages fetch")
    t.end()
  }).catch(function (err) {
    t.ok(err, "got error from invalid fetch")
    t.end()
  })
}

const fetchInvalidTest = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("test", fetchInvalidTestMain)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

var testsMain = function (t) {
  t.test("valid data", fetchValidTest)
  t.test("invalid data", fetchInvalidTest)
  t.end()
}

var exports = testsMain
module.exports = exports
