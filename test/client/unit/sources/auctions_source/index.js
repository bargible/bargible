/* global require, module */

var testsMain = function (t) {
  t.test("fetch active", require('./fetch_active'))
  t.test("fetch upcoming", require('./fetch_upcoming'))
}

var exports = testsMain
module.exports = exports
