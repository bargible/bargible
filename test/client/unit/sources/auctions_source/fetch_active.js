/* global require, module */

const stubs = require('../../../int/stubs')
const setupTeardown = require('./setup_teardown')

const fetchActive = require(
    '../../../../../client/app/sources/auctions_source').fetchActive

const DATA_VALID_AUCTION = Object.freeze({
  upcoming: null,
  active: Object.freeze({
    id: 5,
    reward: Object.freeze({
      type: 'item',
      data: Object.freeze({
        'name': 'some item name',
        'image': 'path/to/img.png',
        'itemId': 2,
        'description': 'some item description',
        'disclaimer': 'some item disclaimer'
      })
    }),
    timeRemaining: 3,
    bidPrice: 5,
    minPrice: 100,
    numUsers: 2,
    joinCost: 1000
  }),
  joined: null,
  ended: null
})

// missing key: minPrice
const DATA_INVALID_AUCTION = Object.freeze({
  upcoming: null,
  active: Object.freeze({
    id: 5,
    reward: Object.freeze({
      type: 'item',
      data: Object.freeze({
        'name': 'some item name',
        'image': 'path/to/img.png',
        'itemId': 2,
        'description': 'some item description',
        'disclaimer': 'some item disclaimer'
      })
    }),
    timeRemaining: 3,
    bidPrice: 5,
    numUsers: 2,
    joinCost: 1000
  }),
  joined: null,
  ended: null
})

const fetchValidTestMain = function (t) {
  stubs.endpoints.fetch['api/auction/auctions']
        .set([DATA_VALID_AUCTION])
  Promise.resolve().then(function () {
    return fetchActive()
  }).then(function (res) {
    t.ok(res, "got result from active fetch")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const fetchValidTest = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("test", fetchValidTestMain)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const fetchInvalidTestMain = function (t) {
  stubs.endpoints.fetch['api/auction/auctions']
        .set([DATA_INVALID_AUCTION])
  Promise.resolve().then(function () {
    return fetchActive()
  }).then(function (res) {
    t.notOk(res, "got result from active fetch")
    t.end()
  }).catch(function (err) {
    t.ok(err, "got error from invalid fetch")
    t.end()
  })
}

const fetchInvalidTest = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("test", fetchInvalidTestMain)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

var testsMain = function (t) {
  t.test("valid data", fetchValidTest)
  t.test("invalid data", fetchInvalidTest)
  t.end()
}

var exports = testsMain
module.exports = exports
