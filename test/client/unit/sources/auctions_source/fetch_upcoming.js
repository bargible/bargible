/* global require, module */

const stubs = require('../../../int/stubs')
const setupTeardown = require('./setup_teardown')

const fetchUpcoming = require(
    '../../../../../client/app/sources/auctions_source').fetchUpcoming

const DATA_VALID_AUCTION = Object.freeze({
  upcoming: Object.freeze({
    id: 3,
    minPrice: 1000,
    reward: Object.freeze({
      type: 'item',
      data: Object.freeze({
        'name': 'some item name',
        'image': 'path/to/img.png',
        'itemId': 2,
        'description': 'some item description',
        'disclaimer': 'some item disclaimer'
      })
    }),
    start: '2016-12-25T12:30:41.170001',
    is_watching: false
  }),
  active: null,
  joined: null,
  ended: null
})

// missing key: minPrice
const DATA_INVALID_AUCTION = Object.freeze({
  upcoming: Object.freeze({
    id: 3,
    reward: Object.freeze({
      type: 'item',
      data: Object.freeze({
        'name': 'some item name',
        'image': 'path/to/img.png',
        'itemId': 2,
        'description': 'some item description',
        'disclaimer': 'some item disclaimer'
      })
    }),
    start: '2016-12-25T12:30:41.170001'
  }),
  active: null,
  joined: null,
  ended: null
})

const fetchValidTestMain = function (t) {
  stubs.endpoints.fetch['api/auction/auctions']
        .set([DATA_VALID_AUCTION])
  stubs.endpoints.fetch['api/time']
        .set({
          now: '2016-12-07T03:10:07.208623'
        })
  Promise.resolve().then(function () {
    return fetchUpcoming()
  }).then(function (res) {
    t.ok(res, "got result from upcoming fetch")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const fetchValidTest = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("test", fetchValidTestMain)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const fetchInvalidTestMain = function (t) {
  stubs.endpoints.fetch['api/auction/auctions']
        .set([DATA_INVALID_AUCTION])
  stubs.endpoints.fetch['api/time']
        .set({
          now: '2016-12-25T12:10:41.170001'
        })
  Promise.resolve().then(function () {
    return fetchUpcoming()
  }).then(function (res) {
    t.notOk(res, "got result from upcoming fetch")
    t.end()
  }).catch(function (err) {
    t.ok(err, "got error from invalid fetch")
    t.end()
  })
}

const fetchInvalidTest = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("test", fetchInvalidTestMain)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

var testsMain = function (t) {
  t.test("valid data", fetchValidTest)
  t.test("invalid data", fetchInvalidTest)
  t.end()
}

var exports = testsMain
module.exports = exports
