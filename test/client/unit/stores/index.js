/* global require, module */

var testsMain = function (t) {
  t.test(("auction container store " +
    "'terribad' name for the home page store"),
  require('./auction_container_store'))
  t.end()
}

var exports = testsMain
module.exports = exports
