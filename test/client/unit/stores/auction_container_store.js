/* global require, module */

const acs = require(
    '../../../../client/app/stores/auction_container_store')
const home_actions = require(
    '../../../../client/app/actions/home_actions')
const apa = require(
    '../../../../client/app/actions/all_pages_actions')

const DATA_USER = {
  credit: 500,
  first_name: 'bob',
  id: 1,
  last_name: "belcher",
  username: "bburgers",
  avatar: null,
  email: "b@b.org"
}

const CONFIG = {
  starLevels: {
    silver: 100,
    gold: 500
  }
}

apa.updateUserData(DATA_USER)
home_actions.setConfig(CONFIG)

const sampleTest = function (t) {
  t.equal(acs.getState(), null,
    ("auction container store returns " +
      "null state before initialization"))
    // 'state' param below is the return value of
    // (in current impl) _stateForComp
    // acs.listen(function (state) {});
  t.equal(acs.getState(), null,
    ("auction container store returns " +
      "null state after partial (user-only) " +
      "initialization"))

  home_actions.updateAllAuctions({
    updatedAuctions: [],
    inits: [
      'endedAuctions',
      'joinedAuctions',
      'activeAuctions',
      'upcomingAuctions'
    ]
  })

  let auctions = acs.getState().auctions

  t.ok(auctions.activeAuctions, "state contains all live auctions")
  t.ok(auctions.allAuctions, "state contains all live and upcoming auctions")
  t.ok(auctions.joinedAuctions, "state contains all joined auctions")

  home_actions.updateAllAuctions({
    updatedAuctions: require("../data/auctions.js").map(function (auction) { return {data: auction} }),
    inits: [
      'endedAuctions',
      'joinedAuctions',
      'activeAuctions',
      'upcomingAuctions'
    ]
  })
  auctions = acs.getState().auctions
  t.ok(auctions.allAuctions[0].level, "First Auction contains a level")
  t.ok(Array.isArray(auctions.allAuctions), "allAuctions is supposed to be an array")
  t.ok(Array.isArray(auctions.activeAuctions), "activeAuctions is supposed to be an array")

  t.ok(Array.isArray(auctions.joinedAuctions), "joinedAuctions is supposed to be an array")
  t.equal(auctions.allAuctions.length, 12, "12 auctions in test data")

  t.end()
}

const testsMain = function (t) {
  t.test("demo", sampleTest)
}

var exports = testsMain
module.exports = exports
