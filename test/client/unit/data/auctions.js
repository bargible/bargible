function generateAuction (status, level) {
  let minPrice = 10
  switch (level) {
    case 'gold':
      minPrice = 600
      break
    case 'silver':
      minPrice = 200
      break
    default:
      break
  }
  return {
    "status": status,
    "bidPrice": 1,
    "minPrice": minPrice,
    "id": 1,
    "numUsers": 0,
    "intermediateTimer": 529,
    "requiredCredits": 50,
    "reward": {
      "type": "item",
      "data": {
        "itemId": 3,
        "image": "static/img/home-depot-gift-card.png",
        "description": "This is an amazing test Home Depot card",
        "disclaimer": "This is a disclaimer",
        "name": "Test Home Depot Gift Card"
      }
    },
    "rewardType": "item",
    "timeRemaining": Math.floor(Math.random() * 10000)
  }
}

module.exports = [
  generateAuction('active'),
  generateAuction('active'),
  generateAuction('upcoming'),
  generateAuction('upcoming'),
  generateAuction('active', 'silver'),
  generateAuction('active', 'silver'),
  generateAuction('upcoming', 'silver'),
  generateAuction('upcoming', 'silver'),
  generateAuction('active', 'gold'),
  generateAuction('active', 'gold'),
  generateAuction('upcoming', 'gold'),
  generateAuction('upcoming', 'gold')
]
