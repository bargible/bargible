/* global require, module */

var testsMain = function (t) {
  t.test("sources", require('./sources'))
  t.test("stores", require('./stores'))
  t.test("components", require('./components'))
  t.end()
}

var exports = testsMain
module.exports = exports
