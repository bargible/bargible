/* global require, module, process */

const tape = require('tape')
const tapSpec = require('tap-spec')

const util = require('./util')

const makeAllTests = function () {
  return function (t) {
    t.test("unit", require('./unit'))
    t.test("integration", require('./int'))
    t.end()
  }
}

const runTests = function () {
  util.installRequireJsx()
  util.installRequireRt()
  util.installDomImpl()
  return new Promise(function (resolve, reject) {
    tape.createStream()
            .pipe(tapSpec())
            .pipe(process.stdout)
    tape.test("all tests for this run", makeAllTests())
    tape.onFinish(function () {
      resolve()
    })
  })
}

var exports = {}

exports.runTests = runTests

module.exports = exports
