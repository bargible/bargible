/* global require, module */

const ModalCompObj = require('./base_modal')

const ModalAuctionPayConfirm = function () {
  ModalCompObj.apply(this, arguments)
}
ModalAuctionPayConfirm.prototype = Object.create(ModalCompObj.prototype)

ModalAuctionPayConfirm.prototype.clickClose = function () {
  this.click('button.redeem-box')
}

var exports = ModalAuctionPayConfirm
module.exports = exports
