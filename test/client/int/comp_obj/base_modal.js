/* global require, module, $ */

const CompObj = require('./base')

const ModalCompObj = function () {
  this._elementIdStr = 'modal-wrapper'
  CompObj.apply(this, arguments)
}

ModalCompObj.prototype = Object.create(CompObj.prototype)

ModalCompObj.prototype.clickClose = function () {
  $('#btn-modal-close').click()
}

var exports = ModalCompObj
module.exports = exports
