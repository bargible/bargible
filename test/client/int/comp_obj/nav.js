/* global require, module, $ */

const _ = require('lodash')

const CompObj = require('./base')
/** abstract markdown/logic connections */
const LINK_CONSTS = require('../mocks/const.js').NAV_LINK_CONSTS

var Nav = function () {
  this._elementIdStr = 'navbar'
  CompObj.apply(this, arguments)
}
Nav.prototype = Object.create(CompObj.prototype)

Nav.prototype.greetingText = function () {
  return this.$el.find('li.dropdown .avatar-label').text()
}

Nav.prototype.creditValStr = function () {
  return this.getText('ul.navbar-right li.attr.credit p.val')
}

Nav.prototype.greetingName = function () {
  var greetingText = this.greetingText().trim()
  var matchArr = greetingText.match(/^([a-zA-Z]*)/)
  if (matchArr === null) {
    throw new Error("greeting text '" + greetingText +
                        "' does not match expected pattern")
  }
  return matchArr[1]
}

Nav.prototype.links = function () {
  var $linkAnchors = this.$el.find('a').toArray()
            .map(function (el_a) { return $(el_a) })
  var linksByText = _.zipObject(
        $linkAnchors.map(function ($a) { return $a.text().trim() }),
        $linkAnchors.map(function ($a) { return $a.attr('href') }))
  return _.mapKeys(linksByText, function (val, key) {
        // there is no link text for home so its key is ''
    var linkName = LINK_CONSTS[key === '' ? 'home' : key]
    if (linkName === undefined) {
      throw new Error("unknown link text '" + key + "' value: " + val)
    }
    return linkName
  })
}

var exports = Nav
module.exports = exports
