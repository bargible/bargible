/* global require, module, $ */

const _ = require('lodash')

const CompObj = require('./base')

var UserProfileMain = function () {
  this._elementIdStr = 'app'
  CompObj.apply(this, arguments)
}
UserProfileMain.prototype = Object.create(CompObj.prototype)

UserProfileMain.prototype.headerText = function () {
  return this.getText('h1')
}

UserProfileMain.prototype.stats = function () {
  var statsTexts = {
    wins: this.getText('.stats .stat.wins .value'),
    numAuctions: this.getText('.stats .stat.num-auctions .value')
  }
  var stats = _.mapValues(statsTexts, function (val, key) {
    var parsedVal = parseInt(val.trim())
    if (Number.isNaN(parsedVal)) {
      throw new Error("couldn't parse value " + val.trim() +
                            " for stat " + key + " to an int")
    }
    return parsedVal
  })
  return stats
}

var _parseElHistory = function (el) {
  var elParsText = $(el).find('p').toArray().map(function (p) {
    return $(p).text()
  })
  return _.zipObject([
    'startDate',
    'endTime',
    'realPrice',
    'position'
  ], elParsText)
}

UserProfileMain.prototype.history = function () {
  return (this.$el
            .find('.history .history-list .auction-column .list-item')
            .toArray()
            .map(_parseElHistory))
}

var exports = UserProfileMain
module.exports = exports
