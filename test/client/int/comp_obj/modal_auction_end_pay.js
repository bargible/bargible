/* global require, module */

const ModalCompObj = require('./base_modal')

const ModalAuctionEndPay = function () {
  ModalCompObj.apply(this, arguments)
}
ModalAuctionEndPay.prototype = Object.create(ModalCompObj.prototype)

ModalAuctionEndPay.prototype.clickPay = function () {
  this.click('button.redeem-box')
}

var exports = ModalAuctionEndPay
module.exports = exports
