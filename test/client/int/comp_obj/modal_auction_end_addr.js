/* global require, module */

const _ = require('lodash')

const ModalCompObj = require('./base_modal')

const ModalAuctionEndAddr = function () {
  ModalCompObj.apply(this, arguments)
}
ModalAuctionEndAddr.prototype = Object.create(ModalCompObj.prototype)

/** field name: the state.formFields variable name */
ModalAuctionEndAddr.prototype.inputAddrField = function (
    fieldName, fieldVal
) {
  const formFieldsOrder = [
    'name',
    'addr_1',
    'addr_2',
    'city',
    'zip_code'
  ]
  const elsInput = this.$el.find('input').toArray()
  if (!_.isEqual(_.size(elsInput), _.size(formFieldsOrder))) {
    throw new Error("unexpected number of input elements: " +
                        _.size(formFieldsOrder) + ", not " +
                        _.size(elsInput))
  }
  const formFields = _.fromPairs(_.zip(formFieldsOrder, elsInput))
  if (_.isEqual(fieldName, 'usa_state')) {
    this.inputValue('select', fieldVal)
  } else if (_.isUndefined(formFields[fieldName])) {
    throw new Error("unknown field name for address input: " +
                        fieldName)
  } else {
    this.inputValue(formFields[fieldName], fieldVal)
  }
}

ModalAuctionEndAddr.prototype.clickNext = function () {
  this.click('button.redeem-box')
}

var exports = ModalAuctionEndAddr
module.exports = exports
