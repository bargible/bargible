/* global require, module */

const ModalCompObj = require('./base_modal')

const ModalWatchConfirm = function () {
  ModalCompObj.apply(this, arguments)
}
ModalWatchConfirm.prototype = Object.create(ModalCompObj.prototype)

ModalWatchConfirm.prototype.getTitleText = function () {
  return this.getText('span.confirm-title').trim()
}

ModalWatchConfirm.prototype.getMsgText = function () {
  return this.getText('span.confirm-msg').trim()
}

ModalWatchConfirm.prototype.clickClose = function () {
  this.click('button')
}

var exports = ModalWatchConfirm
module.exports = exports
