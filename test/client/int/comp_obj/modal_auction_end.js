/* global require, module */

const ModalCompObj = require('./base_modal')

const ModalAuctionEnd = function () {
  ModalCompObj.apply(this, arguments)
}

ModalAuctionEnd.prototype = Object.create(ModalCompObj.prototype)

ModalAuctionEnd.prototype.clickPay = function () {
  this.click('button.redeem-box')
}

var exports = ModalAuctionEnd
module.exports = exports
