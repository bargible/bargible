/* global require, module */

const CompObj = require('./base')

var IndexMain = function () {
  this._elementIdStr = 'app'
  CompObj.apply(this, arguments)
}
IndexMain.prototype = Object.create(CompObj.prototype)

IndexMain.prototype.hasAuctions = function (auctionId) {
  return this.hasEl('.auction-active')
}

var exports = IndexMain
module.exports = exports
