/* global require, module, $ */

const _ = require('lodash')
const ReactDOM = require('react-dom')
const TestUtils = require('react-addons-test-utils')

/**
 * a "component object" is the integration test's view of a React
 * component.  this is the (abstract) base 'class' for all
 * component objects.
 *
 * there are two cases when the test setup needs to access
 * information about a React component:
 * - the test setup may render a component into the DOM itself,
 *   in which case, the component object class is called with
 *   a React Element parameter
 *   <https://facebook.github.io/react/docs/react-api.html>
 *   (as in test setup functions)
 * - the application may render a component into the DOM,
 *   in which case, the component object class is called with
 *   no parameters
 *   (modals, for example, aren't currently rendered by
 *    the top-level `page_*.js` modules or corresponding
 *    setup functions)
 * in either case, derived classes need to define
 * `_elementIdStr`, the id attr of the DOM node being
 * rendered _into_ (as opposed to the root DOM node
 * of the React element itself)
 */
var CompObj = function (reactElement) {
  if (!this._elementIdStr) {
    throw new Error("component object classes must define " +
                         "`this._elementIdStr` before calling " +
                         "base constructor")
  }
  var domNode = document.getElementById(this._elementIdStr)
  if (!domNode) {
    throw new Error("CompObj didn't find dom node with id " +
                         +this._elementIdStr +
                         "check installDomImpl in client test utils")
  }
  if (reactElement) {
    var compRef = ReactDOM.render(
            reactElement,
            domNode)
    this.el = ReactDOM.findDOMNode(compRef)
    this._renderedReactElement = true
  } else {
    if ($(domNode).children().length !== 1) {
      console.error("found " + $(domNode).children().length +
                          "child elements for dom node " +
                          domNode.toString())
      throw new Error(
                "expected exactly one child el (the already-rendered " +
                    "React element) when initializating a component " +
                    "object directly from the DOM")
    }
    this.el = $(domNode).children()[0]
    this._renderedReactElement = false
  }
  this.$el = $(this.el)
}

CompObj.prototype.unmount = function () {
  if (!this._renderedReactElement) {
    throw new Error("this component object wasn't " +
                        "rendered by the test setup")
  }
  var container = this.el.parentElement
  if (!ReactDOM.unmountComponentAtNode(container)) {
    throw new Error("didn't find react component to unmount")
  }
}

CompObj.prototype._clickCssSel = function (cssSel) {
  var elSel = this.$el.find(cssSel).toArray()[0]
  if (elSel === undefined) {
    throw new Error("couldn't find element for css selector '" +
                        cssSel + "'")
  }
  TestUtils.Simulate.click(elSel)
}

CompObj.prototype.click = function (arg) {
  if (typeof arg === 'string') {
    this._clickCssSel(arg)
  } else {
    TestUtils.Simulate.click(arg)
  }
}

CompObj.prototype.hasEl = function (cssSel) {
  var elSel = this.$el.find(cssSel).toArray()[0]
  return elSel !== undefined
}

CompObj.prototype.getTexts = function (cssSel) {
  var elsSel = this.$el.find(cssSel).toArray()
  if (elsSel.length === 0) {
    throw new Error("couldn't find element for css selector '" +
                        cssSel + "'")
  }
  return elsSel.map(function (elSel) {
    return $(elSel).text()
  })
}

CompObj.prototype.getText = function (cssSel) {
  return this.getTexts(cssSel)[0]
}

CompObj.prototype.inputValue = function (arg, strInput) {
  const elSel = (_.isString(arg)
                   ? this.$el.find(arg).toArray()[0]
                   : arg)
  if (_.isString(arg) && _.isUndefined(elSel)) {
    throw new Error("couldn't find element for css selector '" +
                        arg + "'")
  }
  elSel.value = strInput
  TestUtils.Simulate.change(elSel)
}

var exports = CompObj
module.exports = exports
