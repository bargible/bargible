/**
 * component objects
 * similar to page-object pattern, abstracts content from markdown.
 * additionally, handles interface to react test utilities
 */

/* global require, module, $ */

const ReactDOM = require('react-dom')

const ModalAuctionEnd = require('./modal_auction_end')
const ModalAuctionEndAddr = require('./modal_auction_end_addr')
const ModalAuctionEndPay = require('./modal_auction_end_pay')
const ModalAuctionPayConfirm = require('./modal_auction_pay_confirm')
const ModalBuyCredits = require('./modal_buy_credits')
const ModalWatchConfirm = require('./modal_watch_confirm')

const _modalContentId = 'modal-content'
const _innerHTMLModalLoading = document
          .getElementById(_modalContentId)
          .innerHTML

const getModalCompObj = function () {
  const $modalContainer = $('#' + _modalContentId)
  if ($modalContainer.toArray().length !== 1) {
    throw new Error("expected exactly one 'modal-content' id")
  }
  if ($modalContainer.children().length !== 1) {
    throw new Error("expected precisely one child elem in " +
                        "modal container")
  }
  const $modalContainerFirstChild = $modalContainer.children()[0]
  if ($modalContainerFirstChild.nodeName === 'SPAN') {
    return undefined
  } else if ($modalContainerFirstChild.nodeName !== 'DIV') {
    throw new Error("expected loading element (span) or " +
                        "div in modal content")
  } else {
    const idAttr = $modalContainerFirstChild.getAttribute('id')
    if (!idAttr) {
      throw new Error("modal outer div doesn't have id attr")
    }
    if (idAttr === 'buy-credits') {
      return new ModalBuyCredits()
    } else if (idAttr === 'modal-auction-end') {
      return new ModalAuctionEnd()
    } else if (idAttr === 'auction-end-addr') {
      return new ModalAuctionEndAddr()
    } else if (idAttr === 'auction-end-pay') {
      return new ModalAuctionEndPay()
    } else if (idAttr === 'auction-pay-confirm') {
      return new ModalAuctionPayConfirm()
    } else if (idAttr === 'watch-confirm') {
      return new ModalWatchConfirm()
    } else {
      throw new Error("unknown id attr on modal outer div: " +
                            idAttr)
    }
  }
}

const maybeUnmountModal = function () {
  const elModalContent = document.getElementById(_modalContentId)
  ReactDOM.unmountComponentAtNode(elModalContent)
  elModalContent.innerHTML = _innerHTMLModalLoading
}

var exports = {}

exports.Nav = require('./nav')
exports.AuctionContainer = require('./auction_container')
exports.UserProfileMain = require('./main_user_profile')
exports.IndexMain = require('./main_index')
exports.ModalBuyCredits = ModalBuyCredits
exports.ModalAuctionEnd = ModalAuctionEnd
exports.ModalAuctionEndPay = ModalAuctionEndPay
exports.ModalAuctionPayConfirm = ModalAuctionPayConfirm
exports.ModalAuctionEndAddr = ModalAuctionEndAddr
exports.ModalWatchConfirm = ModalWatchConfirm

exports.getModalCompObj = getModalCompObj
exports.maybeUnmountModal = maybeUnmountModal

module.exports = exports
