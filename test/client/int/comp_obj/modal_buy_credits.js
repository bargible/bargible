/* global require, module */

const ModalCompObj = require('./base_modal')

const ModalBuyCredits = function () {
  ModalCompObj.apply(this, arguments)
}
ModalBuyCredits.prototype = Object.create(ModalCompObj.prototype)

ModalBuyCredits.prototype.getMessage = function () {
  return this.getText('div.caption').trim()
}

var exports = ModalBuyCredits
module.exports = exports
