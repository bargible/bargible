/* global require, module */

const _ = require('lodash')
const CompObj = require('./base')

// todo: "inner classes" for tabs, etc.

var AuctionContainer = function () {
  this._elementIdStr = 'app'
  CompObj.apply(this, arguments)
}
AuctionContainer.prototype = Object.create(CompObj.prototype)

AuctionContainer.prototype.selectTab = function (tabName) {
  const TAB_SELECTORS = {
    upcoming: '.upcoming-tab',
    active: '.live-tab',
    joined: '.joined-tab',
    default: '.default-tab'
  }
  this.click(TAB_SELECTORS[tabName])
}

AuctionContainer.prototype.getSelectedTabName = function () {
  var self = this
  const displayStatus = _.mapValues({
    upcoming: 'ul.auctions-upcoming',
    active: 'ul.auctions-active',
    joined: 'div.auctions-joined',
    default: 'div.auctions-default'
  }, function (cssSel) {
    return self.hasEl(cssSel)
  })
  const numTabs = _(displayStatus).values().filter().size()
  if (numTabs !== 1) {
    console.error(displayStatus)
    throw new Error("expected exactly one selected tab " +
                        "(truthy value)")
  }
  return _.invert(displayStatus)[true]
}

AuctionContainer.prototype.buyCredits = function () {
  var cssSel = [
    '.buy-box'].join(' ')
  this.click(cssSel)
}

AuctionContainer.prototype.joinAuction = function (auctionId) {
  var cssSel = [
    '#auction-active[data-auction-id="' + auctionId + '"]',
    '.join-button'].join(' ')
  this.click(cssSel)
}

AuctionContainer.prototype.joinConfirm = function (auctionId) {
  var cssSel = [
    '#auction-active[data-auction-id="' + auctionId + '"]',
    '.confirm-box',
    'button.confirm'].join(' ')
  this.click(cssSel)
}

AuctionContainer.prototype.joinCancel = function (auctionId) {
  var cssSel = [
    '#auction-active[data-auction-id="' + auctionId + '"]',
    '.confirm-box',
    'button.cancel'].join(' ')
  this.click(cssSel)
}

AuctionContainer.prototype.hasJoinedAuctionEls = function (auctionId) {
  return this.hasEl('#auction-joined[data-auction-id="' +
                      auctionId + '"]')
}

AuctionContainer.prototype.hasActiveAuctionEls = function (auctionId) {
  console.log(auctionId)
  return this.hasEl('#auction-active[data-auction-id="' +
                      auctionId + '"]')
}

AuctionContainer.prototype.bid = function (auctionId) {
  var cssSel = [
    '#auction-joined[data-auction-id="' + auctionId + '"]',
    '.bid-button',
    'button'].join(' ')
  this.click(cssSel)
}

AuctionContainer.prototype.steal = function (auctionId) {
  var cssSel = [
    '#auction-joined[data-auction-id="' + auctionId + '"]',
    '.game-elements', 'button.steal'].join(' ')
  this.click(cssSel)
}

AuctionContainer.prototype.destroy = function (auctionId) {
  var cssSel = [
    '#auction-joined[data-auction-id="' + auctionId + '"]',
    '.game-elements', 'button.destroy'].join(' ')
  this.click(cssSel)
}

AuctionContainer.prototype.freeze = function (auctionId) {
  var cssSel = [
    '#auction-joined[data-auction-id="' + auctionId + '"]',
    '.game-elements', 'button.freeze'].join(' ')
  this.click(cssSel)
}

/** check for whatever visual element indicates bids are frozen */
AuctionContainer.prototype.isFreezeVisual = function (auctionId) {
  var cssSel = [
    '#auction-joined[data-auction-id="' + auctionId + '"]',
    '.bids', '.bid-button', '.overlay-frozen'].join(' ')
  var elOverlayFrozen = this.$el.find(cssSel).toArray()[0]
  return !!elOverlayFrozen
}

AuctionContainer.prototype.notificatonTexts = function (auctionId) {
  var cssSel = [
    '#auction-joined[data-auction-id="' + auctionId + '"]',
    '.update-texts', 'ul.notifications', 'li'].join(' ')
  return this.getTexts(cssSel).map(function (str) {
    return str.trim()
  })
}

AuctionContainer.prototype.numBids = function (auctionId) {
  var cssSel = [
    '#auction-joined[data-auction-id="' + auctionId + '"]',
    'section.bids', '.stat.bids-left', 'span.val'].join(' ')
  var text = this.getText(cssSel)
  return parseInt(text)
}

const _parseStrPrice = function (strPrice) {
  var matchArrCurrency = strPrice
            .match(/^(\d+\.\d\d)$/)
  if (matchArrCurrency !== null) {
    return {
      val: parseFloat(matchArrCurrency[1]),
      type: 'currency'
    }
  }
  var matchArrCredit = strPrice
            .match(/^(\d+) Credits?$/)
  if (matchArrCredit !== null) {
    return {
      val: parseInt(matchArrCredit[1]),
      type: 'credit'
    }
  }
  throw new Error("bid price string '" +
                    strPrice +
                    "' does not match any expected pattern")
}

AuctionContainer.prototype.bidPrice = function (auctionId) {
  var cssSel = [
    '#auction-joined[data-auction-id="' + auctionId + '"]',
    'section.bids .stat.bid-price', 'span.val', 'span.price'].join(' ')
  var text = this.getText(cssSel).trim()
  var parseRes = _parseStrPrice(text)
  if (!(parseRes.type === 'credit' || parseRes.type === 'currency')) {
    throw new Error("unexpected bid price type")
  }
  return parseRes.val
}

AuctionContainer.prototype.rewardImgUrls = function () {
  var els_auction = this.$el.find('div.auction-node').toArray()
  console.log(els_auction)
  throw new Error("unimplemented")
}

const CardUpcoming = function (auctionId) {
  this._elementIdStr = 'upcoming-auction-' + auctionId
  CompObj.call(this)
}
CardUpcoming.prototype = Object.create(CompObj.prototype)

CardUpcoming.prototype._getPriceAndRewardNameStrs = function () {
  const strTitle = this.getText('span.title')
  const regexpTitle = /(.*)(\$\d+\.\d\d) (.*)/
  const matchTitle = strTitle.match(regexpTitle)
  if (_.isNull(matchTitle)) {
    throw new Error("upcoming card title doesn't match regexp")
  }
  return [matchTitle[2], matchTitle[3]]
}

CardUpcoming.prototype.getRewardNameStr = function () {
  const [__, rewardName] = this._getPriceAndRewardNameStrs()
  console.log(__)
  return rewardName
}

CardUpcoming.prototype.isNotWatching = function () {
  return (this.hasEl('span.watch-caption') &&
            (_.isEqual(this.getText('span.watch-caption').trim(),
                       'WATCH')))
}

CardUpcoming.prototype.isWatchLoading = function () {
  return (this.hasEl('span.watch-caption') &&
            (_.isEqual(this.getText('span.watch-caption').trim(),
                       'LOADING')))
}

CardUpcoming.prototype.isWatching = function () {
  return (this.hasEl('span.watch-caption') &&
            (_.isEqual(this.getText('span.watch-caption').trim(),
                       'WATCHING')))
}

CardUpcoming.prototype.clickWatch = function () {
  this.click('div.watch')
}

AuctionContainer.prototype.getCardUpcoming = function (auctionId) {
  const cardUpcoming = new CardUpcoming(auctionId)
  return cardUpcoming
}

var exports = AuctionContainer
module.exports = exports
