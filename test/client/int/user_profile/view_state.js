/* global require, module, setTimeout */

const stubs = require('../stubs')
const setupTeardown = require('./setup_teardown')

const TIMEOUT_UPDATE_DOM = 100 // ms

const DATA_USER = {
  credit: 500,
  first_name: 'bob',
  id: 1,
  last_name: "belcher",
  username: "bburgers",
  avatar: null,
  email: "b@b.org"
}

const DATA_AUCTION_HISTORY = [
  {
    upcoming: null,
    active: null,
    joined: null,
    ended: {
      "id": 1,
      "reward": {
        "type": "item",
        "data": {
          "itemId": 2,
          "image": "static/img/home-depot-gift-card.png",
          "description": "This is amazing card",
          "disclaimer": "Disclaim!",
          "name": "Test Home Depot Gift Card"
        }
      },
      "start": "2016-09-03T05:27:08.963231",
      "end": "2016-09-03T07:15:48.164525",
      "realPrice": 1000,
      "position": 1,
      "lastBidUsername": "bob",
      "minPrice": 750,
      "bidPrice": 3
    }
  }
]

/** corresponds to the return value of UserProfileMain.history
TODO: unused?
const EXPECTED_AUCTION_HISTORY = _(
    DATA_AUCTION_HISTORY
).map('history').map(function (
    auctionHistory
) {
  var parsedConst = _(auctionHistory).omit('reward').mapValues(
        function (val, key) {
          if (_.includes([
            'realPrice',
            'minPrice'
          ], key)) {
            return '$' + (val / 100).toFixed(2)
          }
          if (_.includes([
            'start',
            'end'
          ], key)) {
            return moment.utc(val)
          }
          return val
        }).value() // "unwrap" the lodash chainable
  return {
    position: "Position: " + parsedConst.position,
    realPrice: parsedConst.realPrice,
    startDate:
        moment(parsedConst.start.toDate())
            .format('dddd, MMMM Do YYYY'),
    endTime:
        moment(parsedConst.end.toDate())
            .format('h:mmA')
  }
})
*/

/** creates a timeout for use with promise chaining */
const dumbTimeout = function (msDur) {
  return function () {
    return new Promise(function (resolve, reject) {
      setTimeout(resolve, msDur)
    })
  }
}

const readTestMain = function (t) {
  stubs.endpoints.fetch['api/user/get']
        .set(DATA_USER)
  stubs.endpoints.fetch['api/user/auctions']
        .set(DATA_AUCTION_HISTORY)
  Promise.resolve().then(function () {
    setupTeardown.AllPagesActions.fetchUser()
    setupTeardown.UserProfileActions.fetchEndedAuctions()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM * 3)).then(function () {
    const objUserProfile = setupTeardown.getObjUserProfile()
    var stats = objUserProfile.stats()
    t.pass("parsed stats without error")
    t.equal(stats.wins, 1, // reflect sample data
                "correct value for wins stat")
    t.equal(stats.numAuctions, 1, // reflect sample data
                "number of auctions participated in stat")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const readTest = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("test", readTestMain)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const testsMain = function (t) {
  t.test("read state", readTest)
  t.end()
}

var exports = testsMain

module.exports = exports
