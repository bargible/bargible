/* global require, module */

const _ = require('lodash')
const proxyquire = require('proxyquire')
var React = require('react')

const stubs = require('../stubs')
const compObj = require('../comp_obj')

// clear node cache of all application modules before proxyquring
// so that proxying a different transitive dep from preceeding tests
// can be achieved without a global override as described in
// the int/home/index.js tests
_.forEach(_.keys(require.cache), function (moduleName) {
  if (_.includes(moduleName, 'client/app')) {
    delete require.cache[moduleName]
  }
})

proxyquire(
    '../../../../client/app/util',
  {
    './http': stubs.http
  }
)

const AllPagesActions = require(
    '../../../../client/app/actions/all_pages_actions')
const UserProfileActions = require(
    '../../../../client/app/actions/user_profile_actions')

const UserProfileStore = require(
    '../../../../client/app/stores/user_profile_store')
const UserProfileMain = require(
    '../../../../client/app/comp/main_user_profile')

const alt = require('../../../../client/app/alt')

// per-test variables
var objUserProfile

var renderUserProfile = function (storeState) {
  if (_.isNull(storeState)) {
    return
  }
  objUserProfile = new compObj.UserProfileMain(
        React.createElement(UserProfileMain, storeState))
}

const setup = function (t) {
  renderUserProfile(UserProfileStore.getState())
  UserProfileStore.listen(renderUserProfile)
  t.end()
}

const teardown = function (t) {
  UserProfileStore.unlisten(renderUserProfile)
  objUserProfile.unmount()
  alt.recycle()
  stubs.reset()
  objUserProfile = undefined
  t.end()
}

var exports = {}

exports.AllPagesActions = AllPagesActions
exports.UserProfileActions = UserProfileActions
exports.setup = setup
exports.teardown = teardown
exports.getObjUserProfile = function () {
  return objUserProfile
}

module.exports = exports
