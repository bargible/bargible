/* global require, module */

const testsMain = function (t) {
  t.test("view page state", require('./view_state'))
  t.end()
}

var exports = testsMain

module.exports = exports
