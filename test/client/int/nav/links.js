/* global require, module */

const _ = require('lodash')

const setupTeardown = require('./setup_teardown')

const NAV_LINKS = require('./nav_links.json')

const linksTestMain = function (t) {
  t.deepEqual(_(setupTeardown.getObjNav().links())
                .values().filter().sortBy().value(),
                _(NAV_LINKS)
                .values().sortBy().value(),
                "found expected links")
  t.end()
}

const linksTest = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("test", linksTestMain)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const testsMain = function (t) {
  t.test("test links", linksTest)
  t.end()
}

var exports = testsMain

module.exports = exports
