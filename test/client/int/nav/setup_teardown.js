/* global require, module, setTimeout */

const _ = require('lodash')
const proxyquire = require('proxyquire')
var React = require('react')

const stubs = require('../stubs')
const compObj = require('../comp_obj')

// see comment in ../user_profile/setup_teardown.js
_.forEach(_.keys(require.cache), function (moduleName) {
  if (_.includes(moduleName, 'client/app')) {
    delete require.cache[moduleName]
  }
})

proxyquire(
    '../../../../client/app/util',
  {
    './http': stubs.http
  }
)

const NavNode = require(
    '../../../../client/app/comp/nav')
const alt = require('../../../../client/app/alt')

const NAV_LINKS = require('./nav_links.json')

// per-test variables
var objNav

const setup = function (t) {
  objNav = new compObj.Nav(
        React.createElement(NavNode, {navLinks: NAV_LINKS}))
  t.end()
}

const teardown = function (t) {
  objNav.unmount()
  alt.recycle()
  stubs.reset()
  objNav = undefined
  t.end()
}

var exports = {}

exports.setup = setup
exports.teardown = teardown
exports.getObjNav = function () {
  return objNav
}

module.exports = exports
