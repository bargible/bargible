/* global require, module, setTimeout */

const testsMain = function (t) {
  t.test("test links", require('./links'))
  t.end()
}

var exports = testsMain

module.exports = exports
