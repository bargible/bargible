/* global require, module */

var testsMain = function (t) {
  t.test("home page", require('./home'))
  t.test("navbar", require('./nav'))
  t.test("user profile", require('./user_profile'))
    // t.test("landing page", require('./landing'));
  t.skip("landing page")
}

var exports = testsMain
module.exports = exports
