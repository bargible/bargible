/* global require, module, process */

var _ = require('lodash')
var simple = require('simple-mock')

var user_source = require('../user_source')
var auctionsSource = require('../auctions_source')
var auctionHistorySource = require('../auction_history_source')

/**
 * duplicate custom error class since error typechecking
 * uses `err.name` rather than `err instanceof HttpError`
 */
function HttpError (body, statusCode) {
  this.name = 'HttpError'
  this.body = body
  this.statusCode = statusCode
  this.stack = (new Error()).stack
}
HttpError.prototype = Object.create(Error.prototype)
HttpError.prototype.constructor = HttpError

const INST_ATTRS_JOINED_AUCTION = [
  'bids',
  'isBidsFrozen',
  'secsFreezeRemaining',
  'position',
  'leaderboard',
  'auctionId',
  'bidsTilActivation',
  'numActivations'
]

/* TODO: unused?
var _reverseMutateAuction = function (auction) {
  delete auction.joinedAuction
}
*/

var fetchJson = simple.mock(function (strUri) {
  var endpointId
  if (strUri === 'api/user/get') {
    return Promise.resolve().then(function () {
      return user_source.fetchCurr()
    })
  } else if (strUri === 'api/auction/upcomingAuctions') {
    return Promise.resolve([])
  } else if (strUri === 'api/auction/auctions') {
    return Promise.resolve().then(function () {
      return auctionsSource.fetchAll()
    }).then(function (auctions) {
      return _(auctions).map(function (auction) {
        _.unset(auction, 'joinedAuction')
        return _.pick(auction, [
          'id',
          'reward',
          'rewardType',
          'timeRemaining',
          'numUsers',
          'bidPrice',
          'minPrice',
          'requiredCredits',
          'intermediateTimer'
        ])
      }).valueOf()
    })
  } else if (strUri === 'api/user/joinedAuctions') {
    return Promise.resolve().then(function () {
      return auctionsSource.fetchAll()
    }).then(function (auctions) {
      return _(auctions).map('joinedAuction')
                .filter(/*remove undefined*/)
                .map(function (auction) {
                  return _.pick(auction, INST_ATTRS_JOINED_AUCTION)
                })
                .valueOf()
    })
  } else if (strUri.match(/^api\/auction\/auctions\/(\d+)$/)) {
    endpointId = parseInt(strUri.match(
                /^api\/auction\/auctions\/(\d+)$/)[1])
    return Promise.resolve().then(function () {
      return auctionsSource.fetchAll()
    }).then(function (auctions) {
      return _(auctions).map(function (auction) {
        _.unset(auction, 'joinedAuction')
        return _.pick(auction, [
          'id',
          'reward',
          'rewardType',
          'timeRemaining',
          'numUsers',
          'intermediateTimer',
          'bidPrice',
          'minPrice'
        ])
      }).find({'id': endpointId}).valueOf()
    })
  } else if (strUri.match(/^api\/user\/joinedAuctions\/(\d+)$/)) {
    endpointId = parseInt(strUri.match(
                /^api\/user\/joinedAuctions\/(\d+)$/)[1])
    return Promise.resolve().then(function () {
      return auctionsSource.fetchAll()
    }).then(function (auctions) {
      var auction = _.find(auctions, {'id': endpointId})
      if (!auction.joinedAuction) {
        throw new Error("auction id " + endpointId + " not joined")
      }
      return _.pick(auction.joinedAuction,
                          INST_ATTRS_JOINED_AUCTION)
    })
  } else if (strUri === 'api/user/auctionHistory') {
    return Promise.resolve().then(function () {
      return auctionHistorySource.fetchUserAuctionHistories()
    })
  } else if (strUri.match(/^api\/user\/endedAuctions\/(\d+)$/)) {
    endpointId = parseInt(strUri.match(
                /^api\/user\/endedAuctions\/(\d+)$/)[1])
    return auctionsSource.fetchEnded()
  } else {
    throw new Error("mock fetchJson got unrecognized uri: " +
                        strUri)
  }
})

var createJson = simple.mock(function (strUri, json) {
  var endpointId
  if (strUri.match(/^api\/auction\/(\d+)\/join$/)) {
    endpointId = parseInt(strUri.match(
                /^api\/auction\/(\d+)\/join$/)[1])
    return Promise.resolve().then(function () {
      return auctionsSource.join(endpointId)
    })
  } else {
    throw new Error("mock createJson got unrecognized uri: " +
                        strUri)
  }
})

var updateJson = simple.mock(function (strUri, json) {
  var endpointId
  return Promise.resolve().then(function () {
    if (strUri.match(/^api\/auction\/(\d+)\/bid$/)) {
      endpointId = parseInt(strUri.match(
                /^api\/auction\/(\d+)\/bid$/)[1])
      return auctionsSource.bid(endpointId)
    } else if (strUri.match(/^api\/auction\/(\d+)\/stealBids$/)) {
      endpointId = parseInt(strUri.match(
                    /^api\/auction\/(\d+)\/stealBids$/)[1])
      return Promise.resolve().then(function () {
        return auctionsSource.stealBids(endpointId)
      })
    } else if (strUri.match(/^api\/auction\/(\d+)\/stealBids$/)) {
      endpointId = parseInt(strUri.match(
                    /^api\/auction\/(\d+)\/stealBids$/)[1])
      return Promise.resolve().then(function () {
        return auctionsSource.stealBids(endpointId)
      })
    } else if (strUri.match(/^api\/auction\/(\d+)\/freezeBids$/)) {
      endpointId = parseInt(strUri.match(
                    /^api\/auction\/(\d+)\/freezeBids$/)[1])
      return Promise.resolve().then(function () {
        return auctionsSource.freezeBids(endpointId)
      })
    } else if (strUri.match(/^api\/auction\/(\d+)\/destroyBids$/)) {
      endpointId = parseInt(strUri.match(
                    /^api\/auction\/(\d+)\/destroyBids$/)[1])
      return Promise.resolve().then(function () {
        return auctionsSource.destroyBids(endpointId)
      })
    } else {
      throw new Error("mock updateJson got unrecognized uri: " +
                            strUri)
    }
  })
})

var exports = {}

exports.fetchJson = fetchJson
exports.createJson = createJson
exports.updateJson = updateJson

module.exports = exports
