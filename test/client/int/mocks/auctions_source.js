/* global require, module, setTimeout */

var _ = require('lodash')
var CONST = require('./const')

var ws = require('./ws')
var TestStore = require('./stores').TestStore
var TestActions = require('./actions').TestActions

var DEFAULT_BIDS = 300
var NUM_BIDS_TO_DEDUCT = -1
var JOIN_BONUS_BIDS = 10

var _waitStoreUpdate = function () {
    // like process.nextTick, but for browser as well
  return new Promise(function (resolve, reject) {
    setTimeout(resolve, 0)
  })
}

var _fetchAll = function () {
  var dataAuctions = _.cloneDeep(CONST.DATA_AUCTIONS)
  var dataJoinedAuctions = _.cloneDeep(CONST.DATA_JOINED_AUCTIONS)
  var storeState = TestStore.getState()
    // "patch" constant data w/ data that need by dynamic for tests
  storeState.joinedAuctionIds.forEach(function (auctionId) {
    var auctionState = TestStore.getAuctionState(auctionId)
    var numUsers = auctionState.users
        // top-level auctions array object
    var dataAuctionHasJoined =
                _.find(dataAuctions, {id: auctionId})
        // attribute of top-level object when auction has been joined
    var dataJoinedAuction =
                _.find(dataJoinedAuctions, {auctionId: auctionId})
    var updatedBidPrice =
                _.find(storeState.bidPrices, {auctionId: auctionId})
    if (!updatedBidPrice) { throw new Error("implementation") }
    if (dataJoinedAuction === undefined) {
      throw new Error("couldn't find joined auction data " +
                            "for auction id: " + auctionId)
    }
    if (auctionState.bids) {
      dataJoinedAuction.bids = auctionState.bids
      if (auctionState.wasLastBidder) {
        dataJoinedAuction.position = 1
      } else { // assumes at most two users
        dataJoinedAuction.position = 2
      }
    }
        // assumes at most two users
        // moreover, is not fully dynamic b/c (test) store impl doesn't
        // contain enough information.
        // can, however, serve as a test for data flow.
    dataJoinedAuction.leaderboard = _(_.zip(
            _.range(1, numUsers + 1), ['alice', 'bob']))
            .map(_.spread(function (position, username) {
              return {
                position: position,
                username: username,
                avatar: null,
                bids: 13
              }
            })).filter('position').filter('username')
            .value()
    dataJoinedAuction.isBidsFrozen = TestStore
            .isBidsFrozen(auctionId)
    dataAuctionHasJoined.bidPrice = updatedBidPrice.price.toString()
    dataAuctionHasJoined.joinedAuction = dataJoinedAuction
    dataAuctionHasJoined.numUsers = numUsers
  })
  return Promise.resolve(dataAuctions)
}

var fetchAll = _fetchAll

var fetchActive = function () {
  return _fetchAll().then(function (auctions) {
    return _.reject(auctions, function (auction) {
      return !_.isEmpty(auction.joinedAuction)
    })
  })
}

var fetchOne = function (auctionId) {
  return _fetchAll().then(function (auctions) {
    var auction = _.find(auctions, {id: auctionId})
    return auction
  })
}

var fetchEnded = function (auctionId) {
  return Promise.resolve(_.cloneDeep(CONST.DATA_AUCTION_ENDING))
}

var join = function (auctionId) {
  TestActions.setBids(auctionId, DEFAULT_BIDS)
  TestActions.joinAuction(auctionId)
  return new Promise(function (resolve, reject) {
    var auctionState = TestStore.getAuctionState(auctionId)
    if (!auctionState.joined) {
      throw new Error("auction join failed")
    }
    ws.emit('auction update', {
      auctionId: auctionId,
      updateType: 'join'
    })
    resolve()
  })
}

var bid = function (auctionId) {
  var numBids = _.find(TestStore.getState().numsBids,
                         {auctionId: auctionId})
  if (!numBids) {
    throw new Error("programming error")
  }
  var bidsUpdate = NUM_BIDS_TO_DEDUCT
  if (!TestStore.hasBids(auctionId)) {
    TestActions.setAuctionHasBids(auctionId)
    bidsUpdate = JOIN_BONUS_BIDS
  }
    // fragile
  TestActions.updateNumsBids(auctionId, bidsUpdate)
  TestActions.incBidPrice(auctionId)
  return Promise.resolve().then(function () {
    ws.emit('auction update', {
      auctionId: auctionId,
      updateType: 'bid-self'
    })
    return {}
  })
}

var stealBids = function (auctionId) {
  var auctionStateBefore = TestStore.getAuctionState(auctionId)
  TestActions.userSteal(auctionStateBefore)
  return Promise.resolve().then(_waitStoreUpdate).then(function () {
    var auctionStateAfter = TestStore.getAuctionState(auctionId)
    var bidsStolen = (auctionStateAfter.bids -
                          auctionStateBefore.bids)
    ws.emit('auction update', {
      auctionId: auctionId,
      updateType: 'steal-self',
      updateInfo: {
        numBids: bidsStolen,
        usernames: (auctionStateBefore.users === 0 ? []
                            : [CONST.OTHER_USERNAME])
      }
    })
    return {}
  })
}

var destroyBids = function (auctionId) {
  var auctionState = TestStore.getAuctionState(auctionId)
  ws.emit('auction update', {
    auctionId: auctionId,
    updateType: 'destroy-self',
    updateInfo: {
      usernames: (auctionState.users === 1 ? []
                        : [CONST.OTHER_USERNAME])
    }
  })
  return Promise.resolve({})
}

var exports = {}

exports.fetchAll = fetchAll
exports.fetchActive = fetchActive
exports.fetchEnded = fetchEnded
exports.fetchOne = fetchOne
exports.join = join
exports.bid = bid
exports.stealBids = stealBids
exports.destroyBids = destroyBids

module.exports = exports
