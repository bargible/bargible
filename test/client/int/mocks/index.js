/**
 * mocks: this functionality is useful are for cases where one user is
 *     interacting w/ the ui and other updates to the "application
 *     state" are viewed as simulated actions for by other users
 */

/* global require, module */

var _ = require('lodash')

var alt = require('./alt')
var ws = require('./ws')

/** modules in client/app/comp */
var comp_modules = {}

comp_modules.util = require('./comp/util')

/** modules in utilModules client/app/util */

var utilModules = {}

utilModules.http = require('./util/http')

var _resetModule = function (module) {
  _.values(module).forEach(function (moduleProp) {
    if (moduleProp.callCount) { // is stub
      moduleProp.reset()
    }
  })
}

var _resetModules = function (modulesObj) {
  _.values(modulesObj).forEach(_resetModule)
}

var exports = {}

exports.comp_modules = comp_modules
exports.utilModules = utilModules
exports.sim = require('./sim_players')
exports.CONST = require('./const')
exports.wsOn = ws.on
exports.reset = function () {
  _resetModules(comp_modules)
  _resetModules(utilModules)
  alt.recycle()
  ws.resetListeners()
}

module.exports = exports
