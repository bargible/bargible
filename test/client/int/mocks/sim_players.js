/**
 * simulate auction users other than the ui user
 * as well as (contrary to the file name)
 * any other actions (e.g. from the updater process)
 * initiated by the server-side
 */

/* global require, module */

var CONST = require('./const')
var ws = require('./ws')
var TestStore = require('./stores').TestStore
var TestActions = require('./actions').TestActions

var NUM_BIDS_PER_STEAL = 1
var NUM_BIDS_PER_DESTROY = 5

var join = function (auctionId) {
  TestActions.addUser(auctionId)
}

var bid = function (auctionId) {
  var auctionState = TestStore.getAuctionState(auctionId)
  TestActions.incBidPrice(auctionId)
  TestActions.setAuctionHasBids(auctionId)
  if (auctionState.joined) { // the ui user has joined
    ws.emit('auction update', {
      auctionId: auctionState.id,
      updateType: 'bid'
    })
  }
}

var stealBids = function (auctionId) {
  var auctionState = TestStore.getAuctionState(auctionId)
  if (auctionState.joined) { // the ui user has joined
    TestActions.updateNumsBids(auctionState.id,
                                   -1 * NUM_BIDS_PER_STEAL)
    ws.emit('auction update', {
      auctionId: auctionState.id,
      updateType: 'steal',
      updateInfo: {
        numBids: NUM_BIDS_PER_STEAL,
        username: CONST.OTHER_USERNAME
      }
    })
  }
}

var destroyBids = function (auctionId) {
  var auctionState = TestStore.getAuctionState(auctionId)
  if (auctionState.joined) { // the ui user has joined
    TestActions.updateNumsBids(auctionState.id,
                                   -1 * NUM_BIDS_PER_DESTROY)
    ws.emit('auction update', {
      auctionId: auctionState.id,
      updateType: 'destroy',
      updateInfo: {
        numBids: NUM_BIDS_PER_DESTROY,
        username: CONST.OTHER_USERNAME
      }
    })
  }
}

var freezeBids = function (auctionId) {
  var auctionState = TestStore.getAuctionState(auctionId)
  if (auctionState.joined) { // the ui user has joined
    TestActions.freezeBids(auctionState.id)
    ws.emit('auction update', {
      auctionId: auctionState.id,
      updateType: 'bids-freeze',
      updateInfo: {
        username: CONST.OTHER_USERNAME
      }
    })
  }
}

var endAuction = function (auctionId) {
  var auctionState = TestStore.getAuctionState(auctionId)
  if (auctionState.joined) { // the ui user has joined
    ws.emit('auction update', {
      auctionId: auctionState.id,
      updateType: 'end'
    })
  }
}

var exports = {}

exports.join = join
exports.bid = bid
exports.stealBids = stealBids
exports.destroyBids = destroyBids
exports.freezeBids = freezeBids
exports.endAuction = endAuction

module.exports = exports
