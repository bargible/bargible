/* global require, module */

var _ = require('lodash')

var alt = require('./alt')

var TestActions = require('./actions').TestActions

var INIT_BID_PRICE = 1
var BID_INC = 1 // bid increment multiplier

var TestStore = alt.createStore({
  displayName: 'TestStore',

  bindListeners: {
    joinAuction: TestActions.joinAuction,
    addNumsBids: TestActions.setBids,
    setAuctionHasBids: TestActions.setAuctionHasBids,
    updateNumsBids: TestActions.updateNumsBids,
    freezeBids: TestActions.freezeBids,
    thawBids: TestActions.thawBids,
    incBidPrice: TestActions.incBidPrice,
    addUser: TestActions.addUser
  },

  state: {
    joinedAuctionIds: [],
    hasBidsAuctionIds: [],
    wasLastBidderAuctionIds: [],
    bidsFrozenAuctionIds: [],
    numsUsers: [], // auctionId, users
    bidPrices: [], // auctionId, price
    numsBids: [] // auctionId, bids
  },

  publicMethods: {
    wasLastBidder: function (auctionId) {
      return this.getState().wasLastBidderAuctionIds
                .indexOf(auctionId) !== -1
    },

    hasBids: function (auctionId) {
      return this.getState().hasBidsAuctionIds
                .indexOf(auctionId) !== -1
    },

    isBidsFrozen: function (auctionId) {
      return this.getState().bidsFrozenAuctionIds
                .indexOf(auctionId) !== -1
    },

    getAuctionState: function (auctionId) {
      var state = this.getState()
      return {
        id: auctionId,
        joined:
                _.includes(state.joinedAuctionIds, auctionId),
        hasBids:
                _.includes(state.hasBidsAuctionIds, auctionId),
        wasLastBidder:
                _.includes(state.wasLastBidderAuctionIds, auctionId),
        users:
                _.find(state.numsUsers, {auctionId: auctionId})
                    .users,
        price:
                _.find(state.bidPrices, {auctionId: auctionId})
                    .price,
        bids:
                _.find(state.numsBids, {auctionId: auctionId})
                    .bids
      }
    }
  },

  joinAuction: function (auctionId) {
    var joinedAuctionIds = this.state.joinedAuctionIds
    var bidPrices = this.state.bidPrices
    var numsUsersCurr = this.state.numsUsers
    var numsUsers
    if (joinedAuctionIds.indexOf(auctionId) !== -1) {
      throw new Error("attempted to join auction twice")
    }
    joinedAuctionIds.push(auctionId)
    bidPrices.push({
      auctionId: auctionId,
      price: INIT_BID_PRICE
    })
    if (_.find(numsUsersCurr, {auctionId: auctionId})) {
            // user was added to the auction before test joined
      numsUsers = numsUsersCurr.map(function (numUsers) {
        if (numUsers.auctionId !== auctionId) {
          return numUsers
        }
        return _.mapValues(numUsers, function (key, val) {
          if (key === 'users') {
            return val + 1
          }
          return val
        })
      })
    } else {
      numsUsers = numsUsersCurr
      numsUsers.push({
        auctionId: auctionId,
        users: 1
      })
    }
    this.setState({
      joinedAuctionIds: joinedAuctionIds,
      bidPrices: bidPrices,
      numsUsers: numsUsers
    })
  },

  addUser: function (auctionId) {
    var numsUsersCurr = this.state.numsUsers
    var hasUpdatedNum = false
    var numsUsers = numsUsersCurr.map(function (numUsers) {
      if (numUsers.auctionId === auctionId) {
        if (hasUpdatedNum) {
          throw new Error("programming error: duplicate " +
                                    "auctionId entry in numsUsers")
        }
        hasUpdatedNum = true
        return {
          auctionId: auctionId,
          users: numUsers.users + 1
        }
      }
      return numUsers
    })
    if (!hasUpdatedNum) {
      numsUsers.push({
        auctionId: auctionId,
        users: 1
      })
    }
    this.setState({numsUsers: numsUsers})
  },

  incBidPrice: function (auctionId) {
    var bidPricesCurr = this.state.bidPrices
    var bidPrices = bidPricesCurr.map(function (bidPrice) {
      if (bidPrice.auctionId !== auctionId) {
        return bidPrice
      }
      return _.mapValues(bidPrice, function (val, key) {
        if (key === 'price') {
          return val + BID_INC
        }
        return val
      })
    })
    this.setState({bidPrices: bidPrices})
  },

  setAuctionHasBids: function (auctionId) {
    var hasBidsAuctionIds = this.state.hasBidsAuctionIds
    if (hasBidsAuctionIds.indexOf(auctionId) === -1) {
      hasBidsAuctionIds.push(auctionId)
      this.setState({hasBidsAuctionIds: hasBidsAuctionIds})
    }
  },

  addNumsBids: function (numBids) {
    var numsBids = this.state.numsBids
    if (_.find(numsBids, {auctionId: numBids.auctionId})) {
      throw new Error("numBids already set for auction")
    }
    numsBids.push(numBids)
    this.setState({numsBids: numsBids})
  },

  updateNumsBids: function (numBidsUpdate) {
    var currNumsBids = this.state.numsBids
    if (!_.find(currNumsBids,
                    {auctionId: numBidsUpdate.auctionId})) {
      throw new Error("attempting to update bids for auction " +
                            "before setting number of bids")
    }
    var numsBids = currNumsBids.map(function (currNumBids) {
      if (currNumBids.auctionId !== numBidsUpdate.auctionId) {
        return currNumBids
      }
      var bids = currNumBids.bids
      bids += numBidsUpdate.bidsUpdate
      if (Number.isNaN(bids)) {
        throw new Error("programming error: updated number of " +
                                "bids to NaN")
      }
      return {
        auctionId: currNumBids.auctionId,
        bids: bids
      }
    })
    this.setState({numsBids: numsBids})
  },

  freezeBids: function (auctionId) {
    var bidsFrozenAuctionIds = this.state.bidsFrozenAuctionIds
    if (bidsFrozenAuctionIds.indexOf(auctionId) === -1) {
      bidsFrozenAuctionIds.push(auctionId)
      this.setState({
        bidsFrozenAuctionIds: bidsFrozenAuctionIds
      })
    }
  },

  thawBids: function (auctionIdThaw) {
    var bidsFrozenAuctionIds = this.state.bidsFrozenAuctionIds
                .filter(function (auctionId) {
                  return auctionId !== auctionIdThaw
                })
    this.setState({
      bidsFrozenAuctionIds: bidsFrozenAuctionIds
    })
  }
})

var exports = {}

exports.TestStore = TestStore

module.exports = exports
