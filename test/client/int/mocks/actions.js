/* global require, module */

var alt = require('./alt')

var CONST = require('./const')

var TestActions = alt.createActions({
  joinAuction: function (auctionId) {
    if (auctionId !== CONST.JOIN_FAIL_AUCTION_ID &&
            auctionId !== CONST.JOIN_ITEM_AUCTION_ID) {
      throw new Error("may only join auction with id: " +
                            CONST.JOIN_FAIL_AUCTION_ID + " or " +
                            CONST.JOIN_ITEM_AUCTION_ID)
    }
    return auctionId
  },

  setAuctionHasBids: function (auctionId) {
    return auctionId
  },

  setBids: function (auctionId, bids) {
    return {
      auctionId: auctionId,
      bids: bids
    }
  },

  updateNumsBids: function (auctionId, bidsUpdate) {
    return {
      auctionId: auctionId,
      bidsUpdate: bidsUpdate
    }
  },

  freezeBids: function (auctionId) {
    return auctionId
  },

  thawBids: function (auctionId) {
    return auctionId
  },

    /** increments the (simulated) number of users */
  addUser: function (auctionId) {
    return auctionId
  },

    /** increment bid price */
  incBidPrice: function (auctionId) {
    return auctionId
  },

  userSteal: function (auctionState) {
    return auctionState.id
  },

  destroy: function (auctionId, isOther) {

  }
})

var exports = {}

exports.TestActions = TestActions

module.exports = exports
