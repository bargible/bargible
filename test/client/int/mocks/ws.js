/**
 * simulate websockets
 */

/* global require, module */

var _updateListeners = []

var on = function (eventName, cb) {
  if (eventName === 'auction update') {
    _updateListeners.push(cb)
  } else {
    throw new Error("unknown event name '" + eventName + "'")
  }
}

var emit = function (eventName, data) {
  if (eventName === 'auction update') {
    _updateListeners.forEach(function (cb) {
      cb(data)
    })
  } else {
    throw new Error("unknown event name '" + eventName + "'")
  }
}

var resetListeners = function () {
  _updateListeners = []
}

var exports = {}

exports.on = on
exports.emit = emit
exports.resetListeners = resetListeners

module.exports = exports
