/* global require, module */

const USER_FIRST_NAME = 'bob'
const USER_CREDIT = 500
const AUCTION_REQ_CREDITS = 50
const FAIL_AUCTION_REQ_CREDITS = 500000

const JOIN_ITEM_AUCTION_ID = 1
const JOIN_FAIL_AUCTION_ID = 2

/**
 * mock variable populated from server-side template
 */

// used for props argument to test NavNode
const NAV_LINKS = {
  home: 'home',
  logout: 'logout',
  profile: 'profile',
  faq: 'faq'
}

// used to verify navbar links
const NAV_LINK_CONSTS = {
  home: 'home',
  Profile: 'profile',
  'Buy credits': null,
  FAQ: 'faq',
  Logout: 'logout'
}

const DATA_AUCTION_HISTORY = [
  {
    "auctionId": 1,
    "start": "2016-09-03T05:27:08.963231",
    "realPrice": 1000,
    "position": 1,
    "reward": {
      itemId: 2,
      image: "static/img/home-depot-gift-card.png",
      description: "This is an amazing test Home Depot card",
      disclaimer: "Disclaim!",
      name: "Test Home Depot Gift Card"
    },
    "end": "2016-09-03T07:15:48.164525"
  }
]

/**
 * DATA_* variables lifted from console.log statements
 * during functional tests
 */
const DATA_USER = {
  credit: USER_CREDIT,
  first_name: USER_FIRST_NAME,
  id: 1,
  last_name: "belcher",
  username: "bburgers",
  avatar: null
}

const DATA_AUCTIONS = [
  {
    bidPrice: 1,
    minPrice: 10,
    id: 1,
    numUsers: 0,
    intermediateTimer: 529,
    lastBidder: null,
    requiredCredits: AUCTION_REQ_CREDITS,
    reward: {
      itemId: 3,
      image: "static/img/home-depot-gift-card.png",
      description: "This is an amazing test Home Depot card",
      disclaimer: "This is a disclaimer",
      name: "Test Home Depot Gift Card"
    },
    rewardType: "item",
    start: "2016-09-04T02:13:25.409483",
    timeRemaining: 28328
  },
  {
    bidPrice: 1,
    minPrice: 10,
    id: 2,
    numUsers: 0,
    intermediateTimer: 529,
    lastBidder: null,
    requiredCredits: FAIL_AUCTION_REQ_CREDITS,
    reward: {
      itemId: 2,
      image: "static/img/home-depot-gift-card.png",
      description: "This is an amazing test Home Depot card",
      disclaimer: "This is a disclaimer",
      name: "Test Home Depot Gift Card"
    },
    rewardType: "item",
    start: "2016-09-04T02:13:25.409483",
    timeRemaining: 28328
  },
  {
    bidPrice: 1,
    minPrice: 10,
    id: 3,
    numUsers: 0,
    intermediateTimer: 529,
    lastBidder: null,
    requiredCredits: AUCTION_REQ_CREDITS,
    reward: {
      itemId: 3,
      image: "static/img/home-depot-gift-card.png",
      description: "This is an amazing test Home Depot card",
      disclaimer: "This is a disclaimer",
      name: "Test Home Depot Gift Card"
    },
    rewardType: "item",
    start: "2016-09-04T02:13:25.409483",
    timeRemaining: 28328
  },
  {
    bidPrice: 1,
    minPrice: 10,
    id: 4,
    numUsers: 0,
    intermediateTimer: 529,
    lastBidder: null,
    requiredCredits: AUCTION_REQ_CREDITS,
    reward: {
      itemId: 4,
      image: "static/img/macys-gift-card.jpg",
      description: "This is an amazing test Macy's card",
      disclaimer: "This is a disclaimer",
      name: "Test Macy's Gift Card"
    },
    rewardType: "item",
    start: "2016-09-04T02:13:25.409483",
    timeRemaining: 28328
  }
]

const DATA_JOINED_AUCTIONS = [
  {
    auctionId: JOIN_ITEM_AUCTION_ID,
    bids: 300,
    position: null,
    leaderboard: [],
    isBidsFrozen: false,
    secsFreezeRemaining: null,
    bidsTilActivation: {
      freeze: 10, steal: 20, destroy: 30 },
    numActivations: {
      freeze: 6, steal: 3, destroy: 2 }
  },
  {
    auctionId: JOIN_FAIL_AUCTION_ID,
    bids: 300,
    position: null,
    leaderboard: [],
    isBidsFrozen: false,
    secsFreezeRemaining: null,
    bidsTilActivation: {
      freeze: 10, steal: 20, destroy: 30 },
    numActivations: {
      freeze: 6, steal: 3, destroy: 2 }
  },
  {
    auctionId: JOIN_ITEM_AUCTION_ID,
    bids: 300,
    position: null,
    leaderboard: [],
    isBidsFrozen: false,
    secsFreezeRemaining: null,
    bidsTilActivation: {
      freeze: 10, steal: 20, destroy: 30 },
    numActivations: {
      freeze: 6, steal: 3, destroy: 2 }
  }
]

const DATA_AUCTION_ENDING = {
  endedAuction: {
    minPrice: 1000,
    position: 1,
    realPrice: 1000,
    reward: {
      image: "static/img/bucks-kitten.png",
      value: 50
    },
    rewardType: "item",
    lastBidUsername: "asdf"
  }
}

const OTHER_USERNAME = 'bob'

var exports = {}

exports.USER_FIRST_NAME = USER_FIRST_NAME
exports.USER_CREDIT = USER_CREDIT
exports.AUCTION_REQ_CREDITS = AUCTION_REQ_CREDITS
exports.NAV_LINKS = NAV_LINKS
exports.NAV_LINK_CONSTS = NAV_LINK_CONSTS
exports.DATA_USER = DATA_USER
exports.DATA_AUCTIONS = DATA_AUCTIONS
exports.DATA_JOINED_AUCTIONS = DATA_JOINED_AUCTIONS
exports.DATA_AUCTION_ENDING = DATA_AUCTION_ENDING
exports.DATA_AUCTION_HISTORY = DATA_AUCTION_HISTORY
exports.JOIN_FAIL_AUCTION_ID = JOIN_FAIL_AUCTION_ID
exports.JOIN_ITEM_AUCTION_ID = JOIN_ITEM_AUCTION_ID
exports.FAIL_AUCTION_REQ_CREDITS = FAIL_AUCTION_REQ_CREDITS
exports.OTHER_USERNAME = OTHER_USERNAME

module.exports = exports
