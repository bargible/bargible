/* global require, module */

var _ = require('lodash')

const CONST = require('./const')

var fetchUserAuctionHistories = function () {
  return Promise.resolve(_.cloneDeep(CONST.DATA_AUCTION_HISTORY))
}

var exports = {}

exports.fetchUserAuctionHistories = fetchUserAuctionHistories

module.exports = exports
