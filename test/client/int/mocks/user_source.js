/* global require, module */

const CONST = require('./const')

var fetchCurr = function () {
  return Promise.resolve(CONST.DATA_USER)
}

var exports = {}

exports.fetchCurr = fetchCurr

module.exports = exports
