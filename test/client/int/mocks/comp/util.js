/* global require, module */

var simple = require('simple-mock')

var openModal = simple.mock()

var exports = {}

exports.openModal = openModal

module.exports = exports
