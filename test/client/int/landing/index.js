/* global require, module, setTimeout */

const _ = require('lodash')
const proxyquire = require('proxyquire')
var React = require('react')

const mocks = require('../mocks')
const compObj = require('../comp_obj')

// see comment in ../home/index.js re proxyquire
proxyquire(
    '../../../../client/app/util',
    _.mapKeys(mocks.utilModules, function (val, key) {
      return './' + key
    })
)
const IndexActions = require(
    '../../../../client/app/actions/index_actions')
const IndexStore = require(
    '../../../../client/app/stores/index_store')
const IndexMain = require(
    '../../../../client/app/comp/main_index')
const alt = require('../../../../client/app/alt')

const TIMEOUT_UPDATE_DOM = 100 // ms

// per-test variables
var mainIndex

/** creates a timeout for use with promise chaining */
const dumbTimeout = function (msDur) {
  return function () {
    return new Promise(function (resolve, reject) {
      setTimeout(resolve, msDur)
    })
  }
}

var renderIndex = function (storeState) {
  if (mainIndex) {
    mainIndex.unmount()
  }
  mainIndex = new compObj.IndexMain(
        React.createElement(IndexMain, storeState))
}

const setup = function (t) {
  renderIndex(IndexStore.getState())
  IndexStore.listen(renderIndex)
  t.end()
}

const teardown = function (t) {
  IndexStore.unlisten(renderIndex)
  mainIndex.unmount()
  mainIndex = undefined
  alt.recycle()
  mocks.reset()
  t.end()
}

const initTest = function (t) {
  Promise.resolve().then(function () {
    IndexActions.fetchActiveAuctions()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.ok(mainIndex.hasAuctions(),
             "auctions exist.")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const testAuctionExistence = function (t) {
  t.test("setup", setup)
  t.test("initial state", initTest)
  t.test("teardown", teardown)
  t.end()
}

const testsMain = function (t) {
  t.test("test auction existence", testAuctionExistence)
  t.end()
}

var exports = testsMain

module.exports = exports
