/**
 * stubs: functions that return pre-defined data per call
 *        with minimal logic
 */

/* global require, module */

const _ = require('lodash')

const http = require('./http')
const endpoints = require('./endpoints')
const braintree = require('./braintree')
const ws = require('./ws')
const compUtil = require('./comp_util')

var exports = {}

exports.http = http
exports.endpoints = endpoints
exports.braintree = braintree
exports.ws = ws
exports.compUtil = compUtil
exports.reset = function () {
  _(compUtil).values().forEach(function (stub) {
    stub.reset()
  })
  _(http).values().forEach(function (stub) {
    stub.reset()
  })
  _(braintree).values().forEach(function (stub) {
    stub.reset()
  })
  _(endpoints).values().map(function (endpointType) {
    return _.toPairs(endpointType)
  }).flatten().forEach(_.spread(function (urlStr, endpoint) {
    endpoint.reset()
  }))
  ws.resetListeners()
}

module.exports = exports
