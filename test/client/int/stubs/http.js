/**
 * corresponds to client/app/util/http.js
 *
 * all exports of this module are expected to be simple-mock
 * instances (return values of simple.mock())
 */

/* global require, module */

const _ = require('lodash')
const simple = require('simple-mock')

const endpoints = require('./endpoints')

const _lookupEndpointDef = function (endpointDefsObj, urlStr) {
  const matchingEndpointDefsObj = _(
        _.toPairs(endpointDefsObj)
    ).filter(_.spread(function (urlDefStr, endpointObj) {
        // determine whether given path, urlStr, matches
        // one of the defined paths, urlDefStr,
        // by converting urlDefStr to a regexp and matching on urlStr
      return urlStr.match(new RegExp(
            '^' + urlDefStr.replace(/:([^/]+)/g, '[^/]+') + '$'
        ))
    })).fromPairs().value()
  if (_.size(_.keys(matchingEndpointDefsObj)) > 1) {
    throw new Error("found multiple matching paths for " + urlStr)
  }
  if (_.size(_.keys(matchingEndpointDefsObj)) === 0) {
    throw new Error("found no matching paths no " + urlStr)
  }
  const [urlDefStr, endpointObj] = _(matchingEndpointDefsObj)
              .toPairs().first()
    // key-value obj, e.g. {id: 1, action: bid}
  const urlParams = _(_.zip(urlDefStr.split('/'), urlStr.split('/')))
              .filter(_.spread((urlDefComp) => urlDefComp[0] === ':'))
              .fromPairs().mapKeys(function (urlParam, urlParamKey) {
                  // remove preceeding ':'
                return urlParamKey.slice(1)
              }).value()
  return [urlParams, endpointObj]
}

var fetchJson = simple.mock(function (strUri) {
  var fetchUri = (function (strUri) {
    var strUriSplit = strUri.split('?')
    return {
      path: strUriSplit[0],
      query: strUriSplit[1]
    }
  }(strUri))
  var [urlParams, endpoint] = _lookupEndpointDef(
        endpoints.fetch, fetchUri.path)
  if (!endpoint) {
    throw new Error("no fetch function defined for endpoint '" +
                        fetchUri.path + "'")
  }
  try {
    var endpointRes = endpoint.get({
      url: strUri,
      urlParams: urlParams,
      query: fetchUri.query
    })
    return Promise.resolve().then(function () {
      return endpointRes
    }).then(function (resolvedRes) {
      if (resolvedRes instanceof Error) {
        throw resolvedRes
      } else {
        return _.cloneDeep(resolvedRes)
      }
    })
  } catch (err) {
    if (err.message === 'no calls') {
      throw new Error("tried to call fetch endpoint '" +
                             fetchUri.path + "' without a call set")
    }
    throw err
  }
})

var createJson = simple.mock(function (strUri, json) {
  var [urlParams, endpoint] = _lookupEndpointDef(
        endpoints.create, strUri)
  if (!endpoint) {
    throw new Error("no create function defined for endpoint '" +
                        strUri + "'")
  }
  try {
    var endpointRes = endpoint.get({
      json: json || {},
      url: strUri,
      urlParams: urlParams
    })
    return Promise.resolve().then(function () {
      return endpointRes
    }).then(function (resolvedRes) {
      if (resolvedRes instanceof Error) {
        throw resolvedRes
      } else {
        return _.cloneDeep(resolvedRes)
      }
    })
  } catch (err) {
    if (err.message === 'no calls') {
      throw new Error("tried to call create endpoint '" +
                             strUri + "' without a call set")
    }
    throw err
  }
})

var updateJson = simple.mock(function (strUri, json) {
  var [urlParams, endpoint] = _lookupEndpointDef(
        endpoints.update, strUri)
  if (!endpoint) {
    throw new Error("no update function defined for endpoint '" +
                        strUri + "'")
  }
  try {
    var endpointRes = endpoint.get({
      json: json || {},
      url: strUri,
      urlParams: urlParams
    })
    return Promise.resolve().then(function () {
      return endpointRes
    }).then(function (resolvedRes) {
      if (resolvedRes instanceof Error) {
        throw resolvedRes
      } else {
        return _.cloneDeep(resolvedRes)
      }
    })
  } catch (err) {
    if (err.message === 'no calls') {
      throw new Error("tried to call update endpoint '" +
                             strUri + "' without a call set")
    }
    throw err
  }
})

var fetchFile = simple.mock(function (strUri, blobFile, mimeType) {
  throw new Error("fetchFile stub unimpl")
})

var updateFile = simple.mock(function (strUri, blobFile, mimeType) {
  throw new Error("updateFile stub unimpl")
})

var postFormData = simple.mock(function (url, object) {
  throw new Error("updateFormDate stub unimpl")
})

var exports = {}

exports.fetchJson = fetchJson
exports.createJson = createJson
exports.updateJson = updateJson
exports.fetchFile = fetchFile
exports.updateFile = updateFile
exports.postFormData = postFormData

module.exports = exports
