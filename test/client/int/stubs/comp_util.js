/**
 * corresponds to app/comp/util
 */
/* global require, module */

var simple = require('simple-mock')

var setAnimation = simple.mock()
var getAnimationUrls = simple.mock()

var exports = {}

exports.setAnimation = setAnimation
exports.getAnimationUrls = getAnimationUrls

module.exports = exports
