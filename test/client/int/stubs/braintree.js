/* global require, module */

const simple = require('simple-mock')

const createHostedFields = simple.mock(function (
    clientToken, styles, fields) {
    // client uses state.hostedFields object for:
    // 1) truthyness checks re. loading state (in templates)
    // 2) getting the nonce from the braintree server
  return Promise.resolve({isStubHostedFields: true})
})

const getNonce = simple.mock(function (hostedFields) {
  if (!(hostedFields && hostedFields.isStubHostedFields)) {
    throw new Error("stub getNonce didn't get" +
                        " expected stub hosted hosted fields obj")
  }
  return Promise.resolve('stub-nonce')
})

var exports = {}

exports.createHostedFields = createHostedFields
exports.getNonce = getNonce

module.exports = exports
