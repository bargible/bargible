/* global require, module */

const _ = require('lodash')

var Endpoint = function (urlParams) {
    // currently, semantics of what a "call" is are pushed into
    // ./http and the test spec for flexibility
  this.calls = []
  this.urlParams = urlParams || []
  this.requestsParams = []
}

/** used by ./http */
Endpoint.prototype.get = function (reqParams) {
  var call = this.calls.pop()
  if (_.size(_.difference(_.keys(reqParams), [
    'url',
    'urlParams',
    'query',
    'json'
  ]) > 0)) {
    throw new Error("unexpected keys among stub endpoint " +
                        "request params " + _.keys(reqParams))
  }
  this.requestsParams.push(reqParams)
  if (!call) {
    throw new Error('no calls')
  }
  if (_.isFunction(call)) {
    return call()
  }
  return call
}

/** used by test spec */
Endpoint.prototype.set = function (call) {
  this.calls.push(call)
}

Endpoint.prototype.prevReqParams = function () {
  return _.mapValues(_.last(this.requestsParams), function (
        paramData, paramType
    ) {
    if (paramType !== 'query') {
      return paramData
    }
        // ignores duplicate query keys, which are valid http
        // but not (currently) used by the application's api
    return _.fromPairs(
            _.map(paramData.split('&'), function (queryParam) {
              var queryPair = queryParam.split('=')
              if (_.size(queryPair) !== 2) {
                throw new Error(
                        "misformed query string " +
                            "(not key-value format): " +
                            paramData)
              }
              return queryPair
            })
        )
  })
}

Endpoint.prototype.reset = function (call) {
  this.calls = []
  this.requestsParams = []
}

var FetchEndpoint = function () {
  Endpoint.apply(this, arguments)
}
FetchEndpoint.prototype = Object.create(Endpoint.prototype)

FetchEndpoint.prototype.get = function (reqParams) {
  if (reqParams && reqParams['json']) {
    throw new Error("json data invalid for fetch endpoints")
  }
  return Endpoint.prototype.get.apply(this, [reqParams])
}

var CreateUpdateEndpoint = function () {
  Endpoint.apply(this, arguments)
}
CreateUpdateEndpoint.prototype = Object.create(Endpoint.prototype)

const fetch = Object.freeze({
  'api/time': new FetchEndpoint(),
  'api/creditPackages': new FetchEndpoint(),
  'api/user/get': new FetchEndpoint(),
  'api/user/auctions': new FetchEndpoint(),
  'api/user/auctions/:id': new FetchEndpoint(['id']),
  'api/auction/auctions': new FetchEndpoint(),
  'api/auction/auctions/:id': new FetchEndpoint(['id'])
})

const create = Object.freeze({
  'api/user/auctions/:id/actions/:action': new CreateUpdateEndpoint([
    'id', 'action'
  ]),
  'api/user/auctionPayments': new CreateUpdateEndpoint(),
  'api/user/auctionAddress': new CreateUpdateEndpoint()
})

const update = Object.freeze({
  'api/user/auctions/:id': new CreateUpdateEndpoint(['id']),
  'api/user/auctions/:id/actions/:action': new CreateUpdateEndpoint([
    'id', 'action'
  ])
})

_.union(_.toPairs(fetch), _.toPairs(create), _.toPairs(update))
    .forEach(_.spread(function (urlStr, endpointObj) {
      const urlStrParams = _(urlStr.split('/'))
                  .filter((urlStrComp) => urlStrComp[0] === ':')
                  .map((urlStrComp) => urlStrComp.slice(1))
                  .value()
      if (!_.isEqual(_.sortBy(endpointObj.urlParams),
                       _.sortBy(urlStrParams))) {
        throw new Error("endpoint object url parameters " +
                            "mismatch url string parameters " +
                            " for url string '" + urlStr + "'")
      }
    }))

var exports = {}

exports.fetch = fetch
exports.create = create
exports.update = update

module.exports = exports
