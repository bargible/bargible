/* global require, module, setTimeout */

const _ = require('lodash')
const moment = require('moment')

const stubs = require('../stubs')
const setupTeardown = require('./setup_teardown_stubs')

const HomeActions = setupTeardown.HomeActions
const getObjAuctionContainer = setupTeardown.getObjAuctionContainer

const TIMEOUT_UPDATE_DOM = 100 // ms
const AUCTION_DURATION = 1000 // ms

const AUCTION_ID = 3

const DATA_USER = Object.freeze({
  credit: 123,
  first_name: 'bob',
  id: 1,
  last_name: "belcher",
  username: "bburgers",
  avatar: null,
  email: "b@b.org"
})

const DATA_AUCTION_ACTIVE = {
  id: AUCTION_ID,
  reward: {
    type: "item",
    data: {
      itemId: 1,
      image: "img/something.jpg",
      description: "awesome",
      disclaimer: "not actually a thing",
      name: "thing one"
    }
  },
  numUsers: 3,
  bidPrice: 10,
  minPrice: 1000,
  joinCost: 0
}

/** creates a timeout for use with promise chaining */
const dumbTimeout = function (msDur) {
  return function () {
    return new Promise(function (resolve, reject) {
      setTimeout(resolve, msDur)
    })
  }
}

const _setActiveCall = function (momentStart) {
  const activeCall = function () {
    const msSinceStart = moment.utc() - momentStart
    const maybeSecRemaining = AUCTION_DURATION - msSinceStart
    if (maybeSecRemaining < 0) {
      return []
    } else {
      return [{active: _.merge(_.cloneDeep(DATA_AUCTION_ACTIVE), {
        timeRemaining: Math.floor(
                    (moment.utc() - momentStart) / 1000
                )
      })}]
    }
  }
  stubs.endpoints.fetch['api/auction/auctions'].set(activeCall)
}

const unjoinedUltTestMain = function (t) {
  var momentAuctionStart
  Promise.resolve().then(function () {
    setupTeardown.pageLifecycle.setInit({
      user: DATA_USER,
      auctions: {}
    })
    setupTeardown.pageLifecycle.fetchInit()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("initial fetch")
    getObjAuctionContainer().selectTab('active')
    momentAuctionStart = moment.utc()
    _setActiveCall(momentAuctionStart)
    HomeActions.fetchActiveAuctions()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("fetched active auctions")
    t.ok(
            setupTeardown
                .getObjAuctionContainer()
                .hasActiveAuctionEls(AUCTION_ID),
            "found active auction in ui")
        // pause long enough to ensure auction ends
  }).then(dumbTimeout(AUCTION_DURATION * 1.1)).then(function () {
    _setActiveCall(momentAuctionStart)
    HomeActions.fetchActiveAuctions()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("fetched active auctions after test auction end")
    t.skip("skipped pending app impl decision", function (t) {
      t.notOk(
                setupTeardown
                    .getObjAuctionContainer()
                    .hasActiveAuctionEls(AUCTION_ID),
                "active auction not found in ui")
    })
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const testUnjoinedUlt = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("active auction", unjoinedUltTestMain)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const testUnjoinedInt = function (t) {
  t.end("unimplemented")
}

/**
 * regression:
 * the "left" time goes negative and the auction
 * card remains visible, even though the auction
 * is marked as ended in the db.
 */
const testUnjoined = function (t) {
  t.test("on ultimate timer end", testUnjoinedUlt)
  t.skip("on intermediate timer end", testUnjoinedInt)
  t.end()
}

const testsMain = function (t) {
  t.test("ended auctions in joined tab", testUnjoined)
  t.end()
}

var exports = testsMain

module.exports = exports
