/* global require, module, setTimeout */

const _ = require('lodash')
const proxyquire = require('proxyquire')
var React = require('react')

const mocks = require('../mocks')
const compObj = require('../comp_obj')

proxyquire(
    '../../../../client/app/util',
    _.mapKeys(mocks.utilModules, function (val, key) {
      return './' + key
    })
)
const HomeActions = require(
    '../../../../client/app/actions/home_actions')
proxyquire(
    '../../../../client/app/comp/auction_active',
    _.mapKeys(mocks.comp_modules, function (val, key) {
      return './' + key
    })
)
const AuctionContainerNode = require(
    '../../../../client/app/comp/main_home')
const AuctionContainerStore = require(
    '../../../../client/app/stores/auction_container_store')
const alt = require('../../../../client/app/alt')
const auctionUpdateHandler =
          require('../../../../client/app/util_home')
          .auctionUpdateHandler

// per-test variables
var objAuctionContainer

var renderAuctionContainer = function (storeState) {
  if (_.isNull(storeState)) {
    return
  }
  objAuctionContainer = new compObj.AuctionContainer(
        React.createElement(AuctionContainerNode, storeState))
}

const setup = function (t) {
  renderAuctionContainer(AuctionContainerStore.getState())
  AuctionContainerStore.listen(renderAuctionContainer)
  mocks.wsOn('auction update', auctionUpdateHandler)
  t.end()
}

const teardown = function (t) {
  AuctionContainerStore.unlisten(renderAuctionContainer)
  objAuctionContainer.unmount()
  alt.recycle()
  mocks.reset()
  objAuctionContainer = undefined
  t.end()
}

var exports = {}

exports.HomeActions = HomeActions
exports.mocks = mocks
exports.alt = alt
exports.setup = setup
exports.teardown = teardown
exports.getObjAuctionContainer = function () {
  return objAuctionContainer
}

module.exports = exports
