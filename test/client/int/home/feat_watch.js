/* global require, module, setTimeout */

const _ = require('lodash')
const moment = require('moment')

const compObj = require('../comp_obj')

const stubs = require('../stubs')
const setupTeardown = require('./setup_teardown_stubs')

const getObjAuctionContainer = setupTeardown.getObjAuctionContainer

const TIMEOUT_UPDATE_DOM = 100 // ms

const AUCTION_ID = 3

const JOIN_COST = 1000

const DATA_USER_UNJOINED = {
  credit: JOIN_COST,
  first_name: 'bob',
  id: 1,
  last_name: "belcher",
  username: "bburgers",
  avatar: null,
  email: "b@b.org"
}

const DATA_AUCTION_UPCOMING = {
  id: AUCTION_ID,
  reward: {
    type: "item",
    data: {
      itemId: 1,
      image: "img/something.jpg",
      description: "awesome",
      disclaimer: "not actually a thing",
      name: "thing one"
    }
  },
  minPrice: 123,
  start: setupTeardown.util.formatMoment(
        moment.utc().add(5, 'minutes')),
  is_watching: false
}

const DATA_INIT = {
  user: DATA_USER_UNJOINED,
  auctions: {
    upcoming: [DATA_AUCTION_UPCOMING]
  }
}

/** creates a timeout for use with promise chaining */
const dumbTimeout = function (msDur) {
  return function () {
    return new Promise(function (resolve, reject) {
      setTimeout(resolve, msDur)
    })
  }
}

const watchTest = function (t) {
  const auctionId = AUCTION_ID
  const timeoutWatchUpdate = 1000
  Promise.resolve().then(function () {
    setupTeardown.pageLifecycle.setInit(DATA_INIT)
    setupTeardown.pageLifecycle.fetchInit()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    getObjAuctionContainer().selectTab('upcoming')
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    const cardUpcoming = getObjAuctionContainer()
                  .getCardUpcoming(auctionId)
    const dataAuctionUpcoming = _.merge(
            _.cloneDeep(DATA_AUCTION_UPCOMING),
            {is_watching: true})
    t.ok(cardUpcoming, "found upcoming card")
    t.ok(cardUpcoming.isNotWatching(), "unwatched before click")
    stubs.endpoints.update['api/user/auctions/:id']
      .set(Promise.resolve().then(dumbTimeout(
        timeoutWatchUpdate
      )).then(function () {
        return {upcoming: dataAuctionUpcoming}
      }))
    cardUpcoming.clickWatch()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("clicked watch")
    const cardUpcoming = getObjAuctionContainer()
                  .getCardUpcoming(auctionId)
    const dataAuctionUpcoming = _.merge(
            _.cloneDeep(DATA_AUCTION_UPCOMING),
            {is_watching: true})
    t.ok(cardUpcoming, "found upcoming card")
    t.ok(cardUpcoming.isWatchLoading(),
             "watch loading after click")
    stubs.endpoints.fetch['api/time']
      .set(function () {
        return {
          now: setupTeardown.util.formatMoment(moment.utc())
        }
      })
    stubs.endpoints.fetch['api/auction/auctions']
      .set([{
        upcoming: dataAuctionUpcoming
      }])
  }).then(dumbTimeout(timeoutWatchUpdate)).then(function () {
    const modalCompObj = compObj.getModalCompObj()
    t.ok(modalCompObj, "found modal")
    t.ok(modalCompObj instanceof compObj.ModalWatchConfirm,
             "it's the watch confirmation modal")
    t.equal(modalCompObj.getTitleText(),
                "Auction watched!",
                "found expected title text")
    t.equal(modalCompObj.getMsgText(),
                "Check your email for updates.",
                "found expected message text")
    modalCompObj.clickClose()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("clicked close")
    const modalCompObj = compObj.getModalCompObj()
    t.notOk(modalCompObj, "modal closed")
    const cardUpcoming = getObjAuctionContainer()
                  .getCardUpcoming(auctionId)
    t.ok(cardUpcoming.isWatching(),
             "upcoming card displays 'watching' state")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const testView = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("join auction", watchTest)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const testsMain = function (t) {
  t.test("view page state", testView)
  t.end()
}

var exports = testsMain

module.exports = exports
