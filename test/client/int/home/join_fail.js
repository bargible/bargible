/* global require, module, setTimeout */

const compObj = require('../comp_obj')

const setupTeardown = require('./setup_teardown_stubs')

const getObjAuctionContainer = setupTeardown.getObjAuctionContainer

const TIMEOUT_UPDATE_DOM = 100 // ms

const AUCTION_ID = 3

const JOIN_COST = 1000
const USER_CREDIT = 0

const DATA_USER = {
  credit: USER_CREDIT,
  first_name: 'bob',
  id: 1,
  last_name: "belcher",
  username: "bburgers",
  avatar: null,
  email: "b@b.org"
}

const DATA_AUCTION_ACTIVE = {
  id: AUCTION_ID,
  reward: {
    type: "item",
    data: {
      itemId: 1,
      image: "img/something.jpg",
      description: "awesome",
      disclaimer: "not actually a thing",
      name: "thing one"
    }
  },
  numUsers: 3,
  timeRemaining: 45,
  bidPrice: 10,
  minPrice: 1000,
  joinCost: JOIN_COST
}

const DATA_INIT_UNJOINED = {
  user: DATA_USER,
  auctions: {
    active: [DATA_AUCTION_ACTIVE]
  }
}

/** creates a timeout for use with promise chaining */
const dumbTimeout = function (msDur) {
  return function () {
    return new Promise(function (resolve, reject) {
      setTimeout(resolve, msDur)
    })
  }
}

const joinFailTestMain = function (t) {
  const auctionId = AUCTION_ID
  Promise.resolve().then(function () {
    setupTeardown.pageLifecycle.setInit(DATA_INIT_UNJOINED)
    setupTeardown.pageLifecycle.fetchInit()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    getObjAuctionContainer().selectTab('active')
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    getObjAuctionContainer().joinAuction(auctionId)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("clicked join")
    const modalCompObj = compObj.getModalCompObj()
    t.ok(modalCompObj, "found modal")
    t.ok(modalCompObj instanceof compObj.ModalBuyCredits,
             "it's the buy credits modal")
    t.equal(modalCompObj.getMessage(),
                "Oops! You don’t have enough credits " +
                "to join this auction.",
                "and it displays the insufficient credits message")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const testJoinFail = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("fail join auction", joinFailTestMain)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const testsMain = function (t) {
  t.test("insufficient credits", testJoinFail)
  t.end()
}

var exports = testsMain

module.exports = exports
