/* global require, module, setTimeout */

// notes on proxyquire usage in test modules --
//
// 1) proxyquire does not transitively override/proxy modules by default:
// it only proxies require statements contained in the specified module.
//
// 2) node caches requires by default:
// (i.e. require()d modules are singletontons)
// with the module syntax used in this project,
// once a specified module is loaded with proxied dependencies,
// these proxies are still in effect when that particular module is
// required (or transitively required) by another module.

const testsMain = function (t) {
  t.skip("duplicate functional", require('./dupe_func'))
  t.test("test join flow", require('./join_flow'))
  t.test("test join fail", require('./join_fail'))
  t.test("freeze bids", require('./freeze_bids'))
  t.test("tab selection", require('./tab_selection'))
  t.test("auction end", require('./auction_end'))
  t.test("auction end payment flow", require('./auction_end_flow'))
  t.test("test default view", require('./default_view'))
  t.test("view upcoming auctions", require('./view_upcoming'))
  t.test("watch feature", require('./feat_watch'))
  t.end()
}

var exports = testsMain

module.exports = exports
