/* global require, module, setTimeout */

const moment = require('moment')

const setupTeardown = require('./setup_teardown_stubs')

const getObjAuctionContainer = setupTeardown.getObjAuctionContainer

const TIMEOUT_UPDATE_DOM = 100 // ms

const AUCTION_ID = 3

const JOIN_COST = 1000

const DATA_USER_UNJOINED = {
  credit: JOIN_COST,
  first_name: 'bob',
  id: 1,
  last_name: "belcher",
  username: "bburgers",
  avatar: null,
  email: "b@b.org"
}

const DATA_AUCTION_UPCOMING = {
  id: AUCTION_ID,
  reward: {
    type: "item",
    data: {
      itemId: 1,
      image: "img/something.jpg",
      description: "awesome",
      disclaimer: "not actually a thing",
      name: "thing one"
    }
  },
  minPrice: 123,
  start: setupTeardown.util.formatMoment(
        moment.utc().add(5, 'minutes')),
  is_watching: false
}

const DATA_INIT = {
  user: DATA_USER_UNJOINED,
  auctions: {
    upcoming: [DATA_AUCTION_UPCOMING]
  }
}

/** creates a timeout for use with promise chaining */
const dumbTimeout = function (msDur) {
  return function () {
    return new Promise(function (resolve, reject) {
      setTimeout(resolve, msDur)
    })
  }
}

const viewTest = function (t) {
  const auctionId = AUCTION_ID
  Promise.resolve().then(function () {
    setupTeardown.pageLifecycle.setInit(DATA_INIT)
    setupTeardown.pageLifecycle.fetchInit()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    getObjAuctionContainer().selectTab('upcoming')
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    const cardUpcoming = getObjAuctionContainer()
                  .getCardUpcoming(auctionId)
    t.ok(cardUpcoming, "found upcoming card")
    t.equal(cardUpcoming.getRewardNameStr(),
                DATA_AUCTION_UPCOMING['reward']['data']['name'],
                "found expected reward name")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const testView = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("join auction", viewTest)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const testsMain = function (t) {
  t.test("view page state", testView)
  t.end()
}

var exports = testsMain

module.exports = exports
