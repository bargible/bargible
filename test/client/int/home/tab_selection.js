/* global require, module, setTimeout */

const _ = require('lodash')

const stubs = require('../stubs')
const setupTeardown = require('./setup_teardown_stubs')

const getObjAuctionContainer = setupTeardown.getObjAuctionContainer

const TIMEOUT_UPDATE_DOM = 100 // ms

const AUCTION_ID = 3

const JOIN_COST = 1000

const DATA_USER_UNJOINED = {
  credit: JOIN_COST,
  first_name: 'bob',
  id: 1,
  last_name: "belcher",
  username: "bburgers",
  avatar: null,
  email: "b@b.org"
}

const DATA_USER_JOINED = _.mapValues(DATA_USER_UNJOINED, function (val, key) {
  if (_.isEqual(key, 'credit')) {
    return 0
  }
  return val
})

const DATA_AUCTION_ACTIVE = {
  id: AUCTION_ID,
  reward: {
    type: "item",
    data: {
      itemId: 1,
      image: "img/something.jpg",
      description: "awesome",
      disclaimer: "not actually a thing",
      name: "thing one"
    }
  },
  numUsers: 3,
  timeRemaining: 45,
  bidPrice: 10,
  minPrice: 1000,
  joinCost: JOIN_COST
}

const DATA_AUCTION_JOINED = _.merge(_.omit(DATA_AUCTION_ACTIVE, [
  'minPrice',
  'joinCost'
]), {
  bids: 10,
  isBidsFrozen: false,
  position: null,
  leaderboard: [{
    bids: 10,
    position: null,
    avatar: DATA_USER_UNJOINED.avatar,
    username: DATA_USER_UNJOINED.username
  }],
  secsFreezeRemaining: null,
  intermediateTimer: 10,
  bidsTilActivation: {
    freeze: 5, steal: 10, destroy: 20
  },
  numActivations: {
    freeze: 0, steal: 0, destroy: 0
  }
})

const DATA_INIT_UNJOINED = {
  user: DATA_USER_UNJOINED,
  auctions: {
    active: [DATA_AUCTION_ACTIVE]
  }
}

const DATA_INIT_JOINED = {
  user: DATA_USER_JOINED,
  auctions: {
    joined: [DATA_AUCTION_JOINED]
  }
}

/** creates a timeout for use with promise chaining */
const dumbTimeout = function (msDur) {
  return function () {
    return new Promise(function (resolve, reject) {
      setTimeout(resolve, msDur)
    })
  }
}

const joinTest = function (t) {
  const auctionId = AUCTION_ID
  Promise.resolve().then(function () {
    setupTeardown.pageLifecycle.setInit(DATA_INIT_UNJOINED)
    setupTeardown.pageLifecycle.fetchInit()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    getObjAuctionContainer().selectTab('active')
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    getObjAuctionContainer().joinAuction(auctionId)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("clicked join")
    stubs.endpoints.fetch['api/user/get'].set(DATA_USER_JOINED)
    stubs.endpoints.create['api/user/auctions/:id/actions/:action'].set({})
    getObjAuctionContainer().joinConfirm(auctionId)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("clicked join confirm")
    t.deepEqual(_.pick(stubs.endpoints.create['api/user/auctions/:id/actions/:action'].prevReqParams(), ['urlParams']), {
      urlParams: {
        id: auctionId.toString(),
        action: 'join'
      }
    }, "got expected http request for join")
    stubs.endpoints.fetch['api/user/auctions/:id'].set({joined: DATA_AUCTION_JOINED})
    stubs.ws.emit('auction update', {
      auctionId,
      updateType: 'join'
    })
    getObjAuctionContainer().selectTab('joined')
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.ok(getObjAuctionContainer().hasJoinedAuctionEls(auctionId),
             "found expected elements for joined auction after click")
    setupTeardown.pageLifecycle.setInit(DATA_INIT_JOINED)
    setupTeardown.pageLifecycle.simulateReload()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.ok(getObjAuctionContainer().hasJoinedAuctionEls(auctionId),
             "joined tab is selected and joined auction elements exist")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const activeTest = function (t) {
  const auctionId = AUCTION_ID
  Promise.resolve().then(function () {
    setupTeardown.pageLifecycle.setInit(DATA_INIT_UNJOINED)
    setupTeardown.pageLifecycle.fetchInit()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("fetched inital auction data")
    t.ok(getObjAuctionContainer(),
             "home page application componenet displayed")
    t.ok(getObjAuctionContainer().hasActiveAuctionEls(auctionId),
             "active tab is selected and active auction elements exist")
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    setupTeardown.pageLifecycle.setInit(DATA_INIT_UNJOINED)
    setupTeardown.pageLifecycle.simulateReload()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.ok(getObjAuctionContainer().hasActiveAuctionEls(auctionId),
             "active tab is selected and active auction elements exist")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const testWithJoinedAuction = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("join auction", joinTest)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const testWithActiveAuction = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("active auction", activeTest)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const testsMain = function (t) {
  t.test("test joined auction tab selection", testWithJoinedAuction)
  t.skip("test active auction tab selection", testWithActiveAuction)
  t.end()
}

var exports = testsMain

module.exports = exports
