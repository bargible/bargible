/* global require, module, setTimeout */

const _ = require('lodash')
const moment = require('moment')
const proxyquire = require('proxyquire')
const React = require('react')

const compObj = require('../comp_obj')
const stubs = require('../stubs')

// see comment in ../user_profile/setup_teardown.js
_.forEach(_.keys(require.cache), function (moduleName) {
  if (_.includes(moduleName, 'client/app')) {
    delete require.cache[moduleName]
  }
})

proxyquire(
    '../../../../client/app/util',
  {
    './http': stubs.http,
    './braintree': stubs.braintree
  }
)

const AllPagesActions = require(
    '../../../../client/app/actions/all_pages_actions')
const HomeActions = require(
    '../../../../client/app/actions/home_actions')

proxyquire(
    '../../../../client/app/comp/auction_active',
  {
    './util': stubs.compUtil
  }
)

const AuctionContainerNode = require(
    '../../../../client/app/comp/main_home')
const AuctionContainerStore = require(
    '../../../../client/app/stores/auction_container_store')
const alt = require('../../../../client/app/alt')
const auctionUpdateHandler =
          require('../../../../client/app/util_home')
          .auctionUpdateHandler

// global application config as from /api/config
const CONFIG = Object.freeze({
  untilFreeze: 5,
  untilSteal: 10,
  untilDestroy: 15,
  initNumBids: 50,
  intermediateTimer: 10,
  maxUsersPerAuction: 20,
  starLevels: {
    silver: 100,
    gold: 500
  }
})

// util

const formatMoment = function (momentInst) {
  return momentInst.format().replace(/Z$/, '.000000')
}

// per-test variables
var objAuctionContainer

var renderAuctionContainer = function (storeState) {
  if (_.isNull(storeState)) {
    return
  }
  const props = _.merge(storeState, {
    config: CONFIG,
    params: Object.freeze({
      btClientToken: 'stub-braintree-client-token'
    })
  })
  objAuctionContainer = new compObj.AuctionContainer(
        React.createElement(AuctionContainerNode, props))
}

const setup = function (t) {
  renderAuctionContainer(AuctionContainerStore.getState())
  AuctionContainerStore.listen(renderAuctionContainer)
  stubs.ws.on('auction update', auctionUpdateHandler)
  t.end()
}

const teardown = function (t) {
  AuctionContainerStore.unlisten(renderAuctionContainer)
  objAuctionContainer.unmount()
  alt.recycle()
  stubs.reset()
  objAuctionContainer = undefined
  compObj.maybeUnmountModal()
  t.end()
}

const fetchInit = function () {
  AllPagesActions.fetchUser()
  HomeActions.setConfig(CONFIG)
  HomeActions.fetchUpcomingAuctions()
  HomeActions.fetchActiveAuctions()
  HomeActions.fetchJoinedAuctions()
  HomeActions.fetchEndedAuctions()
}

const setInit = function (initData) {
  HomeActions.setConfig(CONFIG)
  stubs.endpoints.fetch['api/time']
    .set(function () {
      return {
        now: formatMoment(moment.utc())
      }
    })
  if (initData.user) {
    stubs.endpoints.fetch['api/user/get']
      .set(initData.user)
  }
  _.forEach([
        ['upcoming', 'api/auction/auctions'],
        ['active', 'api/auction/auctions'],
        ['joined', 'api/user/auctions'],
        ['ended', 'api/user/auctions']
  ], _.spread(function (statusType, urlStr) {
    if (initData.auctions[statusType]) {
      stubs.endpoints.fetch[urlStr].set(
                _.map(initData.auctions[statusType],
                      (aucData) => ({[statusType]: aucData}))
            )
    } else {
      stubs.endpoints.fetch[urlStr].set([])
    }
  }))
}

/** simulate browser reload */
const simulateReload = function () {
    // teardown-like
  AuctionContainerStore.unlisten(renderAuctionContainer)
  objAuctionContainer.unmount()
  alt.recycle()
  objAuctionContainer = undefined
  stubs.ws.resetListeners()
    // setup-like
  renderAuctionContainer(AuctionContainerStore.getState())
  AuctionContainerStore.listen(renderAuctionContainer)
  stubs.ws.on('auction update', auctionUpdateHandler)
  fetchInit()
}

var exports = {}

exports.HomeActions = HomeActions
exports.util = {
  formatMoment: formatMoment
}
exports.pageLifecycle = {
  simulateReload: simulateReload,
  setInit: setInit,
  fetchInit: fetchInit
}
exports.setup = setup
exports.teardown = teardown
exports.getObjAuctionContainer = function () {
  return objAuctionContainer
}
exports.CONFIG = CONFIG

module.exports = exports
