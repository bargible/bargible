/* global require, module, setTimeout */

const _ = require('lodash')

const stubs = require('../stubs')
const setupTeardown = require('./setup_teardown_stubs')

const getObjAuctionContainer = setupTeardown.getObjAuctionContainer

const TIMEOUT_UPDATE_DOM = 100 // ms

const AUCTION_ID = 4

const DATA_USER = {
  credit: 0,
  first_name: 'bob',
  id: 1,
  last_name: "belcher",
  username: "bburgers",
  avatar: null,
  email: "b@b.org"
}

const DATA_AUCTION_JOINED = {
  id: AUCTION_ID,
  reward: {
    type: "item",
    data: {
      itemId: 1,
      image: "img/something.jpg",
      description: "awesome",
      disclaimer: "not actually a thing",
      name: "thing one"
    }
  },
  numUsers: 3,
  timeRemaining: 45,
  bidPrice: 10,
  bids: 10,
  isBidsFrozen: false,
  position: null,
  leaderboard: [{
    bids: 10,
    position: null,
    avatar: DATA_USER.avatar,
    username: DATA_USER.username
  }],
  secsFreezeRemaining: null,
  intermediateTimer: 10,
  bidsTilActivation: {
    freeze: 5, steal: 10, destroy: 20
  },
  numActivations: {
    freeze: 0, steal: 0, destroy: 0
  }
}

const DATA_INIT_JOINED = {
  user: DATA_USER,
  auctions: {
    joined: [DATA_AUCTION_JOINED]
  }
}

/** creates a timeout for use with promise chaining */
const dumbTimeout = function (msDur) {
  return function () {
    return new Promise(function (resolve, reject) {
      setTimeout(resolve, msDur)
    })
  }
}

const freezeTestMain = function (t) {
  const auctionId = AUCTION_ID
  Promise.resolve().then(function () {
    setupTeardown.pageLifecycle.setInit(DATA_INIT_JOINED)
    setupTeardown.pageLifecycle.fetchInit()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("initial fetch")
    const objAuctionContainer = getObjAuctionContainer()
    stubs.endpoints.update['api/user/auctions/:id/actions/:action'].set({})
    objAuctionContainer.freeze(auctionId)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    const prevReqParamsUserActions = stubs.endpoints
      .update['api/user/auctions/:id/actions/:action'].prevReqParams()
    t.pass("clicked freeze")
    t.ok(prevReqParamsUserActions,
             "user actions hit after click")
    t.equals(prevReqParamsUserActions.urlParams['action'],
                 'freeze',
                 "with 'freeze' action")
    t.equals(prevReqParamsUserActions.urlParams['id'],
                 auctionId.toString(),
                 "and correct auction id")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const testFreeze = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("test", freezeTestMain)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const frozenBidsTestMain = function (t) {
  const auctionId = AUCTION_ID
  const otherUsername = 'linda'
  Promise.resolve().then(function () {
    setupTeardown.pageLifecycle.setInit(DATA_INIT_JOINED)
    setupTeardown.pageLifecycle.fetchInit()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("initial fetch")
    getObjAuctionContainer().selectTab('joined')
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    const objAuctionContainer = getObjAuctionContainer()
    t.ok(objAuctionContainer.hasJoinedAuctionEls(auctionId),
             "found joined auction display")
    t.notOk(objAuctionContainer.isFreezeVisual(auctionId),
                "frozen bid visual not displayed before sim freeze")
        // endpoint probably shouldn't be hit twice,
        // but it's probably getting hit on both the
        // 'game-element' _and_ the 'bids-freeze' event
    _.times(2, function () {
      const dataAucJoined = _.merge(DATA_AUCTION_JOINED,
                                          {isBidsFrozen: true})
      stubs.endpoints.fetch['api/user/auctions/:id']
        .set({joined: dataAucJoined})
    })
        // duplicate server-side functionality, even if
        // it's not encoded in the server int tests
    stubs.ws.emit('auction update', {
      auctionId: auctionId,
      updateType: 'bids-freeze',
      updateInfo: {
        username: otherUsername
      }
    })
    stubs.ws.emit('auction update', {
      auctionId: auctionId,
      updateType: 'game-element'
    })
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("emitted freeze-related update events")
    const objAuctionContainer = getObjAuctionContainer()
    t.ok(objAuctionContainer.isFreezeVisual(auctionId),
                "frozen bid visual displayed after sim freeze")
    objAuctionContainer.bid(auctionId)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.equal(_.size(
      stubs.endpoints.update['api/user/auctions/:id/actions/:action']
        .requestsParams), 0,
      "no calls to action endpoint after attempted bid")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const testFrozenBids = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("test", frozenBidsTestMain)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const testsMain = function (t) {
  t.test("user clicks freeze", testFreeze)
  t.test("user's bids are frozen", testFrozenBids)
  t.end()
}

var exports = testsMain

module.exports = exports
