/* global require, module, setTimeout */

const _ = require('lodash')
const moment = require('moment')

const compObj = require('../comp_obj')

const stubs = require('../stubs')
const setupTeardown = require('./setup_teardown_stubs')

const TIMEOUT_UPDATE_DOM = 100 // ms

const AUCTION_ID = 3

const DATA_USER = Object.freeze({
  credit: 123,
  first_name: 'bob',
  id: 1,
  last_name: "belcher",
  username: "bburgers",
  avatar: null,
  email: "b@b.org"
})

const DATA_REWARD_CARD = {
  type: "item",
  data: {
    itemId: 5,
    image: "/stub/img.jpg",
    description: "stub description",
    disclaimer: "stub disclaimer",
    name: "stub name"
  }
}

const DATA_SHIPPING_ADDRESS = {
  'name': 'stub name',
  'addr_1': 'stub addr_1',
  'addr_2': 'stub addr_2',
  'city': 'stub city',
  'usa_state': 'AL',
  'zip_code': '12345'
}

const DATA_REWARD_PHYSICAL_ITEM = {
  type: "physical_item",
  data: {
    itemId: 6,
    image: "/stub/img.jpg",
    description: "stub description",
    name: "stub name"
  }
}

const DATA_AUCTION_ENDED = {
  id: AUCTION_ID,
  reward: null, // tests replace with appropriate data
  start: setupTeardown.util.formatMoment(
        moment.utc().subtract(5, 'minutes')),
  end: setupTeardown.util.formatMoment(moment.utc()),
  realPrice: 235,
  position: 1,
  lastBidUsername: DATA_USER.username,
  minPrice: 235,
  bidPrice: 11
}

/** creates a timeout for use with promise chaining */
const dumbTimeout = function (msDur) {
  return function () {
    return new Promise(function (resolve, reject) {
      setTimeout(resolve, msDur)
    })
  }
}

const makeInitFlow = function (dataReward) {
  return function (t) {
    Promise.resolve().then(function () {
      setupTeardown.pageLifecycle.setInit({
        user: DATA_USER,
        auctions: {}
      })
      setupTeardown.pageLifecycle.fetchInit()
    }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
      t.pass("initial fetch")
      const modalCompObj = compObj.getModalCompObj()
      t.notOk(modalCompObj, "no modal displayed")
      stubs.endpoints.fetch['api/user/auctions/:id'].set({
        ended: _.set(_.cloneDeep(DATA_AUCTION_ENDED),
          'reward', dataReward)
      })
      stubs.ws.emit('auction update', {
        auctionId: AUCTION_ID,
        updateType: 'end'
      })
    }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
      const modalCompObj = compObj.getModalCompObj()
      t.ok(modalCompObj, "found modal")
      t.ok(modalCompObj instanceof compObj.ModalAuctionEnd,
                 "it's the auction end modal")
      t.end()
    }).catch(function (err) {
      console.error(err.stack)
      t.end(err)
    })
  }
}

const paySuccessCardTestMain = function (t) {
  Promise.resolve().then(function () {
    const modalCompObj = compObj.getModalCompObj()
    modalCompObj.clickPay()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("clicked the payment button")
    const modalCompObj = compObj.getModalCompObj()
    t.ok(modalCompObj, "found modal")
    t.ok(modalCompObj instanceof compObj.ModalAuctionEndPay,
      "it's the auction end payment modal")
    stubs.endpoints.create['api/user/auctionPayments'].set({})
    modalCompObj.clickPay()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("clicked the payment button")
    const modalCompObj = compObj.getModalCompObj()
    t.ok(modalCompObj, "found modal")
    t.ok(modalCompObj instanceof compObj.ModalAuctionPayConfirm,
             "it's the payment confirmation modal")
    modalCompObj.clickClose()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("clicked the close button")
    const modalCompObj = compObj.getModalCompObj()
    t.notOk(modalCompObj, "modal closed")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const payCancelTestMain = function (t) {
  Promise.resolve().then(function () {
    const modalCompObj = compObj.getModalCompObj()
    modalCompObj.clickClose()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("clicked the close button")
    const modalCompObj = compObj.getModalCompObj()
    t.notOk(modalCompObj, "no modal displayed")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const paySuccessPhysicalItemMain = function (t) {
  Promise.resolve().then(function () {
    const modalCompObj = compObj.getModalCompObj()
    modalCompObj.clickPay()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("clicked the payment button")
    const modalCompObj = compObj.getModalCompObj()
    t.ok(modalCompObj, "found modal")
    t.ok(modalCompObj instanceof compObj.ModalAuctionEndAddr,
             "it's the auction end address modal")
    _(DATA_SHIPPING_ADDRESS).toPairs()
            .forEach(_.spread(
                modalCompObj.inputAddrField.bind(modalCompObj)
            ))
    t.pass("input shipping address to component")
    stubs.endpoints.create['api/user/auctionAddress'].set({})
    modalCompObj.clickNext()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    const reqJson = stubs.endpoints.create['api/user/auctionAddress']
      .prevReqParams().json
    t.ok(reqJson, "got request data for auction address")
    t.equal(reqJson['auctionId'], AUCTION_ID,
                "it contains the correct auction id")
    t.deepEqual(reqJson['shippingAddress'], DATA_SHIPPING_ADDRESS,
                    "it contains the correct shipping address")
    const modalCompObj = compObj.getModalCompObj()
    t.ok(modalCompObj, "found modal")
    t.ok(modalCompObj instanceof compObj.ModalAuctionEndPay,
             "it's the auction end payment modal")
    stubs.endpoints.create['api/user/auctionPayments'].set({})
    modalCompObj.clickPay()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("clicked the payment button")
    const modalCompObj = compObj.getModalCompObj()
    t.ok(modalCompObj, "found modal")
    t.ok(modalCompObj instanceof compObj.ModalAuctionPayConfirm,
             "it's the payment confirmation modal")
    modalCompObj.clickClose()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("clicked the close button")
    const modalCompObj = compObj.getModalCompObj()
    t.notOk(modalCompObj, "modal closed")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const testSuccessCard = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("init", makeInitFlow(DATA_REWARD_CARD))
  t.test("test", paySuccessCardTestMain)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const testPayCancel = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("init", makeInitFlow(DATA_REWARD_CARD))
  t.test("test", payCancelTestMain)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const testSuccessPhysicalItem = function (t) {
  t.test("setup", setupTeardown.setup)
  t.test("init", makeInitFlow(DATA_REWARD_PHYSICAL_ITEM))
  t.test("test", paySuccessPhysicalItemMain)
  t.test("teardown", setupTeardown.teardown)
  t.end()
}

const testsMain = function (t) {
  t.test("click cancel on end modal", testPayCancel)
  t.test("payment success for giftcard auction", testSuccessCard)
  t.test("payment success for physical item auction",
           testSuccessPhysicalItem)
  t.end()
}

var exports = testsMain

module.exports = exports
