/* global require, module, setTimeout */

const _ = require('lodash')
const proxyquire = require('proxyquire')
var React = require('react')

const mocks = require('../mocks')
const compObj = require('../comp_obj')

proxyquire(
    '../../../../client/app/util',
    _.mapKeys(mocks.utilModules, function (val, key) {
      return './' + key
    })
)
const HomeActions = require(
    '../../../../client/app/actions/home_actions')
const NavNode = require(
    '../../../../client/app/comp/nav')
proxyquire(
    '../../../../client/app/comp/auction_active',
    _.mapKeys(mocks.comp_modules, function (val, key) {
      return './' + key
    })
)
const AuctionContainerNode = require(
    '../../../../client/app/comp/main_home')
const AuctionContainerStore = require(
    '../../../../client/app/stores/auction_container_store')
const alt = require('../../../../client/app/alt')
const auctionUpdateHandler =
          require('../../../../client/app/util_home')
          .auctionUpdateHandler

const TIMEOUT_UPDATE_DOM = 100 // ms
var AID

// per-test variables
var objNav
var objAuctionContainer
var expectedNumBids

/** creates a timeout for use with promise chaining */
const dumbTimeout = function (msDur) {
  return function () {
    return new Promise(function (resolve, reject) {
      setTimeout(resolve, msDur)
    })
  }
}

var renderAuctionContainer = function (storeState) {
  if (_.isNull(storeState)) {
    return
  }
  objAuctionContainer = new compObj.AuctionContainer(
        React.createElement(AuctionContainerNode, storeState))
}

const setup = function (t) {
  objNav = new compObj.Nav(
        React.createElement(NavNode, {navLinks: mocks.CONST.NAV_LINKS}))
  renderAuctionContainer(AuctionContainerStore.getState())
  AuctionContainerStore.listen(renderAuctionContainer)
  mocks.wsOn('auction update', auctionUpdateHandler)
  expectedNumBids = 310
  t.end()
}

const teardown = function (t) {
  AuctionContainerStore.unlisten(renderAuctionContainer)
  objNav.unmount()
  objAuctionContainer.unmount()
  alt.recycle()
  mocks.reset()
  expectedNumBids = undefined
  objAuctionContainer = undefined
  t.end()
}

/// /////// duplicate function test
// the functions until dupFuncTestMain are
// only _nominally_ seperate test cases.  see dupFuncTestMain
// for the actual (i.e. isolated) test.
//

const initTest = function (t) {
  // TODO: unused? var minPriceMult = 0.01
  Promise.resolve().then(function () {
    HomeActions.fetchUser()
    HomeActions.fetchUpcomingAuctions()
    HomeActions.fetchAuctions()
    HomeActions.fetchActiveAuctions()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.equal(objNav.greetingName(),
                mocks.CONST.USER_FIRST_NAME,
                "greeting name in navbar")
    t.skip("correct credit value in navbar")
        // t.equal(objNav.creditValStr(),
        //         mocks.CONST.USER_CREDIT.toString(),
        //         "correct credit value in navbar");
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const joinTest = function (t) {
  Promise.resolve().then(function () {
    t.equal(_(mocks.utilModules.http.createJson.calls)
                .map('args').map((args) => args[0])
                .filter((uri) => uri.match(/^api\/auction\/(\d+)\/join$/))
                .size(), 0,
                "join endpoint not hit before click")
    objAuctionContainer.selectTab('joined')
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.notOk(objAuctionContainer.hasJoinedAuctionEls(AID),
                "expected elements for joined auction not present " +
                "before click")
    objAuctionContainer.selectTab('active')
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    objAuctionContainer.joinAuction(AID)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("clicked join")
    objAuctionContainer.joinConfirm(AID)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("clicked join confirm")
    t.equal(_(mocks.utilModules.http.createJson.calls)
                .map('args').map((args) => args[0])
                .filter((uri) => uri.match(/^api\/auction\/(\d+)\/join$/))
                .size(), 1,
                "join endpoint hit after click")
    t.ok(objAuctionContainer.hasJoinedAuctionEls(AID),
             "found expected elements for joined auction after click")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const bidTestOneP = function (t) {
  var bidMult = 0.01
  Promise.resolve().then(function () {
    t.equal(_(mocks.utilModules.http.createJson.calls)
                .map('args').map((args) => args[0])
                .filter((uri) => uri.match(/^api\/auction\/(\d+)\/bid$/))
                .size(), 0,
                "bid endpoint not hit before click")
    objAuctionContainer.bid(AID)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.equal(_(mocks.utilModules.http.updateJson.calls)
                .map('args').map((args) => args[0])
                .filter((uri) => uri.match(/^api\/auction\/(\d+)\/bid$/))
                .size(), 1,
                "bid endpoint hit after click")
    t.equal(objAuctionContainer.numBids(AID), expectedNumBids,
                "found expected number of bids")
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.equal(objAuctionContainer.bidPrice(AID), bidMult * 2,
                "found expected bid price")
    expectedNumBids -= 1
    objAuctionContainer.bid(AID)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.equal(objAuctionContainer.numBids(AID), expectedNumBids,
                "found expected number of bids")
    t.equal(objAuctionContainer.bidPrice(AID), bidMult * 3,
                "found expected bid price")
    expectedNumBids -= 1
    objAuctionContainer.bid(AID)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.equal(objAuctionContainer.numBids(AID), expectedNumBids,
                "found expected number of bids")
    t.equal(objAuctionContainer.bidPrice(AID), bidMult * 4,
                "found expected bid price")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const gameTestOneP = function (t) {
  Promise.resolve().then(function () {
    t.equal(_(mocks.utilModules.http.updateJson.calls)
                .map('args').map((args) => args[0])
                .filter((uri) => uri.match(
                        /^api\/auction\/(\d+)\/stealBids$/))
                .size(), 0,
                "steal bids endpoint not hit before click")
    objAuctionContainer.steal(AID)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.equal(_(mocks.utilModules.http.updateJson.calls)
                .map('args').map((args) => args[0])
                .filter((uri) => uri.match(
                        /^api\/auction\/(\d+)\/stealBids$/))
                .size(), 1,
                "steal bids endpoint hit after click")
    t.ok(_.includes(
            objAuctionContainer.notificatonTexts(AID),
            'You stole 0 bids from players!'),
             "note text after steal")
    t.equal(objAuctionContainer.numBids(AID), expectedNumBids,
                "found expected number of bids after steal")
    t.equal(_(mocks.utilModules.http.updateJson.calls)
                .map('args').map((args) => args[0])
                .filter((uri) => uri.match(
                        /^api\/auction\/(\d+)\/destroyBids$/))
                .size(), 0,
                "destroy bids endpoint not hit before click")
    objAuctionContainer.destroy(AID)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.equal(_(mocks.utilModules.http.updateJson.calls)
                .map('args').map((args) => args[0])
                .filter((uri) => uri.match(
                        /^api\/auction\/(\d+)\/destroyBids$/))
                .size(), 1,
                "destroy bids endpoint hit after click")
    t.ok(_.includes(
            objAuctionContainer.notificatonTexts(AID),
            'You destroyed no bids!'),
             "note text after destroy")
    t.equal(objAuctionContainer.numBids(AID), expectedNumBids,
                "found expected number of bids after destroy")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const bidTestTwoP = function (t) {
  var bidMult = 0.01
  Promise.resolve().then(function () {
    mocks.sim.join(AID)
  }).then(function () {
    t.pass("simulated join")
    mocks.sim.bid(AID)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("simulated bid")
    t.equal(objAuctionContainer.bidPrice(AID), bidMult * 5,
                "found expected bid price")
    mocks.sim.bid(AID)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("simulated bid")
    t.equal(objAuctionContainer.bidPrice(AID), bidMult * 6,
                "found expected bid price")
    mocks.sim.bid(AID)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("simulated bid")
    t.equal(objAuctionContainer.bidPrice(AID), bidMult * 7,
                "found expected bid price")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const gameTestTwoP = function (t) {
  Promise.resolve().then(function () {
    expectedNumBids -= 1
    mocks.sim.stealBids(AID)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("simulated steal bids")
    t.equal(objAuctionContainer.numBids(AID), expectedNumBids,
                "expected number of bids after simulated steal bids")
    t.ok(_.includes(
            objAuctionContainer.notificatonTexts(AID),
            ('You had a bid stolen by ' +
             mocks.CONST.OTHER_USERNAME + '!')),
             "message after simulated steal bids")
    expectedNumBids -= 5
    mocks.sim.destroyBids(AID)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.pass("simulated destroy bids")
    t.equal(objAuctionContainer.numBids(AID), expectedNumBids,
                "expected number of bids after simulated destroy bids")
    t.ok(_.includes(
            objAuctionContainer.notificatonTexts(AID),
            ('You had 5 bids destroyed by ' +
             mocks.CONST.OTHER_USERNAME + '!')),
             "message after simulated destroy bids")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const bidBugTest = function (t) {
  const TIMEOUT_DISPLAY_MESSAGE = 5000
  Promise.resolve().then(function () {
  }).then(dumbTimeout(TIMEOUT_DISPLAY_MESSAGE)).then(function () {
    t.equal(objAuctionContainer.numBids(AID), expectedNumBids,
                "expected number of bids after message clear")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const auctionEndTest = function (t) {
  Promise.resolve().then(function () {
    t.ok(objAuctionContainer.hasJoinedAuctionEls(AID),
             "found expected elements for joined auction " +
             "before simulated auction end")
    mocks.sim.endAuction(AID)
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    t.notOk(objAuctionContainer.hasJoinedAuctionEls(AID),
                "unexpectedly found elements for joined auction " +
                "after simulated auction end")
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const dupFuncTestMain = function (t) {
    // none of these tests may be skipped.
    // they are only nominally seperate test cases
  t.test("initial state", initTest)
  t.test("join auction", joinTest)
  t.test("one player bids", bidTestOneP)
  t.test("one player game elements", gameTestOneP)
  t.test("simulated player bids", bidTestTwoP)
  t.test("simulated player game elements", gameTestTwoP)
  t.test("bid bug regression", bidBugTest)
  t.test("auction end", auctionEndTest)
  t.end()
}

const testItem = function (t) {
  AID = mocks.CONST.JOIN_ITEM_AUCTION_ID
  t.test("setup", setup)
  t.test("test", dupFuncTestMain)
  t.test("teardown", teardown)
  t.end()
}

const testsMain = function (t) {
  t.test("test item auction", testItem)
  t.end()
}

var exports = testsMain

module.exports = exports
