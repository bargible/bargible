/* global require, module, setTimeout */

const compObj = require('../comp_obj')

const setupTeardown = require('./setup_teardown_stubs')

const getObjAuctionContainer = setupTeardown.getObjAuctionContainer

const TIMEOUT_UPDATE_DOM = 100 // ms

const USER_CREDIT = 0

const DATA_USER = {
  credit: USER_CREDIT,
  first_name: 'bob',
  id: 1,
  last_name: 'belcher',
  username: 'bburgers',
  avatar: null,
  email: 'b@b.org'
}

const DATA_INIT_UNJOINED = {
  user: DATA_USER,
  auctions: {}
}

/** creates a timeout for use with promise chaining */
const dumbTimeout = function (msDur) {
  return function () {
    return new Promise(function (resolve, reject) {
      setTimeout(resolve, msDur)
    })
  }
}

const buyCreditsTestMain = function (t) {
  Promise.resolve().then(function () {
    setupTeardown.pageLifecycle.setInit(DATA_INIT_UNJOINED)
    setupTeardown.pageLifecycle.fetchInit()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    getObjAuctionContainer().selectTab('default')
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM * 10)).then(function () {
    getObjAuctionContainer().buyCredits()
  }).then(dumbTimeout(TIMEOUT_UPDATE_DOM)).then(function () {
    const modalCompObj = compObj.getModalCompObj()
    t.ok(modalCompObj, 'found modal')
    t.ok(modalCompObj instanceof compObj.ModalBuyCredits,
             'it is the buy credits modal')
    t.skip(modalCompObj.getMessage(),
               'Join auctions and steal/freeze/destroy other bids.')
    t.end()
  }).catch(function (err) {
    console.error(err.stack)
    t.end(err)
  })
}

const testBuyCredits = function (t) {
  t.test('setup', setupTeardown.setup)
  t.test('buy credits check', buyCreditsTestMain)
  t.test('teardown', setupTeardown.teardown)
  t.end()
}

const testsMain = function (t) {
  t.test('buy credits', testBuyCredits)
  t.end()
}

var exports = testsMain

module.exports = exports
