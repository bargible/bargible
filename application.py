#!flask/bin/python
import logging
import argparse
import sys

sys.path.append('server')

from app import initFlaskApp
from app import appFlaskSocketIO as socketio


def parseArgs():
    """ parse script arguments """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--debug',
        action="store_true",
        help=('whether enable debugging on async server'),
    )
    return parser.parse_args()

def launch(debug=True):
    app = initFlaskApp()
    logging.basicConfig(filename = '/var/log/bargible/auction.log',
                            level = logging.INFO)
    logger = logging.getLogger(__name__)
    logger.info('---- New instance started ----')
    socketio.run(app, debug=debug)

if __name__ == '__main__':
    args = parseArgs()
    launch(debug=(args.debug is not None))
