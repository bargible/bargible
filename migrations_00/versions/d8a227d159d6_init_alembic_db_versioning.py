"""init alembic db versioning

Revision ID: d8a227d159d6
Revises:
Create Date: 2017-04-10 03:01:20.525867

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd8a227d159d6'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
