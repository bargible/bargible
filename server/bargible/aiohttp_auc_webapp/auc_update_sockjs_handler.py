"""
SockJS (https://github.com/aio-libs/sockjs) handler for
server -> client auction update events
"""
from warnings import warn

from sockjs import MSG_OPEN
from sockjs import MSG_MESSAGE
from sockjs import MSG_CLOSE
from sockjs import MSG_CLOSED

import sockjs
from sockjs import Session as SessionBase
from sockjs import SessionManager as SessionManagerBase

from bargible.model import saveModels

from .auc_update_aioredis_emitter import AucUpdateAioRedisEmitter

class AucUpdateSession(SessionBase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert not hasattr(self, '_user')
        self._user = None

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, value):
        assert self._user is None
        self._user = value

    def __repr__(self):
        return ('<AucUpdateSession ' + str(self) +
                    ' User=' + repr(self.user) + '>')


class AucUpdateSessionManager(SessionManagerBase):
    """
    tacks the underlying aiohttp request onto the sockjs.Session
    """

    # todo: finalize api (e.g. when getUserFromRequest returns None
    # or if such a function is necessary as compared to
    # aiohttp middleware) after revisiting auth
    def __init__(self, getUserFromRequest,
                     *args, **kwargs):
        """
        * aucUpdateAioRedisEmitter
            (.auc_update_aioredis_emitter.AucUpdateAioRedisEmitter)
        * getUserFromRequest
            (aiohttp.web.Request -> bargible.model.User):
          extract the current user from the aiohttp request.
          may return None.
        """
        super().__init__('auc_update', *args, **kwargs)
        self.factory = AucUpdateSession
        assert not hasattr(self, '_getUserFromRequest')
        self._getUserFromRequest = getUserFromRequest
        assert not hasattr(self, '_aucUpdateAioRedisEmitter')
        self._aucUpdateAioRedisEmitter = AucUpdateAioRedisEmitter(self)

    def start(self):
        super().start()
        self._aucUpdateAioRedisEmitter.start()

    def get(self, *args, **kwargs):
        session = super().get(*args, **kwargs)
        request = kwargs.get('request', None)
        if session.user is None and request is not None:
            session.user = self._getUserFromRequest(request)
        return session


async def onAppShutdown(app):
    """
    attach to the web.Application.on_shutdown signal
    to cleanup websockets on application shutdown
    http://aiohttp.readthedocs.io/en/stable/web_reference.html#aiohttp.web.Application.on_shutdown
    """
    aucUpdateSessionManager = sockjs.get_manager('auc_update', app)
    await aucUpdateSessionManager.clear()
    aucUpdateSessionManager.stop()


async def aucUpdateSessionHandler(msg, sess):
    """
    msg (sockjs.protocol.SockjsMessage):
      a collections.namedtuple with fields 'tp', 'data',
      where 'tp' is one of the 'MSG_*' types exported by sockjs
    sess (AucUpdateSession)
    """
    user = sess.user
    if user is None:
        return
    if msg.tp == sockjs.MSG_OPEN:
        user.sid = sess.id
        saveModels(user)
        sess.send('connected')
    elif msg.tp == sockjs.MSG_CLOSE:
        user.sid = None
        saveModels(user)
    elif msg.tp == sockjs.MSG_CLOSED:
        pass
    else:
        warn(("unexpected message (by type) from client side" +
                  repr(msg) + " for session " + repr(sess)))
