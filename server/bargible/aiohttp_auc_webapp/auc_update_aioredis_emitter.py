"""
mechanism for broadcasting events from redis pubsub
to websocket connections.

bolted to both .auc_update_sockjs_handler.AucUpdateSessionManager
and socketio.RedisManager (via bargible.client_auc_events)
"""
import pickle
import json
import asyncio
from warnings import warn
from urllib.parse import urlparse

import aioredis

# matches channel defn in bargible.client_auc_events
CHANNEL = 'auc-socketio'

def _parse_redis_url(url):
    p = urlparse(url)
    if p.scheme != 'redis':
        raise ValueError('Invalid redis url')
    if ':' in p.netloc:
        host, port = p.netloc.split(':')
        port = int(port)
    else:
        host = p.netloc or 'localhost'
        port = 6379
    if p.path:
        db = int(p.path[1:])
    else:
        db = 0
    if not host:
        raise ValueError('Invalid redis hostname')
    return host, port, db


class AucUpdateAioRedisEmitter(object):

    def __init__(self, sessionManager,
                     url='redis://localhost:6379/11'):
        """
        * sessionManager (sockjs.SessionManager)
        * url (str): the url of the redis database to connect
           to for pubsub updates.
        """
        self._sessionManager = sessionManager
        self._emitterTask = None
        self.host, self.port, self.db = _parse_redis_url(url)

    def start(self):
        if not self._emitterTask:
            self._emitterTask = asyncio.ensure_future(
                self._listen(),
                loop=self._sessionManager.loop,
                )

    async def _listen(self):
        sub = await aioredis.create_redis(
            (self.host, self.port),
            db=self.db,
            )
        chan = (await sub.subscribe(CHANNEL))[0]
        while True:
            pickledMsg = await chan.get()
            msg = pickle.loads(pickledMsg)
            if not (msg['event'] == 'auction update' or
                        msg['namespace'] == '/' or
                        msg['method'] == 'emit' or
                        # ?
                        msg['skip_sid'] is None or
                        # ?
                        msg['callback'] is None):
                warn(("unexpected message format out of " +
                          "socketio redis pubsub " + repr(msg)))
            if 'data' not in msg:
                warn(("didn't find 'data' key in socketio "
                          "redis pubsub message"))
                continue
            data = msg['data']
            if 'room' not in msg:
                warn(("didn't find 'room' key in socketio "
                          "redis pubsub message"))
                continue
            sessionId = msg['room']
            self._emitToSession(sessionId, data)

    def _emitToSession(self, sessionId, data):
        session = self._sessionManager.get(sessionId, None)
        if session is None:
            warn(("tried to emit to session, but didn't find "
                      "session id in session manager"))
            return
        session.send(json.dumps(data))
