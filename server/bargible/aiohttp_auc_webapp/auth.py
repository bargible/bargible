"""
auth-related bolts

likely in much flux til "server-side sessions"
( https://app.asana.com/0/105138209178168/273171743565304 )
task is completed
"""
from app.custom_flask_login import load_user

def getUserFromFlaskAndWSGIEnviron(flaskApp, environ):
    """
    tight but unidirectional bolts to Flask application
    for auth,  this helper function is an attempt to start
    unravelling the (suprious) websocket-WSGI metaphor (too?)
    early on
    """
    userIdStr = flaskApp.open_session(
        flaskApp.request_class(environ)
        ).get('user_id', None)
    if userIdStr is None:
        return None
    return load_user(userIdStr)
