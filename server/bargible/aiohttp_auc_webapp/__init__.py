"""
a "websockets and http all-in-one-process" web application
along the lines of Flask-SocketIO, except custom rolled using
* aiohttp instead of eventlet
* python socketio directly
"""
from functools import partial

from aiohttp import web as aioweb
from aiohttp_wsgi import WSGIHandler
import sockjs
import socketio
# extract WSGI environ from aiohttp request.
# aiohttp_wsgi.WSGIHandler._get_environ is similar
from engineio.async_aiohttp import (
    translate_request as _translate_request)

from bargible.model import saveModels
from bargible.model import User
from app import initFlaskApp

from .auc_update_sockjs_handler import aucUpdateSessionHandler
from .auc_update_sockjs_handler import AucUpdateSessionManager
from .auc_update_sockjs_handler import (
    onAppShutdown as cleanupAucUpdateSockjs)
from .auth import getUserFromFlaskAndWSGIEnviron

sio = socketio.AsyncServer(
    client_manager=socketio.AsyncRedisManager(
        url='redis://localhost:6379/11',
        channel='auc-socketio',
    ),
)


def initAiowebApplication(
        eventLoop,
        useSockJS=False,
        ):
    """
    application factory pattern analogous to `initFlaskApp`.
    * eventLoop (asyncio.AbstractEventLoop):
      this is the event loop that the web server is to be run on
      according to the pattern at
      http://aiohttp.readthedocs.io/en/stable/web.html#graceful-shutdown
    * useSockJs: if False, use socket.io
    return: aiohttp.web.Application instance
    """
    flaskApp = initFlaskApp(
        addFlaskSocketIO=False,
        )
    getUserFromEnviron = partial(getUserFromFlaskAndWSGIEnviron,
                                     flaskApp)

    @sio.on('connect')
    async def connect(sid, environ):
        user = getUserFromEnviron(environ)
        if user is None:
            return False
        user.sid = sid
        saveModels(user)
        await sio.emit('connected', {'data': 'Connected'})


    @sio.on('disconnect')
    async def disconnect(sid):
        user = User.findBySid(sid, allowNone=True)
        if user is None:
            return False
        user.sid = None
        saveModels(user)


    aiowebApplication = aioweb.Application(loop=eventLoop)
    if useSockJS:
        aucUpdateSessionManager = AucUpdateSessionManager(
            lambda request: getUserFromEnviron(
                _translate_request(request)),
            aiowebApplication,
            aucUpdateSessionHandler,
            aiowebApplication.loop,
            )
        sockjs.add_endpoint(
            aiowebApplication, aucUpdateSessionHandler,
            name=aucUpdateSessionManager.name,
            prefix='/sockjs', # default
            manager=aucUpdateSessionManager,
            )
        aiowebApplication.on_shutdown.append(cleanupAucUpdateSockjs)
    else:
        sio.attach(aiowebApplication)
    #### must attach socket stuffs before the WSGI handler's glob
    aiowebApplication.router.add_route(
        "*", "/{path_info:.*}",
        WSGIHandler(flaskApp.wsgi_app))
    return aiowebApplication
