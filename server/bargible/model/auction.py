import math
from datetime import timedelta
from datetime import datetime

from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Boolean
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import DateTime
from sqlalchemy import Interval
from sqlalchemy import or_
from sqlalchemy import over
from sqlalchemy import select
from sqlalchemy import func
from sqlalchemy.orm import relationship
from sqlalchemy.orm import validates
from sqlalchemy.orm import column_property
from sqlalchemy.orm import contains_eager

from .base import Base
from .reward import Reward
from .item import Item
from .physical_item import PhysicalItem
from .user import User
from .watch import Watch
from .types import MonetaryPrice

from .exc import ModelError

# todo: make configurable
LEADERBOARD_NUM_USERS = 4

class UserBid(Base):
    __tablename__ = 'user_bid'
    id = Column(Integer, primary_key = True)
    uam_id = Column(Integer, ForeignKey('user_auction_map.id'))
    bid_datetime = Column(DateTime,
                          default = datetime.utcnow,
                          nullable = False)
    uam = relationship('UserAuctionMap', back_populates='user_bids')

class UserAuctionMap(Base):
    __tablename__ = 'user_auction_map'
    id = Column(Integer, primary_key=True,
                autoincrement=True, unique=True)
    user_id = Column(Integer, ForeignKey('user.id'),
                     primary_key=True, nullable=False)
    auction_id = Column(Integer, ForeignKey('auction.id'),
                        primary_key=True, nullable=False)
    bids = Column(Integer)
    ## related to freeze bids functionality:
    # boolean for atomicity
    is_bids_frozen = Column(Boolean, default=False, nullable=False)
    # updates to the boolean source of truth are based on:
    freeze_bids_start = Column(DateTime, default=None, nullable=True)
    freeze_bids_duration = Column(Interval, default=None, nullable=True)
    ## ..end freeze bids related
    ## related to bid to activate feature
    activations_freeze = Column(Integer, default=0, nullable=False)
    activations_steal = Column(Integer, default=0, nullable=False)
    activations_destroy = Column(Integer, default=0, nullable=False)
    auction = relationship('Auction', back_populates = 'users')
    user = relationship('User', lazy = 'subquery',
                        back_populates = 'auctions')
    user_bids = relationship('UserBid',
                             back_populates='uam',
                             cascade="save-update",
                             order_by='desc(UserBid.bid_datetime)')
    num_user_bids = column_property(
        select([func.count(UserBid.id)])
        .where(UserBid.uam_id==id)
        .correlate_except(UserBid))
    last_bid_datetime = column_property(
        select([func.max(UserBid.bid_datetime)])
        .where(UserBid.uam_id==id)
        .correlate_except(UserBid))

    def deductBids(self, amount):
        """
        this function is called when gameplay elements deduct bids.
        """
        if self.bids > 1:
            previousBids = self.bids
            self.bids = max(self.bids - amount, 1)
            return previousBids - self.bids
        return 0

    def updateIsBidsFrozen(self, updateDatetime):
        """
        perform a state transition on a user's frozen/unfrozen
        bids status from frozen -> unfrozen if time has elapsed
        by updateDatetime

        see also the comment at Auction.freezeBids

        updateDatetime (datetime.datetime):  the time at which to decide
        whether bids are to be unfrozen.  requiring this parameter
        helps ensure consistency for bulk updates.

        return: True if bids frozen status has been updated,
                False otherwise
        """
        if self.is_bids_frozen:
            thawDatetime = (self.freeze_bids_start +
                            self.freeze_bids_duration)
            if updateDatetime > thawDatetime:
                self.is_bids_frozen = False
                return True
            return False
        return False

    def toLeaderboardJsonObj(self):
        """
        return a python object that may be serialized
        as a json string representing the user's position in the
        auction from the perspective of the auction's leaderboard
        """
        return {
            'position': self.position,
            'username': self.user.username,
            'avatar': self.user.avatarUrl,
            'bids': self.bids, # bids remaining
        }

    @property
    def position(self):
        query = (
            UserAuctionMap.query
            .filter(UserAuctionMap.auction_id == self.auction_id)
            .filter(UserAuctionMap.last_bid_datetime != None)
            .add_columns(
                func.row_number().over(
                    order_by=UserAuctionMap.last_bid_datetime.desc()
                ))
            .from_self()
            .filter(UserAuctionMap.id == self.id)
        )
        res = query.first()
        if res is None:
            return None
        return res[1]

    def _deductBid(self):
        """
        the preceeding underscore denotes
        that this is viewed as a module-private
        (as opposed to class-private) method.

        Auction.deductBid provides the preferred interface external
        to the .model module
        """
        if self.bids < 1:
            raise AssertionError(("attempted to bid when no bids are "
                                  "available"))
        bidDatetime = datetime.utcnow()
        self.bids -= 1
        UserBid(bid_datetime=bidDatetime, uam=self)

    def _freezeBids(self, duration):
        """
        duration (datetime.timedelta): the amount of time the user's
        bids ought to be "frozen" (i.e. that the user cannot place
        a bid)

        the preceeding underscore denotes that this is viewed
        as a module-private (as opposed to class-private) method.

        Auction.freezeBids provides the preferred interface external
        to the .model module
        """
        self.is_bids_frozen = True
        self.freeze_bids_start = datetime.utcnow()
        self.freeze_bids_duration = duration

    def _getSecsFreezeRemaining(self, now=None):
        """
        get the number of seconds remaining until the user's bids
        _may_ be unfrozen by `updateIsBidsFrozen`.
        private *to this module*, currently used for serialization.
        in case the user's bids are not frozen, return None.
        ...
        addtionally, this function may return 1 to indicate that
        bids are frozen, even if this is not the nominal amount of
        time until thaw.
        """
        if now is None:
            now = datetime.utcnow()
        if not self.is_bids_frozen:
            return None
        thawDatetime = (self.freeze_bids_start +
                        self.freeze_bids_duration)
        if thawDatetime < now:
            return 1
        return math.ceil(
            (thawDatetime - now).total_seconds())

    @validates(
        'activations_freeze',
        'activations_steal',
        'activations_destroy',
    )
    def validateNonNegative(self, key, field):
        if field < 0:
            raise ValueError("negative value for " + key)
        return field


class Auction(Base):
    __tablename__ = 'auction'
    id = Column(Integer, primary_key = True)
    start_datetime = Column(
        DateTime, nullable=False,
        info={
            'label': "Start",
        })
    intermediate_timer = Column(DateTime, nullable=False)
    # before and during an auction, this is the 'ultimate timer',
    # and after an auction it's the actual time the auction ended
    end_datetime = Column(
        DateTime, nullable=False,
        info={
            'label': "End",
            'description': (
                "upper bound on end of auction, "
                "also referred to as the 'ultimate timer'"),
        })
    started = Column(Boolean, unique = False,
                         default=False, nullable=False)
    ended = Column(Boolean, unique = False,
                       default=False, nullable=False)
    reward_id = Column(Integer, ForeignKey('reward.id'),
                       nullable=False)
    total_bids = Column(Integer, unique = False,
                            default=0, nullable=False)
    bid_price = Column(Integer, unique = False,
                           default=0, nullable=False)
    real_price = Column(
        MonetaryPrice, unique = False,
        default = 100, nullable = False,
        info={
            'label': "Retail price",
            'description': (
                "the cost of this auction's reward to Bargible.  "
                "this field is a unique, normalized entry for "
                "giftcard auctions only, where the reward's cost is not "
                "entirely determined by the choice reward"),
        })
    min_price = Column(Integer, unique = False,
                       default = 100, nullable = False)
    join_cost = Column(
        Integer, unique = False,
        default = 1000, nullable = False,
        info={
            'label': 'Join Cost',
            'description': (
                "the cost to a user to join an auction "
                "in Bargible credits"),
        })
    starting_bids = Column(Integer, nullable = True)
    # auction.users.append(user)
    # This automatically add User.auctions.
    # ONLY call append from the auction
    users = relationship('UserAuctionMap', back_populates='auction', cascade="save-update")
    watches = relationship(
        'Watch',
        back_populates='auction',
        cascade="save-update",
        )
    redemption = relationship(
        'Redemption',
        back_populates='auction',
        uselist=False,
        cascade="save-update")
    shipping_address = relationship(
        'ShippingAddress',
        back_populates='auction',
        uselist=False,
        cascade="save-update")
    reward = relationship(
        'Reward',
        uselist=False,
        cascade="save-update")

    def addUser(self, user, initialBids):
        UserAuctionMap(
            user=user,
            bids=initialBids,
            auction=self,
        )

    def addWatch(self, user):
        Watch(
            user=user,
            auction=self,
            )

    def isUserWatching(self, user):
        return user.id in [watch.user.id for watch in self.watches]

    def getUserRelations(self, numUsers=None, omitUser=None):
        """
        get user relation objects ordered by number of bids
        numUsers (int): limit the list length
        omitUser (User): user record to omit from list
        """
        query = UserAuctionMap.query.filter_by(auction_id = self.id)
        if omitUser is not None:
            query = query.filter(UserAuctionMap.user_id != omitUser.id)
        query = query.order_by(UserAuctionMap.bids.desc())
        if numUsers is not None:
            query = query.limit(numUsers)
        return query.all()

    def getUserRelation(self, user, allowNone=False):
        """ get a single user relation object for User record user """
        query = (UserAuctionMap.query
                 .filter_by(auction_id = self.id)
                 .filter_by(user_id = user.id))
        if allowNone:
            return query.first()
        else:
            return query.one()

    def deductBid(self, userOrUam):
        """
        call this function when a user places a bid
        in order to ensure bid datetime is stored

        userOrUam: either a User or UserAuctionMap
        """
        self._userOrUamToUam(userOrUam)._deductBid()

    def freezeBids(self, userOrUam, duration):
        """
        the freeze bids feature means that a user is prevented from
        bidding for a certain time interval.  the model layer's
        related responsibilities consist of keeping track of which
        users' bids are "frozen", when they'll be "unfrozen", and how
        to make transitions between frozen/unfrozen states.

        the model layer is not responsible for gameplay logic
        such as whether as user may bid or not related to a users'
        bids frozen state.

        duration (datetime.timedelta): the amount of time the user's
        bids ought to be "frozen" (i.e. that the user cannot place
        a bid)
        """
        uam = self._userOrUamToUam(userOrUam)
        uam._freezeBids(duration)

    def toJsonObj(self, user=None):
        isUserJoined = (
            user and self.getUserRelation(user, allowNone=True))
        return {
            'upcoming': (
                self._getUpcomingJsonObj(user)
                if not self.started
                else None),
            'active': (
                self.getActiveJsonObj()
                if self.is_active and not isUserJoined
                else None),
            'joined': (
                self._getUserJoinedJsonObj(user)
                if self.is_active and isUserJoined
                else None),
            'ended': (
                self._getUserEndedJsonObj(user)
                if self.ended and isUserJoined
                else None),
        }

    def getActiveJsonObj(self):
        """
        return a python object that may be serialized as a json string
        representing the auction in its active state
        from the perspective of any logged-in user
        """
        if not self.is_active:
            raise ModelError("can't get active auction json object for "
                             "an auction that isn't active")
        dictAuction = {
            'id': self.id,
            'reward': self.reward.toJsonObj(),
            'timeRemaining': self._getTimeRemaining(),
            'bidPrice': self.bid_price,
            'minPrice': self.min_price,
            'numUsers': self.numUsers,
            'joinCost': self.join_cost,
        }
        return dictAuction

    # used heavily in test
    def getUserJsonObj(self, user):
        """
        return a python object that may be serialized
        as a json string representing the auction from the perspective
        of a participating user
        """
        # single-source of truth for all timers based on current time
        now = datetime.utcnow()
        uam = self.getUserRelation(user)
        leaderboardUserRelations = self._getLeaderboardUserRelations()
        dictAuction = {
            'userId': uam.user_id,
            'auctionId': uam.auction_id,
            'bids': uam.bids,
            'isBidsFrozen': uam.is_bids_frozen,
            'start': self.start_datetime.isoformat(),
            'end': self.end_datetime.isoformat(),
            'realPrice': self.real_price,
            'position': uam.position,
            'reward': self.reward.toJsonObj(),
            'lastBidUsername': (self.last_bidder.username
                                if self.last_bidder else None),
            'leaderboard': [ uam.toLeaderboardJsonObj() for uam
                             in leaderboardUserRelations ],
            'secsFreezeRemaining': uam._getSecsFreezeRemaining(now=now),
        }
        return dictAuction

    def _getUpcomingJsonObj(self, user):
        """
        return a python object that may be serialized as a json string
        representing the state of an auction before it begins.
        """
        if self.started:
            raise ModelError("can't get user upcoming json object for "
                             "an auction that has already started")
        dictAuction = {
            'id': self.id,
            'reward': self.reward.toJsonObj(),
            'minPrice': self.min_price,
            'start': self.start_datetime.isoformat(),
            'is_watching': (
                self.isUserWatching(user) if user is not None
                else False),
        }
        return dictAuction

    def _getUserJoinedJsonObj(self, user):
        """
        return a python object that may be serialized
        as a json string representing the auction from the perspective
        of a participating user
        """
        # single-source of truth for all timers based on current time
        now = datetime.utcnow()
        uam = self.getUserRelation(user)
        leaderboardUserRelations = self._getLeaderboardUserRelations()
        dictAuction = {
            'id': uam.auction_id,
            'reward': self.reward.toJsonObj(),
            'numUsers': self.numUsers,
            'timeRemaining': self._getTimeRemaining(),
            'bidPrice': self.bid_price,
            'bids': uam.bids,
            'isBidsFrozen': uam.is_bids_frozen,
            'position': uam.position,
            'leaderboard': [ uam.toLeaderboardJsonObj() for uam
                             in leaderboardUserRelations ],
            'secsFreezeRemaining': uam._getSecsFreezeRemaining(now=now),
            'intermediateTimer': self._getIntermediateTimer(),
        }
        return dictAuction

    def _getUserEndedJsonObj(self, user):
        """
        return a python object that may be serialized
        as a json string representing the auction from the perspective
        of a that has participated in an auction,
        provided the auction has ended.
        """
        if not self.ended:
            raise ModelError("can't get user ended json object for "
                             "an auction that hasn't ended")
        now = datetime.utcnow()
        uam = self.getUserRelation(user)
        leaderboardUserRelations = self._getLeaderboardUserRelations()
        dictAuction = {
            'id': uam.auction_id,
            'reward': self.reward.toJsonObj(),
            'start': self.start_datetime.isoformat(),
            'end': self.end_datetime.isoformat(),
            'realPrice': self.real_price,
            'position': uam.position,
            'lastBidUsername': (self.last_bidder.username
                                if self.last_bidder else None),
            'minPrice': self.min_price,
            'bidPrice': self.bid_price,
        }
        return dictAuction

    def _getLeaderboardUserRelations(self):
        userRelationsUnsorted = (
            UserAuctionMap.query
            .filter_by(auction_id = self.id)
            .all())
        gtMaxPosition = len(userRelationsUnsorted) + 1
        return sorted(userRelationsUnsorted,
                      key=lambda uam: uam.position
                      if uam.position is not None
                      # ensure deterministic ordering
                      else gtMaxPosition + uam.user.id
                      )[:LEADERBOARD_NUM_USERS]

    @property
    def numUsers(self):
        return (UserAuctionMap.query
                .filter(UserAuctionMap.auction_id == self.id)
                .count())

    @property
    def duration(self):
        return self.end_datetime - self.start_datetime

    @property
    def is_active(self):
        return self.started and not self.ended

    @property
    def last_bidder(self):
        lastUserBid = (
            UserBid.query
            .join(UserBid.uam)
            .join(UserAuctionMap.auction)
            .filter(Auction.id == self.id)
            .options(
                contains_eager(UserBid.uam).
                contains_eager(UserAuctionMap.auction)
            ).order_by(
                UserBid.bid_datetime.desc()
            ).first())
        if lastUserBid is None:
            return None
        return lastUserBid.uam.user

    @property
    def numConsecutiveBids(self):
        """ get the number of consecutive bids by a single user """
        uamQuery = (UserAuctionMap.query
                    .filter(UserAuctionMap.auction_id == self.id)
                    .filter(UserAuctionMap.last_bid_datetime != None)
                    .order_by(UserAuctionMap.last_bid_datetime.desc())
                    .limit(2))
        if uamQuery.count() == 0:
            return 0
        elif uamQuery.count() == 1:
            return len(uamQuery.first().user_bids)
        uamLastBid, uamNextToLastBid = uamQuery.all()
        return len([
            userBid for userBid in uamLastBid.user_bids if
            userBid.bid_datetime > uamNextToLastBid.last_bid_datetime
        ])

    @property
    def redeemed(self):
        return (self.redemption is not None and
                self.redemption.isRedeemed)

    @property
    def isPaid(self):
        return self.redemption is not None

    def _getTimeRemaining(self):
        if self.is_active:
            timeDifferenceSeconds = math.ceil((
                (self.start_datetime + self.duration)
                - datetime.utcnow()).total_seconds())
            if timeDifferenceSeconds > 0:
                return timeDifferenceSeconds
        return 0

    def _getIntermediateTimer(self):
        if self.is_active:
            if self.start_datetime.date() == datetime.utcnow().date():
                timeDifferenceSeconds = math.ceil((
                    self.intermediate_timer - datetime.utcnow()
                ).total_seconds())
                if timeDifferenceSeconds > 0:
                    return timeDifferenceSeconds
        return 0

    def _userOrUamToUam(self, userOrUam):
        """
        several public methods could use the UserAuctionMap
        for a particular user.
        sometimes, it's more convenient to call those
        methods with a variable that's not related to the particular
        auction.  so this method converts a User object to a
        UserAuctionMap and may be expanded in the future to convert
        other parameters (a user id, for instance).

        userOrUam: either a User or UserAuctionMap
        """
        if isinstance(userOrUam, User):
            return self.getUserRelation(userOrUam)
        else:
            assert isinstance(userOrUam, UserAuctionMap) # meh...
            assert userOrUam.auction_id == self.id # !important
            # ??? would it make sense to reload the UserAuctionMap
            #     here to prevent stale data?
            return userOrUam

    @staticmethod
    def getActiveAuctions():
        # .options(FromCache('activeAuctions'))
        return (Auction.query
            .filter(Auction.started == True)
            .filter(Auction.ended == False)
            .all())

    @staticmethod
    def getUpcomingAuctions(delta=None):
        """
        get auctions that will start between the current time and
        some specified interval
        delta (datetime.timedelta): the interval.  defaults to None.
        """
        query = (Auction.query
            .filter(Auction.started == False)
            .filter(Auction.ended == False))
        if (delta != None):
            query.filter(
                Auction.start_datetime < datetime.utcnow() + delta)
        return query.all()

    @staticmethod
    def getEndedAuctions():
        return (Auction.query
            .filter(Auction.started == True)
            .filter(Auction.ended == True)
            .all())

    @validates(
        'total_bids',
        'bid_price',
        'starting_bids',
        'join_cost',
    )
    def validateNonNegative(self, key, field):
        if field is not None and field < 0:
            raise ValueError("negative value for " + key)
        return field


    @validates('real_price')
    def validateRealPrice(self, key, field):
        self.validateNonNegative(key, field)
        if isinstance(field, int):
            self.min_price = field
        return field

    @validates('end_datetime')
    def validateEndDatetime(self, key, field):
        """
        check that the end datetime is after the start datetime
        also set (initial) intermediate timer end to be
        the end datetime when the end datetime is set
        """
        if (isinstance(self.start_datetime, datetime)
            and
            self.start_datetime >= field):
            raise ValueError((
                "tried to set auction end to a time "
                "before auction start"))
        if isinstance(field, datetime):
            self.intermediate_timer = field
        return field

    @validates('start_datetime')
    def validateStartDatetime(self, key, field):
        """
        check that the end datetime is after the start datetime
        """
        if (isinstance(self.end_datetime, datetime) and
                self.end_datetime <= field):
            raise ValueError((
                "tried to set auction start to a time "
                "after auction end"))
        return field

    @validates('min_price')
    def validateMinPrice(self, key, field):
        self.validateNonNegative(key, field)
        reward = None
        if self.reward is not None:
            if (isinstance(self.reward, Reward) and
                    self.reward.type == 'item'):
                reward = self.reward
        elif isinstance(self.reward_id, int):
            reward = Reward.byId(self.reward_id)
            if self.reward.type != 'item':
                reward = None
        if (reward is not None and
                    reward.minimum_price > field):
            raise ValueError((
                "auction minimum price must be at least "
                "the minimum price of the associated reward"))
        return field


    @validates('reward', 'reward_id')
    def validateMinPriceAgainstReward(self, key, field):
        if key == 'reward_id':
            reward = Reward.byId(field)
        elif isinstance(field, Reward):
            reward = field
        else:
            from pdb import set_trace; set_trace()
            return field
        if reward.type != 'item':
            return field
        if (isinstance(self.min_price, int) and
                reward.minimum_price > self.min_price):
            raise ValueError((
                "auction minimum price must be at least "
                "the minimum price of the associated reward"))
        return field
