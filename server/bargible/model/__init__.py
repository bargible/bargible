"""
define public interface to models.

also, import all models,
even if they're not part of the public interface,
such that metadata is created.
"""
from .base import db_session
from .base import engine
from .base import Base
from .base import imageStore
from .base import saveModels
from .base import shutdownSession
from .base import resetSession
from .base import initDb
from .base import dropAllTables
from .user import User
from .credit_package import CreditPackage
from .item import Item
from .reward import Reward
from .shipping_address import ShippingAddress
from .auction import UserAuctionMap
from .auction import Auction
from .config import Config
from .redemption import Redemption
from .transaction import Transaction
from .physical_item import PhysicalItem


__all__ = [
    # for use with sqlalchemy-imageattach's context manager
    'imageStore',
    ### function-level api
    'saveModels',
    'initDb',
    'dropAllTables',
    'resetSession',
    ### publicly-visible models
    'User',
    'CreditPackage',
    'Item',
    'Reward',
    'Auction',
    'Config',
    'Redemption',
    'Transaction',
    'PhysicalItem',
    'ShippingAddress',
]
