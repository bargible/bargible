"""
the reward for a Tango gift card auction.
"""
import re

from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from sqlalchemy.orm import validates

from sqlalchemy_imageattach.entity import Image
from sqlalchemy_imageattach.entity import image_attachment
from sqlalchemy_imageattach.context import store_context

from .base import imageStore
from .base import Base
from .reward import Reward

class ItemPicture(Base, Image):
    __tablename__ = 'item_picture'
    item_id = Column(Integer,
                     ForeignKey('item.id'),
                     primary_key=True)
    item = relationship('Item')


class Item(Reward):
    __tablename__ = 'item'
    id = Column(Integer,
                ForeignKey('reward.id'),
                primary_key = True)
    name = Column(String(120), nullable=False)
    minimum_price = Column(Integer, nullable=False)
    description = Column(String(1000), nullable=False)
    disclaimer = Column(String(1000), nullable=False)
    picture = image_attachment('ItemPicture')
    sku = Column(String(32), unique=True, nullable=False)

    __mapper_args__ = {
        'polymorphic_identity': 'item',
    }

    def _getJsonData(self):
        with store_context(imageStore):
            imageUrlPath = self.picture.locate()
        jsonObj =  {
            'name': self.name,
            'image': imageUrlPath,
            'itemId': self.id,
            'description': self.description,
            'disclaimer': self.disclaimer,
        }
        return jsonObj

    @staticmethod
    def bySku(sku):
        return (Item.query
                .filter_by(sku = sku)
                .one())

    @validates('sku')
    def validateSku(self, _, sku):
        skuRe = r'([A-Z])+(\d|(-\d))?-E-V-STD'
        if re.match(skuRe, sku) is None:
            raise ValueError("invalid item sku " + sku)
        return sku
