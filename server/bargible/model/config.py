"""
mutable, application-wide settings
"""
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy.orm import validates

from .base import Base

class Config(Base):
    __tablename__ = 'config'
    id = Column(Integer, primary_key=True, unique=True)
    until_freeze = Column(
        Integer, default=5, nullable=False,
        info={
            'label': "Bids to activate freeze",
            'description': (
                "the number times a user must bid in order to "
                "increment the number of activations of the "
                "freeze game element"),
            'min': 1,
        })
    until_steal = Column(
        Integer, default=10, nullable=False,
        info={
            'label': "Bids to activate steal",
            'description': (
                "the number times a user must bid in order to "
                "increment the number of activations of the "
                "steal game element"),
            'min': 1,
        })
    until_destroy = Column(
        Integer, default=15, nullable=False,
        info={
            'label': "Bids to activate destroy",
            'description': (
                "the number times a user must bid in order to "
                "increment the number of activations of the "
                "destroy game element"),
            'min': 1,
        })
    init_num_bids = Column(
        Integer, default=50, nullable=False,
        info={
            'label': "Initial bids constant",
            'description': (
                "this constant value is used to determine "
                "a gameplay user's inital number of bids "
                "upon entering an auction, possibly along with "
                "additional gameplay logic."),
            'min': 1,
        })
    max_destroy_amount = Column(
        Integer, default=5, nullable=False,
        info={
            'label': "Destroy bids amount",
            'description': (
                "this is the number of bids deducted from a single "
                "one of the possibly many users affected by "
                "an activation of the destroy game element, "
                "provided a user has this many bids"),
            'min': 1,
        })
    max_steal_amount = Column(
        Integer, default=2, nullable=False,
        info={
            'label': "Steal bids amount",
            'description': (
                "this is the number of bids deducted from a single "
                "one of the possibly many users affected by "
                "an activation of the steal game element, "
                "provided a user has this many bids"),
            'min': 1,
        })
    intermediate_timer = Column(
        Integer, default=10, nullable=False,
        info={
            'label': "Intermediate timer (seconds)",
            'description': (
                "this countdown may be started and subsequently "
                "'reset', 'refreshed', or 'rejuvenated' by various "
                "parts of gameplay (e.g. user bids or game "
                "elements), and if it reaches zero or 'expires', "
                "then the auction ends"),
            'min': 1,
        })
    max_users_per_auction = Column(
        Integer, default=20, nullable=False,
        info={
            'label': "Maximum users per auction",
            'description': (
                "the maximum number of users that may join "
                "any given auction"),
            'min': 1,
        })
    init_user_credits = Column(
        Integer, default=10000, nullable=False,
        info={
            'label': "Initial user credits",
            'description': (
                "the number of credits allotted to each user "
                "upon registration."),
            'min': 1,
        })

    @staticmethod
    def get():
        return (Config.query
            .filter(Config.id == 1)
            .one())

    @validates(
        'until_freeze',
        'until_steal',
        'until_destroy',
        'init_num_bids',
        'max_destroy_amount',
        'max_steal_amount',
        'intermediate_timer',
        'max_users_per_auction',
        'init_user_credits',
        )
    def validatePositive(self, key, field):
        if isinstance(field, int) and field <= 0:
            raise ValueError("negative value for " + key)
        return field

    def toJsonObj(self):
        return {
            'untilFreeze': self.until_freeze,
            'untilSteal': self.until_steal,
            'untilDestroy': self.until_destroy,
            'initNumBids': self.init_num_bids,
            'maxUsersPerAuction': self.max_users_per_auction,
        }
