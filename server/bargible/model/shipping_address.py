"""
shipping address information for an auction winner
"""
import re

from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import event
from sqlalchemy.orm import relationship
from sqlalchemy.orm import validates

from .base import Base
from .physical_item import PhysicalItem

USA_STATES = [
    'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL',
    'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA',
    'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE',
    'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK',
    'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT',
    'VA', 'WA', 'WV', 'WI', 'WY',
]

class ShippingAddress(Base):
    __tablename__ = 'shipping_address'
    id = Column(Integer, primary_key = True)
    auction_id = Column(Integer, ForeignKey('auction.id'),
                        nullable=False,
                        unique=True)
    auction = relationship('Auction',
                           back_populates='shipping_address')
    # address fields
    name = Column(String(64), nullable=False)
    addr_1 = Column(String(128), nullable=False)
    addr_2 = Column(String(128), nullable=False)
    city = Column(String(64), nullable=False)
    usa_state = Column(String(2), nullable=False)
    zip_code = Column(String(5), nullable=False)

    @validates('usa_state')
    def validateState(self, _, field):
        if not field in USA_STATES:
            raise ValueError("unknown state: " + field)
        return field

    @validates('zip_code')
    def validateZip(self, _, field):
        if not re.match(r'^\d{5}$', field):
            raise ValueError((
                "zip code must be precisely five digits"))
        return field


@event.listens_for(ShippingAddress.auction, 'set', retval=True)
def validateAuction(_, field, __, ___):
    if not isinstance(field.reward, PhysicalItem):
        raise ValueError((
            "shipping addresses are for "
            "physical item auctions only"))
    return field
