"""
'credits', one of the currency-like concepts in the application's
business logic, may be added to users' accounts in the form of
'packages', an amount of credits available at some price
in actual currency
"""

from sqlalchemy import Column
from sqlalchemy import Integer

from .base import Base

class CreditPackage(Base):
    """
    `price`: denominated in cents
    """
    __tablename__ = 'credit_package'
    id = Column(Integer, primary_key = True)
    num_credits = Column(Integer)
    price = Column(Integer)

    def toJsonObj(self):
        return {
            'id': self.id,
            'numCredits': self.num_credits,
            'price': self.price,
        }
