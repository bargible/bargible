"""
the reward for a Tango gift card auction.
"""
from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import relationship
from sqlalchemy.orm import validates

from sqlalchemy_imageattach.entity import Image
from sqlalchemy_imageattach.entity import image_attachment
from sqlalchemy_imageattach.context import store_context

from .base import imageStore
from .base import Base
from .reward import Reward
from .types import MonetaryPrice

class PhysicalItemPicture(Base, Image):
    __tablename__ = 'physical_item_picture'
    physical_item_id = Column(Integer,
                     ForeignKey('physical_item.id'),
                     primary_key=True)
    physical_item = relationship('PhysicalItem')

class PhysicalItem(Reward):
    __tablename__ = 'physical_item'
    id = Column(Integer,
                ForeignKey('reward.id'),
                primary_key = True)
    brand = Column(String(120), nullable=False)
    name = Column(String(120), nullable=False)
    short_description = Column(String(1000), nullable=False)
    description = Column(String(1000), nullable=False)
    sku = Column(String(32), unique=True, nullable=False)
    price = Column(MonetaryPrice, nullable=False)
    picture = image_attachment('PhysicalItemPicture')

    __mapper_args__ = {
        'polymorphic_identity': 'physical_item',
    }

    def _getJsonData(self):
        with store_context(imageStore):
            imageUrlPath = self.picture.locate()
        jsonObj =  {
            'name': self.name,
            'image': imageUrlPath,
            'itemId': self.id,
            'description': self.description,
        }
        return jsonObj

    @staticmethod
    def bySku(sku):
        return (PhysicalItem.query
                .filter_by(sku = sku)
                .one())
