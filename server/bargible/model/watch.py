"""
through table for the watch feature
"""
from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Boolean
from sqlalchemy import Integer
from sqlalchemy.orm import relationship

from .base import Base

class Watch(Base):
    __tablename__ = 'watch'
    id = Column(Integer, primary_key=True,
                autoincrement=True, unique=True)
    user_id = Column(Integer, ForeignKey('user.id'),
                     primary_key=True, nullable=False)
    auction_id = Column(Integer, ForeignKey('auction.id'),
                        primary_key=True, nullable=False)
    auction = relationship('Auction', back_populates = 'watches')
    user = relationship('User')
