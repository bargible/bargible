"""
polymorphic type for something being auctioned off
"""
from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String

from .base import Base

class Reward(Base):

    __tablename__ = 'reward'
    id = Column(Integer, primary_key = True)
    type = Column(String(50))

    __mapper_args__ = {
        'polymorphic_identity': 'reward',
        'polymorphic_on': type,
        'with_polymorphic': '*',
    }

    def toJsonObj(self):
        jsonObj = {
            'type': self.type,
            'data': self._getJsonData(),
        }
        return jsonObj

    def _getJsonData(self):
        raise NotImplementedError(
            "Reward subclasses must implement getJsonData")
