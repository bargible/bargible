"""
base model functionality and database connection setup.

mostly follows documented Flask pattern
http://flask.pocoo.org/docs/0.11/patterns/sqlalchemy/#declarative
for sqlalchemy declarative syntax
"""
import os

from sqlalchemy import Table
from sqlalchemy import create_engine
from sqlalchemy.schema import MetaData
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.declarative import as_declarative

from sqlalchemy_imageattach.stores.fs import HttpExposedFileSystemStore
from sqlalchemy_imageattach.context import store_context

SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']

imageStore = HttpExposedFileSystemStore(
    path='/var/local/bargible/images',
    prefix='static/images',
)


engine = create_engine(SQLALCHEMY_DATABASE_URI, convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                             autoflush=False,
                                             bind=engine))

@as_declarative()
class Base(object):
    """ base class for all sqlalchemy models """

    # todo: add tablename as lowercased class name

    query = db_session.query_property()

    def toJsonObj(self):
        raise NotImplementedError(
            "toJsonObj not implemented for " + type(self).__name__ )

    @classmethod
    def byId(cls, id):
        return cls.query.get(id)

    @classmethod
    def all(cls):
        return cls.query.all()


def saveModels(*models):
    with store_context(imageStore):
        for model in models:
            db_session.add(model)
        db_session.commit()


def shutdownSession():
    db_session.remove()


def resetSession(scopefunc=None):
    global db_session
    shutdownSession()
    db_session = scoped_session(db_session.session_factory,
                                    scopefunc=scopefunc)
    Base.query = db_session.query_property()


def initDb():
    Base.metadata.create_all(bind=engine)


def dropAllTables():
    Base.metadata.drop_all(bind=engine)
    # use reflection on db state to clear out tables,
    # even if they aren't specified in the metadata
    # set by application code
    meta = MetaData()
    meta.reflect(bind=engine)
    meta.drop_all(bind=engine)
