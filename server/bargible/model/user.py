"""
a user involved in auction gameplay
"""
import re
from datetime import datetime

from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import Boolean
from sqlalchemy import Date
from sqlalchemy import DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.orm import backref
from sqlalchemy.orm import validates

from sqlalchemy.ext.associationproxy import association_proxy

from sqlalchemy_imageattach.entity import Image
from sqlalchemy_imageattach.entity import image_attachment
from sqlalchemy_imageattach.context import store_context

from werkzeug.security import generate_password_hash
from werkzeug.security import check_password_hash

from flask_login import UserMixin

from .base import imageStore
from .base import Base

# for validation
REGEXP_EMAIL = r'^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$'

class UserAvatar(Base, Image):
    __tablename__ = 'user_avatar'
    user_id = Column(Integer,
                     ForeignKey('user.id'),
                     primary_key=True)
    user = relationship('User')


class User(Base, UserMixin):
    __tablename__ = 'user'
    id = Column(Integer, primary_key = True)
    username = Column(String(64), nullable = False, unique = True)
    password = Column(String(255), nullable=False, server_default='')
    email = Column(String(200), nullable = False)
    first_name = Column(String(64), nullable = False)
    last_name = Column(String(64), nullable = False)
    credit = Column(Integer, default = 0, nullable = False)
    sid = Column(String(64), unique=True)
    auctions = relationship("UserAuctionMap", back_populates = "user")
    avatar = image_attachment('UserAvatar')
    transactions = relationship("Transaction",
                                back_populates = "user",
                                cascade="save-update")

    # todo: move these overrides into Base model
    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.id == other.id
        return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return not self.__eq__(other)
        return NotImplemented

    def __hash__(self):
        """Override the default hash behavior (that returns the id or the object)"""
        return hash(self.id)

    @staticmethod
    def findByUsername(username, allowNone=False):
        if allowNone:
            return User.query.filter_by(username = username).first()
        else:
            return User.query.filter_by(username = username).one()

    @staticmethod
    def findByEmail(email, allowNone=False):
        if allowNone:
            return User.query.filter_by(email = email).first()
        else:
            return User.query.filter_by(email = email).one()

    @staticmethod
    def findBySid(sid, allowNone=False):
        if allowNone:
            return User.query.filter_by(sid = sid).first()
        else:
            return User.query.filter_by(sid = sid).one()


    @property
    def avatarUrl(self):
        with store_context(imageStore):
            avatarUrlPath = (self.avatar.locate()
                             if self.avatar.count() != 0 else
                             None)
        return avatarUrlPath

    def toJsonObj(self):
        return {
            'id': self.id,
            'username': self.username,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'credit': self.credit,
            'avatar': self.avatarUrl,
            'email': self.email,
        }

    def __repr__(self):
        return '<User %r>' % (self.username)

    @validates('email')
    def validateEmail(self, _, field):
        if (field is not None and
            type(field) is str and
            not re.match(REGEXP_EMAIL, field)):
            raise ValueError("invalid email: '" + field + "'")
        return field

    @validates('password')
    def validatePassword(self, _, field):
        return generate_password_hash(field)

    def checkPassword(self, password):
        return check_password_hash(self.password, password)
