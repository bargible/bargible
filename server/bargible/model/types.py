"""
custom sqlalchemy types
"""

import sqlalchemy.types as types

class MonetaryPrice(types.TypeDecorator):
    """
    a price is an integer number of cents.
    """

    impl = types.Integer
