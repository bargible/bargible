"""
in-app records of payments from users to 3rd-party payment apis
"""
from datetime import datetime
from enum import Enum
from enum import unique

from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import DateTime
from sqlalchemy.orm import validates
from sqlalchemy.orm import relationship

from .base import Base

@unique
class TransactionType(Enum):
     CREDIT = 1


class Transaction(Base):
    __tablename__ = 'transaction'
    id = Column(Integer, primary_key = True)
    datetime = Column(DateTime, nullable = False,
                      default = datetime.utcnow())
    amount = Column(Integer, nullable = False)
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship("User", back_populates="transactions")
    transaction_type = Column(Integer, nullable = False)

    @property
    def transactionType(self):
        return TransactionType(self.transaction_type)

    @validates('transaction_type')
    def validateTransactionType(self, key, field):
        if not isinstance(field, TransactionType):
            raise ValueError((
                "expected TransactionType enum when setting value "
                "Transaction.transaction_type model field value"),
                type(field))
        return field.value
