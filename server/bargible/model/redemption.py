"""
payment information for an auction winner as well as
information (e.g. for the admin UI) about whether the payment
has been acted upon (e.g. shipped or scheduled for shipment
in the case of physical item auctions, or paid for via the
Tango HTTP api in the case of giftcard auctions)
"""
from datetime import datetime

from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import DateTime
from sqlalchemy import event
from sqlalchemy.orm import relationship

from .base import Base
from .physical_item import PhysicalItem


class Redemption(Base):
    __tablename__ = 'redemption'
    id = Column(Integer, primary_key = True)
    pay_datetime = Column(DateTime, nullable=False,
                          default=datetime.utcnow())
    auction_id = Column(Integer, ForeignKey('auction.id'),
                        nullable=False,
                        unique=True)
    redeem_datetime = Column(DateTime, default=None)
    auction = relationship('Auction', back_populates='redemption')

    @property
    def isRedeemed(self):
        """ whether the the payment has been acted upon """
        return self.redeem_datetime is not None


@event.listens_for(Redemption.auction, 'set', retval=True)
def validateAuction(_, field, __, ___):
    if (isinstance(field.reward, PhysicalItem) and
        field.shipping_address is None):
        raise ValueError((
            "attempted to create a redemption for a "
            "physical item auction before a shipping address "
            "was recorded"))
    return field
