"""
exception classes
"""


class ModelError(Exception):
    """ generic error class """

    def getClientMsg(self):
        return "model layer error: " + (
            self.args[0] if len(self.args) > 0 else
            "unspecified")
