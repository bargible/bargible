"""
server -> client process-independent abstraction

currently implemented with socketio's built-in pub-sub
mechanism for sending events to websockets via redis.
"""
import socketio

from bargible.model import saveModels

_emitter = socketio.RedisManager(
    url='redis://localhost:6379/11',
    channel='auc-socketio',
)

def auctionUpdateEvent(room, args):
    """
    send an update regarding an auction
    * room: in current practice, this is a `User` model's session id
       as set by the setup functions in this module
    * args (dict): expected to contain 'auctionId' (int) and
       'updateType' (str) keys corresponding to an `Auction` model's
       primary key and an enum-like event identifier
    """
    assert len(set(args.keys()) - set([
        'auctionId',
        'updateType',
        'updateInfo',
    ])) == 0
    assert 'auctionId' in args.keys()
    assert 'updateType' in args.keys()
    _emitter.emit('auction update', args, room = room)
