"""
the steal bids game element

steal bids is a game element that allows one user to
remove other users' available bids and add them to that user's
available bids
"""

import math

from bargible.model import saveModels

from .game_element import GameElement
from .exc import AucCoreError

MAX_NUM_USERS_TO_STEAL_FROM = 10

class StealBids(GameElement):

    def __init__(self, auction, percentUsers=.25,
                 fractionToSteal=.1, maxToSteal=1):
        self.auction = auction
        self.percentUsers = percentUsers
        if not self.percentUsers > 0 or not self.percentUsers < 1:
            raise AssertionError((
                "expected percentage expressed as decimal"
            ))
        self.fractionToSteal = fractionToSteal
        self.maxToSteal = maxToSteal

    def run(self, userStealing):
        """
        returns a tuple of two elements.

        the first element of is a list of dictionaries,
        one for each user w/ bids destroyed.

        the second is the number of bids the stealing user has
        directly after the steal.

        this second element is included out of concern for correctness
        in light of concurrency rather than performance
        and may be considered for change after addressing
        async/concurrency in a systematic way.
        """
        otherMaps = self.auction.getUserRelations(omitUser=userStealing)
        userMapStealing = self.auction.getUserRelation(userStealing)
        numUsersToSteal = min(
            MAX_NUM_USERS_TO_STEAL_FROM,
            math.ceil(len(otherMaps) * self.percentUsers))
        dictsStolenBids = []
        if userMapStealing.activations_steal == 0:
            raise AucCoreError(
                "insufficient activations for game element"
            )
        for userMap in otherMaps[:numUsersToSteal]:
            bidsStolen = userMap.deductBids(min(int(userMap.bids*self.fractionToSteal),
                                                self.maxToSteal))
            if bidsStolen > 0:
                dictsStolenBids.append({
                    'user': userMap.user,
                    'bidsStolen': bidsStolen,
                })
        numBidsStolen = sum([ dictStolenBids['bidsStolen'] for
                              dictStolenBids in dictsStolenBids ])
        userMapStealing.bids += numBidsStolen
        userMapStealing.activations_steal -= 1
        saveModels(self.auction)
        return dictsStolenBids
