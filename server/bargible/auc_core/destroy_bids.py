import math

from bargible.model import saveModels

from .game_element import GameElement
from .exc import AucCoreError

class DestroyBids(GameElement):
    def __init__(self, auction, percentUsers=.25,
                 fractionToDestroy=.1, maxToDestroy=10):
        self.auction = auction
        self.percentUsers = percentUsers
        if not self.percentUsers > 0 or not self.percentUsers < 1:
            raise AssertionError((
                "expected percentage expressed as decimal"
            ))
        self.fractionToDestroy = fractionToDestroy
        self.maxToDestroy = maxToDestroy

    def run(self, userDestroying):
        """
        returns a list of dictionaries, one for each user w/ bids
        destroyed.
        """
        otherMaps = (self.auction
                     .getUserRelations(omitUser=userDestroying))
        userMapDestroying = self.auction.getUserRelation(userDestroying)
        numUsersToDestroy = math.ceil(len(otherMaps) * self.percentUsers)
        mapsToDestroy = otherMaps[:numUsersToDestroy]
        dictsDestroyedBids = []
        if userMapDestroying.activations_destroy == 0:
            raise AucCoreError(
                "insufficient activations for game element"
            )
        for userMap in mapsToDestroy:
            bidsDestroyed = (
                userMap.deductBids(min(int(userMap.bids*self.fractionToDestroy),
                                       self.maxToDestroy)))
            if bidsDestroyed > 0:
                dictsDestroyedBids.append({
                    'user': userMap.user,
                    'bidsDestroyed': bidsDestroyed,
                })
        userMapDestroying.activations_destroy -= 1
        saveModels(self.auction)
        return dictsDestroyedBids
