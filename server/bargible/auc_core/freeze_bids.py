"""
the freeze bids game element

freeze bids is a game element that allows one user to prevent
other users from bidding for a certain amount of time.

it freezes bids for a fixed percentage of users with the
highest number of bids.

the freeze bids game element is always to be followed by a bid.
it is the responsibility of modules using the .core interface
to enforce this part of the specification.
"""

import math
from datetime import timedelta

from bargible.model import saveModels

from .game_element import GameElement
from .exc import AucCoreError

class FreezeBids(GameElement):

    def __init__(self, auction, percentUsers=.25, secDuration=5):
        """
        percentUsers: percentage of participating users to
          freeze bids from
        secDuration: duration of time that bids stay frozen
        """
        self.auction = auction
        self.percentUsers = percentUsers
        if not self.percentUsers >= 0 or not self.percentUsers <= 1:
            raise AssertionError((
                "expected percentage expressed as decimal"
            ))
        if not secDuration >= 0:
            raise AssertionError((
                "expected non-negative duration"
            ))
        self.duration = timedelta(seconds=secDuration)

    def run(self, userFreezing):
        """
        userFreezing (model.User): the user that is freezing other
        users' bids

        return: list of users who have had their bids frozen
        """
        otherMaps = self.auction.getUserRelations(omitUser=userFreezing)
        userMapFreezing = self.auction.getUserRelation(userFreezing)
        numUsersToFreeze = math.ceil(len(otherMaps) * self.percentUsers)
        mapsToFreeze = otherMaps[:numUsersToFreeze]
        if userMapFreezing.activations_freeze == 0:
            raise AucCoreError(
                "insufficient activations for game element"
            )
        for uam in mapsToFreeze:
            self.auction.freezeBids(uam, self.duration)
        userMapFreezing.activations_freeze -= 1
        saveModels(self.auction)
        return [uam.user for uam in mapsToFreeze]
