"""
exception classes
"""

class AucCoreError(Exception):

    def getClientMsg(self):
        return "auction core error: " + (
            self.args[0] if len(self.args) > 0 else
            "unspecified")

