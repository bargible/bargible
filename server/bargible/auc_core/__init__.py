""" auction gameplay and "business-logic" specification """

from .auction_manager import AuctionManager

__all__ = [
    'AuctionManager',
]
