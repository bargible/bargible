import warnings
import math
from copy import deepcopy
from datetime import datetime
from datetime import timedelta

from bargible.model import saveModels
from bargible.model import Auction
from bargible.model import User

from .destroy_bids import DestroyBids
from .steal_bids import StealBids
from .freeze_bids import FreezeBids
from .exc import AucCoreError

class AuctionManager:

    DEFAULT_CONFIG = {
        'INTERMEDIATE_TIMER': 60, # sec
        # initial number of bids given to a user upon joining an auction
        'INIT_NUM_BIDS': 300,
        # min bids (inclusive) before users recv notifications
        # that a user has bid multiple times
        'BID_MULT_NOTIFY_COUNT': 4,
        # bids required to to trigger bid multiple times game element
        'BID_MULT_COUNT': 8,
        # bids refunded to the bidding user on bid mult game el
        'BID_MULT_REFUND': 16,
        ## freeze bids related config variables
        # percentage of participating users to freeze bids from
        'FREEZE_PER': .25,
        # duration of time that bids stay frozen
        'FREEZE_DUR': 5, # sec
        # percentage of participating users to destroy bids for
        'DESTROY_PER': .25,
        # num bids to destroy
        'MAX_DESTROY_AMOUNT': 10,
        # fraction of each users bids to destroy
        'DESTROY_FRACTION': .1,
        # percentage of participating users to steal bids from
        'STEAL_PER': .25,
        # fraction of each users bids to steal
        'STEAL_FRACTION': .1,
        # num bids to steal
        'MAX_STEAL_AMOUNT': 5,
        # credit cost to freeze
        'FREEZE_COST': 0, # credit
        # credit cost to freeze
        'STEAL_COST': 0, # credit
        # credit cost to freeze
        'DESTROY_COST': 0, # credit
        ## bid to activate feature related:
        # "initial values" for 'bids until additional'
        'UNTIL_FREEZE': 10,
        'UNTIL_STEAL': 20,
        'UNTIL_DESTROY': 30,
        # the maximum number of users that may join a given auction
        'MAX_USERS_PER_AUCTION': 20,
    }

    # values that are set according to default config,
    # no matter what configuration object is passed to __init__
    DISABLED_CONFIG_KEYS = set([
        'FREEZE_COST',
        'STEAL_COST',
        'DESTROY_COST',
    ])

    def __init__(self, auction, config=None):
        self.auction = auction
        if config is None:
            self.config = deepcopy(self.DEFAULT_CONFIG)
        else:
            # add missing keys and prune unused key-val pairs
            self.config = {
                key: config.get(key, self.DEFAULT_CONFIG[key])
                for key in (set(self.DEFAULT_CONFIG.keys()) -
                                self.DISABLED_CONFIG_KEYS) }
        for disabledConfigKey in self.DISABLED_CONFIG_KEYS:
            self.config.update({
                disabledConfigKey: self.DEFAULT_CONFIG[disabledConfigKey]
            })
            if (config is not None and
                    config.get(disabledConfigKey) is not None and
                    (config.get(disabledConfigKey) !=
                        self.DEFAULT_CONFIG[disabledConfigKey])):
                warnings.warn((
                    "values in AuctionManager.DISABLED_CONFIG_KEYS "
                    "are set by AuctionManager.DEFAULT_CONFIG "
                    "no matter what value is passed as a config "
                    "to initialize an AuctionManager, "
                    "and the value "
                ) + (
                    str(config.get(disabledConfigKey)) +
                    " does not match the default of " +
                    str(self.DEFAULT_CONFIG[disabledConfigKey]) +
                    " for the key " +
                    str(disabledConfigKey)
                ), FutureWarning)

    def joinAuction(self, user):
        """
        gameplay spec for what happens when a user joins an auction.
        user (model.User): user joining the auction.
          _not_ updated/reloaded from database by this function.
        raises: AucCoreError if the user doesn't have sufficient
          credit to join the auction or if the auction isn't active
        returns: the number of bids the user has after joining the
          auction
        or
          the user's current number of bids if the user has already
          joined the auction
        """
        if user.credit < self.auction.join_cost:
            raise AucCoreError("insufficient credit")
        if not self.auction.is_active:
            raise AucCoreError("auction is not active")
        if self.auction.numUsers == self.config['MAX_USERS_PER_AUCTION']:
            raise AucCoreError(("joining an auction cannot result in "
                                "exceeding the maximum number of users "
                                "per auction"))
        assert (self.auction.numUsers <
                self.config['MAX_USERS_PER_AUCTION'])
        joinDatetime = datetime.utcnow()
        user.credit -= self.auction.join_cost
        saveModels(user)
        for userAuctionMap in self.auction.users:
            if userAuctionMap.user.username == user.username:
                return userAuctionMap.bids
        secsSinceStart = (joinDatetime -
                          self.auction.start_datetime).total_seconds()
        assert secsSinceStart >= 0
        bidFraction = 1 - (secsSinceStart /
                           self.auction.duration.total_seconds())
        assert 0 <= bidFraction and bidFraction <= 1
        numBids = math.ceil(self.config['INIT_NUM_BIDS'] * bidFraction)
        assert numBids > 0
        self.auction.addUser(user, numBids)
        saveModels(self.auction)
        return numBids

    def bidOnAuction(self, user):
        if not self.auction.is_active:
            raise AucCoreError("auction is not active")
        # find the user relation object
        uamIter = (uam for uam in self.auction.users if
                   uam.user.username == user.username and uam.bids > 0)
        uam = next(uamIter, None)
        if next(uamIter, None) is not None:
            raise AssertionError((
                "found two user-auction relation objects "
                "for the same user and auction"
            ))
        if uam is None:
            raise AucCoreError("auction not joined")
        if uam.is_bids_frozen:
            raise AucCoreError("bids currently frozen")
        # update auction state and set return values
        flags = {
            'firstBidder': False,
        }
        if self.auction.last_bidder is None:
            uam.bids += 11
            flags['firstBidder'] = True
        if not uam.bids > 0:
            raise AucCoreError("no bids left")
        self.auction.deductBid(uam)
        saveModels(self.auction)
        self._refreshAuctionIntermediateTimer()
        self._adjustAuctionBidPrice(1) # magic!
        saveModels(self.auction)
        resBidMult = self._bidMult(uam) # updates uam.bids
        self._bidToActivate(uam) # updates uam.activations_*
        return flags, resBidMult

    def stealBids(self, user):
        if not self.auction.is_active:
            raise AucCoreError("auction is not active")
        if user.credit < self.config['STEAL_COST']:
            raise AucCoreError("insufficient credit")
        user.credit -= self.config['STEAL_COST']
        saveModels(user)
        result = (StealBids(self.auction,
                            percentUsers=self.config['STEAL_PER'],
                            fractionToSteal=self.config['STEAL_FRACTION'],
                            maxToSteal=self.config['MAX_STEAL_AMOUNT'])
                  .run(user))
        self._refreshAuctionIntermediateTimer()
        saveModels(self.auction)
        return result

    def destroyBids(self, user):
        if not self.auction.is_active:
            raise AucCoreError("auction is not active")
        if user.credit < self.config['DESTROY_COST']:
            raise AucCoreError("insufficient credit")
        user.credit -= self.config['DESTROY_COST']
        saveModels(user)
        result = (DestroyBids(self.auction,
                              percentUsers=self.config['DESTROY_PER'],
                              fractionToDestroy=self.config['DESTROY_FRACTION'],
                              maxToDestroy=self.config['MAX_DESTROY_AMOUNT'])
                  .run(user))
        self._refreshAuctionIntermediateTimer()
        saveModels(self.auction)
        return result

    def freezeBids(self, user):
        """ see freeze_bids module documentation """
        if user.credit < self.config['FREEZE_COST']:
            raise AucCoreError("insufficient credit")
        user.credit -= self.config['FREEZE_COST']
        saveModels(user)
        gameElement = FreezeBids(self.auction,
                                 percentUsers=self.config['FREEZE_PER'],
                                 secDuration=self.config['FREEZE_DUR'])
        result = gameElement.run(user)
        self._refreshAuctionIntermediateTimer()
        saveModels(self.auction)
        return result

    def getUserJsonObj(self, user):
        userRelation = self.auction.getUserRelation(user, allowNone=True)
        if userRelation is None:
            raise ValueError("user hasn't joined auction")
        numUserBids = userRelation.num_user_bids
        bidsTilActivationObj = {
            objKey: (self.config[configKey] -
                     (numUserBids % self.config[configKey]))
            for (objKey, configKey) in [
                    ('freeze', 'UNTIL_FREEZE'),
                    ('steal', 'UNTIL_STEAL'),
                    ('destroy', 'UNTIL_DESTROY'),
            ]}
        return {
            'numActivations': {
                'freeze': userRelation.activations_freeze,
                'steal': userRelation.activations_steal,
                'destroy': userRelation.activations_destroy,
            },
            'bidsTilActivation': bidsTilActivationObj,
        }

    def _bidMult(self, uamBidder):
        """
        helper function for bid multiple times feature
        `uamBidder`: UserAuctionMap for the bidding user
        """
        numConsecutiveBids = self.auction.numConsecutiveBids
        if (numConsecutiveBids < self.config['BID_MULT_NOTIFY_COUNT']
            or self.config['BID_MULT_COUNT'] < numConsecutiveBids):
            return None
        didBidMult = numConsecutiveBids == self.config['BID_MULT_COUNT']
        if didBidMult:
            uamBidder.bids += self.config['BID_MULT_REFUND']
            saveModels(uamBidder)
        return {
            'consecutiveBids': numConsecutiveBids,
            'complete': didBidMult
        }

    def _bidToActivate(self, uamBidder):
        """
        helper function for bid to activate game element(s) feature
        called in order to add "activations" (i.e. number of available)
        game elements when appropriate.
        to be called after bidding is complete and data is updated
        `uamBidder`: UserAuctionMap for the bidding user
        """
        if uamBidder.num_user_bids % self.config['UNTIL_FREEZE'] == 0:
            uamBidder.activations_freeze += 1
        if uamBidder.num_user_bids % self.config['UNTIL_STEAL'] == 0:
            uamBidder.activations_steal += 1
        if uamBidder.num_user_bids % self.config['UNTIL_DESTROY'] == 0:
            uamBidder.activations_destroy += 1
        saveModels(uamBidder)

    def _refreshAuctionIntermediateTimer(self):
        now = datetime.utcnow()
        self.auction.intermediate_timer = now + timedelta(
            seconds=self.config['INTERMEDIATE_TIMER'])

    def _adjustAuctionBidPrice(self, amount):
        self.auction.bid_price += amount
