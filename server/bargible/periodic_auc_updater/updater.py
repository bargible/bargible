import logging
import itertools
from datetime import datetime
from datetime import timedelta

from bargible.model import saveModels
from bargible.model import Auction
from bargible.model import User

logging.basicConfig(filename = '/var/log/bargible/auction.log', level = logging.INFO)
logger = logging.getLogger(__name__)

def update():
    """ run one update cycle """
    updateResults = []
    for auction in Auction.getActiveAuctions():
        for updateResult in [
                _updateActiveAuction(auction),
        ]:
            if updateResult is None:
                continue
            assert 'updateType' in updateResult.keys()
            updateResult.update(
                auctionId=auction.id,
            )
            if 'updateUsers' not in updateResult.keys():
                updateResult.update(
                    updateUsers=[ uam.user for uam in
                                  auction.getUserRelations() ],
                )
            updateResults.append(updateResult)
    for auction in Auction.getUpcomingAuctions():
        _updateUpcomingAuction(auction)
    return updateResults

def _updateActiveAuction(auction):
    now = datetime.utcnow()
    if not auction.is_active:
        raise AssertionError(
            "tried to update inactive auction as if it were active")

    if (auction.intermediate_timer <= now
        or auction.end_datetime <= now):
        logger.info('Ending auction')
        _endAuction(auction)

        return {'updateType': 'end'}

    mapsToSave = auction.getUserRelations()

    bidThawsUsers = [ uam.user
                      for uam
                      in mapsToSave
                      if uam.updateIsBidsFrozen(now) ]

    if len(bidThawsUsers) == 0:
        return None

    saveModels(auction)

    return {
        'updateType': 'thaw',
        'updateUsers': bidThawsUsers,
    }

def _updateUpcomingAuction(auction):
    now = datetime.utcnow()
    if auction.start_datetime <= now:
        logger.info('Starting auction, start: {0}, now: {1}'
                    .format(auction.start_datetime, now))
        auction.started = True
        saveModels(auction)

def _endAuction(auction):
    auction.end_datetime = datetime.utcnow()
    logger.info('Ending auction - end time: ' +
                str(auction.end_datetime))
    auction.ended = True
    uamList = auction.getUserRelations()
    usersList = []
    for uam in uamList:
        currentUser = User.byId(uam.user_id)
        usersList.append(currentUser)

    saveModels(auction)
