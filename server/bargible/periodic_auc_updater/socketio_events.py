"""
expose an interface for propegating events from this package
to `auctionUpdateHandler` in the client-side Javascript.

the interface consists of a single function, `emitUpdate`.

this particular implementation of the "periodic updater" -> "JS"
event system corresponds to the current implementation of the
web-specific components of the server side, Flask-SocketIO particularly.
"""
import os.path
import json
from warnings import warn

import jsonschema
from jsonschema.exceptions import ValidationError

from bargible.client_auc_events import auctionUpdateEvent

__all__ = [
    'emitUpdate'
]

with open(os.path.join(os.path.dirname(__file__),
                           'schemas', 'update_data.json')) as f:
    UPDATA_DATA_SCHEMA = json.load(f)

def emitUpdate(user, data):
    """
    data (dict): corresponds to the data passed to
      `auctionUpdateHandler` in the browser
    user (bargible.model.User): determines which browser to
      send the data to
    """
    try:
        jsonschema.validate(data, UPDATA_DATA_SCHEMA)
    except ValidationError as err:
        warn(("emitting auction update data that doesn't " +
                  "validate against schema " + repr(err)))
    auctionUpdateEvent(user.sid, data)

