"""
task-queue-like functions
to be called by processes external to the Flask application
"""
import sys
from copy import deepcopy
from warnings import warn
import asyncio as aio

from sqlalchemy.exc import OperationalError
from sqlalchemy.exc import ProgrammingError

from bargible.periodic_auc_updater.updater import update as updateAllAuctions

from .socketio_events import emitUpdate

def _updateAuctionsImpl():
    updateResults = updateAllAuctions()
    for updateResult in updateResults:
        assert 'auctionId' in updateResult.keys()
        assert 'updateType' in updateResult.keys()
        assert 'updateUsers' in updateResult.keys()
        updateData = dict(
            auctionId=updateResult['auctionId'],
            updateType=updateResult['updateType'],
        )
        for user in updateResult['updateUsers']:
            emitUpdate(user, updateData)

async def _updateAuctionsAsync():
    _updateAuctionsImpl()

async def updateAuctionsAsyncLoop():
    while True:
        try:
            await aio.gather(
                _updateAuctionsAsync(),
                aio.sleep(.75),
            )
        except OperationalError as err:
            warn(repr(err))
        except ProgrammingError as err:
            warn(repr(err))
        except:
            warn(repr(sys.exc_info()[0]))
            raise

updateAuctions = _updateAuctionsImpl
