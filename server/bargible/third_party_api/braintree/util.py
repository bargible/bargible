"""
abstract over vendor api
"""
import os
import configparser

import braintree

CONF_FILE = os.path.join(os.path.dirname(__file__), 'creds.conf')
CONFIG = configparser.ConfigParser()
CONFIG.read(CONF_FILE)

braintree.Configuration.configure(
    braintree.Environment.Sandbox,
    **dict(CONFIG['creds'])
)

class SaleResult(object):
    """
    decorator pattern wraps vendor api to provide consistent and
    internally-documented interface for the return value of
    this package's `makeSale` function
    """

    def __init__(self, btResult):
        """
        btResult: a result object from the braintree api
         https://developers.braintreepayments.com/reference/general/result-objects/python
        """
        self._btRes = btResult

    @property
    def isSuccess(self):
        return self._btRes.is_success

    @property
    def isFieldsError(self):
        """
        True if an the result is an error that could occur due
        to a user incorrectly entering fields in the payment form
        """
        if self.isSuccess:
            return False
        # todo: fill in upon sorting out more details of braintree api
        return False

    @property
    def jsonErrorData(self):
        return {
            # for development convenience, not to be used in JS api
            '_deepErrs': self._deepErrors,
            '_gatewayRejection': self._gatewayRejectionReason,
        }

    @property
    def _gatewayRejectionReason(self):
        return (
            self._btRes.transaction.gateway_rejection_reason
            if self._btRes.transaction.status == 'gateway_rejected'
            else None)

    @property
    def _deepErrors(self):
        return [dict(
            code=err.code,
            message=err.message,
        ) for err in self._btRes.errors.deep_errors ]


def genClientToken():
    """
    return (str): auth token allowing application user's browser
     to connect to vendor api
    """
    return braintree.ClientToken.generate()


def makeSale(amount, nonce):
    amountStr = '{:.2f}'.format(float(amount)/100.)
    btResult = braintree.Transaction.sale({
        'amount': amountStr,
        'payment_method_nonce': nonce,
    })
    return SaleResult(btResult)


def setEnvironment(envType):
    if envType == 'prod':
        braintree.Configuration.configure(
            braintree.Environment.Production,
            **dict(CONFIG['creds'])
            )
    elif envType == 'sand':
        braintree.Configuration.configure(
            braintree.Environment.Sandbox,
            **dict(CONFIG['creds'])
            )
    else:
        raise ValueError((
            "environment type must be 'prod' (production)"
            " or 'sand' (sandbox)"), envType)
