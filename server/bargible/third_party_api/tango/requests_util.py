"""
Tango-specifc utilities for Python Requests library
http://docs.python-requests.org
"""
import ssl

import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.poolmanager import PoolManager

from . import const

class _MyAdapter(HTTPAdapter):

    def init_poolmanager(self, connections, maxsize, block):
        self.poolmanager = PoolManager(
            num_pools=connections,
            maxsize=maxsize,
            block=block,
            ssl_version=ssl.PROTOCOL_TLSv1_2,
            )


def makeSessionAndAuth():
    sess = requests.Session()
    sess.mount('https://', _MyAdapter())
    requestAuth = (const.CREDS['user'], const.CREDS['password'])
    return sess, requestAuth
