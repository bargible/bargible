import os.path
import json
import datetime
from string import Template
from urllib.parse import urljoin

from .requests_util import makeSessionAndAuth
from .responses import TangoRedeemResponse
from .responses import TangoRewardsResponse
from . import const

# todo: https://docs.python.org/3/howto/logging.html#configuring-logging-for-a-library

def _makeRedeemRequestData(requestParams):
    return {
        'customer': 'Bargible',
        'account_identifier': 'Bargible',
        'campaign': 'emailtemplate1',
        'recipient': {
            'name': requestParams['name'],
            'email': requestParams['email'],
        },
        'sku': requestParams['sku'],
        'amount': requestParams['amount'],
        'reward_from': const.EMAIL_TEMPLATE['from'],
        'reward_subject': (Template(const.EMAIL_TEMPLATE['subject'])
            .substitute(name=requestParams['name'])),
        'reward_message': (Template(const.EMAIL_TEMPLATE['message'])
            .substitute(name=requestParams['name'])),
        'send_reward': True,
        'external_id': requestParams['external_id'],
    }


def redeem(requestParams):
    sess, auth = makeSessionAndAuth()
    requestUrl = urljoin(const.BASE_URL, 'orders')
    try:
        requestJson = _makeRedeemRequestData(requestParams)
    except KeyError as exc:
        raise ValueError((
            "invalid requestParams argument to redeem")
                             ) from exc
    resp = sess.post(requestUrl,
                         json=requestJson,
                         auth=auth)
    return TangoRedeemResponse(resp)


def getRewards():
    sess, auth = makeSessionAndAuth()
    requestUrl = urljoin(const.BASE_URL, 'rewards')
    resp = sess.get(requestUrl,
                        auth=auth)
    return TangoRewardsResponse(resp)

