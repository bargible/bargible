import os
import configparser
import json

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__),
                         'creds.conf'))
CREDS = dict(config['creds'])

_URL_SANDBOX = 'https://sandbox.tangocard.com/raas/v1.1/'
_URL_PRODUCTION = 'https://api.tangocard.com/raas/v1.1/'

with open(os.path.join(os.path.dirname(__file__),
          'email_template.json'), 'r') as templateFile:
    EMAIL_TEMPLATE = json.loads(templateFile.read())

BASE_URL = _URL_SANDBOX

def setEnvironment(envType):
    global BASE_URL
    if envType == 'prod':
        BASE_URL = _URL_PRODUCTION
    elif envType == 'sand':
        BASE_URL = _URL_SANDBOX
    else:
        raise ValueError((
            "environment type must be 'prod' (production)"
            " or 'sand' (sandbox)"), envType)

