class TangoResponse(object):

    def __init__(self, requestsResponse):
        self._requestsResponse = requestsResponse

    def __str__(self):
        req = self.requestsResponse.request
        strReq = '\n'.join([
            ' '.join([req.method, req.url]),
            '\n'.join([
                ("{name}: {value}".format(
                    name=k, value=repr(v)))
                for k, v in req.headers.items()]),
            repr(req.body),
            ])
        res = self.requestsResponse
        strRes = '\n'.join([
            ' '.join([str(res.status_code)]),
            '\n'.join([
                ("{name}: {value}".format(
                    name=k, value=repr(v)))
                for k, v in req.headers.items()]),
            repr(res.text),
            ])
        return '\n'.join([
            "---- request",
            strReq,
            "---- response",
            strRes,
            ])

    def __repr__(self):
        return ('<' + self.__class__.__name__ + ' '
                    + '[' + repr(self.requestsResponse) + ']>')

    @property
    def requestsResponse(self):
        return self._requestsResponse

    @property
    def ok(self):
        return self.requestsResponse.ok


class TangoRedeemResponse(TangoResponse):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @property
    def ok(self):
        try:
            respJson = self.requestsResponse.json()
        except ValueError:
            # body doesn't contain valid json
            return False
        if type(respJson) is not dict:
            return False
        return super().ok and respJson.get('success', False)


class TangoRewardsResponse(TangoResponse):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @property
    def ok(self):
        try:
            respJson = self.requestsResponse.json()
        except ValueError:
            # body doesn't contain valid json
            return False
        if type(respJson) is not dict:
            return False
        # tobo: blah ... use schema (marshmallow, etc.)
        return (super().ok and respJson.get('success', False)
                    and type(respJson.get('brands', None)) is list)

    @property
    def skus(self):
        if not self.ok:
            return None
        raise NotImplementedError()
