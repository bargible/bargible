import os
import logging
import datetime


_TIMEZONE = datetime.timezone(datetime.timedelta(hours=-7))

logger = logging.getLogger(__name__)
_logHandler = logging.FileHandler(
    os.path.abspath(
        os.path.join(
            '/', 'var', 'log', 'bargible',
            'tango_' + (datetime.datetime.now(_TIMEZONE)
                        .strftime('%y%m%d-%H%M')) + '.log')),
            mode='w', # b/c it's timestamped.  new log for every run.
    encoding='utf-8')
_logHandler.setLevel(logging.INFO)
logger.addHandler(_logHandler)
