"""
generate test (both demo and test/ script) data from json fixtures
found in test_data/
"""

import os
import sys
import json
import time
import datetime
from copy import deepcopy

from sqlalchemy_imageattach.context import store_context

PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__))
)

SERVER_DIR = os.path.join(PROJ_ROOT)

sys.path.append(SERVER_DIR)

from bargible.model import imageStore
from bargible.model import saveModels
from bargible.model import Item
from bargible.model import PhysicalItem
from bargible.model import Config
from bargible.model import CreditPackage
from bargible.model import Auction
from bargible.model import User

from config import TestingConfig

INIT_CREDIT = 10000

# use to parse strings formatted according to ISO 8601 standard
# to python datetime objects using datetime.strptime
ISO_8601_FORMAT = '%Y-%m-%dT%H:%M:%S.%f'

PATH_DIR_ITEM_IMGS = os.path.join(
    PROJ_ROOT, 'test_data', 'images', 'item')

PATH_DIR_PHYSICAL_ITEM_IMGS = os.path.join(
    PROJ_ROOT, 'test_data', 'images', 'physical-item')

with open(os.path.join(
        PROJ_ROOT, 'test_data', 'fixtures',
        'auctions_test_core_updater.json')) as f:
    DATA_AUCTION_TEST = json.load(f)

with open(os.path.join(
        PROJ_ROOT, 'test_data', 'fixtures',
        'auctions_test_func.json')) as f:
    DATA_FUNC_TEST = json.load(f)

# convert keys from legacy Auction.__init__ impl to SQLA standard
# init (i.e. field names).  keys are legacy init args, values
# are updated init kwargs
LEGACY_AUCTION_FIXTURE_MAP = {
    'minPrice': 'min_price',
    'joinCost': 'join_cost',
}

### abstract fixture <-> model coupling

def _initItemFromFixture(fixtureData):
    itemFields = { key: fixtureData[key]
                       for key in fixtureData.keys()
                       if key != 'picture' }
    # fixtures are denominated in dollars
    itemFields['minimum_price'] *= 100
    item = Item(**itemFields)
    with store_context(imageStore):
        with open(os.path.join(PATH_DIR_ITEM_IMGS,
                               fixtureData['picture']
        ), mode='rb') as f:
            item.picture.from_file(f)
    return item

def _initPhysicalItemFromFixture(fixtureData):
    physicalItem = PhysicalItem(**{ key: fixtureData[key]
                    for key in fixtureData.keys()
                    if key != 'picture' })
    with store_context(imageStore):
        with open(os.path.join(PATH_DIR_PHYSICAL_ITEM_IMGS,
                                   fixtureData['picture']
        ), mode='rb') as f:
            physicalItem.picture.from_file(f)
    return physicalItem

def _initCreditPackageFromFixture(fixtureData):
    return CreditPackage(**fixtureData)

def _parseStartOrEnd(tVal):
    """ parse the 'start' or 'end' key of an
    auction json data fixture """
    if type(tVal) is str:
        return datetime.datetime.strptime(tVal, ISO_8601_FORMAT)
    elif 'inSecs' in tVal.keys():
        return (datetime.datetime.utcnow() +
                 datetime.timedelta(seconds=tVal['inSecs']))
    elif 'inMins' in tVal.keys():
        return (datetime.datetime.utcnow() +
                datetime.timedelta(minutes=tVal['inMins']))


def _initAuctionFromFixture(fixtureData):
    if not fixtureData['reward']['type'] in ['item', 'physical_item']:
        raise Exception('Unsupported reward type '
                        + fixtureData['reward']['type'])
    reward = (Item.bySku(fixtureData['reward']['sku'])
              if 'item' == fixtureData['reward']['type']
              else PhysicalItem.bySku(fixtureData['reward']['sku']))
    start = _parseStartOrEnd(fixtureData['start'])
    end = _parseStartOrEnd(fixtureData['end'])
    assert start < end
    return Auction(
        reward=reward,
        start_datetime=start,
        end_datetime=end,
        **{ LEGACY_AUCTION_FIXTURE_MAP.get(key, key): fixtureData[key]
            for key in fixtureData.keys()
            if key not in [
                    'reward',
                    'start',
                    'end',
            ]})

### public interface

def createItems(fixtureNumber):
    with open(os.path.join(
            PROJ_ROOT, 'test_data', 'fixtures',
            'items_{:02d}.json'.format(fixtureNumber))
    ) as f:
        dataItems = json.load(f)
    items = [ _initItemFromFixture(dataItem)
              for dataItem in dataItems ]
    saveModels(*items)

def createPhysicalItems(fixtureNumber):
    with open(os.path.join(
            PROJ_ROOT, 'test_data', 'fixtures',
            'physical_items_{:02d}.json'.format(fixtureNumber))
    ) as f:
        dataItems = json.load(f)
    physicalItems = [ _initPhysicalItemFromFixture(dataItem)
                      for dataItem in dataItems ]
    saveModels(*physicalItems)

def createCreditPackages(fixtureNumber):
    with open(os.path.join(
            PROJ_ROOT, 'test_data', 'fixtures',
            'credit_packages_{:02d}.json'.format(fixtureNumber))
    ) as f:
        dataCreditPackages = json.load(f)
    creditPackages = [ _initCreditPackageFromFixture(dataCreditPackage)
                       for dataCreditPackage in dataCreditPackages ]
    saveModels(*creditPackages)

def createAuctionsFromFixture(fixturePathOrJsonData):
    if isinstance(fixturePathOrJsonData, list):
        jsonData = fixturePathOrJsonData
    else:
        with open(fixturePathOrJsonData, 'r') as f:
            jsonData = json.load(f)
    auctions = [ _initAuctionFromFixture(dataAuction)
                 for dataAuction in jsonData ]
    saveModels(*auctions)
    return auctions

def createFuncTestAuctions():
    """
    create auctions for functional (end-to-end) tests
    too many auction records can result in failing tests
    due to database latency making delays longer than specified
    timeouts on, e.g., game elements
    """
    auctions = [ _initAuctionFromFixture(dataAuction)
                 for dataAuction in DATA_FUNC_TEST ]
    saveModels(*auctions)


def createAuctionTestAuctions():
    """
    create auctions for app.core + updater process tests
    """
    auctions = [ _initAuctionFromFixture(dataAuction)
                 for dataAuction in DATA_AUCTION_TEST ]
    saveModels(*auctions)


def createAuctions(fixtureNumber):
    """
    create demo auctions from a json fixture specified by
    optional fixtureNumber arg
    """
    with open(os.path.join(
            PROJ_ROOT, 'test_data', 'fixtures',
            'auctions_{:02d}.json'.format(fixtureNumber))
    ) as f:
        dataAuctions = json.load(f)
    auctions = [ _initAuctionFromFixture(dataAuction)
                 for dataAuction in dataAuctions ]
    saveModels(*auctions)
    return auctions

def createConfig():
    saveModels(Config())

### slightly different patterns around
# fixture <-> model coupling & public interface
# for Flask-User usage

def _createUser(userFields):
    """ functionality from flask-user register endpoint """
    if 'credit' not in userFields.keys():
        userFields['credit'] = INIT_CREDIT
    user = User(**userFields)
    saveModels(user)
    return user


def _userDataToFields(userData):
    userFields = deepcopy(userData)
    return userFields


def createUsers(fixtureNumber):
    """
    must be run inside Flask application context
    while Flask-User is in place
    """
    with open(os.path.join(
            PROJ_ROOT, 'test_data', 'fixtures',
            'users_{:02d}.json'.format(fixtureNumber))
    ) as f:
        dataUsers = json.load(f)
    return [ _createUser(_userDataToFields(dataUser))
             for dataUser in dataUsers ]
