Don't use abbreviations unless the abbreviation can stand as its own word

Always used named arguments when constructing an object, e.g. User(username='jdoe', firstName='John', ...)

Use camel case when naming variables

When naming files, separate filename words with underscores

Indentation should be done using 4 spaces, not tabs

Line endings should be unix format. If using Windows, please take extra steps to ensure line endings are correct

Always use single quotes for strings