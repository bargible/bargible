/*global require, module, __dirname */

var path = require('path');
var fs = require('fs');

const browserify = require('browserify');
const watchify = require('watchify');
const react_templatify = require('react-templatify');
const less = require('less');

const read_file = require('../../client/build_util').read_file;
const write_file = require('../../client/build_util').write_file;

const CLIENT_STYLES_DIR = path.join(
    __dirname, '..', '..', 'client', 'style');

/**
 * prevents deprecation warning in newer versions of react
 */
var updated_react_templatify = function (file, options) {
    var opts = options || {};
    // attempting to match minor version breaks build
    opts.targetVersion= '0.14.0';
    return react_templatify(file, opts);
};

const build_js_file = function (path_src, path_out, opt_args) {
    const opts = opt_args || {};
    const make_write_bundle = function (bfy, path_bundle) {
        return function () {
            return new Promise(function (resolve, reject) {
                var stream_bundle = bfy.bundle();
                stream_bundle.pipe(fs.createWriteStream(path_bundle));
                stream_bundle.on('end', function () {
                    resolve();
                });
            });
        };
    };
    const arr_plugins = [];
    if (opts.cts) {
        arr_plugins.push(watchify);
    }
    const arr_transforms = [];
    arr_transforms.push(updated_react_templatify);
    const bfy = browserify({
        entries: [path_src],
        cache: {},
        packageCache: {},
        debug: true, // source maps
        plugin: arr_plugins,
        transform: arr_transforms
    });
    var write_bundle = make_write_bundle(bfy, path_out);
    if (opts.cts) {
        bfy.on('update', write_bundle);
    }
    return write_bundle();
};

const do_build_step = function (fn_do_once, paths_watch, cts) {
    const cbWatch = function () {
        fn_do_once()
            .catch(function (err) {
                console.log("client build error");
                console.log(err);
            });
    };
    return fn_do_once().then(function () {
        if (cts) {
            paths_watch.forEach(function (path_watch) {
                fs.watch(path_watch, {recursive: true}, cbWatch);
            });
        }
    });
};

const build_style = function (path_src_file, path_output, opt_args) {
    const opts = opt_args || {};
    const dir_src = path.dirname(path_src_file);
    const do_once = function () {
        return read_file(path_src_file)
            .then(function (str_less_input) {
                return less.render(str_less_input, {
                    paths: [
                        dir_src,
                        CLIENT_STYLES_DIR,
                        path.join(CLIENT_STYLES_DIR, 'lib'),
                        path.join(CLIENT_STYLES_DIR, 'vendor')
                    ]
                });
            }).then(function (less_output) {
                var str_css = less_output.css;
                var str_sourcemap = less_output.sourcemap;
                var arr_imports = less_output.imports;
                return write_file(path_output, str_css);
            });
    };
    return do_build_step(do_once, [
        dir_src,
        CLIENT_STYLES_DIR
        ], opts.cts);
};

const build_js = function (dir_out, opt_args) {
    const filenames_src = [
        'auction_joined.js',
        'buy_credits_modal_00.js'
    ];
    return Promise.all(
        filenames_src.map(function (filename_src) {
            var out_file_name = filename_src.replace(/jsx$/, 'js');
            // clean before build
            if (fs.existsSync(out_file_name)) {
                fs.unlinkSync(out_file_name);
            }
            return build_js_file(
                path.join(__dirname, '..', 'setup', filename_src),
                path.join(dir_out, out_file_name),
                opt_args
            );
        })
    );
};

const build_styles = function (dir_out, opt_args) {
    const filepaths_client_src = [
        'auction_joined.less',
        'modal.less',
        'modal_buy_credits.less',
        'common.less'
    ].map(function (filename_client_src) {
        return path.join(CLIENT_STYLES_DIR, filename_client_src);
    });
    const filepaths_local_src =[
        'auction_joined_design.less'
    ].map(function (filename_local_src) {
        return path.join(
            __dirname, '..', 'setup', 'style',
            filename_local_src
        );
    });
    return Promise.all(
        filepaths_client_src.concat(filepaths_local_src)
            .map(function (filepath_src) {
                var filename_out = path.basename(filepath_src)
                        .replace(/less$/, 'css');
                return build_style(
                    filepath_src,
                    path.join(dir_out, filename_out),
                    opt_args
                );
            })
    );
};

var exports = {};

exports.build_js = build_js;
exports.build_styles = build_styles;

module.exports = exports;
