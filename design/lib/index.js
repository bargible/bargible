/*global require, module, __dirname */

var path = require('path');

const build = require('./build');

const DIR_JS_OUT = path.join(__dirname, '..', 'www', 'static', 'js');
const DIR_STYLES_OUT = path.join(
    __dirname, '..', 'www', 'static', 'css', 'gen');

const buildAll = function (optArgs) {
    var opts = optArgs || {};
    return Promise.all([
        build.build_js(DIR_JS_OUT, opts)
            .catch(function (err) {
                console.error("js build failed with error");
                console.error(err);
                throw err;
            }),
        build.build_styles(DIR_STYLES_OUT, opts)
            .catch(function (err) {
                console.error("custom styles build failed with error");
                console.error(err);
                throw err;
            })
    ]);
};

var exports = {};

exports.buildAll = buildAll;

module.exports = exports;
