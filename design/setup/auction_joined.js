/*global require,
 setInterval, clearInterval, setTimeout,
 URL */

var _ = require('lodash');
var moment = require('moment');
var uuid = require('uuid');
var React = require('react');
var ReactDOM = require('react-dom');


var JoinedAuctionNode = require(
    '../../client/app/comp/auction_joined');
var util = require(
    '../../client/app/util');
var compUtil = require(
    '../../client/app/comp/util');
var loadAnimations = require(
    '../../client/app/util_home').loadAnimations;

var animDefs = require('./anim.json');

///////// state ///

var AUCTION_ID = 1;

// mutable auction props
// see renderState for prop names
var UPDATES = [];
var IS_BIDS_FROZEN = false;

var PROP_AUCTION = {
    "id": AUCTION_ID,
    "reward": {
        "image": "static/img/bucks-kitten.png",
        "value": 100
    },
    "rewardType": "bucks",
    "intTimeEnd": moment().add(5, 'minutes'),
    "ultTimeEnd": moment().add(1, 'hours'),
    "bidPrice": 1,
    "bidsTilActivation": {
        "freeze": 10, "steal": 20, "destroy": 30 },
    "numActivations": {
        "freeze": 6, "steal": 3, "destroy": 2 },
    "bids": 10,
    "leaderboard": [],
    "position": null
};

var INTERVAL_UPDATES = null;

/** used for testing updates/notifications/ */
var updatesUpdater = (function () {
    var UPDATE_TYPES = [
        'bids-freeze',
        'destroy',
        'steal'
    ];
    var updateTypeIdx = 0;
    return function () {
        updateTypeIdx += 1;
        updateTypeIdx %= UPDATE_TYPES.length;
        var updateType = UPDATE_TYPES[updateTypeIdx];
        var update = {
            id: uuid.v1(),
            createdAt: moment(),
            auctionId: 1,
            type: updateType,
            info: {
                username: 'bob'
            }
        };
        if (updateType === 'destroy' || updateType === 'steal') {
            update.info.numBids = 3;
        }
        UPDATES.push(update);
    };
}());

////////////// sandbox ui setup ///

document.getElementById('btn-sim-updates')
    .addEventListener('click', function () {
        var updateFrequency = 10000; // ms
        if (INTERVAL_UPDATES) {
            clearInterval(INTERVAL_UPDATES);
            INTERVAL_UPDATES = null;
        } else {
            INTERVAL_UPDATES =
                setInterval(updatesUpdater, updateFrequency);
        }
    });

(function () {
    var elStateFreeze = document.getElementById('dstate-sim-freeze');
    elStateFreeze.textContent = "thawed";
    document.getElementById('btn-sim-freeze')
        .addEventListener('click', function () {
            var freezeDuration = 5000; // ms
            if (INTERVAL_UPDATES) {
                alert("may not sim freeze while sim updates are running");
                return;
            }
            if (IS_BIDS_FROZEN) {
                alert("already running simulated freeze");
                return;
            }
            IS_BIDS_FROZEN = true;
            elStateFreeze.textContent = "frozen";
            setTimeout(function () {
                IS_BIDS_FROZEN = false;
                elStateFreeze.textContent = "thawed";
            }, freezeDuration);
        });
}());

////////////// render components using state ///

var _render = function (auction) {
    ReactDOM.render(React.createElement(JoinedAuctionNode, {
        auction: auction,
        user: {},
        animStealFramerate: 30,
        animDestroyFramerate: 30,
        animThawFramerate: 30,
        animFreezeFramerate: 30
    }), document.getElementById('container-auction-joined'));
};

var renderState = function () {
    _render(_.merge(_.cloneDeep(PROP_AUCTION), {
        updates: _.cloneDeep(UPDATES),
        isBidsFrozen: IS_BIDS_FROZEN
    }));
};

Promise.resolve().then(function () {
    return loadAnimations(animDefs);
}).then(function () {
    // display first frame (in time sequence) of thaw animation
    document.getElementById('img-blob-sample').src =
        compUtil.getAnimationUrls().destroy[53 - 17];

    // don't wait for state updates to re-render:
    // there is no store or event system in this scaffolding
    // around the component
    setInterval(renderState, 500);
});

///////// demo displaying an img file blob in the browser ///
/*
util.fetchFile('static/img/anim/thaw/freeze0083.png',
               'image/png')
    .then(function (res) {
        document.getElementById('img-blob-sample').src =
            URL.createObjectURL(res);
    });
*/
