/*global require, setInterval */

var _ = require('lodash');
var uuid = require('uuid');

var openModal = require(
    '../../client/app/comp/util').openModal;
var factoryBuyCreditsModal = require(
    '../../client/app/comp/modal_buy_credits');

var propsData = _.mapValues(
    require('./fixtures/buy_credits_modal.json'),
    function (val, key) {
        // add id attrs to credit packages
        if (key !== 'creditPackages') {
            return val;
        }
        val.forEach(function (creditPackage) {
            creditPackage.id = uuid.v1();
        });
        return val;
    });

document.getElementById('btn-display-modal')
    .addEventListener('click', function () {
        openModal(factoryBuyCreditsModal, _.cloneDeep(propsData), {
            isFactory: true
        });
    });
