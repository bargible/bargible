### Design Sandbox

Files in this directory are independent of the application.

The design sandbox is used for iterating on design specifications
with a minimal state (data) setup.

All commands are relative to `design/`,
the sandbox root directory,
not the project root uninstall root.

This documentation is written for systems where the
[node](http://nodejs.org/)
executable is `node`.
Note that on some systems, the executable is `nodejs`.

##### install

from the project root directory, run

`npm install`

##### build

`node bin/build.js`

with optional `-c` flag for continuous (build on save) builds

##### run

Change to the `design/www` directory and start an http server
to serve the files there to browsers

```
cd www
python -m http.server 5005
```

The purpose of the latter command is to start an http server.
Any http server capable of serving a directory tree as static files
will do,
and the port number `5005` may be replaced by any open general-purpose
port on the system.
In particular, if python version 2.7 is the only available version
of python on the system,

```
python -m SimpleHTTPServer 5005﻿⁠
```

will also suffice.

After starting the http server, direct a webbrowser to `http://localhost:5005`
(possibly replacing the port number `5005`
if a different port was used when starting the web server)
and click on the desired `.html` link.
