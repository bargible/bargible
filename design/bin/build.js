/*global require, process */

var path = require('path');

const buildAll = require('../lib').buildAll;

const argv = require('yargs').options({
    'c': {
        alias: 'cts',
        desc: "continuous client build",
        type: 'boolean'
    }
}).argv;

Promise.resolve().then(function () {
    return buildAll({cts: argv.cts});
}).then(function () {
    console.log("js and custom styles build finished");
}, function (err) {
    console.error("js or css build failed with error");
    console.error(err);
    process.exit(1);
});
