# CHANGES TO APP.LESS
_______________________________________

Installed Graphik-Regular font, linked to the file in app.less. 

Attempted to establish media query variables in app.less, but repeatedly encountered errors- this confused me, so I ended up placing LESS breakpoints into modal_buy_credits.less. 

#modal_buy_credits.less
_______________________________________

Added LESS breakpoints mixins into the Buy Credits modal-wrapper

Set font as Graphik-Regular

Added classes for pertaining elements of page (caption, balance, package, yellow, pricebox)

Set different box sizes for different screen sizes 


Also added .svg and .png for Coins image. Don't know why the .svg is not appearing, using the png currently. 