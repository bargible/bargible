import os

class Config(object):
    DEBUG = False
    TESTING = False

    CSRF_ENABLED = True
    WTF_CSRF_ENABLED = True

    SECRET_KEY = 'THISISANINSECURESECRET'

    USER_AFTER_LOGIN_ENDPOINT = 'page_views.home'
    USER_ENABLE_CONFIRM_EMAIL = False
    USER_APP_NAME = 'Bargible'
    USER_REGISTER_TEMPLATE = 'register.html'
    USER_REGISTER_URL = '/register'
    USER_LOGIN_TEMPLATE = 'login.html'
    USER_LOGIN_URL = '/login'
    USER_AFTER_LOGOUT_ENDPOINT = 'page_views.index'

    # Flask-Mail settings
    MAIL_USERNAME =           os.getenv('MAIL_USERNAME',        'email@example.com')
    MAIL_PASSWORD =           os.getenv('MAIL_PASSWORD',        'password')
    MAIL_DEFAULT_SENDER =     os.getenv('MAIL_DEFAULT_SENDER',  '"MyApp" <noreply@example.com>')
    MAIL_SERVER =             os.getenv('MAIL_SERVER',          'smtp.gmail.com')
    MAIL_PORT =           int(os.getenv('MAIL_PORT',            '465'))
    MAIL_USE_SSL =        int(os.getenv('MAIL_USE_SSL',         True))
    # currently with braintree.  intended to abstract over
    # multiple vendors via
    # 'sand' -- sandbox
    # 'prod' -- production
    # None -- disable payments
    PAYMENTS_ENV = 'sand'
    # analogous config for Tango
    TANGO_ENV = 'sand'


class ProductionConfig(Config):
    DEBUG = False
    DEVELOPMENT = False
    TESTING = False
    PAYMENTS_ENV = 'prod'
    TANGO_ENV = 'prod'


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    PAYMENTS_ENV = 'sand'
    TANGO_ENV = 'sand'


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    TESTING = True


class TestingConfig(Config):
    TESTING = True
    SERVER_NAME = 'localhost:5000'
    WTF_CSRF_ENABLED = False
