"""
provide a global function for .html templates to load static asset
urls with a "version" string in the form of an md5 checksum
https://en.wikipedia.org/wiki/MD5

there speed/memory tradeoff where the checksum is cached
in the python runtime's memory the first time a given asset
is loaded in order for .html template rendering not to be slowed
down too much by this functionality
"""
import os.path
import hashlib
from urllib.parse import urlencode

from flask import url_for

_hashedFiles = {}

def loadStaticPath(pathStaticFolder, pathStaticFile):
    unversionedUrl = url_for('static',
                                 filename=pathStaticFile)
    if pathStaticFile not in _hashedFiles:
        with open(os.path.join(pathStaticFolder, pathStaticFile),
                      'rb') as f:
            assetBytes = f.read()
        myMd5 = hashlib.md5()
        myMd5.update(assetBytes)
        _hashedFiles[pathStaticFile] = myMd5.hexdigest()
    return '?'.join([
        unversionedUrl,
        urlencode(dict(verhash=_hashedFiles[pathStaticFile]))
    ])
