"""
hook, extend, subclass, etc.
http://flask.pocoo.org/docs/0.11/becomingbig/#hook-extend

the idea for this package (at time of writing -- subject to change,
claro) is to add functionality to Flask that's useful for
this specific application but not specific to the application:
configuration-like details and cross-package/module concerns
(attempt to) remain in the application factory
"""
import functools
from warnings import warn

from flask import Flask as FlaskBase

from .static_asset_urls import loadStaticPath

class Flask(FlaskBase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not hasattr(self, 'static_folder'):
            raise AssertionError((
                "Flask subclass is bolted too tightly to "
                "upstream impl:  expected the Flask object "
                "to have a 'static_folder' attr"))
        if self.static_folder is None:
            warn((
                "without the 'static_folder' attr set, "
                "version information will not be added "
                "to static asset urls"))

    def create_jinja_environment(self):
        """
        the .html template environment is
        a Flask-specific subclass of jinja2.Environment
        http://jinja.pocoo.org/docs/dev/api/#jinja2.Environment
        this override:
        * adds a global function for loading static
          files
        """
        rv = super().create_jinja_environment()
        rv.globals.update(
            static=functools.partial(
                loadStaticPath, self.static_folder),
        )
        return rv
