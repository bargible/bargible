"""
give some space for flask_login customizations

currently provides a standard setup
https://flask-login.readthedocs.io/en/latest/#configuring-your-application
"""

from flask_login import LoginManager

from bargible.model import User

loginManager = LoginManager()

@loginManager.user_loader
def load_user(userId):
    """ userId (str) """
    return User.byId(int(userId))
