"""
routes to api endpoints

every url consumed by the app's (js, etc.) clients
goes in this package and is exported as a Flask blueprint
"""

from .top import blueprint as top
from .user import blueprint as user
from .auction import blueprint as auction
from .user_auction import blueprint as userAuction


__all__ = [
    'top',
    'user',
    'auction',
    'userAuction',
]
