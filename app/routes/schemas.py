"""
schemas for incoming HTTP request data.
unlike the in model layer, validation here is considered
part of the HTTP API.
"""

# these could potentially be translated to client-side schemas by
# https://github.com/fuhrysteve/marshmallow-jsonschema

import re

from marshmallow import Schema
from marshmallow import ValidationError
from marshmallow import fields
from marshmallow import validates
from marshmallow import validates_schema
from marshmallow import post_load

from bargible.model.shipping_address import USA_STATES
from bargible.model import ShippingAddress as ShippingAddressModel
from bargible.model import User as UserModel

class User(Schema):
    lastName = fields.Str(required=True)
    firstName = fields.Str(required=True)
    username = fields.Str(required=True)
    password = fields.Str(required=True)
    passwordRepeat = fields.Str(required=True)
    email = fields.Email(required=True)

    # map schema (json api) keys to model fields
    KEY_MAP = {
        'lastName': 'last_name',
        'firstName': 'first_name',
        'passwordRepeat': 'retype_password',
    }

    @validates('username')
    def validateUsername(self, data):
        if UserModel.findByUsername(data, allowNone=True) is not None:
            raise ValidationError((
                "username '" + data + "' already exists"))


    @validates('password')
    def validatePassword(self, data):
        if len(data) < 8:
            raise ValidationError((
                "password must be at least 8 characters"))

    @validates_schema
    def validatePasswordRepeat(self, data):
        # .get() necessary b/c if individual field (e.g. password)
        # validation fails, that field isn't included in the data
        # to validate here (... i think?  check docs.)
        if (data.get('password', None) !=
                data.get('passwordRepeat', None)):
            raise ValidationError((
                "passwords don't match"),
                'passwordRepeat')

    @post_load
    def makeModel(self, data):
        modelFields = {
            self.KEY_MAP.get(key, key): data[key] for
            key in data.keys() if key != 'passwordRepeat'
            }
        return UserModel(**modelFields)


class UserLogin(Schema):
    usernameOrEmail = fields.Str(required=True)
    password = fields.Str(required=True)


class ShippingAddress(Schema):
    name = fields.Str(required=True)
    addr_1 = fields.Str(required=True)
    addr_2 = fields.Str(required=True)
    city = fields.Str(required=True)
    usa_state = fields.Str(required=True)
    zip_code = fields.Str(required=True)

    @validates('usa_state')
    def validateState(self, data):
        if not data in USA_STATES:
            raise ValidationError("unknown state: " + data)

    @validates('zip_code')
    def validateZip(self, data):
        if not re.match(r'^\d{5}$', data):
            raise ValidationError((
                "zip code must be precisely five digits"))

    @post_load
    def makeModel(self, data):
        return ShippingAddressModel(**data)


user = User()
userLogin = UserLogin()
shippingAddress = ShippingAddress()
