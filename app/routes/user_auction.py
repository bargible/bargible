"""
access to auctions for logged-in users
"""
import json


from flask import request
from flask import jsonify

from flask_login import current_user

from bargible.model import saveModels
from bargible.model import Auction
from bargible.auc_core.auction_manager import AuctionManager

from .base import Base as BaseBlueprint
from .helpers import makeAuctionJsonObj
from .helpers import getConfig
from .helpers import login_required
from . import user_auction_actions as auctionActions

blueprint = BaseBlueprint(
    'user_auction', __name__,
    template_folder='templates')

@blueprint.route('', methods=['GET'])
@login_required
def getAuctionsForUser():
    if len(request.args.getlist('status')) > 1:
        resp = jsonify(msg="may specify at most one status type")
        resp.status_code = 400
        return resp
    allAuctions = [ uam.auction for uam in current_user.auctions ]
    if (request.args.get('status') is None or
        request.args.get('status') == 'joined'):
        activeAuctionsJson = [ makeAuctionJsonObj(auction, current_user)
                               for auction in allAuctions
                               if auction.is_active ]
        output = activeAuctionsJson
    elif request.args.get('status') == 'ended':
        endedAuctionsJson = [ makeAuctionJsonObj(auction, current_user)
                              for auction in allAuctions
                              if auction.ended ]
        output = endedAuctionsJson
    else:
        resp = jsonify(msg=("unknown status type "
                            "for private auctions endpoint"),
                       data=request.args.get('status'))
        resp.status_code = 400
        return resp
    return json.dumps(output)


@blueprint.route('/<int:auctionId>', methods=['GET', 'PUT'])
@login_required
def getAuctionForUser(auctionId):
    auction = Auction.byId(auctionId)
    if auction is None:
        return jsonify(msg="auction not found",
                           auctionId=auctionId), 404
    if (request.args.get('status') == 'joined' and
        auction.getUserRelation(current_user, allowNone=True) is None):
        return jsonify(msg="user has not joined auction",
                           auctionId=auctionId), 400
    if (request.args.get('status') == 'ended' and
        not auction.ended):
        return jsonify(msg='This auction is not ended.'), 400
    if (request.args.get('status') not in set([
            'joined',
            'ended',
    ]) and request.method == 'GET'):
        return jsonify(msg=("unknown status type "
                                "for private auction endpoint "
                                "fetch"),
                       data=request.args.get('status')), 400
    if request.method == 'PUT':
        if (request.args.get('status') not in set([
            'upcoming',
        ])):
            return jsonify(msg=("unknown status type "
                                    "for private auction endpoint "
                                    "update"),
                               data=request.args.get('status')), 400
        if request.json.get('watch', None):
            if auction.isUserWatching(current_user):
                return jsonify(msg=(
                    "user is already watching")), 400
            auction.addWatch(current_user)
            saveModels(auction)
    output = makeAuctionJsonObj(auction, current_user)
    return json.dumps(output)


@blueprint.route('/<int:auctionId>/actions/<action>',
                 methods=['POST', 'PUT'])
@login_required
def auctionActionForUser(auctionId, action):
    auction = Auction.byId(auctionId)
    auctionManager = AuctionManager(auction, getConfig())
    if request.method == 'POST':
        if action == 'join':
            return auctionActions.join(auctionManager)
        else:
            return jsonify(
                msg=("unknown action '" + str(action) + "' " +
                     "for " + str(request.method) + " request")
            ), 400
    # is PUT (update) action
    if action == 'bid':
        return auctionActions.bid(auctionManager)
    elif action == 'freeze':
        return auctionActions.freeze(auctionManager)
    elif action == 'steal':
        return auctionActions.steal(auctionManager)
    elif action == 'destroy':
        return auctionActions.destroy(auctionManager)
    else:
        return jsonify(
            msg=("unknown action '" + str(action) + "' " +
                 "for " + str(request.method) + " request")
        ), 400
