"""
helpers for the auction actions endpoint used to consolidate
all non-RESTful modifications (e.g. gameplay) a logged-in user
may affect for a given auction
"""

from flask import jsonify

from flask_login import current_user

from bargible.auc_core.exc import AucCoreError
import bargible.client_auc_events as events

############ module-private helpers

def _notifyBidMult(resBidMult, auction, bidUser):
    """
    helper for bid route
    to send notifications related to bid multiple times feature
    """
    updateInfo = dict(
        username=current_user.username,
        numConsecutiveBids=resBidMult['consecutiveBids'],
        complete=resBidMult['complete'],
    )
    for user in [uam.user for uam in auction.getUserRelations()]:
        events.auctionUpdateEvent(user.sid, {
            'auctionId': auction.id,
            'updateType': 'bid-mult',
            'updateInfo': updateInfo,
        })


def _notifyGameElement(auction, usersNotToNotify):
    """
    helper for all game element routes
    to send notifications related to game element usage
    """
    users = [ uam.user for uam in
              auction.getUserRelations() ]
    usersToNotify = set(users) - set(usersNotToNotify)
    for user in usersToNotify:
        events.auctionUpdateEvent(user.sid, {
            'auctionId': auction.id,
            'updateType': 'game-element',
        })


def _notifyFreeze(auctionId, freezeUser, frozenUsers):
    updateInfo = dict(
        usernames=[frozenUser.username for frozenUser in frozenUsers],
    )
    events.auctionUpdateEvent(freezeUser.sid, {
        'auctionId': auctionId,
        'updateType': 'froze-bids',
        'updateInfo': updateInfo,
    })
    for user in frozenUsers:
        updateInfo = dict(
            username=freezeUser.username,
        )
        events.auctionUpdateEvent(user.sid, {
            'auctionId': auctionId,
            'updateType': 'bids-freeze',
            'updateInfo': updateInfo,
        })


def _notifySteal(auctionId, dictsStolenBids):
    for dictStolenBids in dictsStolenBids:
        user = dictStolenBids['user']
        bidsStolen = dictStolenBids['bidsStolen']
        if bidsStolen < 0:
            raise AssertionError("got negative number of stolen bids")
        elif bidsStolen == 0:
            # assert duplicate original functionalty
            raise AssertionError("got zero stolen bids")
        else:
            updateInfo = dict(
                numBids=bidsStolen,
                username=current_user.username,
            )
            events.auctionUpdateEvent(user.sid, {
                'auctionId': auctionId,
                'updateType': 'steal',
                'updateInfo': updateInfo,
            })
    updateInfo = dict(
        numBids=sum([ dictStolenBids['bidsStolen'] for
                      dictStolenBids in dictsStolenBids ]),
        usernames=[dictStolenBids['user'].username for
                   dictStolenBids in dictsStolenBids],
    )
    events.auctionUpdateEvent(current_user.sid, {
        'auctionId': auctionId,
        'updateType': 'steal-self',
        'updateInfo': updateInfo,
    })


def _notifyDestroy(auctionId, dictsDestroyedBids):
    # notify users w/ bids destroyed
    for dictDestroyedBids in dictsDestroyedBids:
        user = dictDestroyedBids['user']
        bidsDestroyed = dictDestroyedBids['bidsDestroyed']
        if bidsDestroyed < 0:
            raise AssertionError("got negative number of destroyed bids")
        elif bidsDestroyed == 0:
            continue
        else:
            updateInfo = dict(
                numBids=bidsDestroyed,
                username=current_user.username,
            )
            events.auctionUpdateEvent(user.sid, {
                'auctionId': auctionId,
                'updateType': 'destroy',
                'updateInfo': updateInfo,
            })
    # notify user who destroyed bids
    updateInfo = dict(
        usernames=[dictDestroyedBids['user'].username for
                   dictDestroyedBids in dictsDestroyedBids],
    )
    events.auctionUpdateEvent(current_user.sid, {
        'auctionId': auctionId,
        'updateType': 'destroy-self',
        'updateInfo': updateInfo,
    })


########### public interface

def join(auctionManager):
    try:
        auctionManager.joinAuction(current_user)
    except AucCoreError as err:
        resp = jsonify(msg=err.getClientMsg())
        resp.status_code = 400
        return resp
    events.auctionUpdateEvent(current_user.sid, {
        'auctionId': auctionManager.auction.id,
        'updateType': 'join',
    })
    return jsonify()


def bid(auctionManager):
    try: # all the .core module things
        ( # .core api subject to change.
            flags,
            resBidMult,
        ) = auctionManager.bidOnAuction(current_user)
    except AucCoreError as err:
        resp = jsonify(msg=err.getClientMsg())
        resp.status_code = 400
        return resp
    # notifications to browser(s)
    if flags['firstBidder']:
        events.auctionUpdateEvent(current_user.sid, {
            'auctionId': auctionManager.auction.id,
            'updateType': 'first-bid',
        })
    if resBidMult is not None:
        _notifyBidMult(resBidMult, auctionManager.auction, current_user)
    # notify every user that bid price changed ...
    # ... creates duplicate fetches for bidding user, frozen users,
    # and all users when there's a bid multiple times element
    for user in [uam.user for uam in
                 auctionManager.auction.getUserRelations()]:
        if user == current_user:
            events.auctionUpdateEvent(user.sid, {
                'auctionId': auctionManager.auction.id,
                'updateType': 'bid-self',
            })
        else:
            events.auctionUpdateEvent(user.sid, {
                'auctionId': auctionManager.auction.id,
                'updateType': 'bid',
            })
    return jsonify()


def freeze(auctionManager):
    try:
        resFreeze = auctionManager.freezeBids(current_user)
    except AucCoreError as err:
        resp = jsonify(msg=err.getClientMsg())
        resp.status_code = 400
        return resp
    # notify users w/ bids destroyed
    _notifyFreeze(
        auctionManager.auction.id,
        current_user,
        resFreeze,
    )
    _notifyGameElement(
        auctionManager.auction,
        resFreeze + [current_user],
    )
    return jsonify()


def steal(auctionManager):
    try:
        dictsStolenBids = auctionManager.stealBids(current_user)
    except AucCoreError as err:
        resp = jsonify(msg=err.getClientMsg())
        resp.status_code = 400
        return resp
    _notifySteal(
        auctionManager.auction.id,
        dictsStolenBids,
    )
    _notifyGameElement(
        auctionManager.auction,
        ([dictStolen['user'] for dictStolen in dictsStolenBids] +
         [current_user]),
    )
    return jsonify()


def destroy(auctionManager):
    try:
        dictsDestroyedBids = auctionManager.destroyBids(current_user)
    except AucCoreError as err:
        resp = jsonify(msg=err.getClientMsg())
        resp.status_code = 400
        return resp
    _notifyDestroy(
        auctionManager.auction.id,
        dictsDestroyedBids,
    )
    _notifyGameElement(
        auctionManager.auction,
        ([dictDestroyed['user']
          for dictDestroyed in dictsDestroyedBids] +
         [current_user]),
    )
    return jsonify()

