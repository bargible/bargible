"""
misc. api endpoints that both
* require login
* have behavior specific to the logged-in user
"""
import json

from wand.image import Image

from sqlalchemy_imageattach.context import store_context

from flask import request
from flask import jsonify

from flask_login import current_user

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.exc import IntegrityError

from bargible.model import imageStore
from bargible.model import Auction
from bargible.model import CreditPackage
from bargible.model import Redemption
from bargible.model import Transaction
from bargible.model import saveModels
from bargible.third_party_api.braintree import util as btUtil

from bargible.model.transaction import TransactionType

from .base import Base as BaseBlueprint

from .helpers import resizeAndCropImgSq
from .helpers import login_required
from . import schemas
from . import err_codes

blueprint = BaseBlueprint(
    'user', __name__,
    template_folder='templates')

SQUARE_SIZE_AVATAR = 300 # pixels

@blueprint.route('/get', methods=['GET'])
@login_required
def getUser():
    output = current_user.toJsonObj()
    return json.dumps(output)


@blueprint.route('/creditPackages/<int:creditPackageId>',
                 methods=['POST'])
@login_required
def createUserCreditPackage(creditPackageId):
    creditPackage = CreditPackage.byId(creditPackageId)
    if creditPackage is None:
        resp = jsonify(msg="unknown credit package id",
                       creditPackageId=creditPackageId,
                       code=3)
        resp.status_code = 400
        return resp
    # parse request
    if request.json is None:
        resp = jsonify(msg="no json request data",
                       code=1)
        resp.status_code = 400
        return resp
    if 'nonce' not in request.json.keys():
        resp = jsonify(msg="didn't find nonce in json request data",
                       code=2)
        resp.status_code = 400
        return resp
    # braintree server transaction
    ## todo: error-handling to prevent lost transactions, etc.
    nonceFromClient = request.json['nonce']
    result = btUtil.makeSale(creditPackage.price, nonceFromClient)
    if result.isSuccess:
        current_user.credit += creditPackage.num_credits
        Transaction(
            user=current_user,
            amount=creditPackage.price,
            transaction_type=TransactionType.CREDIT,
            )
        saveModels(current_user)
        return jsonify()
    else:
        return jsonify(msg="transaction error w/ braintree server",
                       code=err_codes.SALE,
                       data=result.jsonErrorData)


@blueprint.route('/avatar', methods=['PUT'])
@login_required
def updateAvatar():
    with Image(blob=request.data) as imgAvatarUpload:
        imgAvatar = resizeAndCropImgSq(
            imgAvatarUpload,
            SQUARE_SIZE_AVATAR)
        with store_context(imageStore):
            current_user.avatar.from_blob(imgAvatar.make_blob('jpeg'))
            saveModels(current_user)
            return jsonify()
        resp = jsonify(msg="failed to store processed image")
        resp.status_code = 500
        return resp
    resp = jsonify(msg="failed to process upload data as image data")
    resp.status_code = 400
    return resp


@blueprint.route('/auctionPayments', methods=['POST'])
@login_required
def createAuctionPayment():
    # parse json
    requestJson = request.get_json()
    if requestJson is None:
        return jsonify(msg="no json data"), 400
    auctionId = requestJson.get('auctionId')
    nonceFromClient = requestJson.get('nonce')
    if auctionId is None:
        return jsonify(msg="no 'auctionId' in json data"), 400
    if nonceFromClient is None:
        return jsonify(msg="no 'nonce' in json data"), 400
    try:
        auctionId = int(auctionId)
    except ValueError:
        return jsonify(
            msg="auctionId must be an int.  got " + repr(auctionId)
        ), 400
    # check whether payment is appropriate
    auction = Auction.byId(auctionId)
    if auction is None:
        return jsonify(
            msg="auction not found", auctionId=auctionId
        ), 404
    if not auction.ended:
        return jsonify(msg="may not pay before auction ended"), 400
    if auction.last_bidder != current_user:
        return jsonify(msg="only winner may pay"), 400
    if auction.isPaid:
        return jsonify(msg="may pay at most once per auction"), 400
    # make and record payment
    result = btUtil.makeSale(auction.bid_price, nonceFromClient)
    if result.isSuccess:
        saveModels(Redemption(auction=auction))
        return jsonify()
    else:
        return jsonify(msg="transaction error w/ braintree server",
                       code=err_codes.SALE,
                       data=result.jsonErrorData)


@blueprint.route('/auctionAddress', methods=['POST'])
@login_required
def createAuctionAddress():
    """ shipping address for winner of auction """
    # parse json
    requestJson = request.get_json()
    if requestJson is None:
        return jsonify(msg="no json data"), 400
    auctionId = requestJson.get('auctionId')
    addrJson = requestJson.get('shippingAddress')
    if auctionId is None:
        return jsonify(msg="no 'auctionId' in json data"), 400
    if addrJson is None:
        return jsonify(msg="no 'shippingAddress' in json data"), 400
    try:
        auctionId = int(auctionId)
    except ValueError:
        return jsonify(
            msg="auctionId must be an int.  got " + repr(auctionId)
        ), 400
    shippingAddress, addrErrs = schemas.shippingAddress.load(addrJson)
    if len(addrErrs.keys()) != 0:
        return jsonify(
            msg="invalid shipping address",
            code=err_codes.SCHEMA,
            data=addrErrs)
    # check whether recording the shipping address is appropriate
    auction = Auction.byId(auctionId)
    if auction is None:
        return jsonify(
            msg="auction not found", auctionId=auctionId
        ), 404
    if not auction.ended:
        return jsonify(msg="may not pay before auction ended"), 400
    if auction.last_bidder != current_user:
        return jsonify(msg="only winner may pay"), 400
    # record the shipping address
    try:
        shippingAddress.auction = auction
    except ValueError as err:
        return jsonify(
            msg="can't set shipping address for this auction",
            code=err_codes.DB), 400
    try:
        saveModels(shippingAddress)
    except IntegrityError as err:
        if err.params['auction_id'] is None:
            return jsonify(
                msg="shipping address already exists for this auction",
                code=err_codes.DB), 400
        return jsonify(msg="failed to save shipping address"), 500
    except SQLAlchemyError as err:
        return jsonify(msg="failed to save shipping address"), 500
    return jsonify()
