"""
api endpoints for auction functionality from the perspective of
a user that may or may not be logged in
"""
import json

from flask import request
from flask import jsonify

from flask_login import current_user

from bargible.model import Auction

from .base import Base as BaseBlueprint
from .helpers import makeAuctionJsonObj

blueprint = BaseBlueprint(
    'auction', __name__,
    template_folder='templates')

@blueprint.route('/auctions', methods=['GET'])
def getAuctions():
    if len(request.args.getlist('status')) > 1:
        resp = jsonify(msg="may specify at most one status type")
        resp.status_code = 400
        return resp
    # includes both active unjoined and active joined
    activeAuctionsJson = [ makeAuctionJsonObj(
        auction,
        (None if current_user.is_anonymous else current_user),
    ) for auction in Auction.getActiveAuctions() ]
    if request.args.get('status') is None:
        output = activeAuctionsJson
    elif request.args.get('status') == 'active':
        unjoinedAuctionsJson = [
            auctionJson for auctionJson in
            activeAuctionsJson
            if auctionJson['joined'] is None ]
        output = unjoinedAuctionsJson
    elif request.args.get('status') == 'upcoming':
        upcomingAuctionsJson = [ makeAuctionJsonObj(
            auction,
            (None if current_user.is_anonymous else current_user),
        ) for auction in Auction.getUpcomingAuctions() ]
        output = upcomingAuctionsJson
    else:
        resp = jsonify(msg=("unknown status type "
                            "for public auctions endpoint"),
                       data=request.args.get('status'))
        resp.status_code = 400
        return resp
    return json.dumps(output)


@blueprint.route('/auctions/<int:auctionId>', methods=['GET'])
def getAuction(auctionId):
    auction = Auction.byId(auctionId)
    if auction is None:
        resp = jsonify(msg="auction not found", auctionId=auctionId)
        resp.status_code = 404
        return resp
    output = makeAuctionJsonObj(
        auction,
        (None if current_user.is_anonymous else current_user),
    )
    return json.dumps(output)
