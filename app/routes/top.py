"""
the endpoints in this module are based on a grouping
suggested by the url naming scheme in a monolithic `views.py`.

specificially, they corresond to routes that didn't have
a path prefix other than '/api' and viewed as "top-level"
routes
"""
from copy import deepcopy
from datetime import datetime

from werkzeug.datastructures import ImmutableMultiDict

from flask import request
from flask import jsonify

from flask_login import login_user

from bargible.model import Config
from bargible.model import CreditPackage
from bargible.model import User
from bargible.model import saveModels

from .base import Base as BaseBlueprint
from .helpers import login_required
from . import err_codes
from . import schemas

blueprint = BaseBlueprint(
    'top', __name__,
    template_folder='templates')

@blueprint.route('/register', methods=['POST'])
def apiRegister():
    """
    duplicate functionality from flask-user register endpoint,
    except for json requests
    """
    ## parse request data
    formFields = deepcopy(request.get_json())
    user, userErrs = schemas.user.load(formFields)
    if len(userErrs.keys()) != 0:
        return jsonify(
            msg="invalid user registration",
            code=err_codes.SCHEMA,
            data=userErrs,
            ), 400
    # set defaults
    user.credit = Config.get().init_user_credits
    ## save user object and login
    saveModels(user)
    login_user(user, remember=True)
    return jsonify(
        next='/home'
    )


@blueprint.route('/login', methods=['POST'])
def apiLogin():
    formFields = deepcopy(request.get_json())
    loginData, loginDataErrs = schemas.userLogin.load(formFields)
    if len(loginDataErrs.keys()) != 0:
        return jsonify(
            msg="invalid login form",
            code=err_codes.SCHEMA,
            data=loginDataErrs,
            ), 400
    user = User.findByUsername(
        loginData['usernameOrEmail'], allowNone=True)
    if user is None:
        user = User.findByEmail(
            loginData['usernameOrEmail'], allowNone=True)
    # HTTP error codes below here could be updated (302)
    if user is None:
        errData = {'usernameOrEmail': [
            # view string could be placed on client-side
            "username or email doesn't exist"]}
        return jsonify(
            msg="invalid login data",
            code=err_codes.LOGIN,
            data=errData,
            # for backward compatability with client-side
            fieldErrors=errData,
            ), 400
    if not user.checkPassword(loginData['password']):
        errData = {'password': [
            # view string could be placed on client-side
            "invalid password"]}
        return jsonify(
            msg="invalid login data",
            code=err_codes.LOGIN,
            data=errData,
            # for backward compatability with client-side
            fieldErrors=errData,
            ), 400
    login_user(user, remember=True)
    return jsonify(
        next='/home'
    )


@blueprint.route('/creditPackages', methods=['GET'])
@login_required
def creditPackages():
    return jsonify(
        creditPackages=[ creditPackage.toJsonObj() for creditPackage
                         in CreditPackage.all() ]
    )


@blueprint.route('/time', methods=['GET'])
@login_required
def getServerTime():
    """
    used to calculate offset between clocks on clients and the server
    """
    return jsonify(
        now=datetime.utcnow().isoformat()
    )

@blueprint.route('/config', methods=['GET'])
@login_required
def getConfig():
    """
    used to get current configuration
    """
    return jsonify(
        Config.get().toJsonObj()
    )
