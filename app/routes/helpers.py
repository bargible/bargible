"""
misc. and sundry helper functions for the .routes package
"""
from functools import wraps

from flask import current_app
from flask import jsonify

from flask_login import current_user

from bargible.model import Config
from bargible.auc_core.auction_manager import AuctionManager

def login_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not current_user.is_authenticated:
            return jsonify(
                msg="this endpoint requires authentication"
            ), 401
        return func(*args, **kwargs)
    return decorated_view


def makeAuctionJsonObj(auction, user):
    """
    unify impl and guarantee same serialization format at
    all endpoints that use this function
    * auction (model.Auction inst)
    * user (model.User inst)
    """
    modelJsonObj = auction.toJsonObj(user=user)
    if modelJsonObj['joined'] is None:
        return modelJsonObj
    coreJsonObj = (AuctionManager(auction, getConfig())
                   .getUserJsonObj(user))
    # make certain keys aren't overwritten
    assert ((set(coreJsonObj.keys()) -
             set(modelJsonObj['joined'].keys())) ==
            set(coreJsonObj.keys()))
    # merge dicts (python 3.5+)
    modelJsonObj['joined'] = {**modelJsonObj['joined'], **coreJsonObj}
    return modelJsonObj


def getConfig():
    dbConfig = Config.get()
    dbConfigDict = {
        str(col.name).upper(): int(getattr(dbConfig, col.name))
        for col in dbConfig.__table__.columns
    }
    return dbConfigDict


def resizeAndCropImgSq(img, squareSize):
    """
    edits *and* returns image (so the input object is mutated)
    resized such that the smallest dimension is `squareSize` px,
    then cropped even amounts off the ends of the larger dimension
    such that the image is `squareSize`x`squareSize`px
    * img (wand.image.Image): uploaded image
    * squareSize (int): number of pixels
    """
    minDim = min(img.width, img.height)
    scaleFactor = (float(squareSize)/img.width
                   if img.width < img.height else
                   float(squareSize)/img.height)
    img.sample(int(scaleFactor * img.width),
             int(scaleFactor * img.height))
    if img.width > img.height:
        cropAmount = int((img.width - squareSize) / 2)
        img.crop(cropAmount, 0, width=squareSize, height=squareSize)
    elif img.width < img.height:
        cropAmount = int((img.height - squareSize) / 2)
        img.crop(0, cropAmount, width=squareSize, height=squareSize)
    return img
