"""
class-based views a-la Django
http://flask.pocoo.org/docs/0.11/views/
"""
from flask import render_template
from flask import request
from flask import flash
from flask.views import View

from sqlalchemy.exc import SQLAlchemyError

from bargible.model import saveModels

class _CreateUpdateView(View):

    def __init__(self, templateName, formCls, confirmMsg):
        self._templatePath = 'admin/' + templateName
        self._formCls = formCls
        self._confirmMsg = confirmMsg

    def getBlankForm(self):
        raise NotImplementedError("derived classes must implement")

    def getModelInst(self):
        raise NotImplementedError("derived classes must implement")

    def dispatch_request(self):
        if request.method == 'GET':
            return render_template(
                self._templatePath,
                form=self.getBlankForm(),
                )
        formInst = self._formCls(request.form)
        if not formInst.validate():
            flash("submitted invalid form data")
            return render_template(
                self._templatePath,
                form=formInst,
                )
        modelInst = self.getModelInst()
        try:
            formInst.populate_obj(modelInst)
        except ValueError as err:
            flash("invalid model data: " + str(err))
            return render_template(
                self._templatePath,
                form=self.getBlankForm(),
                )
        try:
            saveModels(modelInst)
        except SQLAlchemyError as err:
            return ("internal model layer error on save: '" +
                        str(err) + "'"), 500
        flash(self._confirmMsg)
        return render_template(
            self._templatePath,
            form=self.getBlankForm(),
            )


class UpdateView(_CreateUpdateView):

    methods = ['GET', 'POST']

    def __init__(self, templateName, formCls, getCurrInst, confirmMsg):
        super().__init__(templateName, formCls, confirmMsg)
        self._getCurrInst = getCurrInst

    def getBlankForm(self):
        return self._formCls(obj=self._getCurrInst())

    def getModelInst(self):
        return self._getCurrInst()


class CreateView(_CreateUpdateView):

    methods = ['GET', 'POST']

    def __init__(self, templateName, formCls, modelCls, confirmMsg):
        super().__init__(templateName, formCls, confirmMsg)
        self._modelCls = modelCls

    def getBlankForm(self):
        return self._formCls()

    def getModelInst(self):
        return self._modelCls()
