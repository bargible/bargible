"""
the endpoints for all the admin-related pages
"""
import datetime
import os

from flask import render_template
from flask import request
from flask import flash
from flask import redirect
from flask import url_for
from flask import current_app
from flask import jsonify

from sqlalchemy.exc import SQLAlchemyError

from wtforms import validators
from wtforms.fields import SelectField

from werkzeug.utils import secure_filename

from sqlalchemy_imageattach.context import store_context

from bargible.third_party_api import tango

from bargible.model import imageStore
from bargible.model import User
from bargible.model import Auction
from bargible.model import Config
from bargible.model import Item
from bargible.model import PhysicalItem
from bargible.model import Transaction
from bargible.model import saveModels

from .forms import AuctionForm
from .forms import ConfigForm
from .forms import PhysicalItemForm
from .pluggable_crud_views import UpdateView
from .pluggable_crud_views import CreateView

from . import blueprint

@blueprint.route('/', methods=['GET'])
def admin():
    return render_template('admin/base.html')


@blueprint.route('/users', methods=['GET'])
def users():
    return render_template(
        'admin/users.html',
        users=User.all(),
        )

blueprint.add_url_rule('/config', view_func=UpdateView.as_view(
    'config', templateName='config.html', formCls=ConfigForm,
    getCurrInst=lambda: Config.get(),
    confirmMsg="updated config",
    ))

@blueprint.route('/auctions', methods=['GET'])
def auctions():
    return render_template(
        'admin/auction_list.html',
        upcomingAuctions=Auction.getUpcomingAuctions(),
        endedAuctions=Auction.getEndedAuctions(),
        )


@blueprint.route('/transactions', methods=['GET'])
def transactions():
    return render_template(
        'admin/transactions.html',
        transactions=Transaction.all(),
        )


blueprint.add_url_rule('/auctions/create', view_func=CreateView.as_view(
    'createAuction', templateName='auctions.html', formCls=AuctionForm,
    modelCls=Auction,
    confirmMsg="created auction",
    ))

blueprint.add_url_rule(
    '/physicalItems/create', view_func=CreateView.as_view(
    'createPhysicalItem', templateName='auctions.html',
    formCls=PhysicalItemForm, modelCls=PhysicalItem,
    confirmMsg="created physical item",
    ))

@blueprint.route('/redemptions', methods=['POST'])
def redeem():
    auctionId = request.args.get('auctionId')
    auction = Auction.byId(auctionId)
    if not isinstance(auction.reward, Item):
        return jsonify(msg="auction reward type not supported",
                           auctionId=auctionId), 400
    if auction.redeemed:
        flash('auction already redeemed')
        return redirect(url_for('admin.auctions'))
    tangoResponse = tango.redeem({
        'name': auction.last_bidder.first_name,
        'email': auction.last_bidder.email,
        'sku': auction.reward.sku,
        'amount': auction.real_price,
        'external_id': str(auction.id),
    })
    if tangoResponse.ok:
        auction.redemption.redeem_datetime = (
            datetime.datetime.utcnow())
        saveModels(auction)
        flash('\n'.join(["Item sent to winner",
                             str(tangoResponse)]))
        return redirect(url_for('admin.auctions'))
    else:
        flash('\n'.join([("Item redemption failed for auction ID "
                              + str(auction.id)),
                             str(tangoResponse)]))
        return redirect(url_for('admin.auctions'))


@blueprint.route('/watchlist', methods=['GET'])
def watchlist():
    unendedAuctions = [auction for auction in Auction.all()
                           if auction.ended == False]
    unendedAuctions.sort(key=lambda a: a.start_datetime)
    return render_template(
        'admin/watch_list.html',
        auctions=unendedAuctions,
        )
