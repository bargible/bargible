"""
add Jinja template helpers the Flask-specific way.

most of the functionality described at
http://flask.pocoo.org/docs/0.11/templating/#templates
is also availble on Blueprint objects
http://flask.pocoo.org/docs/0.11/api/#blueprint-objects

note, in particular, that these blueprint-specific Jinja
additions are typically applied to the entire Flask app
when the blueprint is registered.
"""
from jinja2 import Markup
from jinja2 import evalcontextfilter
from flask import url_for

from . import blueprint

CRUD_FORM_DEFAULTS = {
    'button_msg': "Submit",
    'wrapper_class': 'crud-form',
    'field_wrapper_class': 'crud-field',
    'description_class': 'description',
    'errors_class': 'errors',
}


@blueprint.app_template_filter()
@evalcontextfilter
def crud_form(
        evalCtx,
        form, header, endpoint,
        **kwargs):
    url = url_for(endpoint)
    formTemplate = evalCtx.environment.get_template(
        'admin/crud_form.html')
    addEnvVars = {
        key: kwargs.get(key, CRUD_FORM_DEFAULTS[key])
        for key in CRUD_FORM_DEFAULTS.keys() }
    renderedFormTemplate = formTemplate.render(
        form=form,
        header=header,
        url=url,
        **addEnvVars)
    return ((Markup if evalCtx.autoescape else id)
                (renderedFormTemplate))

