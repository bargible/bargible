"""
admin functionality
this module provides a web ui to conveniently manage the application
without special tooling to, e.g., access the application's database
"""
from flask import Blueprint

blueprint = Blueprint(
    'admin', __name__,
    template_folder='templates',
    static_folder='static',
    )

from . import views
from . import template_helpers

__all__ = [
    'blueprint',
]
