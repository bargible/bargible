import datetime
import re
from functools import partial

import sqlalchemy as sa

from wtforms import Form
from wtforms.fields import StringField
from wtforms.fields import FileField

from wtforms_components import DateTimeField as BaseDateTimeField
from wtforms_components.widgets import TextInput

from wtforms_alchemy import Form
from wtforms_alchemy import model_form_factory
from wtforms_alchemy import ClassMap
from wtforms_alchemy.generator import FormGenerator as FormGeneratorBase
from wtforms_alchemy.fields import QuerySelectField

from bargible import model

class MonetaryPriceInput(TextInput):

    def __call__(self, *args, **kwargs):
        textInputHtml = super().__call__(*args, **kwargs)
        return ('<span %s>$</span>' %
                    self.html_params(
                        class_='input-prefix',
                        )
                    ) + textInputHtml


class MonetaryPriceField(StringField):
    widget = MonetaryPriceInput()

    def _value(self):
        if self.data:
            return '{price:.02f}'.format(price=float(self.data) / 100)
        else:
            return u''

    def process_formdata(self, valuelist):
        if not valuelist:
            return None
        strValue = valuelist[0]
        if not re.match(r'^\d+(\.\d\d)?$', strValue):
            self.data = None
            raise ValueError(self.gettext((
                "prices must be an integer value or "
                "a value with exactly two decimal places")))
        self.data = int(100 * float(strValue))


class FormGenerator(FormGeneratorBase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.TYPE_MAP.update(ClassMap({
            model.types.MonetaryPrice: MonetaryPriceField,
        }))


ModelForm = model_form_factory(
    Form,
    form_generator=FormGenerator,
    )

class IsoDateTimeField(BaseDateTimeField):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.format = '%Y-%m-%dT%H:%M:%S.%fZ'


class AuctionForm(ModelForm):
    class Meta:
        model = model.Auction
        only = [
            'start_datetime',
            'end_datetime',
            'join_cost',
            'real_price',
            ]
        type_map = ClassMap({
            sa.DateTime: IsoDateTimeField,
        })

    reward = QuerySelectField(
        label='Reward',
        query_factory=lambda: model.Reward.all(),
        get_label=lambda reward: reward.name,
        )


class ConfigForm(ModelForm):
    class Meta:
        model = model.Config


class PhysicalItemForm(ModelForm):
    class Meta:
        model = model.PhysicalItem

    picture = FileField('picture')
