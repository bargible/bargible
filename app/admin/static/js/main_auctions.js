/*global Flatpickr */

(function (Picker) {
  const elsDatetimeInput = [
    document.getElementById('start_datetime'),
    document.getElementById('end_datetime'),
  ];

  /**
   * Flatpickr internals choke on setting an ISO string
   * with the dateFormat option (for some reason?).
   * Also, this function converts to UTC for the hidden value
   * while displaying local time in the UI.
   */
  const setISOValue = function(selectedDates, dateStr, instance) {
    const updatedDate = selectedDates[0];
    if (selectedDates.length > 1) {
      console.warn("unexpected flatpickr api output:  " +
                   "more than one selected date for a single input");
      console.warn(selectedDates);
    }
    if (updatedDate) {
      instance.input.value = updatedDate.toISOString();
    }
  };

  const addPicker = function (el) {
    const picker = new Picker(el, {
      enableTime: true,
      altInput: true,
      onChange: setISOValue,
      onReady: setISOValue,
      altFormat: 'M j, Y h:i K'
    });
  };

  elsDatetimeInput.forEach(addPicker);
}(Flatpickr));
