import sys
import os
import datetime
import logging
import json
from wsgiref import util as wsgiUtil

import eventlet

from .custom_flask import Flask
# _app_ctx_stack is named for module privacy
# but documented for external usage
from flask import _app_ctx_stack

### Flask-SocketIO imports
from flask import request
from flask.ext.socketio import SocketIO as FlaskSocketIO
from flask_login import current_user
from flask.ext.socketio import emit
from bargible.model import saveModels
### end Flask-SocketIO imports

from .custom_flask_login import loginManager

from bargible import model

from .views import registerBlueprints
from bargible.third_party_api.braintree.util import (
    setEnvironment as setBtEnv)
from bargible.third_party_api.tango import (
    setEnvironment as setTangoEnv)

# making zero effort to use gregorian calendar libs
# for encoding daylight savings time rules ...
# ... this is PDT during daylight savings time
_TIMEZONE = datetime.timezone(datetime.timedelta(hours=-7))

def _addConfigConstants(app):
    with open(os.path.join(os.path.dirname(__file__),
        '..', 'constants.json'), 'r') as constantsFile:
        constants = json.loads(constantsFile.read())
        for constantKey in constants:
            app.config[constantKey] = constants[constantKey]


def _makeLogHandler(isFlaskAppDebugMode):
    timestampStr = (datetime.datetime.now(_TIMEZONE)
                        .strftime('%y%m%d-%H%M'))
    handler = logging.FileHandler(
        os.path.abspath(
            os.path.join(
                '/', 'var', 'log', 'bargible',
                'eventlet_server_' + timestampStr + '.log')),
        mode='w', # b/c it's timestamped.  new log for every run.
        encoding='utf-8')
    if isFlaskAppDebugMode:
        handler.setLevel(logging.DEBUG)
    else:
        handler.setLevel(logging.INFO)
    return handler


class _LoggingMiddleware(object):
    """ WSGI logging middleware """

    def __init__(self, app, logger):
        self._logger = logger
        self._app = app

    def __call__(self, environ, start_response):
        self._logger.info(
            "--" +
            "[" +
            (datetime.datetime.now(_TIMEZONE)
                 .strftime('%y/%m/%d %H:%M:%S')) +
            "]" +
            "--" +
            wsgiUtil.request_uri(environ))
        return self._app(environ, start_response)


appFlaskSocketIO = FlaskSocketIO(
    async_mode='eventlet',
)

def initFlaskApp(
        addLogger=False,
        addFlaskSocketIO=True,
        ):
    """
    addLogger (bool): whether to add a file logger to the application.
    """
    ### init flask application object
    app = Flask(__name__)
    # setup config
    app.config.from_object(
        os.getenv('BARGIBLE_CONFIG', 'config.DevelopmentConfig'))
    if app.config['PAYMENTS_ENV'] is not None:
        setBtEnv(app.config['PAYMENTS_ENV'])
    if app.config['TANGO_ENV'] is not None:
        setTangoEnv(app.config['TANGO_ENV'])
    _addConfigConstants(app)
    # logging
    if addLogger:
        app.logger.addHandler(_makeLogHandler(app.config['DEBUG']))

    @app.errorhandler(Exception)
    def logException(err):
        # the flask app default exception handling internals
        # Flask.handle_exception will sometimes handle errors,
        # including logging, according to the
        # Flask.propagate_exceptions property,
        # _not_ the DEBUG config variable as the docstrings state
        if app.propagate_exceptions:
            app.log_exception(sys.exc_info())
            raise err

    ### register database
    model.resetSession(scopefunc=_app_ctx_stack.__ident_func__)
    @app.teardown_appcontext
    def shutdownDatabase(exception=None):
        model.shutdownSession()


    app.wsgi_app = model.imageStore.wsgi_middleware(app.wsgi_app)
    app.wsgi_app = _LoggingMiddleware(app.wsgi_app, logger=app.logger)

    if addFlaskSocketIO:
        ### init Flask-SocketIO
        eventlet.monkey_patch(
            all=False,
            os=True,
            select=True,
            socket=True,
            # patching the threading module breaks
            # sqlalchemy's scoped_session
            thread=False,
            time=True,
        )
        @appFlaskSocketIO.on('connect')
        def _on_connect():
            if not current_user.is_authenticated:
                return False
            current_user.sid = request.sid
            saveModels(current_user)
            emit('connected', {'data': 'Connected'})


        @appFlaskSocketIO.on('disconnect')
        def _on_disconnect():
            if not current_user.is_authenticated:
                return False
            current_user.sid = None
            saveModels(current_user)


        appFlaskSocketIO.init_app(
            app,
            message_queue='redis://localhost:6379/11',
            channel='auc-socketio',
        )
        ### end Flask-SocketIO init

    loginManager.init_app(app)

    registerBlueprints(app)
    return app
