"""
entry point for all parts of app's http layer
"""
import json
import os
import datetime
from functools import wraps

from flask import Blueprint
from flask import render_template
from flask import redirect
from flask import url_for
from flask import request
from flask import current_app

from flask_login import current_user
from flask_login import logout_user

from bargible.third_party_api.braintree.util import (
    genClientToken as genBtClientToken)

# blueprint imports
from app.admin import blueprint as bpAdmin
from . import routes

from bargible.model import Auction
from bargible.model import saveModels

class PageViewBlueprint(Blueprint):
    pass

pageViewBlueprint = PageViewBlueprint(
    'page_views', __name__,
    template_folder='templates')

# view helpers

def isProduction():
    not (current_app.config['DEVELOPMENT']
         or current_app.config['TESTING'])


def _render_app_template(templateName):
    """
    helper function to ensure consistent usage of templates
    dervied from base_app.html.
    these are server-side templates that correspond to logged-in
    views for non-admin users
    """
    client_token = (
        genBtClientToken()
        if current_app.config['PAYMENTS_ENV'] is not None
        else None)
    navLinks = {
        'home': url_for('page_views.home'),
        'profile': url_for('page_views.profile'),
        'faq': url_for('page_views.faq'),
        'logout': url_for('page_views.logout'),
    }
    return render_template(templateName,
                           client_token=client_token,
                           navLinks=json.dumps(navLinks),
                           isProduction=isProduction())


def login_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not current_user.is_authenticated:
            return redirect(url_for('page_views.index'))
        return func(*args, **kwargs)
    return decorated_view


### views (rendered templates, redirects, and other url-bar urls)

@pageViewBlueprint.route('/', methods=['GET'])
@pageViewBlueprint.route('/index', methods=['GET'])
def index():
    if current_user.is_authenticated:
        return redirect(url_for('page_views.home'))
    return render_template('index.html',
                           navLinks=json.dumps(None),
                           isProduction=isProduction())


@pageViewBlueprint.route('/logout', methods=['GET'])
def logout():
    logout_user()
    return redirect(url_for('page_views.index'))


@pageViewBlueprint.route('/home', methods=['GET'])
@login_required
def home():
    return _render_app_template('home.html')


@pageViewBlueprint.route('/profile', methods=['GET'])
@login_required
def profile():
    return _render_app_template('user_profile.html')


@pageViewBlueprint.route('/faq', methods=['GET'])
@login_required
def faq():
    return _render_app_template('faq.html')


@pageViewBlueprint.route('/payment', methods=['GET'])
@login_required
def payment():
    return 'Option ' + request.args.get('option')


def registerBlueprints(app):
    app.register_blueprint(pageViewBlueprint, url_prefix='')
    app.register_blueprint(bpAdmin, url_prefix='/admin')
    ### routes (http api -- i.e. xhr, etc. -- endpoint urls)
    app.register_blueprint(routes.top, url_prefix='/api')
    app.register_blueprint(routes.user, url_prefix='/api/user')
    app.register_blueprint(routes.userAuction,
                           url_prefix='/api/user/auctions')
    app.register_blueprint(routes.auction, url_prefix='/api/auction')
