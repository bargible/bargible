var UserNode = React.createClass({displayName: "UserNode",
    getInitialState: function() {
        return {bidsRemaining: "300"};
    },
    bid: function() {
        var self = this;
        var xhr = new XMLHttpRequest();
        xhr.open('GET', encodeURI('api/user/bid'));
        xhr.onload = function() {
            if (xhr.status === 200 && xhr.response) {
                self.setState({bidsRemaining: xhr.response});
            }
            else {
                alert('Request failed.  Returned status of ' + xhr.status);
            }
        };
        xhr.send();
    },
    render: function() {
        return (
            React.createElement("div", null, 
                React.createElement("p", {className: "col-lg-12"}, "Bids Remaining: ", this.state.bidsRemaining), 
                React.createElement("p", {className: "col-lg-12 bids", id: "bid-button"}, React.createElement("a", {className: "btn btn-lg btn-success", onClick: this.bid, role: "button"}, "Bid"))
            )
        );
    }
});

React.render(React.createElement(UserNode, null), document.getElementById('user-details'));

// var xhr = new XMLHttpRequest();
// xhr.open('GET', encodeURI('api/auction'));
// xhr.onload = function() {
//     if (xhr.status === 200) {
//         console.log(xhr.response.auctionId);
//         self.test = JSON.parse(xhr.response);
//     }
//     else {
//         console.log('Request failed.  Returned status of ' + xhr.status);
//     }
// };
// xhr.send();