var AuctionNode = React.createClass({displayName: "AuctionNode",
    getInitialState: function() {
        return {
            minimumValue: '10.00',
            timeRemaining: '0:00',
            intermediateTimer: '0:00',
            currentPrice: '0.01'
        };
    },
    update: function() {
        var self = this;
        var xhr = new XMLHttpRequest();
        xhr.open('GET', encodeURI('api/auction/get'));
        xhr.onload = function() {
            if (xhr.status === 200 && xhr.response) {
                var auction = JSON.parse(xhr.response);
                self.setState({timeRemaining: auction.timeRemaining,
                        intermediateTimer: auction.intermediateTimer,
                        currentPrice: auction.auctionPrice});
            }
            else {
                alert('Request failed.  Returned status of ' + xhr.status);
            }
        };
        xhr.send();
    },
    componentDidMount: function() {
        this.interval = setInterval(this.update, 500);
    },
    componentWillUnmount: function() {
        clearInterval(this.interval);
    },
    render: function() {
        return (
            React.createElement("div", null, 
                React.createElement("p", null, "Minimum Value: ", this.state.minimumValue), 
                React.createElement("p", null, "Time Remaining: ", this.state.timeRemaining), ",", 
                React.createElement("p", null, "Intermediate Timer: ", this.state.intermediateTimer), ",", 
                React.createElement("p", null, "Current Price: ", this.state.currentPrice)
            )
        );
    }
});

React.render(React.createElement(AuctionNode, null), document.getElementById('auction-details'));

// var xhr = new XMLHttpRequest();
// xhr.open('GET', encodeURI('api/auction'));
// xhr.onload = function() {
//     if (xhr.status === 200) {
//         console.log(xhr.response.auctionId);
//         self.test = JSON.parse(xhr.response);
//     }
//     else {
//         console.log('Request failed.  Returned status of ' + xhr.status);
//     }
// };
// xhr.send();