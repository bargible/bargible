"""
EC2-specific functionality
"""
from warnings import warn

import datetime
from time import sleep

from .boto_util import SERVICE_DEFAULTS
from .boto_util import ServiceType
from .boto_util import makeClient

def _getNameTagValue(tags, nameKey='Name'):
    nameTagValues = [
        tag['Value'] for tag in tags
        if tag['Key'] == nameKey]
    if len(nameTagValues) == 0:
        return None
    if len(nameTagValues) > 1:
        raise AssertionError("expected at most one 'Name' tag " +
                                 "per EC2 object")
    nameTagValue = nameTagValues[0]
    return nameTagValue


def _makeInternalEc2Data(instanceData):
    """
    helper for getEc2InstancesData.  converts boto3 api
    data structure into data structure for internal api
    """
    nameTagValue = _getNameTagValue(instanceData.get('Tags', []))
    if nameTagValue is None:
        warn(("found ec2 instance without name: \n" +
              repr(instanceData) + "\n"))
        return None
    return {
        'name': nameTagValue,
        'domain': instanceData['PublicDnsName'],
        }


def getEc2InstancesData(session=None):
    """
    return (list(str)): a list of EC2 instance names
      as they appear in the AWS console
    """
    client = makeClient(ServiceType.EC2, session=session)
    instancesDataByReservation = client.describe_instances()
    # data from boto api
    instancesDataFlat = [
        instanceData for reservationData
        in instancesDataByReservation['Reservations']
        for instanceData in reservationData['Instances']
    ]
    instancesData = [
        internalInstanceData for internalInstanceData in
            [_makeInternalEc2Data(d) for d in instancesDataFlat]
            if internalInstanceData is not None
        ]
    instanceNames = set([d['name'] for d in instancesData])
    if len(instancesData) != len(instanceNames):
        warn("found duplicate ec2 instance names")
    return instancesData


def ec2DomainByName(ec2Name, session=None):
    """
    # args
    * ec2Name (str): the name of the the EC2 instance as it appears
      in the AWS console
    ----
    return (str): None if there's no EC2 container by the given name,
    an empty string if there's a stopped instance, and
    the public domain name for DNS servers otherwise
    """
    serverDomain = next(iter(
        [d['domain'] for d in getEc2InstancesData(session=session)
             if d['name'] == ec2Name]), None)
    if serverDomain is None:
        raise ValueError(
            "no EC2 instance with name",
            ec2Name)
    if serverDomain.strip() == '':
        raise ValueError(
            "no domain associated with EC2 instanced named",
            ec2Name)
    return serverDomain


def createEc2Instance(serverName, amiId, keyPairName,
                      secGrpIds=[], instanceType='t2.micro',
                      initTimeout=360, # sec
                      session=None):
    client = makeClient(ServiceType.EC2, session=session)
    res = client.run_instances(
        # launch exactly one instance
        MinCount=1,
        MaxCount=1,
        ImageId=amiId,
        KeyName=keyPairName,
        InstanceType=instanceType,
        SecurityGroupIds=secGrpIds,
        Placement={
            'AvailabilityZone': SERVICE_DEFAULTS['availability_zone'],
        },
    )
    if len(res['Instances']) != 1:
        Exception("expected creation of exactly one instance")
    newInstanceId = res['Instances'][0]['InstanceId']
    client.create_tags(
        Resources=[newInstanceId],
        Tags=[{'Key': 'Name', 'Value': serverName}])
    # poll until initialized with Exception on timeout
    sleep(initTimeout/2) # assuming timeout approximates startup time
    createTime = datetime.datetime.now()
    instanceStatus = None
    while instanceStatus is None or instanceStatus == 'initializing':
        instanceStatusesRes = client.describe_instance_status(
            InstanceIds=[newInstanceId])['InstanceStatuses']
        if len(instanceStatusesRes) > 1:
            raise Exception("expected exactly one instance status")
        if len(instanceStatusesRes) == 0:
            instanceStatus = None
            continue
        instanceStatusRes = instanceStatusesRes[0]
        instanceStatus = instanceStatusRes['InstanceStatus']['Status']
        if ((datetime.datetime.now() - createTime)
            .total_seconds()) >= initTimeout:
            raise Exception("initialization timed out")
        sleep(SERVICE_DEFAULTS['poll_interval'])
    if instanceStatus != 'ok':
        raise Exception("initialziation failed.  status: " +
                        repr(instanceStatus))
    return newInstanceId


def _makeInternalSecGrpData(apiData):
    return {
        'name': apiData['GroupName'],
        'vpc_id': apiData['VpcId'],
        'group_id': apiData['GroupId'],
    }


def getSecurityGroupsData(session=None):
    client = makeClient(ServiceType.EC2, session=session)
    res = client.describe_security_groups()
    secGrpsData = [
        secGrpData for secGrpData in
        [_makeInternalSecGrpData(d) for d in res['SecurityGroups']]
        if secGrpData is not None]
    names = set([d['name'] for d in secGrpsData])
    if len(secGrpsData) != len(names):
        warn("found duplicate security group names")
    return secGrpsData
