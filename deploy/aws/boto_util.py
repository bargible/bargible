"""
common boto functionality for all AWS products
"""
import os
import enum
import configparser

from boto3 import Session

AWS_DEFAULT_REGION = 'us-west-2'

## restriction of AWS cli standard credentials file
CREDS_FILE = os.path.join(os.path.dirname(__file__), 'aws_creds.conf')
CREDS = configparser.ConfigParser()
CREDS.read(CREDS_FILE)

# defaults that span multiple AWS products
SERVICE_DEFAULTS = {
    'availability_zone': 'us-west-2c',
    # to prevent request limit
    'poll_interval': 20, # sec
}

class ServiceType(enum.Enum):
    """
    the AWS product line offers several services.
    this is not a complete list.  rather, it's a list used by
    this specific application's deploy scripts.  with values that
    may be edited to account for different underlying libraries
    used to communicate with AWS.
    """
    # the Elastic Compute product is a VPS offering
    EC2 = 'ec2'
    # Relational Database Services
    RDS = 'rds'


def _makeSession(regionName=None):
    if regionName is None:
        regionName = AWS_DEFAULT_REGION
    return Session(**dict(CREDS['default']),
                       region_name=regionName)


def makeClient(serviceType, session=None):
    if session is None:
        session = _makeSession()
    if serviceType.value not in session.get_available_services():
        raise ValueError("unsupported service type: " +
                         str(serviceType))
    if session.region_name is None:
        raise ValueError("session's region name not set")
    return session.client(serviceType.value)
