"""
RDS-specific functionality
"""
from warnings import warn

import datetime
from time import sleep

from .boto_util import SERVICE_DEFAULTS
from .boto_util import ServiceType
from .boto_util import makeClient

def _makeInternalRDSData(instanceData):
    return {
        'name': instanceData['DBInstanceIdentifier'],
        'domain': instanceData['Endpoint']['Address'],
        'status': instanceData['DBInstanceStatus'],
    }


def getRDSInstancesData(session=None):
    client = makeClient(ServiceType.RDS, session=session)
    resDescribe = client.describe_db_instances()
    instancesData = [_makeInternalRDSData(instanceData)
                         for instanceData in resDescribe['DBInstances']]
    instanceNames = set([d['name'] for d in instancesData])
    if len(instancesData) != len(instanceNames):
        warn("found duplicate ec2 instance names")
    return instancesData

def createRDSInstance(rdsName, dbUsername, dbPassword,
                          vpcSecGrpIds=[],
                          initTimeout=360, # sec
                          session=None):
    if len(dbPassword) < 8 or len(dbPassword) > 128:
        ValueError("invalid length for database password",
                       len(dbPassword))
    if ('/' in dbPassword or
            '@' in dbPassword or
            '"' in dbPassword):
        raise ValueError((
            "database password may not contain "
            "'/', '@', or '\"'"))
    client = makeClient(ServiceType.RDS, session=session)
    res = client.create_db_instance(
        DBName='bargible',
        DBInstanceIdentifier=rdsName,
        AllocatedStorage=5, # GB
        DBInstanceClass='db.t2.micro',
        Engine='postgres',
        MasterUsername=dbUsername,
        MasterUserPassword=dbPassword,
        VpcSecurityGroupIds=vpcSecGrpIds,
        AvailabilityZone=SERVICE_DEFAULTS['availability_zone'],
        MultiAZ=False,
        EngineVersion='9.5.4',
        AutoMinorVersionUpgrade=False,
        StorageType='gp2',
    )
    newInstanceIdent = res['DBInstance']['DBInstanceIdentifier']
    # poll until initialized with Exception on timeout
    sleep(initTimeout/2) # assuming timeout approximates startup time
    createTime = datetime.datetime.now()
    instanceStatus = None
    while instanceStatus is None or instanceStatus != 'available':
        resDescribe = client.describe_db_instances(
            DBInstanceIdentifier=newInstanceIdent)
        assert len(resDescribe['DBInstances']) == 1
        instanceStatus = (
            resDescribe['DBInstances'][0]['DBInstanceStatus'])
        if ((datetime.datetime.now() - createTime)
            .total_seconds()) >= initTimeout:
            raise Exception("initialization timed out")
        sleep(SERVICE_DEFAULTS['poll_interval'])
