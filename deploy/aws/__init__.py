"""
Amazon Web Services-specific functionality for deploy
"""

from .ec2 import getEc2InstancesData
from .ec2 import ec2DomainByName
from .ec2 import createEc2Instance
from .ec2 import getSecurityGroupsData
from .rds import getRDSInstancesData
from .rds import createRDSInstance

__all__ = [
    'getEc2InstancesData',
    'ec2DomainByName',
    'createEc2Instance',
    'getSecurityGroupsData',
    'getRDSInstancesData',
    'createRDSInstance',
]
