"""
scripted deploy functionality
also see the notes in the deploy/ directory
"""
import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from . import aws
from . import commands_remote

############### AWS semantic versioning ###
# some module-level constants to map the semantic meaning
# of these scripts to AWS objects in the Bargible account
#
# add the tag "'Scripts': 'True'" to any objects used by
# these scripts to remind admins not to delete them via the
# AWS console

## todo: lookup ids (security group and machine images)
#        using ec2 client

SECURITY_GROUPS = {
    # Group name testdeploy
    'test': 'vpc-43a6ff26',
    # Group name deploy_00
    'deploy': 'sg-24c77b5c',
}

DB_VPC_SECURITY_GROUP_NAME = 'rds_access'

AMAZON_MACHINE_IMAGES = {
    # AMI Name debian_170123_01
    'debian-stable': 'ami-47803e27',
}

EC2_INSTANCE_TYPES = {
    'XS': 't2.micro',
    'S': 't2.small',
    'M': 't2.medium',
    'L': 't2.large',
    'XL': 't2.xlarge',
}

def getDatabasesList():
    return aws.getRDSInstancesData()


def getServersList():
    return aws.getEc2InstancesData()


def createServer(names,
                 machineImage,
                 serverType,
                 securityGroup='deploy'):
    serverName, keyPairName = [names[key] for key in [
        'ec2InstanceName',
        'ec2KeyPairName',
        ]]
    if serverName in [ serverInfo['name'] for serverInfo
                       in getServersList() ]:
        raise ValueError("server with name '" + serverName +
                         "' already exists")
    aws.createEc2Instance(
        serverName,
        AMAZON_MACHINE_IMAGES.get(machineImage, machineImage),
        keyPairName,
        secGrpIds=[
            SECURITY_GROUPS.get(securityGroup, securityGroup),
        ],
        instanceType=EC2_INSTANCE_TYPES.get(serverType, serverType),
    )


def createDb(dbName, dbUsername, dbPass):
    vpcSecGrpData = next(
        iter([d for d in aws.getSecurityGroupsData()
                  if d['name'] == DB_VPC_SECURITY_GROUP_NAME]), None)
    if vpcSecGrpData is None:
        raise Exception((
            "couldn't find a security group named " +
            repr(DB_VPC_SECURITY_GROUP_NAME) +
            " for database access.  " +
            "check the 'Security Groups' list under the " +
            "'Network and Security' heading in the AWS console " +
            "for the EC2 service"))
    aws.createRDSInstance(
        dbName, dbUsername, dbPass,
        vpcSecGrpIds=[vpcSecGrpData['group_id']],
        )


def addUser(serverName, usernameNew, keyFiles):
    """
    keyFiles (dict(ident, pubKey)): file paths
    """
    serverDomain = aws.ec2DomainByName(serverName)
    if not os.path.isfile(keyFiles['ident']):
        raise Exception("couldn't find private key file at: " +
                        keyFiles['ident'])
    with open(keyFiles['pubKey']) as f:
        pubKeyLines = f.readlines()
        if len(pubKeyLines) != 1:
            raise Exception("expected public key file '"+
                                keyFiles['pubKey'] +
                                "' to have exactly one line")
        pubKey = pubKeyLines[0].rstrip('\n')
    commands_remote.addUser(
        serverDomain,
        usernameNew,
        {'keyFilename': keyFiles['ident'], 'pubKey': pubKey},
    )


def sysInit(serverName, username, sysConfig):
    commands_remote.sysInit(
        aws.ec2DomainByName(serverName),
        username,
        sysConfig,
        )


def userInit(serverName, username, dbParams):
    dbName = dbParams.get('name', None)
    if dbName is None:
        dbUrl = None
    else:
        dbUrl = next(
            iter([d['domain'] for d in aws.getRDSInstancesData()
                          if d['name'] == dbName]),
            None)
    if dbUrl is None:
        dbUrl = dbParams['url']
    elif ('url' in dbParams.keys() and dbUrl != dbParams['url']):
        ValueError("database name/url mismatch",
                       (dbName, dbUrl, dbParams['url']))
    fullDbUri = ('postgresql://{user}:{password}@{url}:5432/bargible'
                 .format(
                     user=dbParams['user'],
                     password=dbParams['pass'],
                     url=dbUrl))
    # raises sqlalchemy.exc.OperationalError on bad pass, etc.
    (sessionmaker(create_engine(fullDbUri))()).connection()
    commands_remote.userInit(
        aws.ec2DomainByName(serverName),
        username,
        fullDbUri,
        )


def deployApp(loginInfo, vcInfo, credPaths,
                  flaskConfig='ProductionConfig'):
    for name, path in credPaths.items():
        if not os.path.isfile(path):
            raise ValueError(
                "couldn't find " + name + " creds file at '" +
                credPaths[name] + "'")
    if not os.path.isdir(
            os.path.join(vcInfo['path'], '.git')):
        raise ValueError(
            "couldn't find .git dir at " + vcInfo['path'])
    serverName = loginInfo.pop('serverName')
    vcInfo['remoteName'] = serverName
    loginInfo['serverDomain'] = aws.ec2DomainByName(serverName)
    commands_remote.deployApp(loginInfo, vcInfo, credPaths,
                                  flaskConfig=flaskConfig)


def takeSnapshot(srcInfo, destInfo, timeouts):
    destInfo['domain'] = aws.ec2DomainByName(destInfo.pop('server_name'))
    srcInfo['domain'] = aws.ec2DomainByName(srcInfo.pop('server_name'))
    commands_remote.takeSnapshot(srcInfo, destInfo, timeouts)


__all__ = [
    'getServersList',
    'createServer',
    'addUser',
    'sysInit',
    'deployApp',
]
