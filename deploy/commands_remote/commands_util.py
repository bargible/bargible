"""
here, 'commands' refers to shell commands,
and the general purpose of the module is to utilities
for running the _shell_ commands which are used as part of
_runDeploy.py_ commands.

parameters and variables called "command"
(or "commands" in the case of an iterable)
can be thought of sort of like a typeclass

http://learnyouahaskell.com/types-and-typeclasses#typeclasses-101

even though these scripts take take advantage of Python's
loosely-typed data model.  the expected types for such parameters are
* `str`: the shell command to be executed
* `dict(cmd=, timeout=)`
  - `cmd` (`str`): the shell command to be executed
  - `timeout` (`int`|`float`): a timeout for the command in seconds
"""
import re
from functools import partial

from .paramiko_util import popLoginKwargs
from .paramiko_util import runBySend
from .paramiko_util import runBySpur
from .paramiko_util import runByExec

# todo: escape commands

def commandsFromScript(filename):
    with open(filename, 'r') as f:
        script = f.read()
    lines = script.replace('\\\n', '').split('\n')
    commands = [re.sub(r'\s+', ' ', line) for line in lines
                    if (len(line.strip()) != 0 and
                            line.strip()[0] != '#') ]
    return commands


def _commandToTuple(defaultTimeout, cmdTimeouts, command):
    """
    helper for _runCommands.  unpacks `command` "typeclass"
    into module-internal tuple representation.
    """
    if type(command) is dict:
        return (command['cmd'],
                    command.get('timeout', defaultTimeout),
                    command)
    assert type(command) is str
    cmd = command
    cmdName = cmd.split(' ')[0]
    if cmdName in cmdTimeouts.keys():
        return cmd, cmdTimeouts[cmdName], command
    return cmd, defaultTimeout, {'cmd': command}


def _runSeqBySend(loginTuple, commandTuples):
    serverDomain, username, loginKwargs = loginTuple
    chan = None
    for cmd, timeout, _ in commandTuples:
        stdErrNOut, errCode, chan = runBySend(
            cmd, serverDomain, username,
            chan=chan,
            timeout=timeout,
            **loginKwargs
            )
        if errCode != 0:
            raise Exception('\n'.join([
                "command", cmd, "failed:", repr(stdErrNOut)]))


def _runSeqBySpur(loginTuple, commandTuples):
    serverDomain, username, loginKwargs = loginTuple
    for cmd, timeout, _ in commandTuples:
        # cmd = '; '.join([
        #     '. ~/.bashrc',
        #     cmd,
        #     ])
        stdErrNOut, errCode, loginKwargs = runBySpur(
            cmd, serverDomain, username,
            **loginKwargs
            )
        if errCode != 0:
            raise Exception('\n'.join([
                "command", cmd, "failed:", repr(stdErrNOut)]))


def _runSeqByExec(loginTuple, commandTuples):
    serverDomain, username, loginKwargs = loginTuple
    for cmd, timeout, command in commandTuples:
        if type(command) is dict and 'cwd' in command:
            cmd = 'cd ' + command['cwd'] + '; ' + cmd
        stdErrNOut, errCode, loginKwargs = runByExec(
            cmd, serverDomain, username,
            timeout=timeout,
            **loginKwargs
            )
        if errCode != 0:
            raise Exception('\n'.join([
                "command", cmd, "failed:", repr(stdErrNOut),
                ' '.join(["error code", str(errCode)]),
                ]))


def runCommands(commands, serverDomain, username,
                     defaultTimeout=1,
                     cmdTimeouts={
                         'apt-get': 3
                         },
                     asSu=False,
                     cmdStrategy='send',
                     **kwargs
                     ):
    """
    * `defaultTimeout` (int|float):  timeout for commands specified as
       strings, unless otherwise specified
    * `cmdTimeouts` (dict(name1=timeout1, name2=timeout2, ...)):
       timeouts for specific commands by program name.
       does not override per-command timeouts
    * `asSu` (bool): whether to run these commands with superuser
      permissions
    """
    loginKwargs = popLoginKwargs(kwargs)
    myCommandToTuple = partial(
        _commandToTuple, defaultTimeout, cmdTimeouts)
    commandTuples = [
        (('sudo ' if asSu else '') + cmd, timeout, command)
        for cmd, timeout, command
        in [ myCommandToTuple(c) for c in commands ]
        ]
    loginTuple = (serverDomain, username, loginKwargs)
    if cmdStrategy == 'send':
        _runSeqBySend(loginTuple, commandTuples)
    elif cmdStrategy == 'spur':
        _runSeqBySpur(loginTuple, commandTuples)
    elif cmdStrategy == 'exec':
        _runSeqByExec(loginTuple, commandTuples)
    else:
        raise ValueError("unknown command strategy", cmdStrategy)
    return loginKwargs
