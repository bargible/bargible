"""
sometimes it's useful to run shell commands on the remote in a way
that doesn't essentially amount to running a sequence of commands
in order and making certain that each command is successful
(as with `shell_command_defs`).

this module encapsulates such use cases.
"""
import os
import configparser
import functools

from .paramiko_util import popLoginKwargs
from .paramiko_util import runByExec as defaultRunByExec
from .commands_util import runCommands
from .command_builders import appendToFile

_DEFAULTS_CONF = configparser.ConfigParser()
_DEFAULTS_CONF.read(
    os.path.join(os.path.dirname(__file__), 'defaults.conf')
    )

VARNAMES = _DEFAULTS_CONF['varnames']

runByExec = functools.partial(defaultRunByExec,
                                  decodeBytes=True,
                                  )

def _getDatabaseUrl(domainName, username, loginKwargs):
    (stdOut, stdErr), errCode, loginKwargs = runByExec(
        '; '.join([
            # return non-zero exit code if grep fails
            'set -o pipefail',
            '|'.join([
                'cat ~/.bashrc',
                'grep DATABASE_URL',
                'grep \'export.*=\'',
                'sed \'s/export.*=//\''
            ]),
        ]),
        domainName, username,
        timeout=3,
        **loginKwargs)
    if errCode != 0:
        raise RuntimeError(
            "couldn't find database URL in deploy config",
            ((stdOut, stdErr), errCode))
    return stdOut.rstrip(), loginKwargs


def getServerInfo(domainName, username, **kwargs):
    loginKwargs = popLoginKwargs(kwargs)
    (stdOut, stdErr), errCode, loginKwargs = runByExec(
        'cat ~/.ssh/id_rsa.pub',
        domainName, username,
        timeout=3,
        **loginKwargs)
    ssh_pubkey = (stdOut.rstrip() if errCode == 0 else None)
    (stdOut, stdErr), errCode, loginKwargs = runByExec(
        'cat ~/.ssh/authorized_keys',
        domainName, username,
        timeout=3,
        **loginKwargs)
    authorized_keys = (
        stdOut.rstrip().split('\n') if errCode == 0 else [])
    return {
       'ssh_pubkey': ssh_pubkey,
       'authorized_keys': authorized_keys,
    }, loginKwargs


def getDeployInfo(domainName, username, **kwargs):
    loginKwargs = popLoginKwargs(kwargs)
    dbUrl, loginKwargs = _getDatabaseUrl(
        domainName, username, loginKwargs)
    serverInfo, loginKwargs = getServerInfo(
        domainName, username, **loginKwargs)
    serverInfo.update({
        'db_url': dbUrl,
    })
    return serverInfo, loginKwargs


def genPubKey(domainName, username, **kwargs):
    loginKwargs = popLoginKwargs(kwargs)
    (stdOut, stdErr), errCode, loginKwargs = runByExec(
        'ssh-keygen -f ~/.ssh/id_rsa -P ""',
        domainName, username,
        timeout=3,
        **loginKwargs
        )
    if errCode != 0:
        raise RuntimeError(
            "genPubKey failed", ((stdOut, stdErr), errCode))
    return loginKwargs


def addPubKey(domainName, username, pubKey, **kwargs):
    loginKwargs = popLoginKwargs(kwargs)
    loginKwargs = runCommands(
        appendToFile(pubKey, filePath='~/.ssh/authorized_keys'),
        domainName,
        username,
        defaultTimeout=2,
        cmdStrategy='exec',
        **loginKwargs)
    return loginKwargs
