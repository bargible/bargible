"""
this module defines sequences of _shell_ commands used to
compose _deploy script_ commands.  since many deploy script commands
consist of sequences of shell commands, these are, in effect, the
definitions of those deploy script commands.
"""
import os
import json
import configparser
import datetime

from .commands_util import commandsFromScript
from .command_builders import appendToFile
from .command_builders import regexpReplace
from .command_builders import runInTmux

# todo: use dicts to codify `&> /dev/null`-like behavior

PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..', '..')
)

_DEFAULTS_CONF = configparser.ConfigParser()
_DEFAULTS_CONF.read(
    os.path.join(os.path.dirname(__file__), 'defaults.conf')
    )


FILENAMES = _DEFAULTS_CONF['filenames']
VERSIONS = _DEFAULTS_CONF['versions']
VARNAMES = _DEFAULTS_CONF['varnames']

def addUser(username, pubKey, sudo=True):
    pathSshDir = ('/home/{username}/.ssh'.format(username=username))
    return ([
        # --gecos "" disables Full Name, etc. prompts
        ('adduser {username} --disabled-password --gecos ""'
             .format(username=username)),
        'mkdir -p {ssh_dir}'.format(ssh_dir=pathSshDir),
        'chmod 777 {ssh_dir}'.format(ssh_dir=pathSshDir),
        ('echo "{public_key}" > {ssh_dir}/authorized_keys'
             .format(public_key=pubKey,
                         ssh_dir=pathSshDir)),
        ('chmod 600 {ssh_dir}/authorized_keys'
             .format(ssh_dir=pathSshDir)),
        ('chown {username}:{username} {ssh_dir}/authorized_keys'
             .format(username=username,
                         ssh_dir=pathSshDir)),
        'chmod 700 {ssh_dir}'.format(ssh_dir=pathSshDir),
        ('chown {username}:{username} {ssh_dir}'
             .format(username=username,
                         ssh_dir=pathSshDir)),
    ]) + ([] if not sudo else ([
        'adduser {username} sudo' .format(username=username),
        # gives ALL users in sudo group passwordless access
        'cp /etc/sudoers /etc/sudoers_original',
    ] + regexpReplace(
        '/etc/sudoers',
        '%sudo\s\+ALL=(ALL:ALL)\s\+ALL',
        '%sudo ALL=(ALL:ALL) NOPASSWD:ALL',
        )))


def sysInit(usernameAucAdmin, passwordAucAdmin):
    return (commandsFromScript(
        os.path.join(PROJ_ROOT, 'bin', 'runSysInit.sh')
    ) + regexpReplace(
        '/etc/ssh/sshd_config',
        'PasswordAuthentication no',
        'PasswordAuthentication yes',
    ) + [
        'service sshd restart',
        '|'.join([
            ('echo -e "' +
                 passwordAucAdmin + '\n' +
            passwordAucAdmin + '\n"'),
            'sudo adduser ' + usernameAucAdmin + ' --gecos ""']),
    ])


def userInit(dbUrl):
    return (
    # application env vars
    appendToFile("# auction environment vars") +
    appendToFile(
        ('export ' + VARNAMES['db_url_env'] + '={url}')
        .format(url=dbUrl)) +
    appendToFile((
        'export '+ VARNAMES['flask_config_env'] +
        '=config.ProductionConfig')
    ) + [
    # pyenv setup
    {
    'cmd': ' '.join([
        'git clone https://github.com/yyuu/pyenv.git',
        '~/.pyenv',
        '1>/dev/null',
        ]),
    'timeout': 45,
    },
    'cd ~/.pyenv; git checkout ' + VERSIONS['pyenv'] + '; cd',
    ] + (
    appendToFile(
        "# pyenv environment variables and initialization") +
    appendToFile('export PYENV_ROOT="$HOME/.pyenv"') +
    appendToFile('export PATH="$PYENV_ROOT/bin:$PATH"') +
    appendToFile('eval "$(pyenv init -)"')
    ) + [
    {
    'cmd': ' '.join([
        'git clone https://github.com/yyuu/pyenv-virtualenv.git',
        '~/.pyenv/plugins/pyenv-virtualenv',
        '1>/dev/null',
        ]),
    'timeout': 6,
    },
    # nvm setup
    {
    'cmd': ' '.join([
        'git clone https://github.com/creationix/nvm.git',
        '~/.nvm',
        '1>/dev/null',
    ]),
    'timeout': 6,
    },
    'cd ~/.nvm; git checkout ' + VERSIONS['nvm'] + '; cd',
    ] + (
    appendToFile(
        "# nvm enviornment variables and initialization") +
    appendToFile('export NVM_DIR="$HOME/.nvm"') +
    appendToFile(
        '[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"')
    ) + [
    '. ~/.bashrc',
    # install runtimes
    dict(cmd='pyenv install ' + VERSIONS['python'], timeout=180),
    dict(cmd='nvm install '+ VERSIONS['node'], timeout=5),
    dict(cmd=' '.join(['pyenv', 'virtualenv', VERSIONS['python'],
                           VARNAMES['pyenv']]),
            timeout=5),
    'pyenv activate ' + VARNAMES['pyenv'],
    dict(cmd='pip install --upgrade pip', timeout=5),
    'nvm use ' + VERSIONS['node'],
    ])


# todo: runByExec uses -i (interactive) flag to load .bashrc...
#   a more sensible setup might be to use a non-interactive shell
#   (as with runBySpur) and add pyenv/nvm customizations
# https://github.com/pyenv/pyenv/wiki/Deploying-with-pyenv
def _runAppEnv(deployRepoPath, command):
    if type(command) is str:
        command = dict(cmd=command)
    cmdEnvSetup = ' && '.join([
        '; '.join([
            'eval "$(pyenv init -)"',
            'pyenv activate bargible-auction-00',
        ]),
        'nvm use v7.2.1',
    ])
    if 'cwd' not in command:
        command['cwd'] = deployRepoPath
    command['cmd'] = '; '.join([
        cmdEnvSetup, command['cmd']])
    return command


def runApp(branchName, deployDatetime,
               flaskConfig='ProductionConfig',
               resetDb=False,
               ):
    deployRepoPath = '~/auction-' + deployDatetime.strftime('%y%m%d%H%M')
    return ([
        'sudo service redis-server status | grep "Active: active"',
        ' '.join(['git clone',
                      FILENAMES['REMOTE_REPO'],
                      deployRepoPath]),
    ] + regexpReplace(
        '~/.bashrc',
        (VARNAMES['flask_config_env'] + '=.*'),
        (VARNAMES['flask_config_env'] + '=config.' + flaskConfig)
    ) + [
        _runAppEnv(deployRepoPath, command) for command
        in ([
            'git checkout ' + branchName,
            ' '.join([
                'mv',
                '~/' + FILENAMES['TANGO_CREDS'],
                'server/bargible/third_party_api/tango/creds.conf',
                ]),
            ' '.join([
                'mv',
                '~/' + FILENAMES['BRAINTREE_CREDS'],
                'server/bargible/third_party_api/braintree/creds.conf',
                ]),
            {
                'cmd': './bin/updateInstall.sh -q 1',
                'timeout': 180,
            },
            {
                'cmd': 'python bin/makeNginxConf.py ~/nginx_config',
                'timeout': 3,
            },
            'sudo mv /etc/nginx/sites-enabled/default ~/nginx_bkup',
            'sudo mv ~/nginx_config /etc/nginx/sites-enabled/default',
            'sudo service nginx restart',
            {
                'cmd': 'node bin/build_client.js 1>/dev/null',
                'timeout': 20,
            },
        ] + ([
            'python bin/reset_db.py',
            ' '.join(['python bin/gentestdata.py',
                          '--items 0',
                          '--credit-packages 0',
                          '--config']),
        ] if resetDb else []) + runInTmux(
            'auction-updater',
            'python bin/auction_updater.py --sync --verbose'
        ) + runInTmux(
            'auction-webserver',
            'python application.py'
        ))
    ])


def takeSnapshot(srcInfo, destPathRoot, timeouts):
    bkupDatetime = datetime.datetime.now()
    destDirPath = (destPathRoot + (
        '' if destPathRoot[-1] == '/' else '/'
        ) + 'snapshot-' + bkupDatetime.strftime('%y%m%d%H%M') + '/')
    return ([
        ' '.join(['mkdir', '-p', destDirPath]),
    ] + appendToFile(
        json.dumps({
            'version': 1,
        }, sort_keys=True, indent=4),
        filePath=(destDirPath + 'meta.json'),
    ) + [
        {
            'cmd': ' '.join([
                'scp',
                '-o StrictHostKeyChecking=no',
                '-r',
                ('{user}@{domain}:/var/local/bargible'
                     .format(**srcInfo)),
                destDirPath + 'fs_cp']),
            'timeout': timeouts['files'],
        },
        {
            'cmd': '>'.join([
                ' '.join([
                    'pg_dump', '-Ft',
                    ('\'' + srcInfo['deployInfo']['db_url'] + '\'')]),
                destDirPath + 'db_dump.tar']),
            'timeout': timeouts['db'],
        },
    ])
