"""
frequently, it's useful to define a single, reusable
operation in terms of one or several shell commands.

for a consistent interface, every (public) function in this
module returns a _list_ of "command"s as described in
`commands_util`.
"""

def regexpReplace(filePath, regexp, replacement):
    tmpFilePath = '~/__tmp_deploy_regexp_replace'
    return [
        '>'.join([
            '|'.join([
                ' '.join(['cat', filePath]),
                ('sed "s/'
                     + '/'.join([regexp, replacement]) + '/"'),
                ]),
                tmpFilePath,
            ]),
        ' '.join(['cp', tmpFilePath, filePath]),
        ' '.join(['rm', tmpFilePath]),
    ]


# http://stackoverflow.com/questions/1250079/how-to-escape-single-quotes-within-single-quoted-strings
def appendToFile(text, filePath='~/.bashrc'):
    return [
        ' >> '.join([
            ' '.join([
                'echo',
                ''.join([
                    '\'',
                    text.replace('\'', '"\'"'),
                    '\''])]),
            filePath
        ]),
    ]


def runInTmux(sessName, cmd):
    return [
        'tmux new-session -d -s ' + sessName,
        ' '.join([
            'tmux send-keys',
            '-t ' + sessName,
            '"'+ cmd + '"',
            'Enter']),
    ]
