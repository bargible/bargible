from warnings import warn


from .ssh_client import popLoginKwargs
from .ssh_client import getClient


def _readStdOutNErr(chan, decodeBytes):
    fStdout, fStderr = chan.makefile(), chan.makefile_stderr()
    bytesOut, bytesErr = fStdout.read(), fStderr.read()
    if not decodeBytes:
        return bytesOut, bytesErr
    return bytesOut.decode('utf-8'), bytesErr.decode('utf-8')


def runByExec(cmd, serverDomain, username,
                  decodeBytes=False,
                  timeout=None,
                  **kwargs):
    loginKwargs = popLoginKwargs(kwargs)
    client, loginKwargs = getClient(serverDomain, username,
                                        **loginKwargs)
    chan = client.get_transport().open_session()
    chan.exec_command(' '.join([
        'bash', '-i', '-c',
        # first \ is Python escape, so \' is the string sent to
        # the shell.  also, the shell concatenates strings, so e.g.
        # `echo 'asdf'` produces stdout the same as `echo 'as''df'`
        "'" + cmd.replace("'", "'\\''") + "'"]))
    returnCode = chan.recv_exit_status()
    return (_readStdOutNErr(chan, decodeBytes),
                returnCode,
                loginKwargs)
