"""
adds for paramiko
"""

from .ssh_client import popLoginKwargs
from .run_by_send import runBySend
from .run_by_spur import runBySpur
from .run_by_exec import runByExec
from .scp_file import scpFile

__all__ = [
    'popLoginKwargs',
    'runBySend',
    'runBySpur',
    'runByExec',
    'scpFile',
    ]
