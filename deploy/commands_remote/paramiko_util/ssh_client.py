"""
utils for paramiko SSHClient instances
"""
from getpass import getpass

from paramiko import SSHClient
from paramiko import PasswordRequiredException
from paramiko.client import WarningPolicy

def _getClientHelper(serverDomain, username,
                        keyFilename=None, pkeyPass=None):
    client = SSHClient()
    client.set_missing_host_key_policy(WarningPolicy())
    client.load_system_host_keys()
    client.connect(serverDomain,
                       username=username,
                       key_filename=keyFilename,
                       password=pkeyPass)
    return client, dict(
        keyFilename=keyFilename,
        pkeyPass=pkeyPass,
        )


def getClient(*args, **kwargs):
    try:
        return _getClientHelper(*args, **kwargs)
    except PasswordRequiredException as err:
        if (err.args[0] == 'Private key file is encrypted' and
            kwargs.get('pkeyPass', None) is None):
            pkeyPass = getpass("private key password: ")
            return _getClientHelper(
                *args, **dict(kwargs, pkeyPass=pkeyPass))
        else:
            raise err


def popLoginKwargs(someKwArgsDict):
    return {k: someKwArgsDict.pop(k, None) for k in [
        'pkeyPass',
        'keyFilename'
        ]}
