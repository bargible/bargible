"""
functionality for running remote shell commands via interactive
paramiko shell
channels (`chan`) here are not RFC 4245 SSH channels.
rather, they're emulating VT-100 terminals.
"""
import re
from warnings import warn

from selectors import DefaultSelector
from selectors import EVENT_READ

import paramiko

from .ssh_client import popLoginKwargs
from .ssh_client import getClient

_BYTES_MAX_RECV = 1024

_BEGIN_ANSI_CTRL = u'\x1b'


# http://www.termsys.demon.co.uk/vtansi.htm
_CURSOR_MOVE_CODES = [
    '\[\d*' + c for c in [
        'A', # up
        'B', # down
        'C', # forward
        'D', # backward
        ]
    ]

_ERASE_TEXT_CODES = [
    '\[K',
]

# todo: more general form: [{attr1};...;{attrn}m
_DISPLAY_ATTRIBUTE_CODE = '\[\d\d?m'

def _chanRecv2Str(binRes):
    return (binRes.decode('utf-8')
                .replace(' \r', ''))


def _rmCtrlCodes(termStr):
    res = termStr
    for ansiCode in (_CURSOR_MOVE_CODES +
                         _ERASE_TEXT_CODES +
                         [_DISPLAY_ATTRIBUTE_CODE]):
        res = re.sub(_BEGIN_ANSI_CTRL + ansiCode, '', res)
    if _BEGIN_ANSI_CTRL in res:
        warn("unhandled ansi control code in " + repr(res))
    return res


class _ChanBuffer(object):

    def __init__(self):
        self._data = None

    def readFromChan(self, chan):
        res = _chanRecv2Str(chan.recv(_BYTES_MAX_RECV))
        if self._data is None:
            self._data = res
        else:
            self._data += res
        self._data = _rmCtrlCodes(self._data)

    @property
    def data(self):
        return self._data


# todo: cleanup timeout pathway


def _bufferedRead(chan, timeout=None):
    """ `timeout` (int): seconds.  defaults to 2. """
    if timeout is None:
        timeout = 2
    sel = DefaultSelector()
    buffer = _ChanBuffer()
    sel.register(chan, EVENT_READ, data=buffer)
    events = None
    # chan may indicate the end of a transmission with
    # a special control sequence.  see _ChanBuffer.
    sawEndSequence = False
    while ((events is None or len(events) != 0) and
               not sawEndSequence):
        events = sel.select(timeout=timeout)
        for selectorKey, eventType in events:
            if selectorKey.data is buffer:
                assert selectorKey.fileobj is chan
                if buffer.readFromChan(chan):
                    sawEndSequence = True
            else:
                warn("unexpected event")
    sel.unregister(chan)
    return buffer.data


def _openChan(*args, **kwargs):
    client, loginKwargs = getClient(*args, **kwargs)
    chan = client.invoke_shell()
    _bufferedRead(chan) # clear the welcome message
    return chan, loginKwargs


def _readCmdRes(chan, command,
                    prompt=r'\$$', timeout=None):
    res = _bufferedRead(chan, timeout=timeout)
    if res is not None:
        lines = [line.strip() for line in res.split('\n')]
    else:
        raise Exception("expected command '" + command.strip() +
                        "'" + "to be echoed, but got no data on read")
    if lines[0].strip() != command.strip():
        raise Exception("expected command '" + command.strip() +
                        "'" + "to be echoed, but found '" +
                        lines[0].strip() + "'")
    if prompt is not None and re.search(prompt, lines[-1]) is None:
        raise Exception(
            "didn't find expected prompt regexp '" +
            repr(prompt) + "' at last line of " +
            "echoed output '" + repr('\n'.join(lines)) + "'" +
            "when running command '" + command + "'")
    return '_\n'.join(lines[1:-1])


# doesn't chan.exit_status_ready()
def runBySend(command, serverDomain, username,
                  chan=None,
                  timeout=None,
                  prompt=r'\$$',
                  **kwargs):
    loginKwargs = popLoginKwargs(kwargs)
    if chan is None:
        chan, loginKwargs = _openChan(
            serverDomain,
            username,
            **loginKwargs)
    if timeout is None:
        timeout = 2
    chan.send(command + '\n')
    cmdRes = _readCmdRes(chan, command,
                             prompt=prompt, timeout=timeout)
    cmdErrCode = 'echo $?\n'
    chan.send(cmdErrCode)
    errCodeRes = _readCmdRes(chan, cmdErrCode, prompt=prompt)
    return cmdRes, int(errCodeRes), chan
