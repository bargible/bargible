"""
a thin wrapper around upstream scp lib
"""
from scp import SCPClient

from .ssh_client import getClient


def scpFile(
        loginInfo,
        pathSource,
        pathDest,
        **kwargs):
    sshClient, loginKwargs = getClient(
        loginInfo['serverDomain'],
        loginInfo['username'],
        **kwargs)
    with SCPClient(sshClient.get_transport()) as scpClient:
        scpClient.put(pathSource, pathDest, recursive=True)
    return loginKwargs
