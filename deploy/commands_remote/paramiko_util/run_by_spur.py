from spur import SshShell
from spur.ssh import ShellTypes

from .ssh_client import popLoginKwargs
from .ssh_client import getClient

class _SshShell(SshShell):

    def __init__(self):
        super().__init__(None)

    def connectSsh(self, *args, **kwargs):
        self._client, loginKwargs = getClient(*args, **kwargs)
        return loginKwargs

    def _connect_ssh(self):
        if self._client is None:
            if self._closed:
                raise RuntimeError("Shell is closed")
            raise RuntimeError("run connect_ssh to connect shell")
        return self._client


def runBySpur(command, serverDomain, username,
                  **kwargs):
    loginKwargs = popLoginKwargs(kwargs)
    sshShell = _SshShell()
    loginKwargs = sshShell.connectSsh(
        serverDomain,
        username,
        **loginKwargs)
    sshProcess = sshShell.spawn(
        ['sh', '-c', command],
        allow_error=True,
        )
    executionResult = sshProcess.wait_for_result()
    return ((executionResult.output, executionResult.stderr_output),
            executionResult.return_code,
            loginKwargs
            )
