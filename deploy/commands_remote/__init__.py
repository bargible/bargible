"""
these functions correspond to commands for the deploy scripts that
affect the remote server only.
most implementation details are non-AWS-specific,
but some login details may be specific to AWS
"""
import os
import datetime
import shutil
import configparser
from itertools import repeat
from uuid import uuid4 as uuid

import git

from .commands_util import runCommands
from .paramiko_util import scpFile
from . import shell_command_defs as shDefs
from . import multi_shell as mSh

_DEFAULTS_CONF = configparser.ConfigParser()
_DEFAULTS_CONF.read(
    os.path.join(os.path.dirname(__file__), 'defaults.conf')
    )

FILENAMES = _DEFAULTS_CONF['filenames']
# keys match this module's API (currently, `deployApp`
# functions' `credPaths` parameter only), and
# values match commands_remote package cross-module static
# config defaults
FILENAMES_CREDS = {
    'tango': FILENAMES['TANGO_CREDS'],
    'braintree': FILENAMES['BRAINTREE_CREDS'],
}

def addUser(serverDomain,
            usernameNew,
            keyInfos,
            loginUsername='admin', # the default on new EC2 instances
            ):
    runCommands(
        shDefs.addUser(usernameNew, keyInfos['pubKey']),
        serverDomain,
        loginUsername,
        asSu=True,
        defaultTimeout=2,
        keyFilename=keyInfos['keyFilename'],
        cmdStrategy='spur',
        )


def sysInit(serverDomain, username, sysConfig,
                installTimeout=10):
    runCommands(
        shDefs.sysInit(
            sysConfig['usernameAucAdmin'],
            sysConfig['passwordAucAdmin'],
            ),
        serverDomain,
        username,
        defaultTimeout=2,
        asSu=True,
        cmdStrategy='spur',
        )


def userInit(serverDomain, username, dbUrl):
    runCommands(
        shDefs.userInit(dbUrl),
        serverDomain,
        username,
        defaultTimeout=2,
        )


def _initGitRemote(
        loginInfo,
        gitInfo,
        loginKwargs,
        localRepo,
        ):
    # todo: check for repo on remote before creating one
    # ... OR revisit setup entirely and use /srv/git for
    #     git repositories on the remote
    bareRepoPath = os.path.join(
        os.path.abspath(os.path.join(gitInfo['path'], '..')),
        str(uuid()) + '.git')
    bareRepo = localRepo.clone(bareRepoPath, bare=True)
    # linux on the remote, regardless of local path format
    remoteRepoPath = '~/' + FILENAMES['REMOTE_REPO']
    loginKwargs = scpFile(
        loginInfo,
        bareRepoPath,
        remoteRepoPath,
        **loginKwargs)
    shutil.rmtree(bareRepoPath)
    newRemote = localRepo.create_remote(
        gitInfo['remoteName'],
        ('{user}@{domain}:{path}'.format(
            user=loginInfo['username'],
            domain=loginInfo['serverDomain'],
            path=remoteRepoPath)),
        )
    # test connection
    # todo: avoid password fatigue here and on other git commands
    # if possible ... git module binds some functionality directly
    # to binary, so it may not be
    newRemote.fetch()
    return loginKwargs


def deployApp(
        loginInfo,
        gitInfo,
        credPaths,
        flaskConfig='ProductionConfig',
        resetDb=False):
    deployDatetime = datetime.datetime.now()
    # avoid password fatigue with paramiko_util
    loginKwargs = {}
    ### create a branch and push it to the server
    localRepo = git.Repo(gitInfo['path'])
    if gitInfo['remoteName'] not in [
            remote.name for remote in localRepo.remotes]:
        loginKwargs = _initGitRemote(
            loginInfo,
            gitInfo,
            loginKwargs,
            localRepo,
            )
    remote = next(iter([
        remote for remote in localRepo.remotes
        if remote.name == gitInfo['remoteName']]))
    if gitInfo['branch'] not in localRepo.heads:
        raise ValueError(
            "branch '" + gitInfo['branch'] + "' not found " +
            "in local repo at " + gitInfo['path'])
    deployRef =localRepo.create_head(
        'deploy-' + deployDatetime.strftime('%y%m%d%H%M'),
        commit=localRepo.heads[gitInfo['branch']])
    pushInfo = remote.push(
        ':'.join(list(repeat('refs/heads/' + deployRef.name, 2))))
    ### copy misc files (3rd party http api creds, etc.)
    for (srcPath, destFilename) in [
        (credPaths[k], FILENAMES_CREDS[k]) for k
        in set(FILENAMES_CREDS.keys()).union(set(credPaths.keys()))
        ]:
        loginKwargs = scpFile(
            loginInfo,
            srcPath, '~/'+destFilename,
            **loginKwargs
            )
    ### run commands to start app on remote
    runCommands(
        shDefs.runApp(
            deployRef.name,
            deployDatetime,
            flaskConfig=flaskConfig,
            resetDb=resetDb,
            ),
        loginInfo['serverDomain'],
        loginInfo['username'],
        defaultTimeout=1.5,
        cmdStrategy='exec',
        **loginKwargs)


def _addAuthorizedKeys(srcLoginInfo, destLoginInfo):
    """
    helper for takeSnapshot.
    add ssh public key from src server to dest server,
    creating a public key if it doesn't exist.
    srcLoginInfo, destLoginInfo (`dict(domain, user)`)
    """
    srcInfo, srcLoginKwargs = mSh.getServerInfo(
        srcLoginInfo['domain'], srcLoginInfo['user'])
    if srcInfo['ssh_pubkey'] is None:
        srcLoginKwargs = mSh.genPubKey(
            srcLoginInfo['domain'], srcLoginInfo['user'],
            **srcLoginKwargs)
        srcInfo, srcLoginKwargs = mSh.getServerInfo(
            srcLoginInfo['domain'], srcLoginInfo['user'],
            **srcLoginKwargs)
    assert srcInfo['ssh_pubkey'] is not None
    destInfo, destLoginKwargs = mSh.getServerInfo(
        destLoginInfo['domain'], destLoginInfo['user'])
    if srcInfo['ssh_pubkey'] not in destInfo['authorized_keys']:
        mSh.addPubKey(
            destLoginInfo['domain'], destLoginInfo['user'],
            srcInfo['ssh_pubkey'],
            **destLoginKwargs)
        destInfo, destLoginKwargs = mSh.getServerInfo(
            destLoginInfo['domain'], destLoginInfo['user'],
            **destLoginKwargs
            )
    assert srcInfo['ssh_pubkey'] in destInfo['authorized_keys']
    return srcLoginKwargs, destLoginKwargs


def takeSnapshot(srcInfo, destInfo, timeouts):
    deployLoginKwargs, srcLoginKwargs = _addAuthorizedKeys(
        # destination for public key addition is src for snapshot
        destInfo, srcInfo)
    deployInfo, deployLoginKwargs = mSh.getDeployInfo(
        srcInfo['domain'], srcInfo['user'],
        **deployLoginKwargs)
    srcInfo.update(deployInfo=deployInfo)
    runCommands(
        shDefs.takeSnapshot(
            srcInfo,
            destInfo['path'],
            timeouts,
            ),
        destInfo['domain'],
        destInfo['user'],
        defaultTimeout=3,
        cmdStrategy='exec',
        **srcLoginKwargs
    )


__all__ = [
    'addUser',
    'sysInit',
    'userInit',
    'deployApp',
    'takeSnapshot',
]
