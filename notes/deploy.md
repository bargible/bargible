Development Deploy Notes
========================

While this document may serve as a useful reference,
the general tasks outlined herein are automated by the
Python scripts in `deploy/`.
These scripts are accompanied by a self-documenting
command-line interface, and the output of

```
python bin/runDeploy.py --help
python bin/runDeploy.py <command-name> --help
```

together with the scripts themselves are the primary documentation
of the deploy process.

The deploy process differs slightly from the one found in this
document in that a local copy of the source tree is copied to a
[bare git repository](http://git-scm.com/book/en/v2/Git-on-the-Server-Getting-Git-on-a-Server)
on the server.  A local branch is created and
pushed to the bare repository, and the bare repo is then cloned
on the server in order to complete the deploy process in a working
source tree.  The `deploy/` process is compatible with the process
described below via adding an additional remote to the bare git
repository.

The `runDeploy.py` commands roughly correspond to this documentation
as follows:

* _VPS System Setup_ : `sys-init` and `user-init`
* _VPS application install_ and _Running the application_ : `deploy-app`

The _Aside on terminal multiplexers_ remains a useful read since
`tmux` is used as an ad-hoc monitoring setup by `deploy/` at the
time this documentation is being updated (_Feb 24, 2017_).
Finally, note that once the server and user account have been created,
`htop` can be useful for monitoring the deploy scripts' progress
and may serve as a (very) basic status dashboard.

### `runDemo.py` notes

##### current post-`deploy-app` manual ops

_Last updated: 2017-04-27_

this section details any manual (non-automated) operations
run after the `deploy-app` command.

```
pyenv activate bargible-auction-00
cd auction-<timestamp>/
sudo service nginx stop
tmux attach-session -t auction-webserver
# ^c to stop process, then ^b-d to detach session
tmux attach-session -t auction-updater
# ^c to stop process, then ^b-d to detach session
python bin/reset_db.py
# initialize the database state with  items (giftcards),
# credit packages, physical items, and default configuration
# (e.g. game element charges, initial user credits, etc.).
# see gentestdata.py --help for more details on flags.
python bin/gentestdata.py -i 0 -c 1 -p 1 --config
tmux attach-session -t auction-updater
# run previous command with up arrow key, detach session
tmux attach-session -t auction-webserver
# run previous command with up arrow key, detach session
sudo service nginx start
```

##### on shell commands failing on the remote

several of the subcommands for `runDeploy.py` rely on running a
sequence of shell commands on a remote VPS.
if any of these commands should fail (e.g. -- due to slow
internet connectivity), the deploy scripts are designed to display
an error message with the errant command.
at this point, rather than running the entire subcommand
(or sequence of subcommands) over again,
the `commands_remote.py` script is written such that
blocks of these shell commands may be commented and uncommented
in a text editor with block commenting features.

so in this case, the entire list of commands may be commented out
up to an including the command that fails.  the desired effects
of that command may be produced manually.  and the `runDeploy.py`
subcommand with the failing shell command may be re-run
before uncommenting the block.

these scripts are -- indeed, the entire ops setup is --
a work in progress, and there remain several
options to ease the user experience.  at the time of writing,
this proceedure can still be helpful, particularly if there's
a lot of traffic between AWS and another high-traffic server
such as upstream code bases.

### `deploy-app` notes

##### bare repo init out of source repo
As described above, the `deploy-app` subcommand of `runDeploy.py`
initializes a bare git repository on the remote server by copying
files from the local filesystem to the server.
Since the git repository copy can take a fair amount of time,
particularly on slow internet connections, it's usually desirable to
initialize the bare git repository on the VPS from a another remote
computer (e.g. the bitbucket server).
Since the project has yet to standardize on and/or configure
a version control remote with security measures for
automated authentication via the deploy scripts,
the bare git repository initialization is currently expected to
be performed manually if desired,
and this section of the documentation describes that process.

* Log into the VPS with `ssh <vps_user>@<vps_domain>`,
  where `<vps_domain>` may be found in the AWS console
  (see `aws_console.md`) and `<vps_user>` is a username created by
  the `add-user` command (defaults to `appadmin`).
* From the user's home directory (`cd` with no parameters changes
  to the home directory) on the VPS, clone the bare repo
  by running
  ```
  git clone --bare <vc_remote> auction_deploy
  ```
  and entering the password _for the version control remote_.
  - `<vc_remote>` is the address of the remote version control
    server, currently something like
    `https://<username>@bitbucket.org/bargible/bargible.git`.
  - note that this does not tie the `deploy-app` command to any
    particular account on the version control remote, because
    the code to deploy will still be pushed to the VPS from the
    local repository where `deploy-app` is run.
  - note that the name of the bare repository on the remote must
    follow the `auction_deploy` naming convention used by the
    deploy scripts:  after the `git clone` command above, there
    should be an `auction_deploy` directory in the user's home
    directory, not an `auction` or `bargible` directory.
* in the root directory of the git source tree on the local
  machine (the machine where the `deploy-app` command will be
  run, _not_ the `auction_deploy` directory on the VPS),
  add a remote pointing to the VPS with the command
  ```
  git remote add <vps_name> <vps_user>@<vps_domain>:~/auction_deploy
  ```
  where
  - `<vps_name>` name is the `serverName` argument that was
    passed to `create-server` and `add-user` and will be passed
    to `deploy-app`
    (visible in the "Name" column of the EC2 instances list for
    EC2 deploy -- see `aws_console.md` for more)
  - `<vps_user>` and `<vps_domain>` are the login information
    for the VPS as above

Note that **this proceedure only need be done once
per VPS user account**.  It's not necessary to initialize the
bare repository each time `deploy-app` is run.

##### monitoring and starting/stopping [parts of] the application

> Throughout this section, the convention `^<key>` will be used to denote "Ctrl+<key>", so for example, `^c` mean "hold Ctrl and press 'c'".

As of writing, `tmux` is used to monitor all parts of the server-side
application (both the webserver and updater processes).
These processes are started by `deploy-app` in sessions according
to the naming convention `auction-updater` and `auction-webserver`.

To after attaching to one of these tmux sessions with

```
tmux attach-session -t <session-name>
```

`^c` will stop the process,
and `^d` will close the session's shell
such that tmux will remove the session from its registry.

##### image storage and database reset

Since, under the current deploy strategy,
the application's dynamic images (e.g. reward images)
are stored on the VPS's local filesystem, while the database
records for images are not stored on the VPS, it's necessary to
ensure the database and the VPS's filesystem are in-sync
the *first time* a new VPS (EC2 instance) is created.

This can most readily be done by resetting the database
associated with that particular EC2 instance.
However, resetting the database with the `deploy-app` is
currently disabled since, at the time of writing, there is no
database backup and restore mechanism.  The risk of accidental
data loss is prioritiezed over smoothing automation.

Since the database reset requires a properly-configured Python
environment and that configuration is done during `deploy-app`
(as it is specific to the version of the application that's being
deployed), the suggested method of database reset is to

* finish a `deploy-app` command
* stop the server as described in the previous section
* reset the database
* the run the `deploy-app` command again

For the "reset the database" step in the above, run
```
cd ~/auction-<timestamp>
pyenv activate bargible-auction-00
python bin/reset_db.py
python bin/gentestdata.py --items 0 --credit-packages 0 --config
```
where `<timestamp>` is the most recent timestamp created
`deploy-app` command.  These timestamps are of the format
`YYMMDDHHMM` where Y corresponds to year, M to month, D to day,
H to hour, and M to minute.

### on manual database backup and restore

##### backing and restoring up images

since the database contains information about image file storage
(user avatars, auction item images, etc. -- any image that's
not a static asset),
to properly backup the database, it's necessary to backup
file storage at the same time.

at the time of writing (_March 22, 2017_), files are stored on
the VPS's local filesystem, so any command line tool capable of
the
[secure copy protocol](https://en.wikipedia.org/wiki/Secure_copy)
over SSH is suitable for backing up the directory tree at
`/var/local/bargible`
is suitable for copying files to and from the VPS.
For example,
```
scp -r appadmin@dev.bargbile.com/var/local/bargible ~
```
will copy images from `dev.bargbile.com` to the user's home directory.
[WinSCP](https://winscp.net/eng/index.php)
is a comparable program for Windows.

##### using a restored database

the recommended way to use a restored database is to
initialize a new user with access to the database by using the
`user-init` subcommand of `runDeploy.py`.  however, it's
also possible to change the application database for a user
account by editing `DATABASE_URL` environment variable
on the remote.  the deploy scripts add an `export` command to
`.bashrc`, and this file can be manually edited
(with `nano`, `vi`, `emacs`, etc.) via an `ssh` connection to
the VPS.


Prerequisites
-------------
* database (db) credentials: `<db_url>`, `<db_user>`, `<db_pass>`
  - [**optional**] test connection to DB and database creds
    with your favorite postgres client, such as the GUI
    [pgadmin3](http://www.pgadmin.org/)
    or the standard command-line client,
    [`psql`](https://www.postgresql.org/download/)

    For example, to test connecting with `psql`, run
    ```
    psql -h <db_url>  -U <db_user> -d bargible --password
    ```
    if a prompt such as `bargible=>` appears after entering
    the password `<db_pass>` at the password prompt, the credentials
    are correct, and pressing Ctrl+D will exit the database prompt.
* a `<username>` on the VPS (EC2 instance) at `<domain>` being
  deployed to with
  - remote login
  - admin permissions

  as described in `aws_console.md`

* a username and password for the remote version control
  (bitbucket) repository: `<vc_user>`, `<vc_pass>`

* valid (if only for sandbox environments) credential files,
   `<tango_creds>` and `<braintree_creds>`,
  for the 3rd-party Tango and Braintree HTTP APIs.

  these files should be in the format found in
  `app/tango/creds.conf` and `app/braintree/creds.conf`,
  respectively.


VPS System Setup
----------------

From the root directory (probably `auction/`) of this repository,

* copy the system initialization script to the VPS


    `scp bin/runSysInit.sh <username>@<domain>:~`

* connect to the VPS

    `ssh <username>@<domain>`

From the user's home directory on the VPS
(the current directory immediately after connecting to the VPS),

* run the system initialization script

    ```
    chmod +x runSysInit.sh
    sudo ./runSysInit.sh
    ```

* set enviroment variables in `~/.bashrc`

    > `export DATABASE_URL=postgresql://<db_user>:<db_pass>@<db_url>:5432/bargible`

    > `export APP_SETTINGS="config.DevelopmentConfig"`

  e.g., by running
    ```
    echo "# auction environment vars" >> ~/.bashrc
    echo "export DATABASE_URL=postgresql://<db_user>:<db_pass>@<db_url>:5432/bargible" >> ~/.bashrc
    echo "export APP_SETTINGS=\"config.DevelopmentConfig\"" >> ~/.bashrc
    ```

* install [pyenv](https://github.com/yyuu/pyenv#basic-github-checkout),
  making edits to `remote:~/.bashrc`, by running
  (at the time of writing this documentation)

    ```
    git clone https://github.com/yyuu/pyenv.git ~/.pyenv
    echo "# pyenv environment variables and initialization" >> ~/.bashrc
    echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
    echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
    echo 'eval "$(pyenv init -)"' >> ~/.bashrc
    ```

* install the
  [pyenv-viratualenv](https://github.com/yyuu/pyenv-virtualenv#installing-as-a-pyenv-plugin)
  pyenv plugin by running

    ```
    git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv
    ```

* install [nvm](https://github.com/creationix/nvm#manual-install),
  making edits to `remote:~/.bashrc`, by running

    ```
    git clone https://github.com/creationix/nvm.git ~/.nvm
    cd ~/.nvm
    git checkout v0.16.1
    cd
    echo "# nvm enviornment variables and initialization" >> ~/.bashrc
    echo 'export NVM_DIR="$HOME/.nvm"' >> ~/.bashrc
    echo '[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"' >> ~/.bashrc
    ```

* reload the shell configuration

     `source ~/.bashrc`

* install the python runtime

    `pyenv install 3.5.3`

* install the node runtime

    `nvm install v7.2.1`

* create a python virtualenv

    ```
    pyenv virtualenv 3.5.3 bargible-auction-00
    pyenv activate bargible-auction-00
    pip install --upgrade pip
    ```


VPS application install
-----------------------

From the user's home directory on the VPS
(run `cd` with no parameters to double-check),

* activate the python and node virtual environments

    ```
    pyenv activate bargible-auction-00
    nvm use v7.2.1
    ```

* clone this
  [auction repository](https://bitbucket.org/cpalsulich/auction)
  to the VPS

    `git clone https://<vc_user>@bitbucket.org/cpalsulich/auction.git`

From the project root directory (`cd ~/auction`) on the VPS

* install dependencies

    `./bin/updateInstall.sh`


* build the web application client-side

    `node bin/build_client.js`


* ensure that the redis server is running by checking the output of

    `sudo service redis-server status`


* setup nginx
  - backup the existing configuration
    ```
    sudo mv /etc/nginx/sites-enabled/default ~/nginx_bkup
    ```
  - generate the application's nginx configuration
    ```
    python bin/makeNginxConf.py ~/nginx_config
    ```
  - restart nginx with the application-specific configuration
    ```
    sudo mv ~/nginx_config /etc/nginx/sites-enabled/default
    sudo service nginx restart
    ```


Aside on terminal multiplexers
------------------------------

From [Wikipedia](https://en.wikipedia.org/wiki/Terminal_multiplexer)

> A terminal multiplexer is a software application that can be used ... for separating programs from the session of the Unix shell that started the program, particularly so a remote process continues running even when the user is disconnected.

In particular, these deploy notes will describe how to use the
`tmux` program to run the various parts of the server-side
application.  The relevant concepts and commands are as follows

#### starting a new session

```
tmux new-session -s <session_name>
```

creates a new `tmux` session.

#### detaching from the current session

while in a `tmux` session, press `C-b d`
(holding the Ctrl key while pressing `b` --- the prefix for all tmux
commands --- followed by pressing the `d` key for "detach")

#### attaching to an existing session

`tmux ls` will list all currently
active tmux sessions (or print an error message if there are
no active sessions).

to reattach to an existing tmux session (e.g. to view output to stdio
or stop the application), run
```
tmux attach-session -t <session_name>
```


Running the application
-----------------------

From the user's local machine

* copy 3rd-party HTTP API credentials to user's home
  directory on the VPS
  ```
  scp <tango_creds> <braintree_creds> <username>@<domain>:~
  ```

From the project root directory (`cd ~/auction`) on the VPS

* place the credential files in the project directory
  ```
  mv ~/<tango_creds> app/tango/creds.conf
  mv ~/<braintree_creds> app/braintree/creds.conf
  git update-index --assume-unchanged app/tango/creds.conf app/braintree/creds.conf
  ```

* initialize the database

    `python bin/reset_db.py`

  **Warning:** this will delete _all_ data currently stored in
  the database


* within a new `tmux` session named `auction-updater`,
  start the auction updater process by running

    `python bin/auction_updater.py --sync --verbose`

  wait about 5 seconds, then detach from the `auction-updater`
  session.

* within a new `tmux` session named `auction-webserver`,
  start the webserver process by running

    `gunicorn -k eventlet -w 1 --log-level debug --error-logfile "/var/log/bargible/gunicorn__$(date +%y%m%d-%H%M).log" -b 127.0.0.1:5000 app:app`

  wait about 10 seconds, then detach from the `auction-webserver`
  session.

* populate the database with initial
  values for required admin configuration options
  ```
  python bin/gentestdata.py --credit-packages 0 --items 0 --config
  ```
