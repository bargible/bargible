# admin web UI

the admin web UI is used to manage various aspects of
application state (creating auctions, viewing lists of users,
sending rewards for auctions, etc.).  this document describes
details of the admin UI relevant to a user of that UI:

it is a user manual to provide details on the functionality
available through the admin pages.  it is not an operations
manual describing how that functionality is used to implement
business processes.

### logging in

##### background

in order to prevent unauthorized users from accessing the
admin UI, all access to the admin UI is blocked by nginx,
the reverse proxy server that is the first program to access
any incoming request to the server on port `80`, the default port
used by web browsers.  this setup could be loosely described by
saying, "there is a web firewall around the admin UI."
after going through nginx on port `80`, http requests may be
directed to the actual python application server, which is available
on a different port, one that is not visible outside of the
server (port `5000` at time of writing).

in addition to port `80` for http requests, the servers
(VPSs, EC2 instances) that run the application have port `22`
open to the outside world for connections using a different
protocol called "ssh". among other things, ssh allows for
something called "port forwarding" where a port on the server
may be connected to a port on the local computer where the `ssh`
client program is run.  the current login setup connects a port
(port `5005` in these examples, although any unused port,
`3000` and up, will do) on the user's local computer to the
python application server's port on the remote computer via
port forwarding.

in other words, although the browser will display `localhost`
in the URL bar after setting up port-forwarding,
the page is loaded from the remote, not the local computer.

##### prerequisites

logging in to the VPS requires an ssh username and password.
the current default username and password created by the deploy
scripts upon system initialization are

* username: `aucadmin`

* password: `1qaz2wsx`

----

additionally, an ssh program must be installed on the user's computer.
although there are several programs that support port forwarding
on various platforms, the only offically supported method of setting up
port forwarding at time of writing is the
[OpenSSH](https://www.openssh.com/)
command-line program. it is installed by default on most linux
distributions and on OS X, and it it is available via
[Cygwin](https://www.cygwin.com/)
on Windows.

##### connecting to the admin web UI

to start port forwarding, run

```
ssh -L <local_port>:localhost:5000 <ssh_username>@<domain>
```

at the command prompt (Cygwin on windows, Terminal on OS X,
any shell on linux), where

* `<local_port>` is an open port on the local computer
  (`5005` is a likely choice).

* `<ssh_username>` is the username for an ssh account on the
  remote computer (see [#prerequisites] above)

* `<domain>` is the domain name (e.g. `dev.bargible.com`)
  of the server to connect to

then, in a new browser tab, navigate to `localhost:<local_port>`
and login (registering a new username if necessary)
to the application if the browser does not redirect to
`localhost:<local_port>/home`.

now the admin UI can be access by changing the address in browser's
URL bar to `localhost:<local_port>/admin`.

##### misc notes

the port-forwarding setup is _not_ meant for gameplay use.
aside from the login described in the previous section,
the only URLs that should be accessed via the browser at the
`localhost:<local_port>` domain after port-forwarding is connected
are those beginning with `/admin`.
navigating to `localhost:<local_port>/home` or otherwise won't break
anything on the server-side, but client side behavior may be slow
or buggy (most likely just slow due to requests not passing
through nginx).

as always, it's recommended that passwords be cycled regularly.
to do so, after connecting to a remote shell, use the `passwd`
command:

```
<local_name>$ ssh <ssh_username>@<domain>
# login dialogue
<remote_name>$ passwd
# update password dialogue
```
