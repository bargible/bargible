# database operations

this document has some overlap with `deploy.md`, `aws_console.md`,
and other system operations documentation.
its primary focus is on the operation of systems that are responsible
for persistent data storage (database, file storage, etc.)

in particular, this document describes tasks such as backup and
data migrations that are connected to but not (usually) run on
the same machines (EC2, S3, RDS, etc. instances) used for
application deployment.

### backup server setup

a current method for database backup is the `snapshot` subcommand
to `runDeploy.py`, which may include a server name
(as created by the `create-server` subcommand)
among its parameters.
the ssh user account on such a server may be created via the
`add-user` subcommand just as for a deploy server.
however, both the system setup and user account environment
are somewhat different for the destination of the `snapshot`
subcommand, so the `sys-init` and `user-init` subcommands
shouldn't be run with a `serverName` that's to be used for a
backup server.  this section documents the setup for backup
servers analogous to the setup currently automated
by the `sys-init` and `user-init` subcommands for deploy servers.

##### system setup

current EC2 instances are based of Debian stable (jessie),
but some packages are required from testing (stretch),
so an apt pinning setup similar to the one described on
[the Debian wiki](https://wiki.debian.org/AptPreferences):

append these lines to `/etc/apt/sources.list`

```
### stretch
deb http://cloudfront.debian.net/debian stretch main
deb http://security.debian.org/ stretch/updates main
```

and these to `/etc/apt/preferences`
(creating the file if it doesn't exist)

```
Package: *
Pin: release a=jessie
Pin-Priority: 900

Package: *
Pin: release a=stretch
Pin-Priority: 800
```

update the package registry with `sudo apt-get update` and
install the postgres client from stretch with
`sudo apt-get install postgresql-client-9.6`.
note that, since this package doesn't exist in jessie,
it's not necessary to pass pinning information to the `install`
subcommand as described on the wiki.
also, upstream updates go into the current testing branch
more frequently than stable, so it's possible that the client
will get a minor version bump in the not-too-distant future.
to check the available versions, something like

```
apt-cache search postgresql-client | grep postgresql-client-9 | less
```

could be useful for checking the available versions.
as long as the client version is at least that of the deployment
database (currently `9.5` while jessie contains `9.4`),
the installed suite of client utilities ought to function with the
deployment database.

### generating migrations

_Updated 2017-05-15_

Given the current workflow and codebase,
database migrations will typically be generated upon
preparing a branch named something like `dev-qa-{date}`
for merge into `master`.  Assumptions here include

* all merges to `master` going through a `dev-qa-{date}` branch

* the same database being used for each deploy of a
  `dev-qa-{date}` branch

* the deploy process not resetting the remote database

##### checking for empty migrations

After running the current version of the `deploy-app`
subcommand for `runDeploy.py`, connect to the VPS with
the deployed application via ssh according to

* the username used with `deploy-app`

* the domain from the `list-servers` command output
  corresponding to the `serverName` from `deploy-app`

* the password used with the `deploy-app` command

(see `deploy.md` for more info).

Then run

```
python activate bargible-auction-00
cd auction-{datetime}
alembic revision -m "dev qa {date}"
```

Check the resulting auto-generated migration
(the only untracked file in the `auction-{datetime}`
 git repo at this point -- use `git status` for a path).

If the file contains functions defined as

```
def upgrade():
    pass


def downgrade():
    pass
```

then the migration is empty:  no changes have been
made to the database schema between `master` and the
`dev-qa-{date}` branch up for merge, so there's no need
to include any additional database migrations in version control.

### restoring from backup

these are some quick notes on restoring a snapshot to a
local dev environment as configured according to `README.md`.

* `<bkup_sand>` denotes a sandboxed directory somewhere on
  the local filesystem
* `<username>` is the username of an account on the backup server
* `<domain>` is the domain of the backup server
* `<bkup_dir>` is the path to a particular snapshot on the
  backup server, usually something like `~/bkup/snapshot-{datetime}`.

```
# get the snapshot from the server
scp -r <username>@<domain>:<bkup_dir> <bkup_sand>/snapshot
# restore the images
rm -r /var/local/bargible/images/ # needs root permissions
cp -r <bkup_sand>/snapshot/fs_cp/images/ /var/local/bargible/images/
chmod a+x /var/local/bargible/images/
# restore the database
python bin/reset_db.py --no-create
pg_restore -Ft -d $DATABASE_URL <bkup_sand>/snapshot/db_dump.tar
```

