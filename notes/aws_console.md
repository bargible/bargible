# AWS console usage

These are misc notes related to the **Amazon Web Services** web UI,
the AWS "console."

To login to Bargible's AWS console with supplied credentials,
browse to
[https://bargible.signin.aws.amazon.com/console](https://bargible.signin.aws.amazon.com/console).

Note that there is one AWS _account_ for Bargible, and
multiple AWS _users_ have access to the account.  This is
part of the AWS auth system, termed **Identity Access Management**
(IAM).

Several of the objects (security groups, machine images, etc.)
managed by the console may be deleted.
Instances of these objects are typically presented in a list view
below the navbar, and below the list there is a pane with several
tabs.
Before deleting such an object, check the "Tags" tab.
If it is tagged with a "Key" that reads "Scripts and a
corresponding "Value" of "True", **do not** delete the object
as it's used by the deploy scripts, and deleting it may cause
the scripts to break.

----

## on automation

Several of the tasks possible via the AWS console are also
automated by the Python scripts in `deploy/`.  As documented by

```
python bin/runDeploy.py --help
```

the command-line interface for the python `deploy` module provided
by `runDeploy.py` consists of a several sub-commands,
each of which is self-documenting.  For instance,

```
python bin/runDeploy.py create-server --help
```

documents the `create-server` command.

While these notes may refer to the automated scripts -- and, indeed,
may be a perhaps necessary supplemental read for users of the scripts
not already familiar with AWS -- the scripts' self-documentation is
the primary documentation for the scripts.

----


### creating access keys

AWS **access keys** are used to provide programmatic access to
AWS console-like functionality.
They are specific to the AWS account, not any particular EC2
instance (see below).
In particular, it's necessary to have valid access keys in order
to use the python `.deploy` package, including `bin/deploy.py`.

To create access keys from within the AWS web console,

* click on "My Security Credentials" in the dropdown
  below a username in the AWS console navbar

* enter "access keys" in the input field labelled "Search IAM"
  on the panel that appears on the left side of the viewport
  after the navbar click

* select "Manage Access Keys for user <username>",
  where <username> is the currently logged-in username

* click the "Create Access Key" button in the bottom-right
  corner of the "Manage Access Keys" popup that appears
  after the panel search click.

### starting and stopping EC2 instances

**EC2** is Amazon's Virtual Private Server (VPS) product.
EC2 instances are the (virtualized) machines
that we currently (Jan 2017) use to run all parts of
our application
(except for client-side code run in the users' browser,
and database commands, of course).

To start or stop an existing EC2 instance,

* navigate to the EC2 instances list

  - click on "Services" in the navbar,
    then click on "EC2" dropdown panel

  - click on "Instances" link in the frame that appears
    on the left of the screen

* check the box on the left of the row (next to the "Name" column)
  of the instance to be stopped or started in the list of
  instances that appears below the navbar

* click the "Actions" dropdown above the list of instances,
  and under the dropdown's "Instance State" menu item,
  select "Start" or "Stop"

### creating key pairs

"key pairs" are used by Amazon's EC2 product for authentication when, e.g., connecting to a new instance via ssh.  they consist of a human-readable name as well as some cryptographic information used for authentication.

##### private key via AWS (`.pem`)

in this case, the cryptographic information is contained in a `.pem` file.
although the `.pem` file extension is specific to AWS, at the time of writing, it is among the file formats recognized as an identity file
(`-i` option) by the `ssh` program.
security is provided by allowing the `.pem` file to be generated exactly once for download and thereafter referring to it within AWS usage
(both the AWS console and the `runDeploy.py` script)
by the associated human-readable name,
except when it's used to provide authentication.

in other words, the `.pem` file is a "private key"
to be shared only via secure methods
(encrypted email, removable media, etc.).

to create a `.pem` file using the AWS console

* navigate to the EC2 product by clicking on "Services" in the navbar,
  then click on "EC2" dropdown panel

* click on "Key Pairs" link under the "Network & Security" toggle
  in the frame that appears on the left of the screen

* click the "Create Key Pair" button at the top left of the
  frame on the right of the screen and just below the navbar

* enter a human-readable name for the key pair in the input
  labelled "Key pair name:" in the popup that appears on screen,
  and click "Create" in the bottom right of the popup

* your browser's mechanism for saving files will be activated once
  the `.pem` file is ready for download (almost immediately).

keep the `.pem` file in a secure place.

note that, in order to use the `.pem` file with an ssh client,
it's likely necessary to change permissions on the file such that it's read-only and only root/admin users have access to it.
on [POSIX](https://en.wikipedia.org/wiki/POSIX) systems
such as linux and OS X, this can be done by running

```
chmod 400 <pem_file>
```


### creating an AMI from an EC2 instance

**Amazon Machine Images** provide the initial state when creating new
EC2 instances.
There are several publicly available AMIs to choose from
when creating a new EC2 instance, and new AMIs
may be created from existing EC2 instances.
These new AMIs are private to the AWS account used to create them by
default.

To create a new AMI from an existing EC2 instance,

* navigate to the EC2 instances list as in
  [starting and stopping EC2 instances](#starting-and-stopping-ec2-instances)

* check the box next to the instance to create an AMI from as in
  [starting and stopping EC2 instances](#starting-and-stopping-ec2-instances)

* click the "Actions" dropdown and navigate to "Image" > "Create Image"

* fill in the fields labelled "Image name" and "Image description"
  in the popup that appears

* click the "Create Image" button in the bottom right corner
  of the popup

### creating an EC2 instance

This functionality is also available via the `create-server`
command to `runDeploy.py`.

* navigate to the EC2 instances list as in
  [starting and stopping EC2 instances](#starting-and-stopping-ec2-instances)

* click the "Launch Instance" button at the top left of the
  frame on the right of the screen and just below the navbar

* click the "Community AMIs" tab on the left side of the screen,
  and click "Select" next to
  > debian-jessie-amd64-hvm-2015-04-25-23-22-ebs - ami-0d5b6c3d

* in the table of instance types on following screen,
  select the row with a value of value of "t2.micro" in "type" column

* click "5. Add Tags" in the list at the
  top of the screen below the navbar, and
  - enter a name for the instance in the input under "Value"
    next to the input under "Key" that's already labelled "Name"

* click "6. Configure Security Groups" in the list at the
  top of the screen below the navbar, then
  - leave the "Select an existing security group"
    radio button selected
  - enter a name and description for the security group
    in the fields labeled "Security group name" and "Description
  - using the "Add Rule" button as needed, enter the following rules:
    * select "SSH" in the dropdown in the "Type" column,
      and select "Anywhere" from the dropdown in the "Source" column
    * select "HTTP" as the "Type" (notice the field in the
      "Port Range" column is automatically set to 80), and
      select "Anywhere" as the "Source"
    * [**for testing only**]
      select "Custom TCP Rule" as the "Type", enter "5000"
      as the "Port Range", and select "Anywhere" as the source.

* click "Review and Launch" at the bottom right of the screen, then
  click "Launch" at the bottom right of the screen

* if you already have the `.pem` file for an existing key pair,
  you may select the name of the existing keypair from the
  second dropdown in the popup that appears on the screen
  after clicking "Launch".  be sure to click the checkbox
  acknowledging that you have the `.pem` file.

  otherwise, choose "Create a new key pair" from the first
  dropdown in the popup, enter a name for the key pair in the
  input with the "Key pair name" label that appears below the
  dropdown with "Create a new key pair" selected, and click
  click the "Download Key Pair" button below the input with
  the name of the new key pair.  store the downloaded `.pem`
  file in a safe location.

* click the "Launch instances" button at the bottom right
  of the popup that appears on the screen after clicking
  "Launch".


##### existing security groups

if following the proceedure above, it's desirable to use
an existing security group and clicking the
"Select an existing security group" radio button
after clicking "6. Configure Security Groups" in the list at the
top of the screen below the navbar does not result in a list of
security groups,
try clicking "7. Review", then clicking "6. Configure Security Groups"
and the "Select an existing security group" radio button a second time.


### creating an RDS instance

This functionality is also available via the `create-db`
command to `runDeploy.py`.

RDS is Amazon's relational database product.
It's essentially a wrapper for several off-the-shelf
relational databases.
As this project uses postgresql, this documents how to
create a postgresql instance.

* navigate to the RDS instances list

  - click on "Services" in the navbar,
    then click on "RDS" dropdown panel

  - click on "Instances" link in the frame that appears
    on the left of the screen

* click the "Launch DB Instance" button at the top left of the
  frame on the right of the screen and just below the navbar

* click on "PostgreSQL" (replete with elephant picture)
  in vertical tabs under "Select Engine", and click the
  "Select" button at top right of the tab.

* select the radio button under "Dev/Test" in the following
  dialogue, and click "Next Step"

* in the "Specify DB details" step, select the following options
  for each of the labels under "Instance Specifications"
  - _DB Engine Version_: 9.5.4-R1
  - _DB Instance Class_: db.t2.micro
  - _Multi AZ Deployment_: No
  - _Storage Type_: General Purpose (SSD)
  - _Allocated Storage_: 5GB

* still in the "Specify DB details" step, complete
  the _DB Instance Identifier_, username, and password
  fields under "Settings", recording the values entered in
  a secure location, and click "Next Step"

* in the "Configure Advanced Settings" step
  - under "Network & Security", for _VPC Security Group(s)_,
    select "rds_access (VPC)" -- note that, although this
    rule allows incoming CIDR/IP connections from all IP addresses
    (as opposed to just those of the VPS(es)),
    it's still necessary to authenticate connections with the
    username and password provided in the previous step in order
    to execute arbitrary SQL commands.
    **nonetheless, this is a security measure that could be improved**.
  - under "Database Options", set _Database Name_ to "bargible"
  leave all other options for this step unchanged,
  and click "Launch Db Instance".


### snapshotting and restoring an RDS instance

##### snapshotting

from the RDS "Instances" list,

* select the checkbox next to the RDS instance to be snapshotted
* click on the "Instance Actions" dropdown
  above the list of instances
  and select "Take Snapshot"
* enter a snapshot name and confirm

it's suggested to name the snapshot by appending a date stamp
and index to the database name.  so, for example, the first
snapshot of a database named `some-database`
taken on March 22, 2017 would be called
`some-database-170322-00`.

##### restoring

from the RDS "Snapshots" list,

* select the checkbox next to the snapshot to be restored
* click on the "Snapshot actions" dropdown
  above the list of snapshots
  and select "Restore Snapshot"
* select options as in the
  [creating an RDS instance](#creating-an-RDS-instance)
  section, except
  - leave the default `vpc-43a6ff26` VPC under
    "Network and Security" selected
  - select `us-west-2c` for the "Availability zone"
    (or whatever value is currently used by the deploy scripts
     -- perhaps double-check `SERVICE_DEFAULTS` in
     the `deploy.aws.boto_util` module to make sure)
  - leave "Auto Minor Version Upgrade" as is (it will
    reflect the value from the deploy scripts at the time
    the database was created)
* click "Restore DB Instance"

after creating the RDS instance from the snapshot,
in order to allow EC2 instances to connect to the database,
add the `rds_access` VPS security group to the new instance by

* navigating to the RDS "Instances" list

* clicking on the arrow next to the new RDS instance's
  name in order to expand the (inline with the instances list)
  per-instance details dropdown

* click the "Instance Actions" dropdown at the bottom left of
  the new instance's detail dropdown view and select
  "Modify"

* in the following "Modify DB Instance" dialogue
  - in the "Network & Security" section,
    select `rds_access` in the "Security Group" list
  - in the "Maintenance" section,
    check the "Apply Immediately" checkbox
  - click "Continue" at the bottom of the dialogue,
    then click "Modify DB instance" after reviewing
    the changes in the "Modify DB instance" confirmation dialogue


### associating a subdomain names with an EC2 instances

after starting a new EC2 instance, in addition to an IP address
as provided by all VPS products, it (the EC2 VPS) will be immediately
accessible via a domain name provided by the EC2 product.
these EC2 domains are visible via the AWS console and (at approximately
the time of writing) via the `list-servers` subcommand
of `runDeploy.py`.
since the EC2 "public domain" names are automatically-(re)generated
each time a new EC2 server is created,
and since the full domain names are not easy to remember,
it's often desirable to associate a more user-friendly name
with an EC2 instance.
Bargible owns one domain name (bargible.com), so subdomains
(e.g. demo.bargible.com or dev.demo.bargible.com) may be used
to create such a user experience.

this section documents usage of AWS's **Route 53** product to
associate subdomains with EC2 instances.  as with EC2, RDS,
and other AWS products, managed by the console, Route 53
configuration is accessed via the "Services" link in the console
navbar.

the documentation in this section is lifted pretty much directly from
[this post](https://aws.amazon.com/premiumsupport/knowledge-center/create-subdomain-route-53/)
with some additional information from
[this section](http://docs.aws.amazon.com/Route53/latest/DeveloperGuide/routing-to-ec2-instance.html)
of the Route 53 manual.

##### creating a new subdomain

* navigate to the Route 53 hosted zones list
  - click on "Services" in the navbar,
    then click on "Route 53" dropdown panel
  - click on "Hosted zones" link in the frame that appears
    on the left of the screen

* click the "Create hosted zone" button at the top left of the
  frame on the right of the screen and just below the navbar

* in the dialogue that appears on the right of the screen
  - type in a domain name **ending with `.bargible.com`**
    (e.g. `demo.bargible.com` or `test.bargible.com`)
  - [_optional_] add a brief description in the "Comment" field
  - leave the default "Type" field as "Public Hosted Zone"

  before clicking the "Create" button at the bottom of the dialogue

* after the brief loading spinner, a detail view of the newly created
  _hosted zone_ will occupy the middle of the screen, listing
  several _record sets_ (these terms refer to thin AWS-specific
  wrappers around standard DNS technologies).
  one of the record sets will have `NS` in the "Type" column,
  and the other will have `SOA` in the type column
  (the
  [Route 53 manual](http://docs.aws.amazon.com/Route53/latest/DeveloperGuide/ResourceRecordTypes.html)
  contains more information about what these designations
  mean, although they are not Route 53-specific).

  copy the entry in the "Value" column of the `NS` record
  into a text editor.  it will be a newline-separated list
  of four domain names, each looking something like
  `ns-888.awsdns-47.net.`

* while still in the subdomain hosted zone detail view,
  click on the "Create Record Set" button above the list of
  existing (`NS` and `SOA`) record sets, and in the dialog
  that appears on the right of the screen
  - do not add any additional name information
    (notice that the subdomain name is automatically appended
     to any additional name information that would be entered)
  - leave the "Type" dropdown at the default
    `A -- IPv4 address`
  - set the TTL to 60 seconds (unless the codebase is much more
    stable than at the time of writing)
  - copy and paste the IP address of an EC2 instance
    into the "Value" field -- these IP addresses are visible
    in the EC2 product's AWS console and also may be obtained
    by running `python bin/runDeploy.py list-servers` and then
    running `ping <dns_name>` where `<dns_name>` is the domain
    name associated with a named server by EC2/the deploy scripts

  leave other values as defaults and click "Continue" at the bottom of
  the dialogue.


* navigate from the subdomain hosted zone detail view
  to the top-level (`bargible.com`) domain detail view
  - click on "Hosted zones" link in the frame that appears
    on the left of the screen
  - click on the `bargible.com` link in the "Domain Name" column
    of the hosted zones list

* click on the "Create Record Set" button above the list of
  existing record sets, and in the dialog
  that appears on the right of the screen
  - add _the same prefix as the subdomain hosted zone_
    to the "Name" field (e.g. `demo` or `test`)
  - choose `NS -- Name server` in the "Type" dropdown
  - copy and paste the _newline separated_ entries from the
    text editor (the ones that correspond to the subdomain's
    hosted zone `NS` entry's value) into the "Value"
    field

  leave other values as defaults and click "Create" at the bottom of
  the dialogue.

##### updating a subdomain

to update an existing subdomain created as in the previous section
to point to a different EC2 instance,

* navigate to the Route 53 hosted zones list
  - click on "Services" in the navbar,
    then click on "Route 53" dropdown panel
  - click on "Hosted zones" link in the frame that appears
    on the left of the screen

* click on the link in the "domain name" column corresponding
  to the subdomain to be updated to navigate to the list of
  _record sets_ for that _hosted zone_

* click the checkbox next to record set with "A" in the "Type" column
  such that an "Edit Record Set" dialoge appear on the right side
  of the window

* in the "Edit Record Set" dialog, *replace* (do not append to)
  the existing text (an IP address) in the "Value" field
  with the IP address of an EC2 instance
  -- these IP addresses are visible
  in the EC2 product's AWS console and also may be obtained
  by running `python bin/runDeploy.py list-servers` and then
  running `ping <dns_name>` where `<dns_name>` is the domain
  name associated with a named server by EC2/the deploy scripts

* click the "Save Record Set" button at the bottom of the
  "Edit Record Set" dialog.  it may take slighly longer than the
  Time To Lookup (TTL) value indicated in the dialogue before
  changes are visible to any particular ISP.

### updating the top-level domains

prereqs:

* IP address for an EC2 instance -- this can be obtained from
  a domain name by viewing the output of `ping <domain_name>`

process:

* navigate to the Route 53 hosted zones list
  - click on "Services" in the navbar,
    then click on "Route 53" dropdown panel
  - click on "Hosted zones" link in the frame that appears
    on the left of the screen

* update the `bargible.com` domain
  - click the `barigble.com` hosted zone
  - select the record set named `bargible.com` with type `A`
  - in the "Edit record set dialog" on the right of the screen,
    delete the IP address currently in the "Value" field and
    paste in the new IP address

* navigate back to the Route 53 hosted zones list by
  clicking on "Hosted zones" link in the frame that appears
  on the left of the screen

* update the `www.bargible.com` subdomain
  - click the `www.barigble.com` hosted zone
  - select the record set named `www.bargible.com` with type `A`
  - in the "Edit record set dialog" on the right of the screen,
    delete the IP address currently in the "Value" field and
    paste in the new IP address


### adding a user account to an EC2 instance

This section is written supposing an instance has been created
as described in
[creating an EC2 instance](#creating-an-ec2-instance),
but much of the content is generally applicable to adding
a user account on any Debian-based (e.g. Ubuntu)
linux distribution.

This functionality is also available via the `add-user`
command to `runDeploy.py`.
The
[creating key pairs](#creating-key-pairs)
section might be helpful even if using the automated process
as the `add-user` command currently requires a `pubKeyFile`
positional argument.

##### logging into a new EC2 instance

make certain you know

* where the `.pem` file described in
  [creating an EC2 instance](#creating-an-ec2-instance)
  is stored on your local computer by, for instance,
  running `ls <pem_file>` from a command prompt, where `<pem_file>`
  is the location of the key pair file (e.g. `~/.aws/keys.pem`)

* the domain of the EC2 instance:
  it's listed in the "Public DNS" column of the EC2 instances
  list that's visible after clicking the "Instances" link
  as described in
  [starting and stopping EC2 instances](#starting-and-stopping-ec2-instances)
  and can be (partially) verified by ensuring that
  `ping <domain>` does not print a "Name or service not known"
  error, where `<domain>` is the domain of the EC2 instance.

Now, with the same meaning of `<pem_file>` and `<domain>` as above,
run

```
chmod 400 <pem_file>
ssh -i <pem_file> admin@<domain>
```

from a (POSIX OS X, linux, or Cygwin) terminal to login to the
EC2 instance.

##### adding a user account

After logging into the EC2 instance as an admin
(such that the prompt reads something like
`admin@<ip_addr>:<current_directory>$`),
run

```
sudo adduser <username> --disabled-password
```

where `<username>` is the username for the new account.

Press enter/return at all the prompts
("Full Name", "Room Number", etc.)
to go with the default (blank) values.

#### enabling remote login

In order for someone to login to the newly created user account,
it's necessary to add a file called a "public key" from that
person's _local_ computer to the _remote_ server
(i.e. EC2 instance).

##### generating a key on the _local_ computer

First, try running `cat ~/.ssh/id_rsa.pub` on the _local_ computer
to see if a public key already exists.
**Do not** send the results of `cat ~/.ssh/id_rsa` as this is
a private key for the _local_ computer.

If `cat ~/.ssh/id_rsa.pub` responds with a
"No such file or directory" error, run `ssh-keygen`
and follow the prompts to make one,
noting the password for the generated key in a safe place.

After running `ssh-keygen`, `cat ~/.ssh/id_rsa.pub` should print
the public key for the local computer to the console's standard
output.

##### adding the _local_ key to the _remote_ server

Continuing in the same terminal from
[adding a user account](#adding-a-user-account),
with the current login as `admin` on the _remote_ computer, run

```
sudo su - <username>
```

to change the login to that of the new user,
where `<username>` is the same username provided to the
`adduser` command above.

Then run

```
cd
mkdir .ssh
echo "<public_key>" > .ssh/authorized_keys
```

where `<public_key>` is the new user's public key
(i.e., the output of `cat ~/.ssh/id_rsa.pub` run
on the user's _local_ computer).

##### logging in

Now running
```
ssh <username>@<domain>
```
on the user's local machine,
where `<username>` and `<domain>` are as above,
should log the user into the EC2 server.

##### giving the user admin permissions

To give the user administrator permissions, while still logged
in as `admin` as in
[adding a user account](#adding-a-user-account),
and
[adding the _local_ key to the _remote_ server](#adding-the-local-key-to-the-remote-server],
run

```
sudo adduser <username> sudo
```

Now the user is in the sudo group.  However, as the user account
does not have a password associated with it, it's necessary to allow
`sudo` usage without a password.  To do so, _either_ run

```
cp /etc/sudoers /etc/sudoers_original
sudo cat /etc/sudoers | sed "s/%sudo\s\+ALL=(ALL:ALL)\s\+ALL/%sudo ALL=(ALL:ALL) NOPASSWD:ALL/" > ~/sudoers_update
cp ~/sudoers_update /etc/sudoers
rm ~/sudoers_update
```

(**do not** run something like `cat <file> | sed <expr> > <file>`
as this will result in removing the entire contents of file)

_or_ manually edit `/etc/sudoers` with, e.g. `nano`:

```
sudo nano /etc/sudoers
```

such that the line giving administrator privlages to the
sudo group, something like

> `%sudo   ALL=(ALL:ALL) ALL`

contains the `NOPASSWD` flag, as in

> `%sudo   ALL=(ALL:ALL) NOPASSWD:ALL`

finally, run

```
sudo service sudo restart
```

to put the changes in the update `/etc/sudoers` file into effect.

Per-user password-less `sudo` usage can also be enabled by editing the files in the `/etc/sudoers.d` directory.
