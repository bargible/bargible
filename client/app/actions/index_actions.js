/* global require, module */

var alt = require('../alt')

var AuctionsSource = require('../sources/auctions_source')
var SubscribeSource = require('../sources/subscribe_source')

var IndexActions = alt.createActions({
  updateActiveAuctions: function (auctions) {
    return auctions
  },

  updateUpcomingAuctions: function (upcomingAuctions) {
    return upcomingAuctions
  },

  fetchActiveAuctions: function () {
    var self = this
    return function (dispatch) {
      dispatch()
      AuctionsSource.fetchActive().then(function (auctions) {
        self.updateActiveAuctions(auctions)
      }).catch(function (err) {
        console.error("error fetching auctions")
        console.error(err.stack)
        console.error(err)
      })
    }
  },

  fetchUpcomingAuctions: function () {
    var self = this
    return function (dispatch) {
      dispatch()
      AuctionsSource.fetchUpcoming()
        .then(function (upcomingAuctions) {
          self.updateUpcomingAuctions(upcomingAuctions)
        })
        .catch(function (err) {
          console.error("error fetching upcoming auctions")
          console.error(err.stack)
          console.error(err)
        })
    }
  },

  subscribe: function (formFields) {
    return function (dispatch) {
      dispatch()
      SubscribeSource.subscribe(formFields)
    }
  }
})

var exports = IndexActions

module.exports = exports
