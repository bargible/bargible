/* global require, module */

const alt = require('../alt')

const UserSource = require('../sources/user_source')
const AuctionsSource = require('../sources/auctions_source')

const AllPagesActions = require('./all_pages_actions')

const UserProfileActions = alt.createActions({
  fetchEndedAuctions: function () {
    var self = this
    return function (dispatch) {
      dispatch()
      AuctionsSource.fetchEnded().then(function (
                auctions
            ) {
        self.updateEndedAuctions(auctions)
      }).catch(function (err) {
        console.error("error fetching ended auctions")
        console.error(err.stack)
        console.error(err)
      })
    }
  },

  updateAvatar: function (blobImg) {
    return function (dispatch) {
      dispatch()
      Promise.resolve().then(function () {
        return UserSource.updateAvatar(blobImg)
      }).then(function () {
        return AllPagesActions.fetchUser()
      }).catch(function (err) {
        console.error("error updating avatar")
        console.error(err.stack)
        console.error(err)
      })
    }
  },

  updateEndedAuctions: function (auctionHistories) {
    return auctionHistories
  }
})

var exports = UserProfileActions

module.exports = exports
