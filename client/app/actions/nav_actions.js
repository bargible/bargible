/* global require, module */

var CreditPackagesSource = require('../sources/credit_packages_source')

var alt = require('../alt')

var NavActions = alt.createActions({
  fetchCreditPackages: function () {
    var self = this
    return function (dispatch) {
      dispatch()
      Promise.resolve().then(function () {
        return CreditPackagesSource.fetchAll()
      }).then(function (creditPackages) {
        self.updateCreditPackages(creditPackages)
      }).catch(function (err) {
        console.error("error credit packages")
        console.error(err.stack)
        console.error(err)
      })
    }
  },

  updateCreditPackages: function (creditPackages) {
    return creditPackages
  }
})

var exports = NavActions
module.exports = exports
