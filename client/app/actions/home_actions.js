/* global require, module */

var alt = require('../alt')

var AuctionsSource = require('../sources/auctions_source')

var HomeActions = alt.createActions({
  updateAllAuctions: function (data) {
    return data
  },

  fetchEndedAuctions: function () {
    var self = this
    return function (dispatch) {
      dispatch()
      AuctionsSource.fetchEnded().then(function (
        updatedAuctions
      ) {
        self.updateEndedAuctions(updatedAuctions)
        self.updateAllAuctions({
          updatedAuctions: updatedAuctions,
          inits: ['endedAuctions']
        })
      }).catch(function (err) {
        console.error("error fetching ended auctions")
        console.error(err.stack)
        console.error(err)
      })
    }
  },

  fetchJoinedAuctions: function () {
    var self = this
    return function (dispatch) {
      dispatch()
      AuctionsSource.fetchJoined().then(function (
        updatedAuctions
        ) {
        self.updateAllAuctions({
          updatedAuctions: updatedAuctions,
          inits: ['joinedAuctions']
        })
      }).catch(function (err) {
        console.error("error fetching joined auctions")
        console.error(err.stack)
        console.error(err)
      })
    }
  },

  fetchActiveAuctions: function () {
    var self = this
    return function (dispatch) {
      dispatch()
      AuctionsSource.fetchActive().then(function (
        updatedAuctions
        ) {
        self.updateAllAuctions({
          updatedAuctions: updatedAuctions,
          inits: ['activeAuctions']
        })
      }).catch(function (err) {
        console.error("error fetching active auctions")
        console.error(err.stack)
        console.error(err)
      })
    }
  },

  fetchUpcomingAuctions: function () {
    var self = this
    return function (dispatch) {
      dispatch()
      AuctionsSource.fetchUpcoming().then(function (
        updatedAuctions
        ) {
        self.updateAllAuctions({
          updatedAuctions: updatedAuctions,
          inits: ['upcomingAuctions']
        })
      }).catch(function (err) {
        console.error("error fetching upcoming auctions")
        console.error(err.stack)
        console.error(err)
      })
    }
  },

  fetchOneJoinedAuction: function (auctionId) {
    var self = this
    return function (dispatch) {
      dispatch()
      AuctionsSource
      .fetchOneJoinedAuction(auctionId)
      .then(function (updatedAuction) {
        self.updateAllAuctions({
          updatedAuctions: [updatedAuction]
        })
      }).catch(function (err) {
        console.error("error fetching one joined auction")
        console.error(err.stack)
        console.error(err)
      })
    }
  },

  setConfig: function (config) {
    return function (dispatch) {
      dispatch(config)
    }
  },

  watchAuction: function (auctionId) {
    var self = this
    return function (dispatch) {
      dispatch(auctionId)
      AuctionsSource
      .watchAuction(auctionId)
      .then(function () {
        self.fetchUpcomingAuctions()
      }).catch(function (err) {
        console.error("http error at " +
          "joined auction endpoint")
        console.error(err.stack)
        console.error(err)
      })
    }
  },

  joinAuction: function (auctionId) {
    return function (dispatch) {
      dispatch(auctionId)
      AuctionsSource
      .join(auctionId)
      .then(function () {
      }).catch(function (err) {
        console.error("http error at " +
          "joined auction endpoint")
        console.error(err.stack)
        console.error(err)
      })
    }
  },

  bid: function (joinedAuction) {
    return function (dispatch) {
      if (joinedAuction.isGameplayLocked) {
        console.warn("attempted gameplay while locked")
        dispatch()
        return
      }
      if (joinedAuction.isBidsFrozen) {
        console.warn("attempted to bid while bids frozen")
        dispatch()
        return
      }
      dispatch({
        joinedAuction: joinedAuction,
        updateType: 'bid'
      })
      AuctionsSource.bid(joinedAuction.id)
      .then(function (res) {
      }).catch(function (err) {
        console.error("error bidding in auction")
        console.error(err)
      })
    }
  },

  stealBids: function (joinedAuction) {
    return function (dispatch) {
      if (joinedAuction.isGameplayLocked) {
        console.warn("attempted gameplay while locked")
        dispatch()
        return
      }
      dispatch({
        joinedAuction: joinedAuction,
        updateType: 'steal'
      })
      AuctionsSource.stealBids(joinedAuction.id)
      .then(function (res) {
      }).catch(function (err) {
        console.error("error stealing bids")
        console.error(err)
      })
    }
  },

  destroyBids: function (joinedAuction) {
    return function (dispatch) {
      if (joinedAuction.isGameplayLocked) {
        console.warn("attempted gameplay while locked")
        dispatch()
        return
      }
      dispatch({
        joinedAuction: joinedAuction,
        updateType: 'destroy'
      })
      AuctionsSource.destroyBids(joinedAuction.id)
      .then(function (res) {
      }).catch(function (err) {
        console.error("error destroying bids")
        console.error(err)
      })
    }
  },

  freezeBids: function (joinedAuction) {
    return function (dispatch) {
      if (joinedAuction.isGameplayLocked) {
        dispatch()
        console.warn("attempted gameplay while locked")
        return
      }
      dispatch({
        joinedAuction: joinedAuction,
        updateType: 'freeze'
      })
      AuctionsSource.freezeBids(joinedAuction.id)
      .then(function (res) {
      }).catch(function (err) {
        console.error("error destroying bids")
        console.error(err)
      })
    }
  },

  updateJoinedAuction: function (auction) {
    return auction
  },

  /** info is expected to be parsed already */
  setUpdate: function (auctionId, type, info) {
    return {
      auctionId: auctionId,
      type: type,
      info: info
    }
  },

  endJoinedAuction: function (auctionId) {
    var self = this
    return function (dispatch) {
      dispatch(auctionId)
      AuctionsSource.fetchOneEndedAuction(auctionId)
      .then(function (auction) {
        if (auction) {
          self.addEndedAuction(auction)
        }
      }).catch(function (err) {
        console.error("error fetching one ended auction")
        console.error(err.stack)
        console.error(err)
      })
    }
  },

  addEndedAuction: function (auction) {
    return auction
  },

  addViewedAuctionEndingId: function (auctionEndingId) {
    return auctionEndingId
  },

  updateEndedAuctions: function (endedAuctions) {
    return function (dispatch) {
      dispatch(endedAuctions)
    }
  }
})

var exports = HomeActions

module.exports = exports
