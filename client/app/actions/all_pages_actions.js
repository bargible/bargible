/* global require, module */

const alt = require('../alt')

const UserSource = require('../sources/user_source')

const AllPagesActions = alt.createActions({

  fetchUser: function () {
    const self = this
    return function (dispatch) {
      dispatch()
      UserSource.fetchCurr().then(function (user) {
        self.updateUserData(user)
      }).catch(function (err) {
        console.error("error fetching user")
        console.error(err.stack)
        console.error(err)
      })
    }
  },

  updateUserData: function (user) {
    return user
  }

})

var exports = AllPagesActions

module.exports = exports
