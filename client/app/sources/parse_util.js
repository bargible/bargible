/* global require, module */

var _ = require('lodash')

var _parseInt = function (intVal) {
  var parsedVal = parseInt(intVal)
  if (isNaN(parsedVal)) {
    throw new Error(
            "failed to parse integer value " + intVal)
  }
  return parsedVal
}

var checkExpectedKeys = function (expectedKeys, serializedObj) {
  var objKeys = _.keys(serializedObj)
  var missingKeys = _.difference(expectedKeys, objKeys)
  var additionalKeys = _.difference(objKeys, expectedKeys)
  if (_.size(missingKeys) !== 0 ||
        _.size(additionalKeys) !== 0) {
    return {
      missingKeys: missingKeys,
      additionalKeys: additionalKeys
    }
  }
  return null
}

var exports = {}

exports.parseInt = _parseInt
exports.checkExpectedKeys = checkExpectedKeys

module.exports = exports
