/* global require, module */

var _ = require('lodash')
var moment = require('moment')

const validator = require('../validator')

var _parseJoinedAuctionVal = function (val, key) {
  if (_.includes([
    'intermediateTimer',
    'timeRemaining'
  ], key)) {
    return moment.duration({
      seconds: val
    })
  }
  return val
}

var _replaceJoinedAuctionPair = function (key, val) {
  if (key === 'timeRemaining') {
        // val is a moment.duration
    return ['ultTimeEnd', moment().add(val)]
  }
  if (key === 'intermediateTimer') {
    return ['intTimeEnd', moment().add(val)]
  }
  return [key, val]
}

var parseJoinedAuction = function (auction) {
  var maybeValidationError = validator.joinedAuction(auction)
  if (maybeValidationError) {
    throw maybeValidationError
  }
  var parseRes = _(auction)
            .mapValues(_parseJoinedAuctionVal)
            .toPairs().map(_.spread(
                _replaceJoinedAuctionPair
            )).fromPairs()
            .value()
  return parseRes
}

var exports = parseJoinedAuction

module.exports = exports
