/* global require, module */

var _ = require('lodash')
var moment = require('moment')

const validator = require('../validator')

var _parseActiveAuctionVal = function (aucVal, aucKey) {
  if (aucKey === 'timeRemaining') {
    return moment.duration({
      seconds: aucVal
    })
  }
  return aucVal
}

var _replaceActiveAuctionPair = function (key, val) {
  if (key === 'timeRemaining') {
        // val is a moment.duration
    return ['ultTimeEnd', moment().add(val)]
  }
  return [key, val]
}

var parseActiveAuction = function (auction) {
  var maybeValidationError = validator.activeAuction(auction)
  if (maybeValidationError) {
    throw maybeValidationError
  }
  return _(auction)
        .mapValues(_parseActiveAuctionVal)
        .toPairs().map(_.spread(
            _replaceActiveAuctionPair
        )).fromPairs()
        .value()
}

var exports = parseActiveAuction

module.exports = exports
