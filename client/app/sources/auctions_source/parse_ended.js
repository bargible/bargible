/* global require, module */

var _ = require('lodash')
var moment = require('moment')

const validator = require('../validator')

var _parseEndedAuctionVal = function (aucVal, aucKey) {
  if (_.includes([
    'start',
    'end'
  ], aucKey)) {
    return moment.utc(aucVal).toDate()
  }
  return aucVal
}

var parseEndedAuction = function (auction) {
  var maybeValidationError = validator.endedAuction(auction)
  if (maybeValidationError) {
    throw maybeValidationError
  }
  var parseRes = _(auction)
            .mapValues(_parseEndedAuctionVal)
            .value()
  return parseRes
}

var exports = parseEndedAuction

module.exports = exports
