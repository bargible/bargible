/* global require, module */

const _ = require('lodash')
const moment = require('moment')

const util = require('../../util')

const makeParseAuction = require('./parse_auction')

/**
 * a promise that the resolves with the number of milliseconds that
 * the server time is _ahead_ of the browser time
 */
const _fetchServerTimeOffset = function () {
    // this could fail if the server is off by more ms than
    // js Number precision
  Promise.resolve().then(function () {
    return util.fetchJson('api/time')
  }).then(function (res) {
    var browserNow = moment.utc()
    var serverNow = moment.utc(res.now)
    return serverNow.diff(browserNow)
  })
}

const fetchUpcoming = function () {
  return Promise.resolve().then(function () {
    return Promise.all([
      util.fetchJson('api/auction/auctions?status=upcoming'),
      _fetchServerTimeOffset()
    ])
  }).then(_.spread(function (
        upcomingAuctionsRes,
        msServerOffset
    ) {
    return _(upcomingAuctionsRes)
            .map(makeParseAuction(msServerOffset))
            .value()
  }))
}

const fetchActive = function () {
  return util.fetchJson(
        'api/auction/auctions?status=active'
    ).then(function (
        activeAuctionsRes
    ) {
      return _(activeAuctionsRes)
            .map(makeParseAuction())
            .sortBy('id')
            .value()
    }).catch(function (err) {
      console.error("request error at " +
                      "api/auction/auctions" +
                      " endpoint")
      throw err
    })
}

const fetchJoined = function () {
  return util.fetchJson(
        'api/user/auctions?status=joined'
    ).then(function (
        joinedAuctionsRes
    ) {
      return _(joinedAuctionsRes)
            .map(makeParseAuction())
            .sortBy('id')
            .value()
    }).catch(function (err) {
      console.error("request error at " +
                      "api/user/joinedAuctions" +
                      " endpoint")
      throw err
    })
}

const fetchEnded = function () {
  return util.fetchJson(
        'api/user/auctions?status=ended'
    ).then(function (
        endedAuctionsRes
    ) {
      return _(endedAuctionsRes)
            .map(makeParseAuction())
            .sortBy('id')
            .value()
    }).catch(function (err) {
      console.error("request error at " +
                      "api/user/endedAuctions" +
                      " endpoint")
      throw err
    })
}

const fetchOneJoinedAuction = function (auctionId) {
  return util.fetchJson(
        'api/user/auctions/' + auctionId + '?status=joined'
    ).then(function (
        joinedAuctionRes
    ) {
      return (makeParseAuction()(joinedAuctionRes))
    }).catch(function (err) {
      console.error("request error fetching joined auction at " +
                      "api/user/auctions/:id" +
                      " endpoint")
      throw err
    })
}

const fetchOneEndedAuction = function (auctionId) {
  return util.fetchJson(
        'api/user/auctions/' + auctionId + '?status=ended'
    ).then(function (result) {
      return (makeParseAuction()(result))
    }).catch(function (err) {
      console.error("request error fetching ended auction at " +
                      "api/user/auctions/:id" +
                      " endpoint")
      throw err
    })
}

const watchAuction = function (auctionId) {
  return util.updateJson(
        'api/user/auctions/' + auctionId + '?status=upcoming', {
          watch: true
        }
    ).then(function (res) {
    }).catch(function (err) {
      console.error("request error at " +
                      "api/auction/<id>/join endpoint")
      throw err
    })
}

const join = function (auctionId) {
  return util.createJson(
        'api/user/auctions/' + auctionId + '/actions/join'
    ).then(function (res) {
    }).catch(function (err) {
      console.error("request error at " +
                      "api/auction/<id>/join endpoint")
      throw err
    })
}

const bid = function (auctionId) {
  return util.updateJson(
        'api/user/auctions/' + auctionId + '/actions/bid'
    ).then(function (res) {
      return {}
    }).catch(function (err) {
      console.error("request error at " +
                      "api/auction/<id>/bid endpoint")
      throw err
    })
}

const stealBids = function (auctionId) {
  return util.updateJson(
        'api/user/auctions/' + auctionId + '/actions/steal'
    ).then(function (res) {
      return {}
    }).catch(function (err) {
      console.error("request error at " +
                      "api/auction/<id>/stealBids endpoint")
      throw err
    })
}

const destroyBids = function (auctionId) {
  return util.updateJson(
        'api/user/auctions/' + auctionId + '/actions/destroy'
    ).then(function (res) {
      return {}
    }).catch(function (err) {
      console.error("request error at " +
                      "api/auction/<id>/destroyBids" +
                      " endpoint")
      throw err
    })
}

const freezeBids = function (auctionId) {
  return util.updateJson(
        'api/user/auctions/' + auctionId + '/actions/freeze'
    ).then(function (res) {
      return {}
    }).catch(function (err) {
      console.error("request error at " +
                      "api/auction/<id>/freezeBids" +
                      " endpoint")
      throw err
    })
}

var exports = {}

exports.fetchEnded = fetchEnded
exports.fetchActive = fetchActive
exports.fetchJoined = fetchJoined
exports.fetchUpcoming = fetchUpcoming
exports.fetchOneJoinedAuction = fetchOneJoinedAuction
exports.fetchOneEndedAuction = fetchOneEndedAuction
exports.watchAuction = watchAuction
exports.join = join
exports.bid = bid
exports.stealBids = stealBids
exports.destroyBids = destroyBids
exports.freezeBids = freezeBids

module.exports = exports
