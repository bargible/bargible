/* global require, module */

var _ = require('lodash')
var moment = require('moment')

var validator = require('../validator')

/**
 * strStart (String): the day and time when the auction will start
 *   iso 8601 format, UTC time zone
 * msServerAhead (int): number of ms that server clock is ahead of
 *  of browser clock
 *
 * returns: js `Date` object with start time in local timezone
 */
var _parseStart = function (strStartTime, msServerAhead) {
  var startMoment = moment.utc(strStartTime)
  startMoment.add(msServerAhead, 'ms')
  return startMoment.toDate()
}

// this is a a little different from the other parse functions:
// since it needs to handle an ISO formatted time string
// corresponding to a specific date and time
// (rather than a countdown timer),
// it closes over the time difference between the browser's clock
// and the server's clock in order to ensure that the browser's
// display data is a reasonable approximation of events from the server
var makeParseUpcomingAuction = function (msServerOffset) {
  return function (upcomingAuction) {
    var maybeValidationError = validator.upcomingAuction(
            upcomingAuction)
    if (maybeValidationError) {
      throw maybeValidationError
    }
    return _(upcomingAuction).mapValues(function (
            aucVal, aucKey) {
      if (aucKey === 'start') {
        return _parseStart(aucVal, msServerOffset)
      }
      return aucVal
    }).value()
  }
}

var exports = makeParseUpcomingAuction

module.exports = exports
