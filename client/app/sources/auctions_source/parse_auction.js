/* global require, module */

const _ = require('lodash')

const parseActiveAuction = require('./parse_active')
const parseJoinedAuction = require('./parse_joined')
const parseEndedAuction = require('./parse_ended')
const makeParseUpcomingAuction = require('./parse_upcoming')

// msServerOffset is not used consistently throughout
// ... currently, it's only necessary to set msServerOffset
// in order to parse upcoming auctions.
// otherwise, this function may be called with no args.
const makeParseAuction = function (msServerOffset) {
  return function (auctionObj) {
    var typedAuctionPairs = _(auctionObj)
                .omitBy(_.isNull)
                .toPairs()
                .value()
    if (_.size(typedAuctionPairs) !== 1) {
      throw new Error(
                "auction parse error: " +
                    "expected exactly one non-null entry in top-level " +
                    "auction object.  found '" +
                    _.map(typedAuctionPairs, _.head).join(", ") + "'")
    }
    var typedAuctionPair = _.head(typedAuctionPairs)
    var auctionStatusType = _.first(typedAuctionPair)
    var auctionDataObj = _.last(typedAuctionPair)
    var parsedData
    if (auctionStatusType === 'upcoming') {
      parsedData = (makeParseUpcomingAuction(msServerOffset)(auctionDataObj))
    } else if (auctionStatusType === 'active') {
      parsedData = parseActiveAuction(auctionDataObj)
    } else if (auctionStatusType === 'joined') {
      parsedData = parseJoinedAuction(auctionDataObj)
    } else if (auctionStatusType === 'ended') {
      parsedData = parseEndedAuction(auctionDataObj)
    } else {
      throw new Error(
                "auction parse error: " +
                    "unexpected auction status type '" +
                    auctionStatusType + "'")
    }
    if (!_.isInteger(parsedData.id)) {
      throw new Error(
                "auction parse error: " +
                    "didn't find integer id on parsed data")
    }
    return {
      id: parsedData.id,
      status: auctionStatusType,
      data: parsedData
    }
  }
}

var exports = makeParseAuction

module.exports = exports
