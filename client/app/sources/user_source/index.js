/* global require, module */

var _ = require('lodash')

var util = require('../../util')

var parseUser = require('./parse_user')

// duplicate server-side values in app.routes.user
const ERR_CODE_SALE = 99
const ERR_CODE_SCHEMA = 98

/**
 * see SaleResult.jsonErrorData in the app.braintree package
 */
function SaleError (data) {
  this.name = 'SaleError'
  this.data = data
  this.stack = (new Error()).stack
}
SaleError.prototype = Object.create(Error.prototype)
SaleError.prototype.constructor = SaleError

/**
 * error resulting from invalid schema on the server-side
 */
function SchemaError (data) {
  this.name = 'SchemaError'
  this.data = data
  this.stack = (new Error()).stack
}
SaleError.prototype = Object.create(Error.prototype)
SaleError.prototype.constructor = SaleError

const _checkForSaleError = function (res) {
  if (res && res.code &&
        _.isEqual(res.code, ERR_CODE_SALE)) {
    throw new SaleError(res.data)
  }
}

const _checkForSchemaError = function (res) {
  if (res && res.code &&
        _.isEqual(res.code, ERR_CODE_SCHEMA)) {
    throw new SchemaError(res.data)
  }
}

const fetchCurr = function () {
  return util.fetchJson(
        'api/user/get'
    ).then(function (user) {
      return parseUser(user)
    })
}

const createUserCreditPackage = function (creditPackageId, nonce) {
  return util.createJson(
        '/api/user/creditPackages/' + creditPackageId, {
          nonce: nonce
        }
    ).then(_checkForSaleError)
}

const createAuctionPayment = function (auctionId, nonce) {
  return util.createJson(
        'api/user/auctionPayments', {
          auctionId: auctionId,
          nonce: nonce
        }
    ).then(_checkForSaleError)
}

const updateAvatar = function (blobImg) {
  return util.updateFile(
        'api/user/avatar', blobImg, 'image/*'
    ).catch(function (err) {
      console.error("request error at " +
                      "api/user/avatar endpoint")
      throw err
    })
}

const createAuctionAddress = function (auctionId, addrData) {
  return util.createJson(
        'api/user/auctionAddress', {
          auctionId: auctionId,
          shippingAddress: addrData
        }
    ).then(_checkForSchemaError)
}

var exports = {}

exports.fetchCurr = fetchCurr
exports.createUserCreditPackage = createUserCreditPackage
exports.createAuctionPayment = createAuctionPayment
exports.updateAvatar = updateAvatar
exports.createAuctionAddress = createAuctionAddress

module.exports = exports
