/* global require, module */

const validator = require('../validator')

var parseUser = function (objUser) {
  var maybeValidationError = validator.user(
        objUser)
  if (maybeValidationError) {
    throw maybeValidationError
  }
  return objUser
}

var exports = {}

exports = parseUser

module.exports = exports
