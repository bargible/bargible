/* global require, module */

var _ = require('lodash')

var parseClientErr = function (errBody) {
  var fieldErrors = errBody.data
  _.values(fieldErrors).forEach(function (arrVal) {
    if (!Array.isArray(arrVal)) {
      throw new Error("field errors are expected to be arrays")
    }
    if (_.size(arrVal) === 0) {
      throw new Error("field errors expected to be nonempty")
    }
    arrVal.forEach(function (strVal) {
      if (typeof strVal !== 'string') {
        throw new Error("field error arrays expect to " +
                                "contain strings only")
      }
    })
  })
  return {
    fieldErrStrs: fieldErrors
  }
}

var exports = parseClientErr

module.exports = exports
