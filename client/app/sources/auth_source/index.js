/* global require, module */

var util = require('../../util')

var parseNextRes = require('./parse_next')
var parseClientErr = require('./parse_client_error')

var register = function (formFields) {
  return Promise.resolve().then(function () {
    return util.createJson('api/register', formFields)
  }).then(function (res) {
    return parseNextRes(res)
  }).catch(function (err) {
    if (err.name === 'HttpError' && err.statusCode === 400) {
      return parseClientErr(err.body)
    }
    throw err
  })
}

var login = function (formFields) {
  return Promise.resolve().then(function () {
    return util.createJson('api/login', formFields)
  }).then(function (res) {
    return parseNextRes(res)
  }).catch(function (err) {
    if (err.name === 'HttpError' && err.statusCode === 400) {
      return parseClientErr(err.body)
    }
    throw err
  })
}

var exports = {}

exports.register = register
exports.login = login

module.exports = exports
