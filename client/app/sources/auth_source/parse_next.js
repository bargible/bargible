/**
 * parse an http response indicating the next server-side
 * url to navigate to
 */
/* global require, module */

var _ = require('lodash')

var parseNextRes = function (res) {
  if (!_.isEqual(_.keys(res), ['next'])) {
    throw new Error("next res obj has unexpected keys: " +
                        _.keys(res).join(", "))
  }
  return {
    strUriNext: res.next.replace(/^\//, '')
  }
}

var exports = parseNextRes

module.exports = exports
