/* global require, module */

var util = require('../../util')

var subscribe = function (formFields) {
  var url = 'http://bargible.us13.list-manage.com/subscribe/post'
  var formData = {
    u: '135b9574fb153af1db9617f32',
    id: '7ca71e86f9',
    subscribe: 'Submit',
    EMAIL: formFields.subscribe,
    b_135b9574fb153af1db9617f32_7ca71e86f9: ''
  }
  return Promise.resolve().then(function () {
    return util.postFormData(url, formData)
  }).catch(function (err) {
    throw err
  })
}

var exports = {}

exports.subscribe = subscribe

module.exports = exports
