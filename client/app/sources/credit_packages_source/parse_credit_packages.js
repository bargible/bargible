/* global require, module */

const validator = require('../validator')

var parseCreditPackages = function (creditPackages) {
  var maybeValidationError = validator.creditPackages(
        creditPackages)
  if (maybeValidationError) {
    throw maybeValidationError
  }
  creditPackages.sort(function (a, b) {
    return a.price - b.price
  })
  return creditPackages
}

var exports = parseCreditPackages

module.exports = exports
