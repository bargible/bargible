/* global require, module */

var util = require('../../util')

var parseCreditPackages = require('./parse_credit_packages')

var fetchAll = function () {
  return util.fetchJson('api/creditPackages')
        .then(function (res) {
          if (!Array.isArray(res.creditPackages)) {
            throw new Error("api/creditPackages response object" +
                                "doesn't have creditPackages array")
          }
          return parseCreditPackages(res.creditPackages)
        })
}

var exports = {}

exports.fetchAll = fetchAll

module.exports = exports
