/* global require, module */

const Ajv = require('ajv')

const ajv = new Ajv()

class ValidationError extends Error {
  constructor (errors, text) {
    super()
    this.name = 'ValidationError'
    this.errors = errors
    this.text = text
  }
}

[require('./schemas/reward_item.json'),
  require('./schemas/reward_physical_item.json'),
  require('./schemas/game_element_counters.json')
].forEach(function (schemaDefn) {
  ajv.addSchema(schemaDefn)
});

[require('./schemas/auction_active.json'),
  require('./schemas/auction_upcoming.json'),
  require('./schemas/auction_joined.json'),
  require('./schemas/auction_ended.json'),
  require('./schemas/user.json'),
  require('./schemas/credit_packages.json'),
  require('./schemas/config.json')
].forEach(function (schemaDefn) {
  ajv.addSchema(schemaDefn)
})

var _makeValidator = function (schemaId) {
  return function (jsonObj) {
    var valid = ajv.validate(schemaId, jsonObj)
    if (!valid) {
      return new ValidationError(ajv.errors, ajv.text)
    }
    return null
  }
}

var exports = {}

exports.upcomingAuction = _makeValidator('#auction-upcoming')
exports.activeAuction = _makeValidator('#auction-active')
exports.joinedAuction = _makeValidator('#auction-joined')
exports.endedAuction = _makeValidator('#auction-ended')
exports.user = _makeValidator('#user')
exports.creditPackages = _makeValidator('#credit-packages')
exports.config = _makeValidator('#config')

module.exports = exports
