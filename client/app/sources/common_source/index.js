/* global require, module */

 /**
 * Source for generic endpoints that relate
 * to the application as a whole
 */

var util = require('../../util')

const validator = require('../validator')
const constants = require('../constants.json')

var fetchConfig = function () {
  return util.fetchJson('api/config')
    .then(function (config) {
      // TODO: configure based on database or env variables
      config.starLevels = {
        silver: constants.silverPrice,
        gold: constants.goldPrice,
        silverLevelUp: constants.silverLevelUp,
        goldLevelUp: constants.goldLevelUp
      }
      if (config.starLevels.silver > config.starLevels.gold) {
        throw new Error("Silver should not be greater than gold")
      }
      var maybeValidationError = validator.config(config)
      if (maybeValidationError) {
        throw maybeValidationError
      }
      return config
    })
}

var exports = {}

exports.fetchConfig = fetchConfig

module.exports = exports
