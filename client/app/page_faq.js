/* global require */

const _ = require('lodash')
const React = require('react')
const ReactDOM = require('react-dom')

const NavNode = require('./comp/nav')
const FaqMain = require('./comp/main_faq')

const AllPagesActions = require('./actions/all_pages_actions')
const NavActions = require('./actions/nav_actions.js')

const navNodeParams = require('./util').getNavNodeParams()

const qaItems = require('./strings/qa_items.json')

if (_.size(qaItems) !== _(qaItems).map('tag').uniq().size()) {
  throw new Error("QA items expected to have unique tags")
}

const renderFaq = function () {
  var main_component = React.createElement(FaqMain, {
    qaItems: qaItems
  })
  ReactDOM.render(main_component, document.getElementById('app'))
}

ReactDOM.render(React.createElement(NavNode, navNodeParams),
                document.getElementById('navbar'))
renderFaq()

NavActions.fetchCreditPackages()
AllPagesActions.fetchUser()
