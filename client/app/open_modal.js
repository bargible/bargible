/* global require, module */

const _ = require('lodash')
const React = require('react')
const ReactDOM = require('react-dom')

/**
 * modalClassOrFactory: either a react component (e.g. the return value
 *  React.createClass) or a factory function that returns a react
 *  component.
 *  the factory function is called with a single positional argument:
 *  a function to "close the modal" by removing it from the DOM tree.
 *
 * optArgs:  an object.  use truthy 'isFactory' attr to indicate
 *  modalClassOrFactory is a factory.
 */
const openModal = function (modalClassOrFactory, props, optArgs) {
  const args = optArgs || {}
  const elModalContent = document.getElementById('modal-content')
    // preserve the loading HTML so it can be reset upon close
  const innerHTMLLoading = elModalContent.innerHTML
  return new Promise(function (resolve, reject) {
    var elWrapper = document.getElementById('modal-wrapper')
        /** modalRes: use to return result from modal */
    var closeModal = function (modalRes) {
      elWrapper.hidden = true
      ReactDOM.unmountComponentAtNode(elModalContent)
      elModalContent.innerHTML = innerHTMLLoading
      document.getElementById('btn-modal-close')
                .removeEventListener('click', closeModalEventListener)
      resolve(modalRes)
    }
    var closeModalEventListener = function (ev) {
      closeModal()
    }
    var modalClass
    if (args.isFactory) {
      modalClass = modalClassOrFactory(closeModal)
    } else {
      modalClass = modalClassOrFactory
    }
    var modalComponent = React.createElement(
            modalClass,
            _.merge(_.cloneDeep(props), {closeModal: closeModal}))
    ReactDOM.render(modalComponent, elModalContent)
    document.getElementById('btn-modal-close')
            .addEventListener('click', closeModalEventListener)
    elWrapper.hidden = false
  })
}

var exports = openModal

module.exports = exports
