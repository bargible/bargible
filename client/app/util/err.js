/**
 * custom Error classes
 *
 * error typechecking uses `err.name` rather than
 * `err instanceof HttpError`
 */

/* global require, module, process */

const _ = require('lodash')

/**
 * HTTP error, typically out of sources/
 */
function HttpError (body, statusCode) {
  this.name = 'HttpError'
  this.body = body
  this.statusCode = statusCode
  this.stack = (new Error()).stack
}
HttpError.prototype = Object.create(Error.prototype)
HttpError.prototype.constructor = HttpError

/**
 * wraps errors related to braintree payments in order to expose
 * error details to template API (and elsewhere?)
 *
 * parameters:
 * - `errCause` -- the error class being wrapped
 *
 * properties:
 * - `type` (`string`) -- the type of error that occurred, one of
 *   > `'sale'`: the credit card transaction between the bargible
 *      server and the braintree server failed, possibly due to
 *      user input
 *   > `'fields'`: one of more of the fields entered in the payment form
 *      is invalid
 *   > `'client'`: any type of error other than an error with field
 *      between the user's browser and the Braintree server
 * - `invalidFieldKeys` (`Array(string)`) -- defined only when
 *   the `type` property is `'fields'`, these are the attrs of
 *   the hostedFields object's `_fields` attr that have errors..
 *   *for the template API, see invalidFieldIds below*
 * - `data` (`Object`) -- defined only when
 *   the `type` property is `'fields'`, this JS object
 *   is intended to contain more detailed error information
 *   for use in the template api.
 *   *except for properties beginning with an underscore*
 * as usual, attributes/properties beginning with an underscore
 * are to be viewed as internal implementation details, not part
 * of the object's public interface.
 */
var PaymentError = function (errCause) {
  if (_.isEqual(errCause.name, 'SaleError')) {
    this.type = 'sale'
    this.data = errCause.data
  } else if (_.isEqual(errCause.name, 'BraintreeError')) {
    if (errCause.code === 'HOSTED_FIELDS_FIELDS_INVALID') {
      this.type = 'fields'
      this.invalidFields = errCause.details.invalidFieldKeys
    } else {
      this.type = 'client'
    }
  } else {
    throw new Error("implementation error: " +
                        "unsupported error type for PaymentError, " +
                        "'" + errCause.name + "'")
  }
  this._cause = errCause
  this.stack = (new Error()).stack
}

/**
 * "glue" for the template API
 * return (`Array(String)`): html ids corresponding to invalid fields
 *  or an empty array if the error is not due to invalid fields ..
 *  .. in particular, note that empty arrays are truthy in JS, so
 *  testing the truthiness of the return value of this function
 *  will not accurately determine whether the error is due to invalid
 *  fields
 */
PaymentError.prototype.invalidFieldIds = function () {
  if (!this.hostedFields) {
    return []
  }
  return _.map(this.invalidFields, function (fieldAttrName) {
    return _(
            fieldAttrName
                .replace(/([A-Z])/g, '-$1')
                .split('-')
        ).map(_.lowerCase).join('-')
  })
}

/** errors related to shipping address */
var AddressError = function (errCause) {
  if (_.isEqual(errCause.name, 'SchemaError')) {
    this._data = errCause.data
  } else {
    throw new Error("implementation error: " +
                        "unsupported error type for AddressError, " +
                        "'" + errCause.name + "'")
  }
  this.stack = (new Error()).stack
}

var exports = {}

exports.HttpError = HttpError
exports.PaymentError = PaymentError
exports.AddressError = AddressError

module.exports = exports
