/* global require, module */

const braintree = require('braintree-web')

var createClient = function (authorization) {
  return new Promise(function (resolve, reject) {
    braintree.client.create({
      authorization: authorization
    }, function (err, client) {
      if (err) {
        reject(err)
      } else {
        resolve(client)
      }
    })
  })
}

var createHostedFieldsFromClient = function (
    clientInstance, styles, fields) {
  return new Promise(function (resolve, reject) {
    braintree.hostedFields.create({
      client: clientInstance,
      styles: styles,
      fields: fields
    }, function (err, hostedFieldsInstance) {
      if (err) {
        reject(err)
      } else {
        resolve(hostedFieldsInstance)
      }
    })
  })
}

var createHostedFields = function (clientToken, styles, fields) {
  return Promise.resolve().then(function () {
    return createClient(clientToken)
  }).then(function (client) {
    return createHostedFieldsFromClient(
            client, styles, fields)
  })
}

var getNonce = function (hostedFields) {
  return new Promise(function (resolve, reject) {
    hostedFields.tokenize(function (err, payload) {
      if (err) {
        reject(err)
      } else {
        resolve(payload.nonce)
      }
    })
  })
}

var exports = {}

exports.createHostedFields = createHostedFields
exports.getNonce = getNonce

module.exports = exports
