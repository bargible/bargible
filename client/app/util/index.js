/**
 * utility functions.
 */

/* global require, module, process */

var utilHttp = require('./http')
const braintree = require('./braintree')

/**
 * "client-side" routing: does an actual GET for impl where
 *   all routes are on server-side
 *
 * strUri (str): a url path relative to the server (i.e. a route)
 *   _without_ a preceeding '/' matches http utility function api
 */
var navigate = function (strUri) {
  window.location.pathname = '/' + strUri
}

/**
 * get parameters (e.g. page urls, but not HTTP API endpoint urls)
 * for the ~~~~~~~~ non~index ~~~~~~~~~ navigation component
 * out of the markdown rendered on the server-side
 */
var getNavNodeParams = function () {
  const navbarElement = document.getElementById('navbar')
  return {
    navLinks: JSON.parse(navbarElement.dataset.navLinks),
    btClientToken: navbarElement.dataset.btClientToken
  }
}

var getAppNodeParams = function () {
  const appElement = document.getElementById('app')
  return {
    btClientToken: appElement.dataset.btClientToken
  }
}

var exports = {}

exports.fetchJson = utilHttp.fetchJson
exports.createJson = utilHttp.createJson
exports.updateJson = utilHttp.updateJson
exports.fetchFile = utilHttp.fetchFile
exports.updateFile = utilHttp.updateFile
exports.postFormData = utilHttp.postFormData
exports.navigate = navigate
exports.getNavNodeParams = getNavNodeParams
exports.getAppNodeParams = getAppNodeParams
exports.braintree = braintree

module.exports = exports
