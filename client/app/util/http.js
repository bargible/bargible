/**
 * http transport layer utility functions.
 *
 * strUri (str): a url path relative to the server (i.e. a route)
 *   _without_ a preceeding '/'
 */

/* global require, module, process, Blob */

var nets = require('nets')
const HttpError = require('./err').HttpError

var URL_PREFIX = ''
if (!process.browser) { // for testing
  URL_PREFIX = 'http://example.com/'
}

var fetchJson = function (strUri) {
  return new Promise(function (resolve, reject) {
    nets({
      url: URL_PREFIX + strUri,
      method: 'GET',
      jar: true, // send cookie header
      json: true,
      encoding: undefined
    }, function (err, resp, body) {
      if (err) {
        reject(err)
      } else if (resp.statusCode !== 200) {
        reject(new HttpError(body, resp.statusCode))
      } else {
        resolve(body)
      }
    })
  })
}

var postFormData = function (url, object) {
  return new Promise(function (resolve, reject) {
    url = url + '?'
    url = url + Object.keys(object)
            .map(function (key) {
              return encodeURIComponent(key) + '=' + encodeURIComponent(object[key])
            })
            .join('&')
    window.open(url)
    resolve()
  })
}

var createJson = function (strUri, json) {
  return new Promise(function (resolve, reject) {
    nets({
      url: URL_PREFIX + strUri,
      method: 'POST',
      jar: true, // send cookie header
      json: json || {},
      encoding: undefined
    }, function (err, resp, body) {
      if (err) {
        reject(err)
      } else if (resp.statusCode !== 200) {
        reject(new HttpError(body, resp.statusCode))
      } else {
        resolve(body)
      }
    })
  })
}

var updateJson = function (strUri, json) {
  return new Promise(function (resolve, reject) {
    nets({
      url: URL_PREFIX + strUri,
      method: 'PUT',
      jar: true, // send cookie header
      json: json || {},
      encoding: undefined
    }, function (err, resp, body) {
      if (err) {
        reject(err)
      } else if (resp.statusCode !== 200) {
        reject(new HttpError(body, resp.statusCode))
      } else {
        resolve(body)
      }
    })
  })
}

/**
 * returns a Blob instance
 */
var fetchFile = function (strUri, mimeType) {
  return new Promise(function (resolve, reject) {
    nets({
      url: URL_PREFIX + strUri,
      method: 'GET',
      jar: true // send cookie header
    }, function (err, resp, body) {
      if (err) {
        reject(err)
      } else if (resp.statusCode !== 200) {
        reject(new HttpError(body, resp.statusCode))
      } else {
        resolve(new Blob(
                    [body], {
                      type: mimeType
                    }))
      }
    })
  })
}

var updateFile = function (strUri, blobFile, mimeType) {
  var reqHeaders = {}
  if (mimeType) {
    reqHeaders['Content-Type'] = mimeType
  }
  return new Promise(function (resolve, reject) {
    nets({
      url: URL_PREFIX + strUri,
      method: 'PUT',
      jar: true, // send cookie header
      responseType: "json",
      encoding: undefined,
      body: blobFile,
      headers: reqHeaders
    }, function (err, resp, body) {
      if (err) {
        reject(err)
      } else if (resp.statusCode !== 200) {
        reject(new HttpError(body, resp.statusCode))
      } else {
        resolve(body)
      }
    })
  })
}

var exports = {}

exports.fetchJson = fetchJson
exports.createJson = createJson
exports.updateJson = updateJson
exports.fetchFile = fetchFile
exports.updateFile = updateFile
exports.postFormData = postFormData

module.exports = exports
