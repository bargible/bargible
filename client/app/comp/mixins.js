/**
 * https://facebook.github.io/react/docs/reusable-components.html#mixins
 */

/* global require, module, setInterval, clearInterval */

var SetIntervalMixin = {
  componentWillMount: function () {
    this.intervals = []
  },

  setInterval: function () {
    this.intervals.push(setInterval.apply(null, arguments))
  },

  componentWillUnmount: function () {
    this.intervals.forEach(clearInterval)
  }
}

var exports = {}

exports.SetIntervalMixin = SetIntervalMixin

module.exports = exports
