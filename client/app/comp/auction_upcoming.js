/* global require, module */

const React = require('react')

const HomeActions = require('../actions/home_actions')
const openModal = require('../open_modal')
const makeFactoryGenericModal = require('./modal_generic')

const rt_auction_upcoming = require('../templates/auction_upcoming.rt')
const rtWatchConfirm = require('../templates/modal_watch_confirm.rt')

const UpcomingAuctionNode = React.createClass({

  getInitialState: function () {
    return {
      showDescription: false
    }
  },

  componentWillReceiveProps: function (nextProps) {
    const currProps = this.props
    if (nextProps.auction.is_watching &&
            !currProps.auction.is_watching) {
      openModal(
                makeFactoryGenericModal(rtWatchConfirm), {
                }, {isFactory: true})
    }
  },

  handleWatchClick: function () {
    if (!(this.props.auction.is_watching ||
              this.props.auction.isLoadingWatch)) {
      HomeActions.watchAuction(this.props.auction.id)
    }
  },

  toggleDescription: function () {
    this.setState(function (prevState) {
      return {showDescription: !prevState.showDescription}
    })
  },

  render: rt_auction_upcoming

})

var exports = UpcomingAuctionNode
module.exports = exports
