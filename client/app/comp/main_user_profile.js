/* global require, module */

var _ = require('lodash')
var React = require('react')

var rtUserProfile = require('../templates/user_profile.rt')
var UserProfileActions = require('../actions/user_profile_actions')

var UserProfileMain = React.createClass({

  getInitialState: function () {
    return {}
  },

    /** new avatar edit interface */
  handleAvatarSelect: function (ev) {
    UserProfileActions.updateAvatar(
            ev.nativeEvent.target.files[0].slice())
  },

  numAuctions: function () {
    return _.size(this.props.auctionHistories)
  },

  numAuctionsWon: function () {
    return (this.props.auctionHistories
                .filter(function (auctionHistory) {
                  return auctionHistory.position === 1
                }).length)
  },

  getLeftAuctions: function () {
    return this.props.auctionHistories.filter(function (auction, index) {
      return index % 2 === 1
    })
  },

  getRightAuctions: function () {
    return this.props.auctionHistories.filter(function (auction, index) {
      return index % 2 === 0
    })
  },

  useOffsetClass: function (history) {
    return (this.props.auctionHistories[1] &&
                this.props.auctionHistories[1].auctionId !== history.auctionId)
  },

  hasAuctionHistories: function () {
    return this.props.auctionHistories.length !== 0
  },

  render: rtUserProfile
})

var exports = UserProfileMain

module.exports = exports
