/* global require, module */

var _ = require('lodash')
var React = require('react')
var RegisterModal = require('./modal_register')
const openModal = require('../open_modal')

var IndexActions = require('../actions/index_actions')
var rt_index = require('../templates/index.rt')

var IndexNode = React.createClass({
  getInitialState: function () {
    return {
      loading: false,
      formFields: {
        subscribe: ''
      },
      fieldErrStrs: {}
    }
  },

  makeChangeHandler: function (formFieldVarName) {
    var self = this
    return function (ev) {
      var updatedVal = ev.target.value
      self.setState(function (prevState) {
        var formFields = prevState.formFields
        formFields[formFieldVarName] = updatedVal
        return {formFields: formFields}
      })
    }
  },

  handleClickRegister: function () {
    openModal(RegisterModal, {})
  },

  handleSubscribeSubmit: function (event) {
    event.preventDefault()
    var formFields = _.cloneDeep(this.state.formFields)
    IndexActions.subscribe(formFields)
  },

  render: rt_index
})

var exports = IndexNode
module.exports = exports
