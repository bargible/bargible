/* global require, module */

var _ = require('lodash')
var moment = require('moment')
var React = require('react')

var HomeActions = require('../actions/home_actions')
var mixins = require('./mixins')
var getAnimationUrls = require('./util').getAnimationUrls

var rt_auction_joined = require('../templates/auction_joined.rt')

var TIMEOUT_DISPLAY_NOTIFICIATION = 5000 // ms
var TIMEOUT_TILE_FLASH = 2000 // ms

var _update2Notification = function (update) {
  var updateType = update.type
  var updateInfo = update.info
  var notificationStr
  if (updateType === 'bid-mult') {
    if (updateInfo.complete) {
      notificationStr = updateInfo.username +
                " got the bidding bonus!"
    }
  } else if (updateType === 'froze-bids') {
    notificationStr = "you froze bids from " +
            updateInfo.usernames.join(", ")
  } else if (updateType === 'bids-freeze') {
    notificationStr = updateInfo.username + " froze your bids"
  } else if (updateType === 'first-bid') {
    notificationStr = "You're the first bidder! 10 extra bids."
  } else if (updateType === 'destroy') {
    if (updateInfo.numBids === 1) {
      notificationStr = "You had a bid destroyed by " +
                updateInfo.username + "!"
    } else {
      notificationStr = "You had " + updateInfo.numBids +
                " bids destroyed by " + updateInfo.username + "!"
    }
  } else if (updateType === 'destroy-self') {
    if (_.size(updateInfo.usernames) === 0) {
      notificationStr = "You destroyed no bids!"
    } else {
      notificationStr = "You destroyed bids for " +
                updateInfo.usernames.join(", ") + "!"
    }
  } else if (updateType === 'steal') {
    if (updateInfo.numBids === 1) {
      notificationStr = "You had a bid stolen by " +
                updateInfo.username + "!"
    } else {
      notificationStr = "You had " + updateInfo.numBids +
                " bids stolen by " + updateInfo.username + "!"
    }
  } else if (updateType === 'steal-self') {
    if (updateInfo.numBids === 0) {
      notificationStr = "You stole 0 bids from players!"
    } else {
      notificationStr = "You stole " + updateInfo.numBids + " bid" +
        (updateInfo.numBids === 1 ? '' : 's') +
        " from " + updateInfo.usernames.join(", ") + "!"
    }
  }
  if (!notificationStr) {
    return null
  }
  return {
    str: notificationStr,
    id: update.id
  }
}

/**
 * make the parts of component state that are based on current time
 * auction (joinedAuction): single joined auction instance as
 *   exposed by alt continer
 * now (moment): a moment w/ the current time
 *
 * return: an object to be merged into component state with all
 *   state variables that contain the current time
 */
var _makeNowState = function (auction, now) {
  return {
        // expected each to have 'str' and 'id' properties
    notifications: _(auction.updates)
            .filter(function (update) {
              return now - update.createdAt <
                    TIMEOUT_DISPLAY_NOTIFICIATION
            }).sortBy('createdAt')
            .map(_update2Notification).filter(null)
            .value(),
    tileFlashUpdateType: _(auction.updates)
            .filter(function (update) {
              return now - update.createdAt <
                    TIMEOUT_TILE_FLASH
            }).sortBy('createdAt')
            .map('type').filter(function (updateType) {
              return _.includes([
                'bids-freeze',
                'destroy',
                'steal'
              ], updateType)
            }).reverse()
            .value()[0],
    timeRemaining: moment.duration(
            auction.ultTimeEnd - now),
    intermediateTimer: moment.duration(
            auction.intTimeEnd - now)
  }
}

var JoinedAuctionNode = React.createClass({
  mixins: [mixins.SetIntervalMixin],

  getInitialState: function () {
    return _.merge({
      animDestroyFrameIdx: null,
      animStealFrameIdx: null,
      animThawFrameIdx: null,
      animFreezeFrameIdx: null
    }, _makeNowState(this.props.auction, moment()), function () {
      throw new Error("attempted to set duplicate attributes " +
                            "in joined auction component state")
    })
  },

  componentWillMount: function () {
    this.animationUrls = getAnimationUrls()
  },

  componentDidMount: function () {
    this.setInterval(this.tick, 1000)
    this.setInterval(this.animFreeze, this.props.animFreezeFramerate)
    this.setInterval(this.animThaw, this.props.animThawFramerate)
    this.setInterval(this.animSteal, this.props.animStealFramerate)
    this.setInterval(this.animDestroy, this.props.animDestroyFramerate)
  },

  tick: function () {
    this.setState(_makeNowState(this.props.auction, moment()))
  },

  componentWillReceiveProps: function (nextProps) {
    var currProps = this.props
    var numFreezeAnimationUrls = _.size(this.animationUrls.freeze)
    var numThawAnimationUrls = _.size(this.animationUrls.thaw)
    var numStealAnimationUrls = _.size(this.animationUrls.steal)
    var numDestroyAnimationUrls = _.size(this.animationUrls.destroy)
    var newUpdates = (function () {
      var newUpdateIds = _.difference(
                _.map(nextProps.auction.updates, 'id'),
                _.map(currProps.auction.updates, 'id'))
      return _.filter(nextProps.auction.updates, function (update) {
        return _.includes(newUpdateIds, update.id)
      })
    }())
    if (nextProps.auction.isBidsFrozen &&
            (!this.props.auction.isBidsFrozen)) {
      if (numFreezeAnimationUrls === 0) {
        console.warn("freeze animation urls not set")
      } else {
        this.setState({
          animFreezeFrameIdx: numFreezeAnimationUrls - 1
        })
      }
    }
    if ((!nextProps.auction.isBidsFrozen) &&
            this.props.auction.isBidsFrozen) {
      if (numThawAnimationUrls === 0) {
        console.warn("thaw animation urls not set")
      } else {
        this.setState({
          animThawFrameIdx: numThawAnimationUrls - 1
        })
      }
    }
    if (_.find(newUpdates, {type: 'steal'})) {
      if (numStealAnimationUrls === 0) {
        console.warn("steal animation urls not set")
      } else {
        this.setState({
          animStealFrameIdx: numStealAnimationUrls - 1
        })
      }
    }
    if (_.find(newUpdates, {type: 'destroy'})) {
      if (numDestroyAnimationUrls === 0) {
        console.warn("destroy animation urls not set")
      } else {
        this.setState({
          animDestroyFrameIdx: numDestroyAnimationUrls - 1
        })
      }
    }
    this.setState(_makeNowState(nextProps.auction, moment()))
  },

  animFreeze: function () {
    this.setState(function (prevState) {
      var prevAnimFreezeFrameIdx = prevState.animFreezeFrameIdx
      if (_.isNull(prevAnimFreezeFrameIdx)) {
        return {}
      }
      if (prevAnimFreezeFrameIdx === 0) {
        return {animFreezeFrameIdx: null}
      }
      return {animFreezeFrameIdx: prevAnimFreezeFrameIdx - 1}
    })
  },

  animThaw: function () {
    this.setState(function (prevState) {
      var prevAnimThawFrameIdx = prevState.animThawFrameIdx
      if (_.isNull(prevAnimThawFrameIdx)) {
        return {}
      }
      if (prevAnimThawFrameIdx === 0) {
        return {animThawFrameIdx: null}
      }
      return {animThawFrameIdx: prevAnimThawFrameIdx - 1}
    })
  },

  animSteal: function () {
    this.setState(function (prevState) {
      var prevAnimStealFrameIdx = prevState.animStealFrameIdx
      if (_.isNull(prevAnimStealFrameIdx)) {
        return {}
      }
      if (prevAnimStealFrameIdx === 0) {
        return {animStealFrameIdx: null}
      }
      return {animStealFrameIdx: prevAnimStealFrameIdx - 1}
    })
  },

  animDestroy: function () {
    this.setState(function (prevState) {
      var prevAnimDestroyFrameIdx = prevState.animDestroyFrameIdx
      if (_.isNull(prevAnimDestroyFrameIdx)) {
        return {}
      }
      if (prevAnimDestroyFrameIdx === 0) {
        return {animDestroyFrameIdx: null}
      }
      return {animDestroyFrameIdx: prevAnimDestroyFrameIdx - 1}
    })
  },

  freezePercentage: function () {
    return Math.floor(100 * ((this.props.config.untilFreeze - this.props.auction.bidsTilActivation.freeze) /
                                 this.props.config.untilFreeze))
  },

  destroyPercentage: function () {
    return Math.floor(100 * ((this.props.config.untilDestroy - this.props.auction.bidsTilActivation.destroy) /
                                 this.props.config.untilDestroy))
  },

  stealPercentage: function () {
    return Math.floor(100 * ((this.props.config.untilSteal - this.props.auction.bidsTilActivation.steal) /
                                 this.props.config.untilSteal))
  },

  bidPercentage: function () {
    return Math.floor(100 * (this.props.auction.bids /
                                 this.props.config.initNumBids))
  },

    /// // callbacks //

  steal: function () {
    HomeActions.stealBids(this.props.auction)
  },

  destroy: function () {
    HomeActions.destroyBids(this.props.auction)
  },

  freeze: function () {
    HomeActions.freezeBids(this.props.auction)
  },

  bid: function () {
    HomeActions.bid(this.props.auction)
  },

  render: rt_auction_joined
})

var exports = JoinedAuctionNode
module.exports = exports
