/* global require, module */

var _ = require('lodash')
var React = require('react')

var AuthSource = require('../sources/auth_source')
var navigate = require('../util').navigate

var rt_login = require('../templates/login.rt')

var LoginModal = React.createClass({
  getInitialState: function () {
    return {
      loading: false,
      formFields: {
        usernameOrEmail: '',
        password: ''
      },
      fieldErrStrs: {}
    }
  },

  makeChangeHandler: function (formFieldVarName) {
    var self = this
    return function (ev) {
      var updatedVal = ev.target.value
      self.setState(function (prevState) {
        var formFields = prevState.formFields
        formFields[formFieldVarName] = updatedVal
        return {formFields: formFields}
      })
    }
  },

  handleConfirmClick: function () {
    var formFields = _.cloneDeep(this.state.formFields)
    var self = this
    if (this.state.loading) {
      return
    }
    this.setState({loading: true})
    Promise.resolve().then(function () {
      return AuthSource.login(formFields)
    }).then(function (res) {
      if (res.strUriNext) {
        navigate(res.strUriNext)
      } else if (res.fieldErrStrs) { // parsed client error
        self.setState({
          fieldErrStrs: res.fieldErrStrs,
          loading: false
        })
      } else {
        self.setState({loading: false})
        console.error(res)
        throw new Error("unexpected res from auth source login")
      }
    }).catch(function (err) {
      self.setState({loading: false})
      console.error(err.stack)
      console.error(err)
      throw new Error("unexpected err from auth source login")
    })
  },

  render: rt_login
})

var exports = LoginModal
module.exports = exports
