/* global require, module */

var _ = require('lodash')
var moment = require('moment')
var React = require('react')

var NavStore = require('../stores/nav_store')
var HomeActions = require('../actions/home_actions')
var mixins = require('./mixins')
var factoryBuyCreditsModal = require('./modal_buy_credits')
const openModal = require('../open_modal')

var rt_auction_active = require('../templates/auction_active.rt')

/**
 * ultTimeEnd (moment): the ultimate timer end
 *
 * return: moment.duration

TODO: find out if this function is needed
var _makeTimeRemaining = function (ultTimeEnd) {
  return moment.duration(moment() - ultTimeEnd)
}
 */

var ActiveAuctionNode = React.createClass({
  mixins: [mixins.SetIntervalMixin],

  getInitialState: function () {
    console.log(rt_auction_active)
    return {
            // whether the join confirmation dialog is active
      joinConfirm: false,
      timeRemaining: this.props.auction.ultTimeEnd - moment() > 0 ? moment.duration(
                this.props.auction.ultTimeEnd - moment()) : moment.duration(0),
      showDescription: false
    }
  },
  componentDidMount: function () {
    this.setInterval(this.tick, 1000)
  },

    /** part of the SetIntervalMixin api, not the template api */
  tick: function () {
    this.setState({
      timeRemaining: this.props.auction.ultTimeEnd - moment() > 0 ? moment.duration(
                this.props.auction.ultTimeEnd - moment()) : moment.duration(0)
    })
  },

    /**
     * all logic about whether a user can join the auction.
     * currently, only checks whether the maximum number of users has
     * been exceeded.
     */
  canJoin: function () {
        // TODO: disable joining auctions above accessible star level
        // Maybe this.props.auction.level
    return this.props.auction.numUsers <
            this.props.config.maxUsersPerAuction
  },

    /** handle first join button click on auction tile,
     *  _not_ the join button click in the confirmation dialog
     */
  handleJoinClick: function () {
    if (this.state.joinConfirm) {
      return
    }
    if (!this.canJoin()) {
      console.warn("attempted to join an auction that can't " +
                         "be joined, likely b/c the maximum number " +
                         "of users per auction has been reached")
      return
    }
    if (NavStore.getState().credit <
            this.props.auction.joinCost) {
      this._openBuyCreditsModal()
      return
    }
    this.setState({joinConfirm: true})
  },

    // duplicates NavNode.openBuyCreditsModal
  _openBuyCreditsModal: function () {
    var keyReplacements = {
      'credit': 'userCredits'
    }
    var modalProps = _(NavStore.getState()).pick([
      'credit',
      'creditPackages'
    ]).mapKeys(function (val, key) {
      if (_.includes(_.keys(keyReplacements), key)) {
        return keyReplacements[key]
      }
      return key
    }).value()
    openModal(factoryBuyCreditsModal,
                  _.merge(_.cloneDeep(modalProps), {
                    onJoin: true,
                    btClientToken: this.props.params.btClientToken
                  }), {
                    isFactory: true
                  })
  },

    /** handle the join button click in the confirmation dialog */
  handleJoinConfirmClick: function () {
    HomeActions.joinAuction(this.props.auction.id)
    this.setState({joinConfirm: false})
    this.props.selectJoinedTab()
  },

    /** handle the cancel button click in the confirmation dialog */
  handleJoinCancelClick: function () {
    this.setState({joinConfirm: false})
  },

  toggleDescription: function () {
    this.setState(function (prevState) {
      return {showDescription: !prevState.showDescription}
    })
  },

  render: rt_auction_active
})

var exports = ActiveAuctionNode
module.exports = exports
