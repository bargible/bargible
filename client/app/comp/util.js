/* global require, module, URL */

var _ = require('lodash')

/**
 * each animation corresponds to an array of objects
 * with attrs
 * - 'blob' - a file Blob w/ img data for each frame in the animation
 * - 'url' - an blob: url set by URL.createObjectURL for the blob
 *           object in the 'blob' attr
 *
 * note, in particular, that
 * >>>> animation frames are sorted from last to first <<<<
 */
var animations = {
  freeze: [],
  thaw: [],
  steal: [],
  destroy: []
}

/**
 * setter function for animations, which are immutable once set.
 * - arrBlobs - Blob w/ img data for animation frames in forward order
 */
var setAnimation = function (animationName, arrBlobs) {
  if (!_.includes(_.keys(animations), animationName)) {
    throw new Error("unrecognized animation name " +
                        animationName + " when trying to set " +
                        "blob data")
  }
  if (_.size(animations[animationName])) {
    throw new Error("attempted to set " + animationName +
                        " blob data twice")
  }
  animations[animationName] = _.map(arrBlobs, function (blob) {
    return {
      blob: blob,
      url: URL.createObjectURL(blob)
    }
  })
  _.reverse(animations[animationName])
  animations[animationName].lastFrame =
        _.head(animations[animationName])
}

var getAnimationUrls = function () {
  return _.mapValues(animations, function (arrAnim, animationName) {
    var urlArr = _.map(arrAnim, 'url')
    urlArr.lastFrame = arrAnim.lastFrame && arrAnim.lastFrame.url
    return urlArr
  })
}

var exports = {}

exports.setAnimation = setAnimation
exports.getAnimationUrls = getAnimationUrls

module.exports = exports
