/* global require, module */

var React = require('react')

var RegisterModal = require('./modal_register')
var LoginModal = require('./modal_login')
const openModal = require('../open_modal')

var rt_nav_index = require('../templates/nav_index.rt')

var NavIndexNode = React.createClass({

  handleClickRegister: function () {
    openModal(RegisterModal, {})
  },

  handleClickLogin: function () {
    openModal(LoginModal, {})
  },

  render: rt_nav_index
})

var exports = NavIndexNode
module.exports = exports
