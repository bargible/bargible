/* global require, module */

var React = require('react')
var _ = require('lodash')

var NavStore = require('../stores/nav_store')
var rtDefaultView = require('../templates/default_view.rt')
var factoryBuyCreditsModal = require('./modal_buy_credits')
const openModal = require('../open_modal')

var DefaultView = React.createClass({

  getInitialState: function () {
    return {}
  },

  handleLiveClick: function () {
    this.props.selectActiveTab()
  },

  handleUpcomingClick: function () {
    this.props.selectUpcomingTab()
  },

    // duplicates NavNode.openBuyCreditsModal
  openBuyCreditsModal: function () {
    var keyReplacements = {
      'credit': 'userCredits'
    }
    var modalProps = _(NavStore.getState()).pick([
      'credit',
      'creditPackages'
    ]).mapKeys(function (val, key) {
      if (_.includes(_.keys(keyReplacements), key)) {
        return keyReplacements[key]
      }
      return key
    }).value()
    openModal(factoryBuyCreditsModal,
                  _.merge(_.cloneDeep(modalProps), {
                    btClientToken: this.props.params.btClientToken
                  }), {
                    isFactory: true
                  })
  },

  render: rtDefaultView
})

var exports = DefaultView
module.exports = exports
