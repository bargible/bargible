/* global require, module */

const React = require('react')
const _ = require('lodash')

const braintree = require('../util').braintree
const createAuctionPayment = require(
    '../sources/user_source')
        .createAuctionPayment
const PaymentError = require('../util/err').PaymentError

const rtModalAuctionEndPay = require(
    '../templates/modal_auction_end_pay.rt')

const auctionEndPayStyles = require(
    '../templates/auction_end_pay_styles.json')
const auctionEndPayFields = require(
    '../templates/auction_end_pay_fields.json')

const factoryAuctionEndPayModal = function (closeModal) {
  return React.createClass({
    closeModal: closeModal,

    getInitialState: function () {
      return {
        loadingHostedFields: true,
        hostedFields: null,
        paymentError: null
      }
    },

    componentDidMount: function () {
            // hosted fields setup requires render
      this._setupHostedFields()
    },

    _setupHostedFields: function () {
      const comp = this
      comp.setState({loadingHostedFields: true})
      var cbFinally = function () {
        comp.setState({loadingHostedFields: false})
      }
      Promise.resolve().then(function () {
        return braintree.createHostedFields(
                    comp.props.btClientToken,
                    auctionEndPayStyles,
                    auctionEndPayFields
                )
      }).then(function (hostedFields) {
        comp.setState({hostedFields: hostedFields})
      }).catch(function (err) {
        console.error("braintree setup error")
        console.err(err)
      }).then(cbFinally, cbFinally)
    },

    onClickPay: function () {
      var comp = this
      Promise.resolve().then(function () {
        return braintree.getNonce(comp.state.hostedFields)
      }).then(function (nonce) {
        return createAuctionPayment(
                    comp.props.auction.id, nonce)
      }).then(function (res) {
        comp.closeModal({hasPaid: true})
      }).catch(function (err) {
        if (_.isEqual(err.name, 'SaleError') ||
                    _.isEqual(err.name, 'BraintreeError')) {
          comp.setState({
            paymentError: new PaymentError(err)
          })
        } else {
          console.error("unexpected payment error")
          console.error(err.stack)
          console.error(err)
        }
      })
    },

    render: rtModalAuctionEndPay
  })
}

var exports = factoryAuctionEndPayModal
module.exports = exports
