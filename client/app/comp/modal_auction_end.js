/* global require, module */

var React = require('react')

var rtModalAuctionEnd = require('../templates/modal_auction_end.rt')

var factoryAuctionEndModal = function (closeModal) {
  return React.createClass({
    closeModal: closeModal,

    handleClickPay: function () {
      closeModal({clickedPay: true})
    },

    render: rtModalAuctionEnd
  })
}

var exports = factoryAuctionEndModal
module.exports = exports
