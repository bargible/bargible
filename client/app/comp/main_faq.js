/* global require, module */

var _ = require('lodash')
var React = require('react')

var rtFaq = require('../templates/faq.rt')

var FaqNode = React.createClass({
  getInitialState: function () {
    return {
      expandedTags: []
    }
  },

    /** Make Handler for qa item toggle click */
  mhToggleQaItem: function (qaItem) {
    var self = this
    var qaItemTag = qaItem.tag
    return function () {
      self.setState(function (prevState) {
        var expandedTags = prevState.expandedTags
        if (_.includes(expandedTags, qaItemTag)) {
          expandedTags =
                        _.reject(expandedTags, _.matches(qaItemTag))
        } else {
          expandedTags.push(qaItemTag)
        }
        return {expandedTags: expandedTags}
      })
    }
  },

  isAnswerDisp: function (qaItem) {
    return _.includes(this.state.expandedTags, qaItem.tag)
  },

  render: rtFaq
})

var exports = FaqNode
module.exports = exports
