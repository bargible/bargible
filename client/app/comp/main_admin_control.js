/* global require, module */

var React = require('react')

var rtAdminControl = require(
    '../templates/admin_control.rt')

var AdminControlMain = React.createClass({

  getInitialState: function () {
    return {}
  },

  render: rtAdminControl
})

var exports = AdminControlMain

module.exports = exports
