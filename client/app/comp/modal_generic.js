/* global require, module */

var React = require('react')

var makeFactoryGenericModal = function (renderFn) {
  return function (closeModal) {
    return React.createClass({
      closeModal: closeModal,
      render: renderFn
    })
  }
}

var exports = makeFactoryGenericModal
module.exports = exports
