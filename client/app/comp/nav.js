/* global require, module */

var _ = require('lodash')
var React = require('react')

var NavStore = require('../stores/nav_store')
const openModal = require('../open_modal')
var factoryBuyCreditsModal = require('./modal_buy_credits')

var rtNav = require('../templates/nav.rt')

var NavNode = React.createClass({

  getInitialState: function () {
    return NavStore.getState()
  },

  componentDidMount: function () {
    NavStore.listen(this.onStoreChange)
  },

  componentWillUnmount: function () {
    NavStore.unlisten(this.onStoreChange)
  },

  openBuyCreditsModal: function () {
    var keyReplacements = {
      'credit': 'userCredits'
    }
    var modalProps = _(NavStore.getState()).pick([
      'credit',
      'creditPackages'
    ]).mapKeys(function (val, key) {
      if (_.includes(_.keys(keyReplacements), key)) {
        return keyReplacements[key]
      }
      return key
    }).value()
    openModal(factoryBuyCreditsModal,
                  _.merge(_.cloneDeep(modalProps), {
                    btClientToken: this.props.btClientToken
                  }), {
                    isFactory: true
                  })
  },

    /** handle change events from alt store(s) */
  onStoreChange: function (newStoreState) {
    this.setState(newStoreState)
  },

  render: rtNav
})

var exports = NavNode
module.exports = exports
