/* global require, module */

const React = require('react')

const braintree = require('../util').braintree
const createUserCreditPackage = require(
    '../sources/user_source')
          .createUserCreditPackage
const AllPagesActions = require('../actions/all_pages_actions')

const rtModalBuyCredit = require('../templates/modal_buy_credits.rt')

const factoryBuyCreditsModal = function (closeModal) {
  return React.createClass({
    closeModal: closeModal,

    getInitialState: function () {
      return {
        loadingHostedFields: true,
        hostedFields: null,
        loadingPurchase: false,
        purchasedCreditPackage: null,
        purchaseError: null,
        currCreditPackage: null
      }
    },

    isCreditPackageSelected: function (creditPackage) {
      return this.state.currCreditPackage && (
                creditPackage.id === this.state.currCreditPackage.id
            )
    },

    makeHandleClickPackage: function (creditPackage) {
      var comp = this
      return function () {
        comp.setState({currCreditPackage: creditPackage})
        comp.setState({loadingHostedFields: true})
        var cbFinally = function () {
          comp.setState({loadingHostedFields: false})
        }
        Promise.resolve().then(function () {
          return braintree.createHostedFields(
                        comp.props.btClientToken,
                        require('../templates/buy_credits_styles.json'),
                        require('../templates/buy_credits_fields.json')
                    )
        }).then(function (hostedFields) {
          comp.setState({hostedFields: hostedFields})
        }).catch(function (err) {
          console.error("braintree setup error")
          console.err(err)
        }).then(cbFinally, cbFinally)
      }
    },

    onClickPurchase: function () {
      var comp = this
      var cbFinally = function () {
        comp.setState({hostedFields: null})
      }
      comp.setState({loadingPurchase: true})
      Promise.resolve().then(function () {
        return braintree.getNonce(comp.state.hostedFields)
      }).then(function (nonce) {
        return createUserCreditPackage(
                    comp.state.currCreditPackage.id,
                    nonce)
      }).then(function () {
        AllPagesActions.fetchUser()
        comp.setState(function (prevState) {
          return {
            purchasedCreditPackage:
                        prevState.currCreditPackage
          }
        })
      }).catch(function (err) {
        console.error("purchase error")
        if (err.name === 'HttpError') {
          console.error("from bargible server")
          console.error(err.body)
        } else {
          console.error(err)
        }
        comp.setState({purchaseError: err})
      }).then(cbFinally, cbFinally)
    },

    render: rtModalBuyCredit
  })
}

var exports = factoryBuyCreditsModal

module.exports = exports
