/* global require, module */

var React = require('react')
var _ = require('lodash')

var makeFactoryGenericModal = require('./modal_generic')
var factoryAuctionEndModal = require('./modal_auction_end')
const factoryAuctionEndAddrModal = require('./modal_auction_end_addr')
var factoryAuctionEndPayModal = require('./modal_auction_end_pay')
const openModal = require('../open_modal')
var HomeActions = require('../actions/home_actions')

var rtAllAuctions = require('../templates/home_all_auctions.rt')
var rtModalAuctionPayConfirm = require(
    '../templates/modal_auction_pay_confirm.rt')

var _sortAuctions = function (props, state) {
  var live = _statusFilter(props.auctions.allAuctions, "active")
  var gold = _starFilter(props.auctions.allAuctions, "gold")
  var silver = _starFilter(props.auctions.allAuctions, "silver")
  var bronze = _starFilter(props.auctions.allAuctions, "bronze")
  if (state && state.statusFilter) {
    if (state.statusFilter === "active") {
      gold = _statusFilter(gold, "active")
      silver = _statusFilter(silver, "active")
      bronze = _statusFilter(bronze, "active")
    }
  } else {
    gold = _statusFilter(gold, "upcoming").splice(0, 2)
    silver = _statusFilter(silver, "upcoming").splice(0, 2)
    bronze = _statusFilter(bronze, "upcoming").splice(0, 2)
  }
  if (state && state.starFilter) {
    live = _starFilter(live, state.starFilter)
  }
  return {live: live, gold: gold, silver: silver, bronze: bronze}
}

var _starFilter = function (array, filter) {
  let _database = _(array)
  if (filter) {
    _database = _database.filter({level: filter})
  }
  return _database.value()
}

var _statusFilter = function (array, filter) {
  let _database = _(array)
  if (filter) {
    _database = _database.filter({status: filter})
  }
  return _database.value()
}

var _provideLevelUpConfig = function (props) {
  let participationNumber = _.size(props.endedAuctions)
  let requiredForSilver = props.config.starLevels.silverLevelUp
  let requiredForGold = props.config.starLevels.goldLevelUp
  return {
    auctionCount: participationNumber,
    silver: (participationNumber >= requiredForSilver),
    gold: (participationNumber >= requiredForGold),
    neededForSilver: requiredForSilver - participationNumber,
    neededForGold: requiredForGold - participationNumber
  }
}

var AuctionContainerNode = React.createClass({
  getInitialState: function () {
    return {
      selectedTab: _.size(this.props.auctions.joinedAuctions) > 0
                          ? 'joined'
                          : 'default',
      endModalOpen: false,
      levelUps: _provideLevelUpConfig(this.props),
      visibleAuctions: _sortAuctions(this.props, this.state)
    }
  },

  componentWillReceiveProps: function (nextProps) {
    if (!this.state.endModalOpen && nextProps.endedAuction) {
      this._openAuctionEndModalFlow(nextProps.endedAuction)
    }
    this.setState({visibleAuctions: _sortAuctions(nextProps, this.state)})
  },

    /** initialize payment flow for auction end */
  _openAuctionEndModalFlow: function (endedAuction) {
    var comp = this
    var cbFinally = function () {
      HomeActions.addViewedAuctionEndingId(endedAuction.id)
      comp.setState({endModalOpen: false})
    }
    Promise.resolve().then(function () {
      comp.setState({endModalOpen: true})
      return openModal(factoryAuctionEndModal, {
        auction: endedAuction,
        user: comp.props.user
      }, {isFactory: true})
    }).then(
            comp._makeDoPayModalFlow(endedAuction)
        ).then(function (payModalRes) {
          if (payModalRes && payModalRes.hasPaid) {
            return openModal(
                    makeFactoryGenericModal(rtModalAuctionPayConfirm), {
                      user: comp.props.user
                    }, {isFactory: true})
          }
          return undefined
        }).then(cbFinally, cbFinally)
  },

  _makeDoPayModalFlow: function (endedAuction) {
    const comp = this
    const doPayModalFlow = function (endModalRes) {
      return Promise.resolve().then(function () {
        if (_.isEqual(endedAuction.reward.type,
                              'physical_item')) {
          return openModal(
                        factoryAuctionEndAddrModal, {
                          auction: endedAuction
                        }, {isFactory: true})
        }
        return undefined
      }).then(function (addrModalRes) {
        if (_.isUndefined(addrModalRes) ||
                    addrModalRes.gotAddr) {
          return openModal(
                        factoryAuctionEndPayModal, {
                          auction: endedAuction,
                          btClientToken: comp.props.params.btClientToken
                        }, {isFactory: true})
        }
        return undefined
      }).then(function (endModalRes) {
        return {hasPaid: !!endModalRes && endModalRes.hasPaid}
      })
    }

    return function (endModalRes) {
      if (endModalRes && endModalRes.clickedPay) {
        if (endedAuction.position !== 1) {
          throw new Error("non-winner clicked pay")
        }
        return doPayModalFlow(endModalRes)
      }
      return undefined
    }
  },

  selectTab: function (selectedTab) {
    var self = this
    return function () {
      self.setState({selectedTab: selectedTab})
    }
  },

  setStatusFilter: function (filter) {
    var self = this
    return function () {
      self.setState(function (prevState, props) {
        return {
          statusFilter: filter,
          visibleAuctions: _sortAuctions(props, {statusFilter: filter})
        }
      })
    }
  },

  setStarFilter: function (filter) {
    var self = this
    return function () {
      if (self.state.starFilter === filter) {
        self.setState(function (prevState, props) {
          return {
            starFilter: null,
            visibleAuctions: _sortAuctions(props, {starFilter: null})
          }
        })
      } else {
        self.setState(function (prevState, props) {
          return {
            starFilter: filter,
            visibleAuctions: _sortAuctions(props, {starFilter: filter})
          }
        })
      }
    }
  },

    /** used to limit api surface accessible to
     * active auction components.
     * not part of templating (markdown) interface.
     */
  _selectJoinedTab: function () {
    (this.selectTab('joined')())
  },

  _selectActiveTab: function () {
    (this.selectTab('active')())
  },

  _selectUpcomingTab: function () {
    (this.selectTab('upcoming')())
  },

  render: rtAllAuctions
})

var exports = AuctionContainerNode
module.exports = exports
