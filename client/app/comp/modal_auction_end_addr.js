/* global require, module */

const _ = require('lodash')
const React = require('react')

var createAuctionAddress = require(
    '../sources/user_source')
        .createAuctionAddress
const AddressError = require('../util/err').AddressError

const rtModalAuctionEnd = require(
    '../templates/modal_auction_end_addr.rt')

const STATES = [
    {abbrev: 'AL', name: 'Alabama'},
    {abbrev: 'AK', name: 'Alaska'},
    {abbrev: 'AZ', name: 'Arizona'},
    {abbrev: 'AR', name: 'Arkansas'},
    {abbrev: 'CA', name: 'California'},
    {abbrev: 'CO', name: 'Colorado'},
    {abbrev: 'CT', name: 'Connecticut'},
    {abbrev: 'DE', name: 'Delaware'},
    {abbrev: 'FL', name: 'Florida'},
    {abbrev: 'GA', name: 'Georgia'},
    {abbrev: 'HI', name: 'Hawaii'},
    {abbrev: 'ID', name: 'Idaho'},
    {abbrev: 'IL', name: 'Illinois'},
    {abbrev: 'IN', name: 'Indiana'},
    {abbrev: 'IA', name: 'Iowa'},
    {abbrev: 'KS', name: 'Kansas'},
    {abbrev: 'KY', name: 'Kentucky'},
    {abbrev: 'LA', name: 'Louisiana'},
    {abbrev: 'ME', name: 'Maine'},
    {abbrev: 'MD', name: 'Maryland'},
    {abbrev: 'MA', name: 'Massachusetts'},
    {abbrev: 'MI', name: 'Michigan'},
    {abbrev: 'MN', name: 'Minnesota'},
    {abbrev: 'MS', name: 'Mississippi'},
    {abbrev: 'MO', name: 'Missouri'},
    {abbrev: 'MT', name: 'Montana'},
    {abbrev: 'NE', name: 'Nebraska'},
    {abbrev: 'NV', name: 'Nevada'},
    {abbrev: 'NH', name: 'New Hampshire'},
    {abbrev: 'NJ', name: 'New Jersey'},
    {abbrev: 'NM', name: 'New Mexico'},
    {abbrev: 'NY', name: 'New York'},
    {abbrev: 'NC', name: 'North Carolina'},
    {abbrev: 'ND', name: 'North Dakota'},
    {abbrev: 'OH', name: 'Ohio'},
    {abbrev: 'OK', name: 'Oklahoma'},
    {abbrev: 'OR', name: 'Oregon'},
    {abbrev: 'PA', name: 'Pennsylvania'},
    {abbrev: 'RI', name: 'Rhode Island'},
    {abbrev: 'SC', name: 'South Carolina'},
    {abbrev: 'SD', name: 'South Dakota'},
    {abbrev: 'TN', name: 'Tennessee'},
    {abbrev: 'TX', name: 'Texas'},
    {abbrev: 'UT', name: 'Utah'},
    {abbrev: 'VT', name: 'Vermont'},
    {abbrev: 'VA', name: 'Virginia'},
    {abbrev: 'WA', name: 'Washington'},
    {abbrev: 'WV', name: 'West Virginia'},
    {abbrev: 'WI', name: 'Wisconsin'},
    {abbrev: 'WY', name: 'Wyoming'}
]

const factoryAuctionEndAddrModal = function (closeModal) {
  return React.createClass({
    getInitialState: function () {
      return {
        formFields: {
          name: '',
          addr_1: '',
          addr_2: '',
          city: '',
          usa_state: this.usa_states[0],
          zip_code: ''
        },
        addressError: null
      }
    },

    usa_states: _.map(STATES, 'abbrev'),

    closeModal: closeModal,

    makeChangeHandler: function (formFieldVarName) {
      var self = this
      return function (ev) {
        var updatedVal = ev.target.value
        self.setState(function (prevState) {
          var formFields = prevState.formFields
          formFields[formFieldVarName] = updatedVal
          return {formFields: formFields}
        })
      }
    },

    handleClickNext: function () {
      var comp = this
      Promise.resolve().then(function () {
        return createAuctionAddress(
                    comp.props.auction.id,
                    _.cloneDeep(comp.state.formFields))
      }).then(function (res) {
        comp.closeModal({gotAddr: true})
      }).catch(function (err) {
        if (_.isEqual(err.name, 'SchemaError')) {
          comp.setState({
            addressError: new AddressError(err)
          })
        } else {
          console.error("unexpected auction address error")
          console.error(err.stack)
          console.error(err)
        }
      })
    },

    render: rtModalAuctionEnd
  })
}

var exports = factoryAuctionEndAddrModal
module.exports = exports
