/* global require */

const _ = require('lodash')
const React = require('react')
const ReactDOM = require('react-dom')

const NavNode = require('./comp/nav')
const UserProfileMain = require('./comp/main_user_profile')

const AllPagesActions = require('./actions/all_pages_actions')
const UserProfileActions = require('./actions/user_profile_actions.js')
const NavActions = require('./actions/nav_actions')

const UserProfileStore = require('./stores/user_profile_store')

const navNodeParams = require('./util').getNavNodeParams()

const renderUserProfile = function (storeState) {
  if (_.isNull(storeState)) {
    return
  }
  ReactDOM.render(React.createElement(UserProfileMain, storeState),
                    document.getElementById('app'))
}

ReactDOM.render(React.createElement(NavNode, navNodeParams),
                document.getElementById('navbar'))

UserProfileStore.listen(renderUserProfile)

NavActions.fetchCreditPackages()
UserProfileActions.fetchEndedAuctions()
AllPagesActions.fetchUser()
