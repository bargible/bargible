/* global require, setTimeout, setInterval, clearInterval */

const _ = require('lodash')
const React = require('react')
const ReactDOM = require('react-dom')

const INTERVAL_ACTIVE_AUCTION_UPDATE = 10000 // ms
const INTERVAL_UPCOMING_AUCTION_UPDATE = 10000 // ms

const NavNode = require('./comp/nav')
const AuctionContainerNode = require('./comp/main_home')

const AllPagesActions = require('./actions/all_pages_actions')
const HomeActions = require('./actions/home_actions')
const NavActions = require('./actions/nav_actions')

// TODO: remove unused variable // const NavStore = require('./stores/nav_store')
const AuctionContainerStore = require(
    './stores/auction_container_store')

const CommonSource = require('./sources/common_source')

const util = require('./util')
const utilHome = require('./util_home')

const aucEventEmitter = new utilHome.AucEventEmitter()

const navNodeParams = Object.freeze(util.getNavNodeParams())
const appNodeParams = Object.freeze(util.getAppNodeParams())

const makeRenderHome = function (config) {
  return function (storeState) {
    if (_.isNull(storeState)) {
      return
    }
    const props = _.merge(storeState, {
      config: config,
      params: appNodeParams
    })
    ReactDOM.render(React.createElement(
            AuctionContainerNode, props),
                        document.getElementById('app'))
  }
}

ReactDOM.render(React.createElement(NavNode, navNodeParams),
                document.getElementById('navbar'))

Promise.all([
  utilHome.loadAnimations(require('./anim.json')),
  CommonSource.fetchConfig()
])
.then(_.spread(function (resLoadAnimations, config) {
  AuctionContainerStore.listen(
        makeRenderHome(Object.freeze(config))
    )
  aucEventEmitter.on('update', utilHome.auctionUpdateHandler)
  NavActions.fetchCreditPackages()
  AllPagesActions.fetchUser()
  HomeActions.fetchUpcomingAuctions()
  HomeActions.fetchActiveAuctions()
  HomeActions.fetchJoinedAuctions()
  HomeActions.fetchEndedAuctions()
  HomeActions.setConfig(config)
  setInterval(HomeActions.fetchActiveAuctions,
                INTERVAL_ACTIVE_AUCTION_UPDATE)
  setInterval(HomeActions.fetchUpcomingAuctions,
                INTERVAL_UPCOMING_AUCTION_UPDATE)
}))
