/* global require, module */

const moment = require('moment')

/// //////// from spec-design late-integration cycle

const idxArr = function (arr) {
  if (arr === null) {
    return null
  }
  if (typeof arr === 'undefined') {
    return []
  }
  return arr.map(function (val, idx) {
    return {
      idx: idx,
      val: val
    }
  })
}

/** a few pre-stored moment.js formatting strings */
const DATE_FORMATS = {
  monthDay: 'M/D',
  monthDayYear: 'M/D/Y',
  default: 'dddd, MMMM Do YYYY'
}

/**
 * format a date string for display
 * dateObj (Date): js `Date` object in local timezone
 * format (String|undefined): either one of the keys in DATE_FORMATS
 *   or a string to pass to moment.js's format() function.
 * return: formatted time string to display,
 *   e.g. 'Sunday, February 14th 2010'
 */
const formatDate = function (dateObj, format) {
  format = format || 'default'
  const momentFormatStr = DATE_FORMATS[format] || format
  const momentObj = moment(dateObj)
  const strDate = momentObj.format(momentFormatStr)
  return strDate
}

/** a few pre-stored moment.js formatting strings */
const TIME_FORMATS = {
  default: 'h:mmA'
}

/**
 * format a time string for display
 * dateObj (Date): js `Date` object in local timezone
 * return: formatted time string 'HH:MM(AM/PM)' to display
 */
const formatTime = function (dateObj, format) {
  format = format || 'default'
  const momentFormatStr = TIME_FORMATS[format] || format
  var momentObj = moment(dateObj)
  var strTime = momentObj.format(momentFormatStr)
  return strTime
}

const formatDuration = function (duration) {
  var strSecs = duration.seconds().toString()
  var strMins = duration.minutes().toString()
  if (strSecs.length === 1) {
    strSecs = '0' + strSecs
  }
  if (duration.hours() === 0) {
    return strMins + ':' + strSecs
  }
  if (strMins.length === 1) {
    strMins = '0' + strMins
  }
  return duration.hours().toString() + ':' + strMins + ':' + strSecs
}

/**
 * given an (int) number of cents, return a string
 * '$d+.cc'.
 *
 * optional args
 * - omitDollarSign (truthy): if true, return a string 'd+.cc'
 */
const formatDollarsAndCents = function (numCents, optArgs) {
  const opts = optArgs || {}
  var strCents = (numCents % 100).toString()
  if (strCents.length === 1) {
    strCents = '0' + strCents
  }
  var strDollars = Math.floor(numCents / 100).toString()
  var strDollarsAndCents = strDollars + '.' + strCents
  if (opts.omitDollarSign) {
    return strDollarsAndCents
  } else {
    return '$' + strDollarsAndCents
  }
}

var exports = {}

exports.moment = moment
exports.idxArr = idxArr
exports.formatDate = formatDate
exports.formatTime = formatTime
exports.formatDuration = formatDuration
exports.formatDollarsAndCents = formatDollarsAndCents

module.exports = exports
