/* global require, module */

const alt = require('../alt')

const AllPagesActions = require('../actions/all_pages_actions')
const NavActions = require('../actions/nav_actions')

const NavStore = alt.createStore({
  displayName: 'NavStore',

  bindListeners: {
    updateCreditPackages: NavActions.updateCreditPackages,
    updateUserData: AllPagesActions.updateUserData
  },

  state: {
    creditPackages: [],
    displayName: '',
    credit: undefined
  },

  updateUserData: function (user) {
    this.setState({
      displayName: user.first_name,
      credit: user.credit,
      avatar: user.avatar
    })
  },

  updateCreditPackages: function (creditPackages) {
    this.setState({creditPackages: creditPackages})
  }
})

var exports = NavStore
module.exports = exports
