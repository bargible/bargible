/* global require, module */

const _ = require('lodash')
const alt = require('../alt')

const AllPagesActions = require('../actions/all_pages_actions')
const UserProfileActions = require('../actions/user_profile_actions')

const _stateForComp = function (state) {
  if (!_(state.isInit).values().reduce(function (a, b) {
    return a && b
  }, true)) {
    return null
  }
  return _(state).omit([
    'isInit'
  ]).mapValues(function (value, key) {
    if (key === 'endedAuctions') {
      return _.map(value, 'data')
    }
    return value
  }).mapKeys(function (value, key) {
    if (key === 'endedAuctions') {
      return 'auctionHistories'
    }
    return key
  }).value()
}

const UserProfileStore = alt.createStore({
  displayName: 'UserProfileStore',

  bindListeners: {
    updateUserData: AllPagesActions.updateUserData,
    updateEndedAuctions: UserProfileActions.updateEndedAuctions
  },

  state: {
        // initialization status for internal state variables
    isInit: {
      user: false,
      endedAuctions: false
    },
    user: {},
    endedAuctions: []
  },

  config: {
    getState: _stateForComp
  },

  output: _stateForComp,

  updateUserData: function (user) {
    var isInitUpdate = _(this.state.isInit)
                .omit('user')
                .merge({user: true}).value()
    this.setState({user: user,
      isInit: isInitUpdate})
  },

  updateEndedAuctions: function (endedAuctions) {
    _.forEach(endedAuctions, function (auction) {
      if (auction.status !== 'ended') {
        throw new Error("got auction of with status '" +
                                auction.status +
                               "' expecting 'ended'")
      }
    })
    var isInitUpdate = _(this.state.isInit)
                .omit('endedAuctions')
                .merge({endedAuctions: true}).value()
    this.setState({endedAuctions: endedAuctions,
      isInit: isInitUpdate})
  }
})

var exports = UserProfileStore
module.exports = exports
