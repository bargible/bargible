/* global require, module, setTimeout */

const _ = require('lodash')
const moment = require('moment')
const uuid = require('uuid')
const alt = require('../alt')

const AllPagesActions = require('../actions/all_pages_actions')
const HomeActions = require('../actions/home_actions')

const GAMEPLAY_LOCK_TIMEOUT = 3500 // ms

const _isAuctionGameplayLocked = function (
    auctionId, storeState, updateMoment
) {
  var auctionGameplayLocks = _.filter(storeState.gameplayLocks,
                                        {auctionId: auctionId})
  var numUndertimeLocks = _(auctionGameplayLocks)
        .filter(function (gameplayLock) {
          var timeSinceLock = updateMoment
                    .diff(gameplayLock.createdAt)
          return timeSinceLock < GAMEPLAY_LOCK_TIMEOUT
        }).size()
  return numUndertimeLocks > 0
}

// TODO: deprecated in stars view
const _makeUpcomingAuctionMapper = function (storeState) {
  return function (
    upcomingAuction
  ) {
    upcomingAuction.isLoadingWatch = _.includes(
      storeState.loadingWatchAuctionIds, upcomingAuction.id)
    return upcomingAuction
  }
}

const _makeJoinedAuctionMapper = function (storeState) {
  var updateMoment = moment()
  return function (
        joinedAuction
    ) {
    var auctionId = joinedAuction.id
    if (joinedAuction.updates) {
      throw new Error("implementation error: " +
                            "joinedAuctions objects not expected to " +
                            "have update attrs in store state")
    }
    joinedAuction.updates = _.filter(
            storeState.updates, {auctionId: auctionId})
    joinedAuction.isGameplayLocked = _isAuctionGameplayLocked(
            auctionId, storeState, updateMoment)
    return joinedAuction
  }
}

const _mapRewardForComp = function (aucVal, aucKey) {
  if (!_.isEqual(aucKey, 'reward')) {
    return aucVal
  }
  if (!['item', 'physical_item'].includes(aucVal['type'])) {
    throw new Error("component api not updated for " +
            aucVal['type'])
  }
  const rewardData = _.cloneDeep(aucVal['data'])
  if (_.includes(_.keys(rewardData), 'type')) {
    throw new Error("type is not a valid key for reward data")
  }
  rewardData.type = aucVal['type']
  return rewardData
}

/** _mapStateForComp helper */
const _mapStateToAuctionsForComp = function (state) {
  state = _.cloneDeep(state)
  state.allAuctions
    .map(function (auction) {
      auction.data.status = auction.status
      auction.data.level = 'bronze'
      if (auction.data.minPrice >= state.config.starLevels.silver) {
        auction.data.level = 'silver'
      }
      if (auction.data.minPrice >= state.config.starLevels.gold) {
        auction.data.level = 'gold'
      }
      return auction
    })
  return {
    allAuctions: _(state.allAuctions)
      .map('data')
      .map((auc) => _.mapValues(auc, _mapRewardForComp))
      .value(),
    /* TODO: deprecated buckets */
    activeAuctions: _(state.allAuctions)
            .filter({status: 'active'})
            .reject(function (auction) {
              return _.includes(
                    _.union(
                        state.loadingJoinedAuctionIds,
                        _(state.allAuctions)
                            .filter({status: 'joined'})
                            .map('id')
                            .value()
                        , state.unviewedAuctionEndingsIds
                    ), auction.id)
            }).map('data')
            .map((auc) => _.mapValues(auc, _mapRewardForComp))
            .value(),
    upcomingAuctions: _(
            _(state.allAuctions)
                .filter({status: 'upcoming'})
                .map('data')
                .map((auc) => _.mapValues(auc, _mapRewardForComp))
                .cloneDeep()) // avoid mutating store state
            .map(_makeUpcomingAuctionMapper(state))
            .value(),
    /* end deprecated buckets */
    joinedAuctions: _( // cloneDeep is not lodash chainable
      _(state.allAuctions)
        .filter({status: 'joined'})
        .reject(function (auction) {
          // possibly waiting on auction end data
          return _.includes(
            state.unviewedAuctionEndingsIds,
            auction.id)
        })
        .map('data')
        .map((auc) => _.mapValues(auc, _mapRewardForComp))
        .cloneDeep()) // avoid mutating store state
      .map(_makeJoinedAuctionMapper(state))
      .value()
  }
}

const _makeUserMapper = function (storeState) {
  return function (userStoreState) {
    const userForComp = _.pick(userStoreState, [
      'avatar',
      'email',
      'credit',
      'username'
    ])
    const lastEndedAuction = _(storeState.allAuctions)
                  .filter({status: 'ended'})
                  .sortBy('end').reverse()
                  .first()
    if (lastEndedAuction) {
      userForComp.lastPlayed = lastEndedAuction.end
    } else {
      userForComp.lastPlayed = null
    }
    return userForComp
  }
}

/**
 * specify how store internal state is exposed to react component
 *
 * written here as a pure fuction so it can be used for both
 * update events and initial state
 */
const _mapStateForComp = function (state) {
  var auctions = _mapStateToAuctionsForComp(state)
  var user = (_makeUserMapper(state)(_.cloneDeep(state.user)))
  var endedAuctions = _(_.cloneDeep(state.endedAuctions))
    .map('data')
    .value()
  var endedAuction = (function (unviewedEndingId) {
    if (!unviewedEndingId) {
      return null
    }
    var maybeEndedAuction = _.find(state.allAuctions, {
      id: unviewedEndingId,
      status: 'ended'
    })
        // possibly waiting on auction end data
    return maybeEndedAuction &&
            _.mapValues(maybeEndedAuction['data'], _mapRewardForComp)
  }(_.head(state.unviewedAuctionEndingsIds)))
  return {
    auctions: auctions,
    user: user,
    endedAuction: endedAuction,
    endedAuctions: endedAuctions,
    starFilter: _.clone(state.starFilter),
    config: _.clone(state.config)
  }
}

/**
 * don't expose state before initial fetch
 * expects corresponding render logic
 */
const _stateForComp = function (state) {
  if ((!_(state.isInit).values().reduce(function (a, b) {
    return a && b
  }, true)) ||
        _.isNull(state.user)) {
    return null
  }
  return _mapStateForComp(state)
}

const AuctionContainerStore = alt.createStore({
  displayName: 'AuctionContainerStore',

  bindListeners: {
    setUpdate: HomeActions.setUpdate,
    updateAllAuctions: HomeActions.updateAllAuctions,
    updateUserData: AllPagesActions.updateUserData,
    updateJoinedAuction: HomeActions.updateJoinedAuction,
    addEndedAuction: HomeActions.addEndedAuction,
    addViewedAuctionEndingId: HomeActions.addViewedAuctionEndingId,
    watchAuction: HomeActions.watchAuction,
    setConfig: HomeActions.setConfig,
    joinAuction: HomeActions.joinAuction,
    auctionGameplayUpdate: [
      HomeActions.bid,
      HomeActions.stealBids,
      HomeActions.destroyBids,
      HomeActions.freezeBids
    ],
    endJoinedAuction: HomeActions.endJoinedAuction,
    updateEndedAuctions: HomeActions.updateEndedAuctions
  },

  state: {
    isInit: {
      endedAuctions: false,
      joinedAuctions: false,
      activeAuctions: false,
      upcomingAuctions: false
    },
    allAuctions: [],
    user: null,
    gameplayLocks: [],
        // for client side loading state
    loadingWatchAuctionIds: [],
    loadingJoinedAuctionIds: [],
    updates: [], // parsed data from server-side events
    unviewedAuctionEndingsIds: []
  },

  config: {
    getState: _stateForComp
  },

  output: _stateForComp,

  updateJoinedAuction: function (auctionUpdated) {
    this._updateOrAddAnAuction(auctionUpdated)
  },

  updateEndedAuctions: function (endedAuctions) {
    this.setState({endedAuctions: endedAuctions})
  },

  updateAllAuctions: function (data) {
    var isInitUpdate = _.merge(
            this.state.isInit,
            _(data.inits).map(function (initName) {
              return [initName, true]
            }).fromPairs().value())

    var updatedAuctionIds = _.map(data.updatedAuctions, 'id')
    var updatedJoinedAuctionIds = _(data.updatedAuctions)
                .filter({status: 'joined'})
                .map('id')
                .value()
    var updatedUpcomingAuctionIds = _(data.updatedAuctions)
                .filter({status: 'upcoming'})
                .map('id')
                .value()
    var auctionsWithoutUpdates = _(this.state.allAuctions)
                .reject(function (auction) {
                  return _.includes(updatedAuctionIds,
                                      auction.id)
                }).value()
    var currJoinedAuctionIds = this.state.loadingJoinedAuctionIds
    var currWatchAuctionIds = this.state.loadingWatchAuctionIds
    this.setState({
      allAuctions: _.union(
                auctionsWithoutUpdates,
                data.updatedAuctions),
      isInit: isInitUpdate,
      loadingJoinedAuctionIds: _.difference(
                currJoinedAuctionIds,
                updatedJoinedAuctionIds),
      loadingWatchAuctionIds: _.difference(
                currWatchAuctionIds,
                updatedUpcomingAuctionIds)
    })
  },

    // todo: js api wip
  _updateOrAddAnAuction: function (auctionUpdated) {
    var allAuctions
    if (auctionUpdated['status'] === 'joined') {
      if (!this.state.isInit.joinedAuctions) {
        throw new Error("tried to update joined auction before " +
                                "initial joined auctions update")
      }

      allAuctions = _.reject(this.state.allAuctions, {
        id: auctionUpdated.id,
        status: 'joined'
      })
      var currJoinedAuctionIds =
                    this.state.loadingJoinedAuctionIds
      allAuctions.push(auctionUpdated)
      this.setState({
        allAuctions: allAuctions,
        loadingJoinedAuctionIds: _.difference(
                    currJoinedAuctionIds,
                    [auctionUpdated.id])
      })
    } else if (auctionUpdated['status'] === 'ended') {
      allAuctions = _.reject(this.state.allAuctions, {
        id: auctionUpdated.id
      })
      allAuctions.push(auctionUpdated)
      this.setState({
        allAuctions: allAuctions
      })
    } else {
      throw new Error(
                "singly updating auctions with status " +
                    "other than joined or ended unimplemented. " +
                    "tried to update for status " +
                    auctionUpdated['status']
            )
    }
  },

  updateUserData: function (user) {
    this.setState({user: user})
  },

  addEndedAuction: function (auctionNew) {
    this._updateOrAddAnAuction(auctionNew)
  },

  auctionGameplayUpdate: function (data) {
    if (!data) {
      this.emitChange()
      return
    }
    var joinedAuction = data.joinedAuction
    var updateType = data.updateType
    this._addGameplayLock({
      auctionId: joinedAuction.id,
      type: updateType
    })
  },

  _addGameplayLock: function (gameplayLockDetails) {
    var prevGameplayLocks = this.state.gameplayLocks
    var auctionId = gameplayLockDetails.auctionId
    var newGameplayLock = {
      id: uuid.v1(),
      createdAt: moment(),
      auctionId: auctionId,
      type: gameplayLockDetails.type
    }
    var gameplayLocksUpdate
    if (_.find(prevGameplayLocks, {auctionId: auctionId})) {
      console.warn("adding a gameplay lock to an auction " +
                         "that already has a gameplay lock.  " +
                         "duplicate lock will be removed.")
      gameplayLocksUpdate = _.reject(prevGameplayLocks, {
        auctionId: auctionId
      })
    } else {
      gameplayLocksUpdate = prevGameplayLocks
    }
    if (!_.find(this.state.allAuctions, {
      id: auctionId,
      status: 'joined'
    })) {
      console.warn("adding a gameplay lock for an auction " +
                         "that is not stored as a joined auction")
    }
    gameplayLocksUpdate.push(newGameplayLock)
    this.setState({gameplayLocks: gameplayLocksUpdate})
  },

  addViewedAuctionEndingId: function (auctionId) {
    var unviewedAuctionEndingsIds = _.reject(
            this.state.unviewedAuctionEndingsIds,
            function (unviewedAuctionEndingsId) {
              return _.isEqual(unviewedAuctionEndingsId, auctionId)
            })
    this.setState({
      unviewedAuctionEndingsIds: unviewedAuctionEndingsIds
    })
  },

  watchAuction: function (auctionId) {
    var loadingWatchAuctionIds = this.state.loadingWatchAuctionIds
    var isUpcomingAuction = _(this.state.allAuctions)
                .filter({status: 'upcoming'})
                .filter({id: auctionId})
                .size() === 1
    if (!isUpcomingAuction) {
      throw new Error("attempted to join an auction w/ id not " +
                            "in activeAuctions")
    }
    loadingWatchAuctionIds.push(auctionId)
    this.setState({
      loadingWatchAuctionIds: loadingWatchAuctionIds
    })
  },

  joinAuction: function (auctionId) {
    var loadingJoinedAuctionIds = this.state.loadingJoinedAuctionIds
    var isActiveAuction = _(this.state.allAuctions)
                .filter({status: 'active'})
                .filter({id: auctionId})
                .size() === 1
    if (!isActiveAuction) {
      throw new Error("attempted to join an auction w/ id not " +
                            "in activeAuctions")
    }
    loadingJoinedAuctionIds.push(auctionId)
    this.setState({
      loadingJoinedAuctionIds: loadingJoinedAuctionIds
    })
  },

  setUpdate: function (updateDetails) {
    var updateType = updateDetails.type
    var auctionId = updateDetails.auctionId
    var newUpdate = {
      id: uuid.v1(),
      createdAt: moment(),
      auctionId: auctionId,
      type: updateType,
      info: updateDetails.info
    }
    var updates = this.state.updates
    var gameplayLocks = this.state.gameplayLocks
    var gameplayLocksUpdate
    updates.push(newUpdate)
    this.setState({updates: updates})
        // remove gameplay lock if applicable
    _.forEach([
            ['bid-self', 'bid'],
            ['steal-self', 'steal'],
            ['destroy-self', 'destroy'],
            ['froze-bids', 'freeze']
    ], _.spread(function (unlockUpdateType, gameplayLockType) {
      if (unlockUpdateType === updateType) {
        gameplayLocksUpdate = _.reject(gameplayLocks, {
          auctionId: auctionId,
          type: gameplayLockType
        })
      }
    }))
    if (gameplayLocksUpdate) {
      this.setState({gameplayLocks: gameplayLocksUpdate})
    }
  },

  setConfig: function (config) {
    this.setState({
      config: config
    })
  },

  endJoinedAuction: function (auctionId) {
    var loadingJoinedAuctionIds = this.state.loadingJoinedAuctionIds
    var unviewedAuctionEndingsIds =
                this.state.unviewedAuctionEndingsIds
    if (!this.state.isInit.joinedAuctions) {
      throw new Error("tried to end joined auction before " +
                            "initial joined auctions set")
    }
    unviewedAuctionEndingsIds.push(auctionId)
    this.setState({
      allAuctions: _.reject(this.state.allAuctions, {
        id: auctionId,
        status: 'joined'
      }),
            // loadingJoinedAuctionIds update for good measure ..
            //   .. usually (i.e. barring some error condition)
            //   this will occur when allAuctions is updated
      loadingJoinedAuctionIds: _.reject(
                loadingJoinedAuctionIds,
                function (joinedAuctionId) {
                  return _.isEqual(auctionId, joinedAuctionId)
                }
            ),
      unviewedAuctionEndingsIds: unviewedAuctionEndingsIds
    })
  }
})

var exports = AuctionContainerStore

module.exports = exports
