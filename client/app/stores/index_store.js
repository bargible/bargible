/* global require, module, setTimeout */

var alt = require('../alt')

var IndexActions = require('../actions/index_actions')

/**
 * specify how store internal state is exposed to react component
 *
 * written here as a pure fuction so it can be used for both
 * update events and initial state
 */
var _stateForComp = function (state) {
  return {
    activeAuctions: state.activeAuctions,
    upcomingAuctions: state.upcomingAuctions
  }
}

var IndexStore = alt.createStore({
  displayName: 'IndexStore',

  bindListeners: {
    updateActiveAuctions: IndexActions.updateActiveAuctions,
    updateUpcomingAuctions: IndexActions.updateUpcomingAuctions
  },

  state: {
    activeAuctions: [],
    upcomingAuctions: []
  },

  config: {
    getState: _stateForComp
  },

  output: _stateForComp,

  updateActiveAuctions: function (auctions) {
    this.setState({activeAuctions: auctions})
  },

  updateUpcomingAuctions: function (upcomingAuctions) {
    this.setState({upcomingAuctions: upcomingAuctions})
  }
})

var exports = IndexStore
module.exports = exports
