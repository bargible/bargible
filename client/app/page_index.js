/* global require, setInterval */

var React = require('react')
var ReactDOM = require('react-dom')

// TODO: remove unused variable // var INTERVAL_ACTIVE_AUCTION_UPDATE = 10000 // ms
// TODO: remove unused variable // var INTERVAL_UPCOMING_AUCTION_UPDATE = 10000 // ms

var NavIndexNode = require('./comp/nav_index')
var MainIndex = require('./comp/main_index')

// TODO: remove unused variable // var IndexActions = require('./actions/index_actions')
var IndexStore = require('./stores/index_store')

var renderIndex = function (storeState) {
  ReactDOM.render(
        React.createElement(MainIndex, storeState),
        document.getElementById('app'))
}

ReactDOM.render(React.createElement(NavIndexNode, {}),
    document.getElementById('navbar'))

renderIndex(IndexStore.getState())

IndexStore.listen(renderIndex)
