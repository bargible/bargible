/* global location */

const io = require('socket.io-client')
const EventEmitter = require('events').EventEmitter

const _AUCTION_UPDATE_EVENT = 'auction update'

class AucEventEmitter extends EventEmitter {
  constructor () {
    super()
    let socket = io.connect('http://' + document.domain + ':' +
                            location.port)
    socket.on(_AUCTION_UPDATE_EVENT,
              (data) => this._onAuctionUpdate(data))
    this._socketioSocket = socket
  }

  _onAuctionUpdate (data) {
    super.emit('update', data)
  }

  emit (eventName) {
    let err = new Error(
      "current implementation of auction event emitter " +
        "is unidirectional (server -> client)")
    err.name = 'ImplementationError'
    throw err
  }
}

module.exports = AucEventEmitter
