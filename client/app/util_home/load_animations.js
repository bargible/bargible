/* global module, require */

var _ = require('lodash')

var fetchFile = require('../util').fetchFile
var setAnimation = require('../comp/util').setAnimation

var _parseKeyframesDefn = function (objKf) {
  if (!_.isEqual([
    'imgType',
    'filenamePrefix',
    'numFrames',
    'frameImgStartIdx',
    'urlPrefix'
  ].sort(), _.keys(objKf).sort())) {
    throw new Error("keyframes object doesn't have expected keys")
  }
  if (!_.includes(['png'], objKf['imgType'])) {
    throw new Error("invalid keyframe image type")
  }
  if (!_.isInteger(objKf['numFrames'])) {
    throw new Error("number of frames not an integer")
  }
  if (!_.isInteger(objKf['frameImgStartIdx'])) {
    throw new Error("frame start index not an integer")
  }
  if (!_.isString(objKf['filenamePrefix'])) {
    throw new Error("filename prefix is not a string")
  }
  if (!_.isString(objKf['urlPrefix'])) {
    throw new Error("url prefix is not a string")
  }
  return objKf
}

var fetchKeyframesBlobs = function (objKf) {
  var defnKf = _parseKeyframesDefn(objKf)
  var imgType = defnKf.imgType
  var frameNum2Filename = function (frameNum) {
    return (defnKf.filenamePrefix +
                ('0000' + frameNum.toString()).slice(-4) +
                '.' + imgType)
  }
  var numFrames = defnKf.numFrames
  var frameImgStartIdx = defnKf.frameImgStartIdx
  var pArr = _(_.range(frameImgStartIdx,
                         numFrames + frameImgStartIdx))
            .map(frameNum2Filename)
            .map(function (filename) {
              var urlPrefix = defnKf.urlPrefix
              return urlPrefix + filename
            }).map(function (frameUri) {
              return fetchFile(frameUri, 'image/' + imgType)
            }).value()
  return Promise.all(pArr)
}

var loadAnim = function (animName, animDef) {
  return Promise.resolve().then(function () {
    return fetchKeyframesBlobs(animDef.keyframes)
  }).then(function (blobs) {
    return {
      name: animName,
      blobs: blobs
    }
  })
}

var loadAnims = function (animDefs) {
  return Promise.all(_(animDefs).toPairs().map(_.spread(function (
        animName, animDef) {
    return loadAnim(animName, animDef)
  })))
}

var loadAndSetAnims = function (animDefs) {
  return Promise.resolve().then(function () {
    return loadAnims(animDefs)
  }).then(function (anims) {
    _.forEach(anims, function (anim) {
      setAnimation(anim.name, anim.blobs)
    })
  })
}

var exports = loadAndSetAnims

module.exports = exports
