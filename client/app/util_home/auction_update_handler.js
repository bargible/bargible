/* global module, require */

const _ = require('lodash')

const AllPagesActions = require('../actions/all_pages_actions')
const HomeActions = require('../actions/home_actions')

/** all possible event types */
const EVENT_TYPES = Object.freeze([
  'end',
  'bid-self',
  'bid',
  'froze-bids',
  'bids-freeze',
  'steal-self',
  'steal',
  'destroy-self',
  'destroy',
    // a game element update by another user that doesn't
    // affect logged-in user
  'game-element',
  'bid-mult',
  'first-bid',
  'thaw',
  'join'
])

const EVENT_TYPES_AFFECTS_CREDITS = Object.freeze([
  'join',
  'froze-bids',
  'steal-self',
  'destroy-self'
])

if (_.size(_.difference(EVENT_TYPES_AFFECTS_CREDITS,
                        EVENT_TYPES)) !== 0) {
  throw new Error("found unknown event type(s) in update " +
                    "user credits events: " +
                    _.difference(EVENT_TYPES_AFFECTS_CREDITS,
                                 EVENT_TYPES).join(', '))
}

/// // common parse functions

var _parseInt = function (intVal) {
  var parsedVal = parseInt(intVal)
  if (isNaN(parsedVal)) {
    throw new Error(
            "failed to parse integer value " + intVal)
  }
  return parsedVal
}

var _parseBool = function (boolVal) {
  if (boolVal !== true && boolVal !== false) {
    throw new Error(boolVal + "not a boolean")
  }
  return boolVal
}

/** return true if two arrays are equal as sets, false otherwise */
var _setEqual = function (arr1, arr2) {
  return _.size(_.difference(arr1, arr2)) === 0 &&
        _.size(_.difference(arr2, arr1)) === 0
}

/// // per-update-type parse functions

const updateInfoParsers = Object.freeze({

  'bid-mult': function (updateInfo) {
    var expectedKeys = [
      'username',
      'numConsecutiveBids',
      'complete'
    ]
    if (!_setEqual(_.keys(updateInfo), expectedKeys)) {
      throw new Error("bid mult update info unexpected keys")
    }
    return _.mapValues(updateInfo, function (val, key) {
      if (key === 'numConsecutiveBids') {
        return _parseInt(val)
      } else if (key === 'complete') {
        return _parseBool(val)
      } else {
        return val
      }
    })
  },

  'froze-bids': function (updateInfo) {
    var expectedKeys = [
      'usernames'
    ]
    if (!_setEqual(_.keys(updateInfo), expectedKeys)) {
      throw new Error("froze bids update info unexpected keys")
    }
    return _.mapValues(updateInfo, function (val, key) {
      if (key === 'usernames') {
        if (!Array.isArray(val)) {
          throw new Error("usernames attr not an array")
        }
        return val
      } else {
        return val
      }
    })
  },

  'bids-freeze': function (updateInfo) {
    var expectedKeys = [
      'username'
    ]
    if (!_setEqual(_.keys(updateInfo), expectedKeys)) {
      throw new Error("bids freeze update info unexpected keys")
    }
    return _.mapValues(updateInfo, function (val, key) {
      return val
    })
  },

  'destroy': function (updateInfo) {
    var expectedKeys = [
      'username',
      'numBids'
    ]
    if (!_setEqual(_.keys(updateInfo), expectedKeys)) {
      throw new Error("destroy update info unexpected keys")
    }
    return _.mapValues(updateInfo, function (val, key) {
      if (key === 'numBids') {
        return _parseInt(val)
      } else {
        return val
      }
    })
  },

  'destroy-self': function (updateInfo) {
    var expectedKeys = [
      'usernames'
    ]
    if (!_setEqual(_.keys(updateInfo), expectedKeys)) {
      throw new Error("user destroy update info unexpected keys")
    }
    return _.mapValues(updateInfo, function (val, key) {
      if (key === 'usernames') {
        if (!Array.isArray(val)) {
          throw new Error("usernames attr not an array")
        }
        return val
      } else {
        return val
      }
    })
  },

  'steal': function (updateInfo) {
    var expectedKeys = [
      'username',
      'numBids'
    ]
    if (!_setEqual(_.keys(updateInfo), expectedKeys)) {
      throw new Error("destroy update info unexpected keys")
    }
    return _.mapValues(updateInfo, function (val, key) {
      if (key === 'numBids') {
        return _parseInt(val)
      } else {
        return val
      }
    })
  },

  'steal-self': function (updateInfo) {
    var expectedKeys = [
      'usernames',
      'numBids'
    ]
    if (!_setEqual(_.keys(updateInfo), expectedKeys)) {
      throw new Error("user destroy update info unexpected keys")
    }
    return _.mapValues(updateInfo, function (val, key) {
      if (key === 'usernames') {
        if (!Array.isArray(val)) {
          throw new Error("usernames attr not an array")
        }
        return val
      } else if (key === 'numBids') {
        return _parseInt(val)
      } else {
        return val
      }
    })
  }
})

/// ///// update handler and helper function(s)

var _handleAuctionUpdateByType = function (
    auctionId, updateType, updateInfo) {
  if (!_.includes(EVENT_TYPES, updateType)) {
    throw new Error("unknown auction update type: " +
                        updateType)
  }
  if (updateType === 'end') {
    HomeActions.endJoinedAuction(auctionId)
    return
  }
  var parsedInfo
  if (updateInfoParsers[updateType]) {
    parsedInfo = updateInfoParsers[updateType](updateInfo)
  }
  HomeActions.fetchOneJoinedAuction(auctionId)
  if (_.includes(EVENT_TYPES_AFFECTS_CREDITS, updateType)) {
    AllPagesActions.fetchUser()
  }
  HomeActions.setUpdate(auctionId, updateType, parsedInfo)
}

var auctionUpdateHandler = function (updateData) {
  var auctionId = updateData.auctionId
  var updateType = updateData.updateType
  var updateInfo = updateData.updateInfo
  var unexpectedKeys = _.difference(_.keys(updateData), [
    'auctionId',
    'updateType',
    'updateInfo'
  ])
  var missingKeys = _.difference([
    'auctionId',
    'updateType'
  ], _.keys(updateData))
  if (_.size(missingKeys) !== 0) {
    throw new Error("auction update event missing keys: " +
                        missingKeys.join(', '))
  }
  if (_.size(unexpectedKeys) !== 0) {
    throw new Error("auction update event unexpected keys: " +
                        unexpectedKeys.join(', '))
  }
  _handleAuctionUpdateByType(
        auctionId, updateType, updateInfo)
}

var exports = auctionUpdateHandler

module.exports = exports
