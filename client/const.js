/* global require, module */

var reactToolsTransformOpts = {
  es5: false,
  sourceMap: true,
  stripTypes: undefined,
  harmony: undefined
}

var exports = {}

exports.reactToolsTransformOpts = reactToolsTransformOpts

module.exports = exports
