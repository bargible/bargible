/**
 * originally lifted from reactify npm package
 */

/* global require, module, Buffer */

var _ = require('lodash')
var ReactTools = require('react-tools')
var through = require('through')

var reactToolsTransformOpts = require('./const').reactToolsTransformOpts

var isJSXFile = function (filename) {
    // removing js extension drops source-map support
  var extensions = ['jsx']
            .filter(Boolean)
            .map(function (ext) {
              return ext[0] === '.' ? ext.slice(1) : ext
            })
  return new RegExp('\\.(' + extensions.join('|') + ')$')
        .exec(filename)
}

var jsx_transform = function (filename) {
  var buf = []

  var write = function (chunk) {
    if (!Buffer.isBuffer(chunk)) {
      // TODO: new Buffer()' was deprecated since v6. Use 'Buffer.alloc()' or 'Buffer.from()' (use 'https://www.npmjs.com/package/safe-buffer')
      /* eslint node/no-deprecated-api: "off" */
      chunk = new Buffer(chunk)
    }
    return buf.push(chunk)
  }

  var compile = function () {
    var source = Buffer.concat(buf).toString()
    if (isJSXFile(filename)) {
      try {
        var output = ReactTools.transform(source, _.merge(
                    reactToolsTransformOpts,
                    {sourceFilename: filename}))
        this.queue(output)
      } catch (error) {
        error.name = 'JsxTransformError'
        error.message = filename + ': ' + error.message
        error.fileName = filename
        this.emit('error', error)
      }
    } else {
      this.queue(source)
    }
    return this.queue(null)
  }

  return through(write, compile)
}

module.exports = jsx_transform
