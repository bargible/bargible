# Bargible #

### Install ###

##### System setup
The application expects the following system installs to be available.
They're typically available through the system's package manager or
installation binaries.

* node version 7.2.1 (optionally installed with
  [nvm](https://github.com/creationix/nvm)
  to prevent conflicts with existing system installs)
* python 3.5.3 (optionally installed with
  [pyenv](https://github.com/yyuu/pyenv) 1.0.7
  to prevent conflicts with existing system installs)
* postgres 9.4.x (optionally including
  [pgadmin](https://www.pgadmin.org/)
  for an easy GUI for application install)
* [redis](http://redis.io/) 3.2.5

##### Application install
Run all commands in this section from the project root directory
unless otherwise noted.
Follow all instructions sequentially.

* Configure a postgres user and password

* Create a database 'bargible'

* Configure `DATABASE_URL` in your path
    * the format is
      `postgresql://<user>:<password>@<hostname>:<port>/bargible`
    * usually the hostname and port are localhost:5432

* install pip

* create a virtual environment for the project
    * for system-wide python installs:
      http://docs.python-guide.org/en/latest/dev/virtualenvs/
    * for pyenv python installs:
      https://github.com/yyuu/pyenv-virtualenv

* Activate the virtual environment

* install requirements:

    `./bin/updateInstall.sh`

* Create a directory /var/log/bargible for logging

* If necessary, configuration can be changed by setting the `BARGIBLE_CONFIG`. The default is `config.TestingConfig`.
    * `config.ProductionConfig`
    * `config.StagingConfig`
    * `config.DevelopmentConfig`
    * `config.TestingConfig`

* Ensure redis is on port `6379` and databases
  - `11` -- Flask-SocketIO `message_queue`
  are available.

  For example,

    ```
    $ redis-cli
    127.0.0.1:6379> SELECT 11
    OK
    127.0.0.1:6379[11]> KEYS *
    (empty list or set)
    ```

* Create a directory for image storage

    ```
    mkdir -p /var/local/bargible/images
    chmod a+w /var/local/bargible/images
    ```

    where the `chmod` command is suitable for development environments only

* Build dependencies for the
    [Wand](http://docs.wand-py.org/en/0.4.3/guide/install.html#install-wand-on-debian-ubuntu)
    python image library according to

    `./bin/buildImageMagick.sh`

    and set the environment variable `MAGICK_HOME` to the
    _absolute path_ of the directory specified in that script.

    In particular note that the path in the `MAGICK_HOME` environment
    variable is a path to the directory that contains the `lib/`
    directory that contains the native libraries, not the path
    to the `lib/` directory itself.

* Create a file `app/braintree/creds.conf`
  according to the example `app/braintree/creds.conf.example`
  with either
  [production](https://www.braintreegateway.com/login) or
  [sandbox](https://sandbox.braintreegateway.com/login) credentials,
  optionally setting up
  [address and card verification](https://articles.braintreepayments.com/guides/fraud-tools/basic/avs-cvv-rules).

* Create a file `app/tango/creds.conf`
  according to the example `app/tango/creds.conf.example`
  with either
  [production](https://www.tangocard.com/login) or
  [sandbox](https://sandbox.tangocard.com) credentials.

    * Create a tango card account ([sandbox] only, should only be run once):
    ```
        curl -H "Content-Type: application/json" -X POST -d '{"customer":"Bargible", "identifier":"Bargible", "email":"<email>"}' https://<username>:<password>@sandbox.tangocard.com/raas/v1.1/accounts
    ```

    * Register a test credit card ([sandbox] only, should only be run once):
    ```
        curl -H "Content-Type: application/json" -X POST -d '{"customer": "Bargible", "account_identifier": "Bargible", "client_ip": "127.0.0.1", "credit_card": {"number": "4111111111111111", "security_code": "123", "expiration": "2016-01", "billing_address": {"f_name": "FName", "l_name": "LName", "address": "Address", "city": "Seattle", "state": "WA", "zip": "98116", "country": "USA", "email": "<email>"}}}' https://<username>:<password>@sandbox.tangocard.com/raas/v1.1/cc_register
    ```

    * Fund the account using a test credit card ([sandbox] only):
    ```
        curl -H "Content-Type: application/json" -X POST -d '{"customer": "Bargible","account_identifier": "Bargible","amount": 500000,"client_ip": "127.0.0.1","cc_token": "<cc_token>","security_code": "123"}' https://<username>:<password>@sandbox.tangocard.com/raas/v1.1/cc_fund
    ```

### Development ###

##### JavaScript Development Scripts

***Note that you should export the DATABASE_URL and activate the bargible Python virtualenv before running any scripts that use Python services or tests***

* `npm run dev` - runs the Python server and the JavaScript frontend compiler in watch mode *(does not run the auction updater)*
* `npm run start:updater` - runs the auction updater *(must be shut down during python test runs)*
* `npm run data:reset` - resets the database
* `npm run data:gen` - generates some test auctions and config setting
* `npm run build` - builds the JavaScript frontend
* `npm run test` - lints code, runs JavaScript and Python tests (make sure the auction updater is not running)
* `npm run test:js` - runs the `./bin/test.js` JavaScript test suite
* `npm run test:js:watch` - watches frontend JavaScript for changes and runs frontend tests
* `npm run lint` - runs the `./bin/lint.sh` script

##### Using npm module `precommit` to test before committing

Because `npm run test` runs all Python and JavaScript
tests, [the npm precommit module](https://www.npmjs.com/package/precommit) may
be installed by any developer to automatically run `npm run test` as a git
precommit hook (the tests will be run during the `git commit` command, and if
the tests fail the commit will not proceed).

To install `precommit` without adding it to the `package.json`, run `npm install
precommit` from the root directory of this project.

To skip running tests during the commit command use `git commit -n -m 'commit
message'`, the `-n` flag disables the precommit hook.

##### JavaScript Linting

Client side JavaScript is linted using [ESLint](http://eslint.org/) using
the [JavaScript Standard Style](https://standardjs.com/) with additional
settings that **allow** variable names with underscores in them and double
quotes.

[ESLint Plugins](http://eslint.org/docs/user-guide/integrations) are available
for many text editors.

To install all the eslint plugins globally that `npm run lint` uses, install
`npm install -g eslint eslint-config-standard eslint-plugin-standard eslint-plugin-promise eslint-plugin-import eslint-plugin-node`

##### .editorconfig

An [editorconfig](http://editorconfig.org/) file has been included to assist in
configuration of how text editors save whitespace, line endings, and tabs.
Installing [an editorconfig plugin](http://editorconfig.org/#download) is
suggested but not required.

### Build ###

##### application

* `node bin/build_client.js`
  - `-c` flag for continuous (rebuild on save) builds
  - `-d` flag for deploy/production (e.g. minifed js) builds

### Running ###

##### application

1. In one terminal:
```
python bin/reset_db.py
python bin/gentestdata.py -i 0 -c 1 --config
python bin/auction_updater.py
```
2. In another terminal:
```
python bin/runAsyncioServer.py
```
3. Go to http://localhost:5000 in your browser
4. To join an auction, you'll have to create a user account via the interface.
    * The information doesn't matter at this point and the email is not confirmed so choose whatever you like
5. Once registered you can join and bid in auctions

##### test
All tests are run with the command
`./bin/runAllTests.sh`.
Refer to the `runAllTests.sh` script as a starting point for
further information about available tests.
