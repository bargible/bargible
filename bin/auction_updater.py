import logging
import os
import time
import sys
import argparse
import asyncio

from sqlalchemy.exc import OperationalError
from sqlalchemy.exc import ProgrammingError

PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')
)

SRC_DIR = os.path.join(PROJ_ROOT, 'server')
SERVER_DIR = os.path.join(PROJ_ROOT)

sys.path.append(SRC_DIR)
sys.path.append(SERVER_DIR)

from bargible import periodic_auc_updater

logging.basicConfig(filename = '/var/log/bargible/auction.log', level = logging.INFO)
logger = logging.getLogger(__name__)

def parseArgs():
    """ parse script arguments """
    argParser = argparse.ArgumentParser()
    argParser.add_argument(
        '-s', '--sync', dest='runSync',
        help=("run the synchronous updater "
              "rather than the async (celery) updater"),
        action='store_true')
    argParser.add_argument(
        '-v', '--verbose', dest='verbose',
        help=("print info regarding updater type"),
        action='store_true')
    return argParser.parse_args()


def update():
    while(True):
        try:
            periodic_auc_updater.updateAuctions()
        except OperationalError as e:
            logger.error(e)
        except ProgrammingError as e:
            logger.error(e)
        except:
            logger.error(sys.exc_info()[0])
            raise
        time.sleep(0.5)


if __name__ == '__main__':
    args = parseArgs()
    if args.runSync:
        if args.verbose:
            print("starting sync (non-celery) updater")
        try:
            update()
        except KeyboardInterrupt:
            if args.verbose:
                print("updater got interrupt, exiting")
    else:
        if args.verbose:
            print("starting asyncio updater")
        loop = asyncio.get_event_loop()
        try:
            loop.run_until_complete(
                periodic_auc_updater.updateAuctionsAsyncLoop())
        except KeyboardInterrupt:
            if args.verbose:
                print("updater got interrupt, exiting")
            loop.stop()
