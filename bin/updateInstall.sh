#! /bin/sh

### run this script to update existing install upon changes
#   changes from version control

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"

# ensure the script is run from the project root directory
cwd="$(pwd)"
script_parent_dir="$(dirname $SCRIPT_DIR)"
if [ $cwd != $script_parent_dir ]
then
    echo "run init_db.sh from project root directory" 1>&2
    exit 64
fi

SKIP_NODE=0
SKIP_PYTHON=0
QUIET_LEVEL=0

while getopts "npq:" opt; do
    case "$opt" in
        n) SKIP_NODE=1
           ;;
        p) SKIP_PYTHON=1
           ;;
        q) QUIET_LEVEL=$OPTARG
    esac
done

# getopts cleanup
shift $((OPTIND-1))
[ "$1" = "--" ] && shift

if [ $QUIET_LEVEL = 0 ]
then
    PROG_BARS=1
elif [ $QUIET_LEVEL = 1 ]
then
    PROG_BARS=0
else
    echo "invalid quiet level (-q opt) $QUIET_LEVEL" 1>&2
    exit 64
fi


if [ $SKIP_NODE != 1 ]
then
    NPM_PROG_SWICH="true"
    if [ $PROG_BARS = 0 ]
    then
       NPM_PROG_SWICH="false"
    fi
    echo "updating node packages"
    npm prune --progress=$NPM_PROG_SWICH
    if [ $? != 0 ]
    then
        echo "npm prune failed" 1>&2
        exit 2
    fi
    npm install --progress=$NPM_PROG_SWICH
    if [ $? != 0 ]
    then
        echo "npm install failed" 1>&2
        exit 2
    fi
fi

if [ $SKIP_PYTHON != 1 ]
then
    echo "checking python version.."
    EXPECTED_VERSION="3.5.3"
    ACTUAL_VERSION="$(python --version | sed 's/Python //')"
    if [ "$ACTUAL_VERSION" != "$EXPECTED_VERSION" ]
    then
        echo "expecting python $EXPECTED_VERSION, " \
             "found $ACTUAL_VERSION" 1>&2
        exit 1
    fi
    echo "..ok"
    echo "updating python packages"
    PIP_FLAGS=""
    if [ $PROG_BARS = 0 ]
    then
        PIP_FLAGS="$PIP_FLAGS -q"
    fi
    PACKAGES_TO_UNINSTALL=$(pip freeze | grep -v -f requirements.txt -)
    if [ "$PACKAGES_TO_UNINSTALL" != '' ]
    then
        echo "$PACKAGES_TO_UNINSTALL" | \
            xargs pip uninstall -y $PIP_FLAGS
        if [ $? != 0 ]
        then
            echo "pip uninstall failed" 1>&2
            exit 2
        fi
    fi
    pip install -r requirements.txt $PIP_FLAGS
    if [ $? != 0 ]
    then
        echo "pip install failed" 1>&2
        exit 2
    fi
fi
