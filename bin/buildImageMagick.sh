#! /bin/sh

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
# ensure the script is run from the project root directory
cwd="$(pwd)"
script_parent_dir="$(dirname $SCRIPT_DIR)"
if [ $cwd != $script_parent_dir ]
then
    echo "run buildImageMagick.sh from project root directory" 1>&2
    exit 64
fi

MAGICK_HOME=vendor/ImageMagick-local
MAGICK_SOURCE=ImageMagick-6.7.9-10

cd vendor/
if [ -d $MAGICK_SOURCE ]
then
    rm -r $MAGICK_SOURCE
fi
tar xf ImageMagick-6.7.9-10.tar.xz
cd $MAGICK_SOURCE
./configure
make
cd ../..
if [ ! -d $MAGICK_HOME/lib ]
then
   mkdir -p $MAGICK_HOME/lib
fi
cp vendor/$MAGICK_SOURCE/magick/.libs/libMagickCore.so \
   vendor/$MAGICK_SOURCE/Magick++/lib/.libs/libMagick++.so \
   vendor/$MAGICK_SOURCE/wand/.libs/libMagickWand.so \
   $MAGICK_HOME/lib

