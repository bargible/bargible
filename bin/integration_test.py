""" top-level server-side integration test script """
import os
import sys
import unittest
import argparse

PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')
)

SRC_DIR = os.path.join(PROJ_ROOT, 'server')
SERVER_DIR = os.path.join(PROJ_ROOT)
TEST_DIR = os.path.join(PROJ_ROOT, 'test')

sys.path.append(SRC_DIR)
sys.path.append(SERVER_DIR)
sys.path.append(TEST_DIR)

import apptest.int.http as httpTests
import apptest.int.admin as adminTests
import apptest.int.auction_updater as auctionUpdaterTests

def parseArgs():
    """ parse script arguments """
    argParser = argparse.ArgumentParser()
    argParser.add_argument(
        '-n', '--name', dest='testName',
        help=("test name string specifier matching "
              "unittest.TestLoader.loadTestsFromName format"),
        default=None)
    return argParser.parse_args()


def suite(testName):
    suite = unittest.TestSuite()
    for testModule in [
            httpTests,
            adminTests,
            auctionUpdaterTests,
    ]:
        suite.addTests(unittest.defaultTestLoader
                           .loadTestsFromModule(
                               testModule,
                               pattern=testName,
                               ))
    return suite


def runTests():
    args = parseArgs()
    testResult = unittest.TextTestRunner().run(suite(args.testName))
    return testResult.wasSuccessful()


if __name__ == '__main__':
    if not runTests():
        quit(1)
