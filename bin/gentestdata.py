import time
import datetime
import os
import sys
import argparse

PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')
)

SRC_DIR = os.path.join(PROJ_ROOT, 'server')
SERVER_DIR = os.path.join(PROJ_ROOT)

sys.path.append(SRC_DIR)
sys.path.append(SERVER_DIR)

# to create context for Flask-User
from app import initFlaskApp

import gentestmodels

def parseArgs():
    """ parse script arguments """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-a', '--auctions', dest='auctions',
        help=('this argument specifies which '
              '/test_data/fixtures/auctions_XX.json'
              'file to use, e.g. \'-a 1\' translates to '
              'auctions_01.json'),
        type=int,
        default=None,
    )
    parser.add_argument(
        '-u', '--users', dest='users',
        help=('this argument specifies which '
              '/test_data/fixtures/users_XX.json'
              'file to use, e.g. \'-a 1\' translates to '
              'users_01.json'),
        type=int,
        default=None,
    )
    parser.add_argument(
        '-c', '--credit-packages', dest='credit_packages',
        help=('this argument specifies which '
              '/test_data/fixtures/credit_packages_XX.json'
              'file to use, e.g. \'-c 1\' translates to '
              'credit_packages_01.json'),
        type=int,
        default=None,
    )
    parser.add_argument(
        '-i', '--items', dest='items',
        help=('this argument specifies which '
              '/test_data/fixtures/items_XX.json'
              'file to use, e.g. \'-i 1\' translates to '
              'items_01.json'),
        type=int,
        default=None,
    )
    parser.add_argument(
        '-p', '--physical-items', dest='physical_items',
        help=('this argument specifies which '
              '/test_data/fixtures/physical_items_XX.json'
              'file to use, e.g. \'-i 1\' translates to '
              'physical_items_01.json'),
        type=int,
        default=None,
    )
    parser.add_argument(
        '--config',
        action="store_true",
        help=('this argument specifies whether '
              'to create a config model record'),
    )
    return parser.parse_args()

if __name__ == '__main__':
    args = parseArgs()
    if args.items is not None:
        gentestmodels.createItems(args.items)
    if args.physical_items is not None:
        gentestmodels.createPhysicalItems(args.physical_items)
    if args.credit_packages is not None:
        gentestmodels.createCreditPackages(args.credit_packages)
    if args.users is not None:
        with initFlaskApp().app_context():
            gentestmodels.createUsers(args.users)
    if args.auctions is not None:
        gentestmodels.createAuctions(args.auctions)
    if args.config is not None:
        gentestmodels.createConfig()
