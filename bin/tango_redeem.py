import os
import json
import argparse

import requests

from urllib.parse import urljoin

PRODUCTION_URL = 'https://api.tangocard.com/raas/v1.1/'

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.poolmanager import PoolManager
import ssl

PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')
)

with open(os.path.join(
        PROJ_ROOT, 'test_data', 'fixtures', 'items_00.json')) as f:
    DATA_ITEMS = json.load(f)

class MyAdapter(HTTPAdapter):
    def init_poolmanager(self, connections, maxsize, block):
        self.poolmanager = PoolManager(num_pools=connections,
                                       maxsize=maxsize,
                                       block=block,
                                       ssl_version=ssl.PROTOCOL_TLSv1_2)

def parseArgs():
    """ parse script arguments """
    argParser = argparse.ArgumentParser()
    argParser.add_argument(
        '-u', '--user', dest='user',
        type=str,
        required=True,
        help=("tango username"),
        default=None)
    argParser.add_argument(
        '-p', '--password', dest='password',
        type=str,
        required=True,
        help=("tango pasword"),
        default=None)
    argParser.add_argument(
        '-w', '--winner', dest='winner',
        type=str,
        required=True,
        help=("winner full name"),
        default=None)
    argParser.add_argument(
        '-e', '--email', dest='email',
        type=str,
        required=True,
        help=("winner email"),
        default=None)
    argParser.add_argument(
        '-s', '--sku', dest='sku',
        type=str,
        required=True,
        help=("item sku"),
        choices=[itemData['sku'] for itemData in DATA_ITEMS],
        default=None)
    argParser.add_argument(
        '-a', '--amount', dest='amount',
        type=int,
        required=True,
        help=("gift card amount in cents"),
        default=None)
    return argParser.parse_args()

def _makeRequestData(params):
    """ params -- dict with keys 'amount', 'winner', 'email', 'sku' """
    if params['amount'] < 500:
        raise ValueError('amount must be 500 or higher')
    if params['amount'] > 10000:
        raise ValueError('amount must be 10000 or lower')
    data = {
        'customer': 'Bargible',
        'account_identifier': 'Bargible',
        'campaign': 'emailtemplate1',
        'recipient': {
            'name': params['winner'],
            'email': params['email']
        },
        'sku': params['sku'],
        'amount': params['amount'],
        'reward_from': 'Bargible',
        'reward_subject': ('Bargible Winnings: Congrats ' +
                           params['winner'] + '!'),
        'reward_message': ('Way to go, ' + params['winner'] +
                           '! Thanks for playing in' +
                           ' our 7 days of Alpha!'),
        'send_reward': True,
        'external_id': 'test'
    }
    return data

def redeem(user, password, requestParams):
    data = _makeRequestData(requestParams)
    s = requests.Session()
    s.mount('https://', MyAdapter())
    result = s.post(urljoin(PRODUCTION_URL, 'orders'),
                           json=data,
                           auth=(user, password))
    return result.text

if __name__ == '__main__':
    args = parseArgs()
    resultText = redeem(args.user, args.password, {
        'winner': args.winner,
        'email': args.email,
        'sku': args.sku,
        'amount': args.amount,
    })
    print(resultText)
