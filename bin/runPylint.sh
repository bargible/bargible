#! /bin/sh

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"

# ensure the script is run from the project root directory
cwd="$(pwd)"
script_parent_dir="$(dirname $SCRIPT_DIR)"
if [ $cwd != $script_parent_dir ]
then
    echo "run from project root directory" 1>&2
    exit 64
fi

exit_code=0

find . -type f -name '*.py' | \
    grep -v '^\./node_modules' | \
    # https://lists.logilab.org/pipermail/python-projects/2013-January/003402.html
    grep -v 'dupe_func_home.py$' | \
    while read py_file
    do
        echo "--------- linting $py_file"
        pylint -E $py_file
        if [ $? != 0 ]
        then
           exit_code=1
        fi
    done

exit $exit_code
