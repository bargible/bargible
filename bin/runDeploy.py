"""
top-level deploy script
"""
import os
import sys
import argparse
import inspect
from warnings import warn

import sqlalchemy.exc

# both the application and deploy packages
PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')
)

SRC_DIR = os.path.join(PROJ_ROOT, 'server')
SERVER_DIR = os.path.join(PROJ_ROOT)

sys.path.append(SRC_DIR)
sys.path.append(SERVER_DIR)

import deploy
import config as flaskConfigDefs

DEFAULT_USERNAME = 'appadmin' # for VPS
DEFAULT_DB_USERNAME = 'dbadmin'
DEFAULT_KEYPAIR = 'devdeploy'
DEFAULT_BASE_IMAGE = 'debian-stable'
DEFAULT_INSTANCE_SIZE = 'XS'
DEFAULT_SECURITY_GROUP = 'deploy'
DEFAULT_BKUP_USERNAME = 'appadmin' # for VPS hosting snapshots

# todo: Invoke

def _configParserCreateDb(parser):
    parser.add_argument(
        'nameTag', help=(
            "a (unique) nametag for the database.  " +
            "note this is _not_ the name of the (postgres) " +
            "database created by this command, which is always " +
            "'bargible'.  rather, it's an identifier for the " +
            "_instance_ of the 'bargible' database created by this " +
            "command."))
    parser.add_argument(
        'dbPassword',
        help=(
            "the password for incoming database connections."))
    parser.add_argument(
        '-u', '--db-username', dest='db_username',
        help=(
            "the username for incoming database connections."),
        default=DEFAULT_DB_USERNAME)


def _configParserServer(parser):
    parser.add_argument(
        'serverName', help=(
            "a (unique) name for the server"))
    parser.add_argument(
        '-g', '--security-group', dest='secGroup',
        help=(("the security group for the new server: "
               "  test -- ports 22, 80, 5000 open everywhere"
               "  deploy -- ports 22, 80 open everywhere")),
        choices=['test', DEFAULT_SECURITY_GROUP],
        default=DEFAULT_SECURITY_GROUP)
    parser.add_argument(
        '-b', '--base-image', dest='baseImage',
        help="the base image for the new server",
        choices=[DEFAULT_BASE_IMAGE],
        default=DEFAULT_BASE_IMAGE)
    parser.add_argument(
        '-i', '--base-image-id', dest='baseImageId',
        help=(("an AWS id for the image of the new server, "
               "this option is mutually exclusive with the "
               "base image option")),
        default=None)
    parser.add_argument(
        '-s', '--size', dest='instanceSize',
        help=(("size for the new instance.  "
               "like t-shirts and soft drinks, sizes range from "
               "very small to very large.")),
        choices=[DEFAULT_INSTANCE_SIZE, 'S', 'M', 'L', 'XL'],
        default=DEFAULT_INSTANCE_SIZE)
    parser.add_argument(
        '-k', '--key-pair', dest='keyPairName',
        help=("name for the AWS key pair to be associated"
                  " with this server for authentication.  "
                  "usually the same as the name as a `.pem` file"
                  " (see also aws_deploy.md)."),
        default=DEFAULT_KEYPAIR)


def _configParserAddUser(parser):
    parser.add_argument(
        'serverName', help=(
            "the name of the server to add the user to"))
    parser.add_argument(
        'identFile', help=(
            "called a .pem file by AWS, this is a private key "
            "file corresponding to the server to add the user to"))
    parser.add_argument(
        'pubKeyFile', help=(
            "as the new user account will be passwordless, "
            "this is a public key file to allow the user to "
            "login remotely after the account is created"))
    parser.add_argument(
        '-u', '--username', dest='usernameNew',
        help="the username for the new account",
        default=DEFAULT_USERNAME)


def _configParserSysInit(parser):
    parser.add_argument(
        'serverName', help=(
            "the name of the server to initialize.  "
            ""
            "remember to use quotes if the name contains spaces"))
    parser.add_argument(
        '-u', '--username', dest='username',
        help=(
            "the username of the account used for "
            "system initialization.  "
            "this account should have root (sudo) permissions"),
        default=DEFAULT_USERNAME)
    parser.add_argument(
        '--auc-admin', dest='auc_admin_login',
        help=(
            "single-colon-delimited <username>:<password> "
            "for the auction admin user account.  "
            "that is, an account will be created on the VPS with "
            "clear-text ssh login enabled for these credentials"),
        default='aucadmin:1qaz2wsx')


def _configParserUserInit(parser):
    parser.add_argument(
        'serverName', help=(
            "the name of the server to initialize"
            "for this user.  "
            "remember to use quotes if the name contains spaces"))
    parser.add_argument(
        'dbPassword',
        help=(
            "the password for the database connection to be used "
            "with the application."))
    parser.add_argument(
        '-u', '--username', dest='username',
        help=(
            "the username of to be initialized "
            "for running the application.  "
            "this account should have root (sudo) permissions"),
        default=DEFAULT_USERNAME)
    parser.add_argument(
        '--db-username', dest='db_username',
        help=(
            "the username for the database connection to be used "
            "with the application."),
        default=DEFAULT_DB_USERNAME)
    parser.add_argument(
        '--db-name', dest='db_name',
        help=(
            "the name tag for the database connection to be used "
            "with the application.  "
            "corresponds to the 'nameTag' in the 'create-db' "
            "subcommand.  "
            "if the --db-url flag is passed, it must agree with "
            "the name tag here, although the url may be omitted "
            "if this flag is used"),
        default=None)
    parser.add_argument(
        '--db-url', dest='db_url',
        help=(
            "the url for the database connection to be used "
            "with the application."),
        default=None)


def _configParserDeployApp(parser):
    parser.add_argument(
        'serverName', help=(
            "the name of the server to deploy the app to "
            "remember to use quotes if the name contains spaces"))
    parser.add_argument(
        '-u', '--username', dest='username',
        help="the username for the account used for deploy",
        default=DEFAULT_USERNAME)
    parser.add_argument(
        '--git-path', dest='git_path',
        help=(
            "the path to the git repo (on the local filesystem) "
            "to be used for deploy"),
        default=PROJ_ROOT)
    parser.add_argument(
        '--git-branch', dest='git_branch',
        help="the branch to be used for deploy",
        default='master')
    parser.add_argument(
        '--flask-config', dest='flask_config',
        help="one of the Flask configurations specified in config.py",
        choices=[
            k for k, v in
            inspect.getmembers(flaskConfigDefs, inspect.isclass) ],
        default='ProductionConfig')
    parser.add_argument(
        '--creds-tango', dest='creds_tango',
        help="the tango credentials file to be used for deploy",
        default=os.path.join(
            PROJ_ROOT, 'server', 'bargible', 'third_party_api',
            'tango', 'creds.conf'))
    parser.add_argument(
        '--creds-braintree', dest='creds_braintree',
        help="the braintree credentials file to be used for deploy",
        default=os.path.join(
            PROJ_ROOT, 'server', 'bargible', 'third_party_api',
            'braintree', 'creds.conf'))


def _configParserSnapshot(parser):
    parser.add_argument(
        'serverName', help=(
            "the name of the server to backup"))
    parser.add_argument(
        'destServerName', help=(
            "name of the server to save snapshot to"))
    parser.add_argument(
        '-u', '--username', dest='username_src',
        help="the username for the app deployment to backup",
        default=DEFAULT_USERNAME)
    parser.add_argument(
        '--dest-username', dest='username_dest',
        help="username for the account on the destination server",
        default=DEFAULT_BKUP_USERNAME)
    parser.add_argument(
        '--dest-path', dest='path_dest',
        help=(
            "root directory for backup.  "
            "the backup files will be stored in a timestamped "
            "subdirectory of this directory.  "
            "use quotes to prevent shell expansion of special "
            "characters.  (e.g. pass '~/dirname' instead "
            "of ~/dirname)."),
        default='~/bkup')
    parser.add_argument(
        '--timeout-files', dest='timeout_files',
        type=int,
        help=(
            "a timeout (int: seconds) for backing up files."),
        default=10)
    parser.add_argument(
        '--timeout-db', dest='timeout_db',
        type=int,
        help=(
            "a timeout (int: seconds) for backing up the db."),
        default=20)


class MyHelpFormatter(argparse.ArgumentDefaultsHelpFormatter):

    def __init__(self, *args, **kwargs):
        width = kwargs.pop('width', 60)
        super().__init__(*args, width=width, **kwargs)


def parseArgs():
    """ parse script arguments """
    argParser = argparse.ArgumentParser(
        formatter_class=MyHelpFormatter,
        )
    subParsers = argParser.add_subparsers(dest='cmd')
    subParsers.add_parser(
        'list-dbs',
        formatter_class=MyHelpFormatter,
        help=("list databases."),
    )
    _configParserCreateDb(subParsers.add_parser(
        'create-db',
        formatter_class=MyHelpFormatter,
        help=(
            "create a new database instance.  " +
            "this command does not perform any database initalization"),
    ))
    subParsers.add_parser(
        'list-servers',
        formatter_class=MyHelpFormatter,
        help=(
            "list available server names for deployment of " +
            "remote application instances"),
    )
    _configParserServer(subParsers.add_parser(
        'create-server',
        formatter_class=MyHelpFormatter,
        help=(
            "create a new VPS instance"),
    ))
    _configParserAddUser(subParsers.add_parser(
        'add-user',
        formatter_class=MyHelpFormatter,
        help=(
            "add a user account"),
    ))
    _configParserSysInit(subParsers.add_parser(
        'sys-init',
        formatter_class=MyHelpFormatter,
        help=(
            "initialize system-wide dependencies for the application.  "
            "the specified server is expected to be running a "
            "debian-based linux distribution.  "
            "this command only need by run once per server instance."),
    ))
    _configParserUserInit(subParsers.add_parser(
        'user-init',
        formatter_class=MyHelpFormatter,
        help=(
            "initialize the environment on this user account "
            "to run the application, setting environment variables "
            "and installing node and python runtimes"),
    ))
    _configParserDeployApp(subParsers.add_parser(
        'deploy-app',
        formatter_class=MyHelpFormatter,
        help=(
            "deploy the application to a specified server.  "
            "this command will deploy development files "
            "(application code) from the local machine.  "
            "it will create a new, timestamped local branch "
            "with a name of the format 'deploy-YYMMDDHHMM', "
            "and a new remote with the same name as the "
            "specified server name (if such a remote doesn't "
            "already exist)."
            "afterward, the user account on the server "
            "may be considered paired with the remote on the "
            "local machine"),
    ))
    _configParserSnapshot(subParsers.add_parser(
        'snapshot',
        formatter_class=MyHelpFormatter,
        help=(
            "backup the database, image storage, and any other parts "
            "of the application state "
            "(as opposed to application config) "
            "associated with a particular deployment"),
    ))
    return argParser.parse_args()


def _runListDbs():
    displayKeys = ['name', 'domain']
    print(' : '.join(displayKeys))
    print("----")
    print('\n'.join([
        ' : '.join([server.get(k, 'Not found') for k in displayKeys])
        for server in deploy.getDatabasesList()
        ]))


def _runListServers():
    displayKeys = ['name', 'domain']
    print(' : '.join(displayKeys))
    print("----")
    print('\n'.join([
        ' : '.join([server.get(k, 'Not found') for k in displayKeys])
        for server in deploy.getServersList()
        ]))


def _runCreateDb(args):
    deploy.createDb(
        args.nameTag,
        args.db_username,
        args.dbPassword,
        )


def _runCreateServer(args):
    if args.baseImageId is not None:
        warn(("specifying an explicit base image id is "
              "for testing only, to be deprecated, and "
              "not to be used in scripts"))
    deploy.createServer(
        {
            'ec2InstanceName': args.serverName,
            'ec2KeyPairName': args.keyPairName,
        },
        (args.baseImageId if args.baseImageId else args.baseImage),
        args.instanceSize,
        securityGroup=args.secGroup,
    )


def _runAddUser(args):
    deploy.addUser(
        args.serverName,
        args.usernameNew,
        {'ident': args.identFile,
         'pubKey': args.pubKeyFile},
    )


def _runSysInit(args):
    usernameAucAdmin, passwordAucAdmin = args.auc_admin_login.split(':')
    deploy.sysInit(
        args.serverName,
        args.username,
        {
            'usernameAucAdmin': usernameAucAdmin,
            'passwordAucAdmin': passwordAucAdmin,
        })


def _runUserInit(args):
    dbParams = {
        'user': args.db_username,
        'name': args.db_name,
        'url': args.db_url,
        'pass': args.dbPassword,
        }
    try:
        deploy.userInit(
            args.serverName,
            args.username,
            dbParams,
            )
    except sqlalchemy.exc.OperationalError as err:
        sys.stderr.write(
            "database test connection failed for params:" + '\n' +
            repr(dbParams) +
            "error info:" + '\n' +
            err.args[0] + '\n')
        quit(1)


def _runDeployApp(args):
    deploy.deployApp(
        {
            'serverName': args.serverName,
            'username': args.username,
        },
        {
            'path': args.git_path,
            'branch': args.git_branch,
        },
        {
            'tango': args.creds_tango,
            'braintree': args.creds_braintree,
        },
        flaskConfig=args.flask_config,
        )


def _runSnapshot(args):
    deploy.takeSnapshot(
        {
            'user': args.username_src,
            'server_name': args.serverName,
        },
        {
            'user': args.username_dest,
            'server_name': args.destServerName,
            'path': args.path_dest,
        },
        {
            'files': args.timeout_files,
            'db': args.timeout_db,
        })


def runDeploy():
    args = parseArgs()
    if args.cmd == 'list-dbs':
        print("getting databases info...")
        _runListDbs()
    elif args.cmd == 'list-servers':
        print("getting servers info...")
        _runListServers()
    elif args.cmd == 'create-db':
        print("creating database named " + repr(args.nameTag) + "...")
        _runCreateDb(args)
    elif args.cmd == 'create-server':
        print("creating server named '" + args.serverName + "'...")
        _runCreateServer(args)
    elif args.cmd == 'add-user':
        print("adding user '" + args.usernameNew + "' to server '" +
              args.serverName + "'...")
        _runAddUser(args)
    elif args.cmd == 'sys-init':
        print("initializing system deps on " + args.serverName + "...")
        _runSysInit(args)
    elif args.cmd == 'user-init':
        print("initializing application deps on " +
                  args.serverName + " for user " +
                  args.username + "...")
        _runUserInit(args)
    elif args.cmd == 'deploy-app':
        print("deploying to " + args.serverName + "...")
        _runDeployApp(args)
    elif args.cmd == 'snapshot':
        print("taking snapshot backup of " + args.serverName + "...")
        _runSnapshot(args)
    elif args.cmd is not None:
        sys.stderr.write(args.cmd + " unimplemented" + '\n')
        quit(9)
    else:
        sys.stderr.write(
            "this script must be supplied with a command. " +
            "run with the '--help' argument to see " +
            "a list of available commands." + '\n')
        quit(2)


if __name__ == '__main__':
    runDeploy()
