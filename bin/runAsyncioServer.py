"""
top-level script to run a Flask-SocketIO-like
aiohttp webserver
"""
import os
import sys
import argparse
import asyncio

import aiohttp.web

PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')
)

SRC_DIR = os.path.join(PROJ_ROOT, 'server')
SERVER_DIR = os.path.join(PROJ_ROOT)

sys.path.append(SRC_DIR)
sys.path.append(SERVER_DIR)

from bargible.aiohttp_auc_webapp import initAiowebApplication

def parseArgs():
    """ parse script arguments """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--debug',
        action='store_true',
        default=False,
        help=("whether enable debugging on async server"),
    )
    parser.add_argument(
        '--sockjs',
        action='store_true',
        default=False,
        help=("whether to use sockjs (defaults to socket.io)"),
    )
    parser.add_argument(
        '-p', '--port',
        type=int,
        default=5000,
        help=("the port number for the async server"),
    )
    return parser.parse_args()


def _getFlaskAppPort(flaskApp):
    flaskServerName = flaskApp.config.get('SERVER_NAME', None)
    if flaskServerName is not None:
        hostAndPort = flaskServerName.split(':')
        if len(hostAndPort) > 1:
            return int(hostAndPort[1])
    return None


if __name__ == '__main__':
    args = parseArgs()
    loop = asyncio.get_event_loop()
    aiohttpWsgiApp = initAiowebApplication(
        loop,
        useSockJS=args.sockjs,
        )
    handler = aiohttpWsgiApp.make_handler()
    srv = loop.run_until_complete(
        loop.create_server(
            handler, '0.0.0.0',
            (args.port or _getFlaskAppPort(flaskApp)))
    )
    print('serving on', srv.sockets[0].getsockname())
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        srv.close()
        loop.run_until_complete(srv.wait_closed())
        loop.run_until_complete(aiohttpWsgiApp.shutdown())
        loop.run_until_complete(handler.shutdown(60.0))
        loop.run_until_complete(aiohttpWsgiApp.cleanup())
    loop.close()
