#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"

# ensure the script is run from the project root directory
cwd="$(pwd)"
script_parent_dir="$(dirname $SCRIPT_DIR)"
if [ $cwd != $script_parent_dir ]
then
    echo "run circleDecryptSecrets.sh from project root directory" 1>&2
    exit 64
fi

# An ENCRYPTION_KEY is set in the circle-ci environment and must be set locally to run this script
if [ -z "$ENCRYPTION_KEY" ]
then
    echo "ENCRYPTION_KEY must be set in local environment" 1>&2
    exit 64
fi


### decrypt tango and braintree credentials from test/circle-assets to appropriate locations

openssl aes-256-cbc -d -in test/circle-assets/braintree.creds.enc -out server/bargible/third_party_api/braintree/creds.conf -pass env:ENCRYPTION_KEY
openssl aes-256-cbc -d -in test/circle-assets/tango.creds.enc -out server/bargible/third_party_api/tango/creds.conf -pass env:ENCRYPTION_KEY
