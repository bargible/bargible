/**
 * client build executable
 */

/*global require, __dirname, process */

var path = require('path');

const build_js = require('../client').build_js;
const build_styles = require('../client').build_styles;

const argv = require('yargs').options({
    'p': {
        alias: 'dir_out',
        desc: "output directory path",
        type: 'string',
        default: path.join(__dirname, '..',
                           'app', 'static')
    },
    'd': {
        alias: 'prod',
        desc: "deploy/production build",
        type: 'boolean'
    },
    'c': {
        alias: 'cts',
        desc: "continuous client build",
        type: 'boolean'
    }
}).argv;

Promise.resolve().then(function () {
    return Promise.all([
        build_js(path.join(argv.dir_out, 'js'), {
            cts: argv.cts,
            prod: argv.prod
        }).catch(function (err) {
            console.error("js build failed with error");
            console.error(err);
            console.log(err.stack);
            process.exit(1);
        }),
        build_styles(path.join(argv.dir_out, 'css'), {
            cts: argv.cts
        }).catch(function (err) {
            console.error("custom styles build failed with error");
            console.error(err);
            console.log(err.stack);
            process.exit(1);
        })
    ]);
}).then(function () {
    console.log("js and custom styles build finished");
}, function (err) {
    console.error("js or css build failed with error");
    console.error(err);
    console.log(err.stack);
    process.exit(1);
});
