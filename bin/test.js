/*global require */

const test = require('../test/client');


Promise.resolve().then(function () {
    return test.runTests();
}).catch(function (err) {
    console.log("test error:\n" + err.toString());
});
