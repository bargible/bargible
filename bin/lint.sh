#! /bin/sh

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"

# ensure the script is run from the project root directory
cwd="$(pwd)"
script_parent_dir="$(dirname $SCRIPT_DIR)"
if [ $cwd != $script_parent_dir ]
then
    echo "run init_db.sh from project root directory" 1>&2
    exit 64
fi

echo "$(find . -name '*.py' | grep -v '^./node_modules/' | grep -v '^./migrations/')
$(find app/ -name '*.html')
$(find client/ -name '*.js')
$(find client/ -name '*.json')
$(find client/ -name '*.less')
$(find client/ -name '*.rt')
$(find test/ -name '*.js')
$(find bin/ -name '*.js')
$(find bin/ -name '*.sh')
$(find test_data/ -name '*.json')
$(find notes/ -name '*.md')
nginx.conf
package.json
constants.json
credit_options.json
README.md" | \
    # todo: revisit linting on design files
    grep -v '\.rt$' | grep -v '\.less$' | \
    while read file_lint_check
do
    if grep -n '\s$' "$file_lint_check"
    then
       echo "*** found EOL space in $file_lint_check" 1>&2
       exit 1
    fi
    if grep -n -P "\t" "$file_lint_check"
    then
       echo "*** found tabs in $file_lint_check" 1>&2
       exit 1
    fi
done
