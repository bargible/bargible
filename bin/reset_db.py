"""
drop and restore all database tables, and
remove image system storage
"""
import os
import sys
import argparse

from sqlalchemy import Table

from alembic.config import Config as AlembicConfig
from alembic import command as alembic_command

PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')
)

SRC_DIR = os.path.join(PROJ_ROOT, 'server')
SERVER_DIR = os.path.join(PROJ_ROOT)

sys.path.append(SRC_DIR)
sys.path.append(SERVER_DIR)

from app import model
from bargible.model import base

ALEMBIC_CFG = AlembicConfig(os.path.join(PROJ_ROOT, 'alembic.ini'))

class MyHelpFormatter(argparse.ArgumentDefaultsHelpFormatter):

    def __init__(self, *args, **kwargs):
        width = kwargs.pop('width', 60)
        super().__init__(*args, width=width, **kwargs)


def parseArgs():
    """ parse script arguments """
    argParser = argparse.ArgumentParser(
        formatter_class=MyHelpFormatter,
        )
    argParser.add_argument(
        '--no-create', dest='no_create',
        help=(
            "add this flag in order to remove all database tables "
            "and image files without re-initializing the database.  "
            "this may be useful in local dev environments as "
            "a means of hard reset without running migrations."),
        action="store_true",
        )
    return argParser.parse_args()


def deleteAll():
    model.dropAllTables()
    ## remove all image files tracked by database
    # assumes imageStore is sqlalchemy-imageattach filesystem store
    for dirpath, dirnames, filenames in os.walk(model.imageStore.path):
        for filename in filenames:
            os.remove(os.path.join(dirpath, filename))


def createAll():
    model.initDb()
    # http://alembic.zzzcomputing.com/en/latest/cookbook.html#building-an-up-to-date-database-from-scratch
    alembic_command.stamp(ALEMBIC_CFG, "head")


if __name__ == '__main__':
    args = parseArgs()
    deleteAll()
    if not args.no_create:
        createAll()
