#!/usr/bin/env node

var exec = require('child_process').execSync

const CIRCLE_COMPARE_URL = process.env.CIRCLE_COMPARE_URL

console.log("checking if we should try a merge")

if (!CIRCLE_COMPARE_URL) {
  console.log('no merge is necessary')
  process.exit()
} else {
  console.log('Attempting merge')
  let hashes = CIRCLE_COMPARE_URL.replace('https://bitbucket.org/bargible/bargible/branches/compare/', '')
  let hashesArray = hashes.split('..')
  let requestSHA = hashesArray[0]
  let targetSHA = hashesArray[1]
  console.log({requestSHA, targetSHA})
  if (requestSHA && targetSHA) {
    exec(`git config --global user.email "testing@diff.mx"`, {stdio: [ 0, 1, 2 ]})
    exec(`git fetch`, {stdio: [ 0, 1, 2 ]})
    exec(`git checkout ${targetSHA}`, {stdio: [ 0, 1, 2 ]})
    exec(`git merge -m 'testing merge of ${requestSHA} into ${targetSHA}' --no-ff ${requestSHA}`, {stdio: [ 0, 1, 2 ]})
    exec(`./bin/runAllTests.sh`, {stdio: [ 0, 1, 2 ]})
  } else {
    process.exit(1)
  }
}
