#! /bin/sh



### installs system dependencies on debian-based linux distribution
# to be run, e.g., with root permissions (sudo) to intialize new VPS

# system deps and tools
apt-get install --yes tmux
apt-get install --yes htop
apt-get install --yes git
apt-get install --yes postgresql-client
apt-get install --yes redis-server
apt-get install --yes nginx
apt-get install --yes libpq-dev
apt-get install --yes python-dev
apt-get install --yes libffi-dev
apt-get install --yes emacs

#python build deps
apt-get build-dep --yes python3
apt-get build-dep --yes python
apt-get install --yes build-essential
apt-get install --yes libncursesw5-dev
apt-get install --yes libreadline-dev
apt-get install --yes libssl-dev
apt-get install --yes libgdbm-dev
apt-get install --yes libc6-dev
apt-get install --yes libsqlite3-dev
apt-get install --yes libbz2-dev

# log and image storage setup
mkdir -p \
     /var/log/bargible \
     /var/local/bargible/images
chmod a+w \
     /var/log/bargible \
     /var/local/bargible/images
