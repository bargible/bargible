#! /bin/sh

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"

# ensure the script is run from the project root directory
cwd="$(pwd)"
script_parent_dir="$(dirname $SCRIPT_DIR)"
if [ $cwd != $script_parent_dir ]
then
    echo "run init_db.sh from project root directory" 1>&2
    exit 64
fi

### run tests

echo "client-side tests"
node bin/test.js
[ $? != 0 ] && echo "fail" 1>&2 && exit 1

echo "model-layer unit tests"
python bin/unit_test.py -t model
[ $? != 0 ] && echo "fail" 1>&2 && exit 1

echo ".core module unit tests"
python bin/unit_test.py -t core
[ $? != 0 ] && echo "fail" 1>&2 && exit 1

echo "third party http api tests"
python bin/unit_test.py -t api
[ $? != 0 ] && echo "fail" 1>&2 && exit 1

echo "server-side integration tests"
python bin/integration_test.py
[ $? != 0 ] && echo "fail" 1>&2 && exit 1

# ... not certain if this is exiting w/ 0 or not...
exit 0
