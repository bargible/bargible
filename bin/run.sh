#! /bin/sh

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"

# ensure the script is run from the project root directory
cwd="$(pwd)"
script_parent_dir="$(dirname $SCRIPT_DIR)"
if [ $cwd != $script_parent_dir ]
then
    echo "run init_db.sh from project root directory" 1>&2
    exit 1
fi

python bin/auction_updater.py &
python application.py