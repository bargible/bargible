"""
top-level test script
"""
import os
import sys
import unittest
import argparse

PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')
)

SRC_DIR = os.path.join(PROJ_ROOT, 'server')
SERVER_DIR = os.path.join(PROJ_ROOT)
TEST_DIR = os.path.join(PROJ_ROOT, 'test')

sys.path.append(SRC_DIR)
sys.path.append(SERVER_DIR)
sys.path.append(TEST_DIR)

from apptest.unit import TYPE2MODULE
from apptest.unit import suite as makeUnitTestSuite

def parseArgs():
    """ parse script arguments """
    argParser = argparse.ArgumentParser()
    argParser.add_argument(
        '-t', '--type', dest='testType',
        help="the type of test to run",
        choices=list(TYPE2MODULE.keys()),
        default='model')
    argParser.add_argument(
        '-n', '--name', dest='testName',
        help=("the name of the test to run -- "
              "unittest.TestLoader.loadTestsFromName format"),
        default=None)
    return argParser.parse_args()


def runTests():
    args = parseArgs()
    testType = args.testType
    testName = args.testName
    testResult = unittest.TextTestRunner().run(makeUnitTestSuite(
        testTypes=[testType], testName=testName))
    return testResult.wasSuccessful()


if __name__ == '__main__':
    if not runTests():
        quit(1)
