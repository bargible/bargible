"""
build nginx config file from template
"""
import os
import sys
import argparse
from urllib.parse import urlparse

from jinja2 import Template

from flask import url_for

PROJ_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')
)

SRC_DIR = os.path.join(PROJ_ROOT, 'server')
SERVER_DIR = os.path.join(PROJ_ROOT)

sys.path.append(SRC_DIR)
sys.path.append(SERVER_DIR)

from app import initFlaskApp
from bargible.model import imageStore

# matches default from eventlet (via Flask-SocketIO)
# as used in application.py
APP_PORT = 5000
# matches default from Flask-SocketIO
URL_GAMEPLAY_WS = 'socket.io'

with open(os.path.join(PROJ_ROOT, 'nginx.conf')) as tFile:
    TEMPLATE = Template(tFile.read())

def _urlPrefix2NginxConf(urlPath):
    """
    check for and remove leading and trailing
    slashes from a string.  conceptually, this maps
    Flask/Werkzeug url path conventions to the
    url path conventions used in the nginx config template
    """
    if urlPath[0] != '/' or urlPath[-1] != '/' or len(urlPath) < 3:
        raise RuntimeError((
            "url prefix paths expected to begin and end with '/'"),
            urlPath)
    return urlPath[1:-1]


def _safeUpdate(dictToUpdate, dictWithUpdates):
    duplicateKeys = set(dictWithUpdates.keys()).intersection(
        set(dictToUpdate.keys()))
    if len(duplicateKeys) != 0:
        raise ValueError((
            "safe dictionary update failed: "
            "duplicate keys"),
            (duplicateKeys, dictToUpdate, dictWithUpdates))
    dictToUpdate.update(**dictWithUpdates)


def getTemplateVarsDict():
    flaskApp = initFlaskApp()
    with flaskApp.test_request_context():
        prefixedFlaskUrls = {
            'admin': url_for('admin.admin'),
            'static': url_for('static', filename=''),
        }
    urlPrefixPathsNginx = {
        key: _urlPrefix2NginxConf(
            urlparse(prefixedFlaskUrls[key]).path)
        for key in prefixedFlaskUrls.keys() }
    urls = {
        'wsGame': URL_GAMEPLAY_WS,
        'images': imageStore.prefix,
    }
    _safeUpdate(urls, urlPrefixPathsNginx)
    if (not hasattr(flaskApp, 'static_folder') or
            flaskApp.static_folder is None):
        raise RuntimeError((
            "Flask application's `static_folder` attr "
            "is not set"))
    paths = {
        'static': os.path.abspath(flaskApp.static_folder),
        'images': os.path.abspath(imageStore.path),
    }
    return {
        'projRoot': PROJ_ROOT,
        'appPort': APP_PORT,
        'urls': urls,
        'paths': paths,
    }


def parseArgs():
    """ parse script arguments """
    argParser = argparse.ArgumentParser()
    argParser.add_argument(
        'output',
        metavar="output",
        type=str,
        # nargs='+',
        help=("path for output file"),
        default=os.path.abspath(
            os.path.join('etc', 'nginx', 'sites-enabled', 'default')),
    )
    return vars(argParser.parse_args())


if __name__ == '__main__':
    argsDict = parseArgs()
    if os.path.exists(argsDict['output']):
        sys.stderr.write("path '" + argsDict['output'] + "' exists\n")
        sys.exit(1)
    tVarsDict = getTemplateVarsDict()
    with open(argsDict['output'], 'w') as oFile:
        oFile.write(TEMPLATE.render(**tVarsDict))
